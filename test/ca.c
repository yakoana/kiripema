#include "test.h"
#include <stdio.h>
#include <string.h>
#include <time.h>

static CK_BBOOL __true = CK_TRUE, __false = CK_FALSE;
static CK_OBJECT_CLASS __privKey = CKO_PRIVATE_KEY, __cert = CKO_CERTIFICATE, __pubKey = CKO_PUBLIC_KEY;
static CK_BYTE __pubExponent[] = { 0x01, 0x00, 0x01 };
static unsigned char __userKeyUsage[] = { 0x05, 0xE0 }, __caKeyUsage[] = { 0x01, 0x03 };
static CK_CERTIFICATE_TYPE __certType = CKC_X_509;

static int __iKeySize = 2048;
static unsigned int* __hHash = sha256_oid;
static size_t __hashLen = NHC_SHA256_OID_COUNT;
static CK_MECHANISM_TYPE __hSign = CKM_SHA256_RSA_PKCS;
static unsigned int *__hSignOID = sha256WithRSAEncryption;
static size_t __hSignOIDLen = NHC_SHA256_WITH_RSA_OID_COUNT;


static unsigned int _o_oid[]	= { 2, 5, 4, 10 };
static unsigned int _ou_oid[]	= { 2, 5, 4, 11 };
static unsigned int _cn_oid[]	= { 2, 5, 4, 3 };
static NH_OID_STR pO_OID  = { _o_oid,  NHC_OID_COUNT(_o_oid)  };
static NH_OID_STR pOU_OID = { _ou_oid, NHC_OID_COUNT(_ou_oid) };
static NH_OID_STR pCN_OID = { _cn_oid, NHC_OID_COUNT(_cn_oid) };

CK_IMPLEMENTATION(CK_RV, __key_container)(CK_IN CKT_DEVICE_STR *self, CK_IN char *szPrefix, CK_OUT int *iId, CK_OUT char **szOut)
{
	CK_RV rv;
	CK_SESSION_INFO info;
	CK_BBOOL match = CK_FALSE;
	int id = 0;
	char szLabel[32], *szRet;
	CK_ATTRIBUTE privKey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL };
	CK_ULONG ulRet;

	printf("%s", "Key label generation... ");
	if
	(
		(rv = self->cryptoki->C_GetSessionInfo(self->hSession, &info)) == CKR_OK &&
		(info.state != CKS_RO_USER_FUNCTIONS)
	)	rv = self->cryptoki->C_Login(self->hSession, CKU_USER, (CK_UTF8CHAR_PTR) self->szPIN, strlen(self->szPIN));

	while (rv == CKR_OK && !match)
	{
		ulRet = 0;
		memset(szLabel, 0, sizeof(szLabel));
		sprintf(szLabel, szPrefix, ++id);
		privKey[0].pValue = szLabel;
		privKey[0].ulValueLen = strlen(szLabel);
		if
		(
			(rv = self->cryptoki->C_FindObjectsInit(self->hSession, privKey, 4UL)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjects(self->hSession, pRet, 1UL, &ulRet)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjectsFinal(self->hSession)) == CKR_OK
		)	match = ulRet == 0;
	}
	if (rv == CKR_OK && (rv = (szRet = (char*) malloc(strlen(szLabel) + 1)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		strcpy(szRet, szLabel);
		printf("Succeeded: %s\n", szRet);
		*szOut = szRet;
		*iId = id;
	}
	if (rv != CKR_OK) printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __generate_keys)(CK_INOUT CKT_DEVICE_STR *self, CK_IN char *szAlias)
{
	CK_RV rv;
	CK_SESSION_INFO info;
	CK_SESSION_HANDLE hRWSession;
	CK_ATTRIBUTE pubKey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_MODULUS_BITS, &__iKeySize, sizeof(int) },
		{ CKA_PUBLIC_EXPONENT, &__pubExponent, sizeof(__pubExponent) },
		{ CKA_VERIFY, &__true, sizeof(CK_BBOOL) }
	},
	privKey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__true, sizeof(CK_BBOOL) },
		{ CKA_SENSITIVE, &__true, sizeof(CK_BBOOL) },
		{ CKA_SIGN, &__true, sizeof(CK_BBOOL) }
	},
	modulus[] = {{ CKA_MODULUS, NULL, 0UL }},
	id[] = {{ CKA_ID, NULL, 0UL }};
	CK_MECHANISM mechanism = { CKM_RSA_PKCS_KEY_PAIR_GEN, NULL, 0 };
	CK_OBJECT_HANDLE hPubKey, hPrivKey;

	printf("%s", "RSA key pair generation... ");
	pubKey[0].pValue = (CK_VOID_PTR) szAlias;
	pubKey[0].ulValueLen = strlen(szAlias);
	privKey[0].pValue = (CK_VOID_PTR) szAlias;
	privKey[0].ulValueLen = strlen(szAlias);
	if
	(
		(rv = self->cryptoki->C_GetSessionInfo(self->hSession, &info)) == CKR_OK &&
		(rv = self->cryptoki->C_OpenSession(info.slotID, CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hRWSession)) == CKR_OK
	)
	{
		if
		(
			(rv = self->cryptoki->C_GetSessionInfo(hRWSession, &info)) == CKR_OK &&
			(info.state != CKS_RW_USER_FUNCTIONS)
		)	rv = self->cryptoki->C_Login(hRWSession, CKU_USER, (CK_UTF8CHAR_PTR) self->szPIN, strlen(self->szPIN));
		if
		(
			rv == CKR_OK &&
			(rv = self->cryptoki->C_GenerateKeyPair(hRWSession, &mechanism, pubKey, 4UL, privKey, 5UL, &hPubKey, &hPrivKey)) == CKR_OK &&
			(rv = self->cryptoki->C_GetAttributeValue(hRWSession, hPubKey, modulus, 1UL)) == CKR_OK &&
			(rv = (modulus[0].pValue = malloc(modulus[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = self->cryptoki->C_GetAttributeValue(hRWSession, hPubKey, modulus, 1UL)) == CKR_OK)
			{
				id[0].pValue = modulus[0].pValue;
				id[0].ulValueLen = modulus[0].ulValueLen;
				if
				(
					(rv = self->cryptoki->C_SetAttributeValue(hRWSession, hPubKey, id, 1UL)) == CKR_OK &&
					(rv = self->cryptoki->C_SetAttributeValue(hRWSession, hPrivKey, id, 1UL)) == CKR_OK
				)
				{
					printf("%s\n", "Succeeded");
					self->szKeyAlias = (char*) szAlias;
				}
			}
			free(modulus[0].pValue);
		}
		self->cryptoki->C_CloseSession(hRWSession);
	}
	if (rv != CKR_OK) printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __get_pubkey)(CK_IN CKT_DEVICE_STR *self, CK_OUT NH_RSA_PUBKEY_HANDLER *hHandler)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_CLASS, &__pubKey, sizeof(CK_OBJECT_CLASS) }
	},
	attrs[] =
	{
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_PUBLIC_EXPONENT, NULL, 0UL }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;
	NH_BIG_INTEGER n = { NULL, 0 }, e = { NULL, 0 };
	NH_RSA_PUBKEY_HANDLER hPubKey;

	printf("%s", "RSA public key read... ");
	if ((rv = self->szKeyAlias ? CKR_OK : CKR_ACTION_PROHIBITED) == CKR_OK)
	{
		search[0].pValue = self->szKeyAlias;
		search[0].ulValueLen = strlen(self->szKeyAlias);
		if
		(
			(rv = self->cryptoki->C_FindObjectsInit(self->hSession, search, 2UL)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjects(self->hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjectsFinal(self->hSession)) == CKR_OK &&
			(rv = ulRet == 1 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
			(rv = self->cryptoki->C_GetAttributeValue(self->hSession, pRet[0], attrs, 2UL)) == CKR_OK &&
			(rv = (attrs[0].pValue = malloc(attrs[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = (attrs[1].pValue = malloc(attrs[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = self->cryptoki->C_GetAttributeValue(self->hSession, pRet[0], attrs, 2UL)) == CKR_OK
		)
		{
			n.data = (unsigned char*) attrs[0].pValue;
			n.length = attrs[0].ulValueLen;
			e.data = (unsigned char*) attrs[1].pValue;
			e.length = attrs[1].ulValueLen;
			if ((rv = NH_new_RSA_pubkey_handler(&hPubKey)) == CKR_OK)
			{
				if ((rv = hPubKey->create(hPubKey, &n, &e)) == CKR_OK)
				{
					*hHandler = hPubKey;
					printf("%s\n", "Succeeded");
				}
				else NH_release_RSA_pubkey_handler(hPubKey);
			}
		}
	}
	if (attrs[0].pValue) free(attrs[0].pValue);
	if (attrs[1].pValue) free(attrs[1].pValue);
	if (rv != CKR_OK) printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __generate_CN)(CK_UNUSED CK_IN CKT_DEVICE_STR *self, CK_IN int id, CK_UNUSED char *szO, CK_UNUSED char *szOU, char *szPrefix, CK_OUT char **szOut)
{
	CK_RV rv = CKR_OK;
	char szCN[64], *szRet;

	printf("%s", "Certificate common name generation... ");
	memset(szCN, 0, sizeof(szCN));
	sprintf(szCN, szPrefix, id);
	if ((rv = (szRet = (char*) malloc(strlen(szCN) + 1)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		strcpy(szRet, szCN);
		printf("Succeeded: %s\n", szRet);
		*szOut = szRet;
	}
	if (rv != CKR_OK) printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
typedef struct __sign_params_str
{
	CK_FUNCTION_LIST_PTR	cryptoki;
	CK_SESSION_HANDLE		hSession;

} __sign_params_str, *__sign_params;

static NH_RV __signature_callback
(
	_IN_ NH_BLOB *data,
	CK_UNUSED _IN_ CK_MECHANISM_TYPE mechanism,
	_IN_ void *params,
	_OUT_ unsigned char *signature,
	_INOUT_ size_t *sigSize
)
{
	NH_RV rv;
	NH_ASN1_ENCODER_HANDLE hEncoder;
	NH_BLOB hash = { NULL, 0UL };
	__sign_params p = (__sign_params) params;

	if ((rv = NH_encode_digest_info(__hHash, __hashLen, data->data, data->length, &hEncoder)) == CKR_OK)
	{
		hash.length = hEncoder->encoded_size(hEncoder, hEncoder->root);
		if ((rv = (hash.data = (unsigned char*) malloc(hash.length)) ? NH_OK : NH_OUT_OF_MEMORY_ERROR) == CKR_OK)
		{
			if
			(
				(rv = hEncoder->encode(hEncoder, hEncoder->root, hash.data)) == CKR_OK
			)	rv = p->cryptoki->C_Sign(p->hSession, hash.data, hash.length, signature, sigSize);
			free(hash.data);
		}
		NH_release_digest_encoder(hEncoder);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_request)
(
	CK_IN CKT_DEVICE_STR *self,
	char *szO,
	char *szOU,
	char *szCN,
	CK_IN NH_RSA_PUBKEY_HANDLER hPubKey,
	CK_OUT KP_BLOB *pOut
)
{
	CK_RV rv;
	CK_SESSION_INFO info;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;
	NH_NAME_STR pO, pOU, pCN;
	NH_NAME pName[3];
	NH_CREQUEST_ENCODER hEncoder;
	CK_MECHANISM mech = { CKM_RSA_PKCS, NULL, 0UL };
	__sign_params_str params = { NULL, 0UL };

	printf("%s", "Certificate request generation... ");
	if
	(
		(rv = self->szKeyAlias ? CKR_OK : CKR_ACTION_PROHIBITED) == CKR_OK &&
		(rv = pOut ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = self->cryptoki->C_GetSessionInfo(self->hSession, &info)) == CKR_OK &&
		(info.state != CKS_RO_USER_FUNCTIONS)
	)	rv = self->cryptoki->C_Login(self->hSession, CKU_USER, (CK_UTF8CHAR_PTR) self->szPIN, strlen(self->szPIN));
	if (rv == CKR_OK)
	{
		search[0].pValue = self->szKeyAlias;
		search[0].ulValueLen = strlen(self->szKeyAlias);
		if
		(
			(rv = self->cryptoki->C_FindObjectsInit(self->hSession, search, 2UL)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjects(self->hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjectsFinal(self->hSession)) == CKR_OK &&
			(rv = ulRet == 1 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
		)
		{
			pO.pOID = &pO_OID;
			pO.szValue = szO;
			pOU.pOID = &pOU_OID;
			pOU.szValue = szOU;
			pCN.pOID = &pCN_OID;
			pCN.szValue = szCN;
			pName[0] = &pO;
			pName[1] = &pOU;
			pName[2] = &pCN;
			if ((rv = NH_new_certificate_request(&hEncoder)) == CKR_OK)
			{
				params.cryptoki = self->cryptoki;
				params.hSession = self->hSession;
				if
				(
					(rv = self->cryptoki->C_SignInit(self->hSession, &mech, pRet[0])) == CKR_OK &&
					(rv = hEncoder->put_version(hEncoder, 0)) == CKR_OK &&
					(rv = hEncoder->put_subject(hEncoder, pName, 3UL)) == CKR_OK &&
					(rv = hEncoder->put_pubkey(hEncoder, hPubKey)) == CKR_OK &&
					(rv = hEncoder->sign(hEncoder, __hSign, __signature_callback, &params)) == CKR_OK &&
					(rv = hEncoder->encode(hEncoder, NULL, &pOut->length)) == CKR_OK &&
					(rv = (pOut->data = (unsigned char *) malloc(pOut->length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)
				{
					if ((rv = hEncoder->encode(hEncoder, pOut->data, &pOut->length)) == CKR_OK) printf("%s\n", "Succeeded");
					else free(pOut->data);
				}
				NH_delete_certificate_request(hEncoder);
			}
		}
	}
	if (rv != CKR_OK) printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __get_serial)(CK_INOUT CKT_DEVICE_STR *self, CK_OUT NH_BIG_INTEGER *pOut)
{
	CK_RV rv;
	size_t size, i = 0;
	unsigned char buffer[sizeof(int)];

	if ((rv = pOut ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
	{
		++self->iSerial;
		memcpy(buffer, &self->iSerial, sizeof(int));
		NH_swap(buffer, sizeof(int));
		while (!buffer[i] && i < sizeof(int) - 1) i++;
		size = sizeof(int) - i;
		if ((rv = (pOut->data = (unsigned char*) malloc(size)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			memcpy(pOut->data, &buffer[i], size);
			pOut->length = size;
		}
	}
	return rv;	
}
CK_IMPLEMENTATION(CK_RV, __tbs_cert)
(
	CK_UNUSED CK_IN CKT_DEVICE_STR *self,
	char *szO,
	char *szOU,
	char *szIssuer,
	char *szSubject,
	CK_IN NH_BIG_INTEGER *pSerial,
	CK_IN KP_BLOB *pRequest,
	CK_IN CK_BBOOL isUser,
	char *pCDP,
	CK_OUT NH_TBSCERT_ENCODER *hOut
)
{
	CK_RV rv;
	NH_CREQUEST_PARSER hRequest;
	NH_TBSCERT_ENCODER hTBS = NULL;
	NH_OID_STR OID = { __hSignOID, __hSignOIDLen };
	NH_NAME_STR pO, pOU, pCN;
	NH_NAME pName[3];
	struct tm *pNotBefore, *pNotAfter;
	time_t instant;
	char notBefore[16], notAfter[16];
	NH_OCTET_SRING octets = { NULL, 0UL };

	printf("%s", "TBS Certificate generation... ");
	if ((rv = NH_parse_cert_request(pRequest->data, pRequest->length, &hRequest)) == CKR_OK)
	{
		if
		(
			(rv = hRequest->verify(hRequest)) == CKR_OK &&
			(rv = NH_new_tbscert_encoder(&hTBS)) == CKR_OK &&
			(rv = hTBS->put_version(hTBS, 2)) == CKR_OK &&
			(rv = hTBS->put_serial(hTBS, pSerial)) == CKR_OK &&
			(rv = hTBS->put_sign_alg(hTBS, &OID)) == CKR_OK &&
			(rv = hTBS->put_cdp(hTBS, pCDP)) == CKR_OK
		)
		{
			pO.pOID = &pO_OID;
			pO.szValue = szO;
			pOU.pOID = &pOU_OID;
			pOU.szValue = szOU;
			pCN.pOID = &pCN_OID;
			pCN.szValue = szIssuer;
			pName[0] = &pO;
			pName[1] = &pOU;
			pName[2] = &pCN;
			rv = hTBS->put_issuer(hTBS, pName, 3UL);
		}
		if (rv == CKR_OK)
		{
			pCN.szValue = szSubject;
			rv = hTBS->put_subject(hTBS, pName, 3UL);
		}
		if (rv == CKR_OK)
		{
			time(&instant);
			pNotBefore = localtime(&instant);
			sprintf(notBefore, "%04d%02d%02d%02d%02d%02dZ", pNotBefore->tm_year + 1900, pNotBefore->tm_mon + 1, pNotBefore->tm_mday, pNotBefore->tm_hour, pNotBefore->tm_min, pNotBefore->tm_sec);
			pNotAfter = pNotBefore;
			pNotAfter->tm_year++;
			sprintf(notAfter, "%04d%02d%02d%02d%02d%02dZ", pNotAfter->tm_year + 1900, pNotAfter->tm_mon + 1, pNotAfter->tm_mday, pNotAfter->tm_hour, pNotAfter->tm_min, pNotAfter->tm_sec);
			rv = hTBS->put_validity(hTBS, notBefore, notAfter);
		}
		if
		(
			rv == CKR_OK &&
			(rv = hTBS->put_pubkey(hTBS, hRequest->subjectPKInfo)) == CKR_OK
		)
		{
			if (isUser)
			{
				octets.data = __userKeyUsage;
				octets.length = sizeof(__userKeyUsage);
			}
			else
			{
				octets.data = __caKeyUsage;
				octets.length = sizeof(__caKeyUsage);
				rv = hTBS->put_basic_constraints(hTBS, TRUE);
			}
			if (rv == CKR_OK) rv = hTBS->put_key_usage(hTBS, &octets);
		}
		NH_release_cert_request(hRequest);
	}
	if (rv == CKR_OK)
	{
		*hOut = hTBS;
		printf("%s\n", "Succeeded");
	}
	else
	{
		if (hTBS) NH_delete_tbscert_encoder(hTBS);
		printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	}
	fflush(stdout);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __tbs_sign)(CK_IN CKT_DEVICE_STR *self, CK_IN NH_TBSCERT_ENCODER hTBS, CK_OUT KP_BLOB *pOut)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;
	NH_CERT_ENCODER hCert;
	__sign_params_str params = { NULL, 0UL };
	CK_MECHANISM mech = { CKM_RSA_PKCS, NULL, 0UL };
	size_t ulSize;
	unsigned char *pBuffer;

	printf("%s", "Certificate signature... ");
	if
	(
		(rv = self->szKeyAlias ? CKR_OK : CKR_ACTION_PROHIBITED) == CKR_OK &&
		(rv = pOut ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
	)
	{
		search[0].pValue = self->szKeyAlias;
		search[0].ulValueLen = strlen(self->szKeyAlias);
		if
		(
			(rv = self->cryptoki->C_FindObjectsInit(self->hSession, search, 2UL)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjects(self->hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
			(rv = self->cryptoki->C_FindObjectsFinal(self->hSession)) == CKR_OK &&
			(rv = ulRet == 1 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
		)
		{
			if ((rv = NH_new_cert_encoder(&hCert)) == CKR_OK)
			{
				params.cryptoki = self->cryptoki;
				params.hSession = self->hSession;
				if
				(
					(rv = self->cryptoki->C_SignInit(self->hSession, &mech, pRet[0])) == CKR_OK &&
					(rv = hCert->sign(hCert, hTBS, __hSign, __signature_callback, &params)) == CKR_OK
				)
				{
					ulSize = hCert->hEncoder->encoded_size(hCert->hEncoder, hCert->hEncoder->root);
					if ((rv = (pBuffer = (unsigned char*) malloc(ulSize)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
					{
						if ((rv = hCert->hEncoder->encode(hCert->hEncoder, hCert->hEncoder->root, pBuffer)) == CKR_OK)
						{
							pOut->data = pBuffer;
							pOut->length = ulSize;
							printf("%s\n", "Succeeded");
						}
						else free(pBuffer);
					}
				}
				NH_delete_cert_encoder(hCert);
			}
		}
	}
	if (rv != CKR_OK) printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_install)(CK_IN CKT_DEVICE_STR *self, CK_IN KP_BLOB *pEncoding)
{
	CK_RV rv;
	CK_SESSION_INFO info;
	CK_SESSION_HANDLE hRWSession;
	NH_CERTIFICATE_HANDLER hCert;
	CK_ATTRIBUTE searchPubKey[] =
	{
		{ CKA_PUBLIC_KEY_INFO, NULL, 0UL },
		{ CKA_CLASS, &__pubKey, sizeof(CK_OBJECT_CLASS) }
	},
	getAttrs[] =
	{
		{ CKA_ID, NULL, 0UL },
		{ CKA_LABEL, NULL, 0UL }
	},
	searchPrivKey[] =
	{
		{ CKA_ID, NULL, 0UL },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	},
	certAttrs[] =
	{
		{ CKA_PUBLIC_KEY_INFO, NULL, 0UL },
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_ID, NULL, 0UL },
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_ISSUER, NULL, 0UL },
		{ CKA_SERIAL_NUMBER, NULL, 0UL },
		{ CKA_VALUE, NULL, 0UL },
		{ CKA_CERTIFICATE_TYPE, &__certType, sizeof(CK_CERTIFICATE_TYPE) },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) }
	},
	pubKeyAttrs[] =
	{
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) }
	},
	privKeyAttrs[] =
	{
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL }, hPubKey, hPrivKey, hCertificate;
	CK_ULONG ulRet;

	printf("%s", "Signed certificate installation... ");
	if
	(
		(rv = self->cryptoki->C_GetSessionInfo(self->hSession, &info)) == CKR_OK &&
		(rv = self->cryptoki->C_OpenSession(info.slotID, CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hRWSession)) == CKR_OK
	)
	{
		if
		(
			(rv = self->cryptoki->C_GetSessionInfo(hRWSession, &info)) == CKR_OK &&
			(info.state != CKS_RW_USER_FUNCTIONS)
		)	rv = self->cryptoki->C_Login(hRWSession, CKU_USER, (CK_UTF8CHAR_PTR) self->szPIN, strlen(self->szPIN));
		if
		(
			(rv == CKR_OK) &&
			(rv = pEncoding && pEncoding->data ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
			(rv = NH_parse_certificate(pEncoding->data, pEncoding->length, &hCert)) == CKR_OK
		)
		{
			searchPubKey[0].pValue = hCert->pubkey->identifier;
			searchPubKey[0].ulValueLen = hCert->pubkey->size + hCert->pubkey->contents - hCert->pubkey->identifier;
			if
			(
				(rv = self->cryptoki->C_FindObjectsInit(hRWSession, searchPubKey, 2UL)) == CKR_OK &&
				(rv = self->cryptoki->C_FindObjects(hRWSession, pRet, 2UL, &ulRet)) == CKR_OK &&
				(rv = self->cryptoki->C_FindObjectsFinal(hRWSession)) == CKR_OK &&
				(rv = ulRet == 1 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)
			{
				hPubKey = pRet[0];
				if
				(
					(rv = self->cryptoki->C_GetAttributeValue(hRWSession, hPubKey, getAttrs, 2UL)) == CKR_OK &&
					(rv = (getAttrs[0].pValue = malloc(getAttrs[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
					(rv = (getAttrs[1].pValue = malloc(getAttrs[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)
				{
					rv = self->cryptoki->C_GetAttributeValue(hRWSession, hPubKey, getAttrs, 2UL);
					searchPrivKey[0].pValue = getAttrs[0].pValue;
					searchPrivKey[0].ulValueLen = getAttrs[0].ulValueLen;
					if
					(
						(rv = self->cryptoki->C_FindObjectsInit(hRWSession, searchPrivKey, 2UL)) == CKR_OK &&
						(rv = self->cryptoki->C_FindObjects(hRWSession, pRet, 2UL, &ulRet)) == CKR_OK &&
						(rv = self->cryptoki->C_FindObjectsFinal(hRWSession)) == CKR_OK &&
						(rv = ulRet == 1 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
					)
					{
						hPrivKey = pRet[0];
						certAttrs[0].pValue = hCert->pubkey->identifier;
						certAttrs[0].ulValueLen = hCert->pubkey->size + hCert->pubkey->contents - hCert->pubkey->identifier;
						certAttrs[1].pValue = hCert->subject->node->identifier;
						certAttrs[1].ulValueLen = hCert->subject->node->size + hCert->subject->node->contents - hCert->subject->node->identifier;
						certAttrs[2].pValue = getAttrs[0].pValue;
						certAttrs[2].ulValueLen = getAttrs[0].ulValueLen;
						certAttrs[3].pValue = getAttrs[1].pValue;
						certAttrs[3].ulValueLen = getAttrs[1].ulValueLen;
						certAttrs[4].pValue = hCert->issuer->node->identifier;
						certAttrs[4].ulValueLen = hCert->issuer->node->size + hCert->issuer->node->contents - hCert->issuer->node->identifier;
						certAttrs[5].pValue = hCert->serialNumber->identifier;
						certAttrs[5].ulValueLen = hCert->serialNumber->size + hCert->serialNumber->contents - hCert->serialNumber->identifier;
						certAttrs[6].pValue = pEncoding->data;
						certAttrs[6].ulValueLen = pEncoding->length;

						pubKeyAttrs[0].pValue = certAttrs[1].pValue;
						pubKeyAttrs[0].ulValueLen = certAttrs[1].ulValueLen;
						privKeyAttrs[0].pValue = certAttrs[1].pValue;
						privKeyAttrs[0].ulValueLen = certAttrs[1].ulValueLen;

						if
						(
							(rv = self->cryptoki->C_CreateObject(hRWSession, certAttrs, 11UL, &hCertificate)) == CKR_OK &&
							(rv = self->cryptoki->C_SetAttributeValue(hRWSession, hPubKey, pubKeyAttrs, 2UL)) == CKR_OK
						)	rv = self->cryptoki->C_SetAttributeValue(hRWSession, hPrivKey, privKeyAttrs, 2UL);
					}
					if (getAttrs[0].pValue) free(getAttrs[0].pValue);
					if (getAttrs[1].pValue) free(getAttrs[1].pValue);
				}
			}
			NH_release_certificate(hCert);
		}
		self->cryptoki->C_CloseSession(hRWSession);
	}
	if (rv == CKR_OK) printf("%s\n", "Succeeded");
	else printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}


static CKT_DEVICE_STR __defaultDevice =
{
	NULL,
	NULL,
	0UL,
	NULL,
	0,
	__key_container,
	__generate_keys,
	__get_pubkey,
	__generate_CN,
	__cert_request,
	__get_serial,
	__tbs_cert,
	__tbs_sign,
	__cert_install
};
CK_NEW(CKT_new_device)(CK_IN CK_FUNCTION_LIST_PTR cryptoki, CK_IN CK_SLOT_ID slotID, char *szPIN, CK_OUT CKT_DEVICE *hOut)
{
	CK_RV rv;
	CK_SESSION_HANDLE hSession = 0UL;
	CK_SESSION_INFO info;
	CKT_DEVICE hDevice;

	if
	(
		(rv = cryptoki->C_OpenSession(slotID,  CKF_SERIAL_SESSION, NULL, NULL, &hSession)) == CKR_OK &&
		(rv = cryptoki->C_GetSessionInfo(hSession, &info)) == CKR_OK
	)
	{
		if (info.state != CKS_RO_USER_FUNCTIONS) rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) szPIN, strlen(szPIN));
		if
		(
			rv == CKR_OK &&
			(rv = (hDevice = (CKT_DEVICE) malloc(sizeof(CKT_DEVICE_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			memcpy(hDevice, &__defaultDevice, sizeof(CKT_DEVICE_STR));
			hDevice->cryptoki = cryptoki;
			hDevice->szPIN = szPIN;
			hDevice->hSession = hSession;
			*hOut = hDevice;
		}
	}
	if (rv != CKR_OK) cryptoki->C_CloseSession(hSession);
	return rv;
}
CK_DELETE(CKT_delete_device)(CK_INOUT CKT_DEVICE hDevice)
{
	if (hDevice)
	{
		hDevice->cryptoki->C_CloseSession(hDevice->hSession);
		free(hDevice);
	}
}
