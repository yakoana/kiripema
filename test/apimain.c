#include "test.h"
#include "kpapi.h"

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

static char szPKI[CK_FILENAME_MAX], szChain[CK_FILENAME_MAX], szPFX[CK_FILENAME_MAX];
static void parse_cmd_line(int argc, char** argv)
{
	int i;
	char *szToken;

	memset(szPKI, 0, CK_FILENAME_MAX);
	memset(szChain, 0, CK_FILENAME_MAX);
	if (argc > 0)
	{
		for (i = 0; i < argc; i++)
		{
			if (strstr(argv[i], "--issue"))
			{
				strtok(argv[i], "=");
				szToken = strtok(NULL, "=");
				if (szToken && !access(szToken, F_OK)) strcpy(szPKI, szToken);
				else
				{
					printf("%s\n", "--issue argument requires complete path to certificate issue script");
					exit(EXIT_FAILURE);
				}
			}
			else if (strstr(argv[i], "--chain"))
			{
				strtok(argv[i], "=");
				szToken = strtok(NULL, "=");
				if (szToken && !access(szToken, F_OK)) strcpy(szChain, szToken);
				else
				{
					printf("%s\n", "--chain argument requires complete path to CA chain certificates");
					exit(EXIT_FAILURE);
				}
			}
			else if (strstr(argv[i], "--import"))
			{
				strtok(argv[i], "=");
				szToken = strtok(NULL, "=");
				if (szToken && !access(szToken, F_OK)) strcpy(szPFX, szToken);
				else
				{
					printf("%s\n", "--import argument requires complete path to PKCS #12 file");
					exit(EXIT_FAILURE);
				}
			}
		}
	}
}
int main(int argc, char** argv)
{
	CK_RV rv = CKR_OK;
	KP_BLOB pSubject = { NULL, 0UL };

	parse_cmd_line(argc, argv);
	if (szPKI[0]) rv = CKT_issue(&szPKI[0], &pSubject);
	if (rv == CKR_OK && szChain[0]) rv = CKT_sign(&pSubject, &szChain[0]);
	if (rv == CKR_OK && szPFX[0]) rv = CKT_import(&szPFX[0]);
	if (pSubject.data) free(pSubject.data);
	return (int) rv;
}
