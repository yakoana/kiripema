#ifndef __TEST_H__
#define __TEST_H__

#include "kiripema.h"

/*
 * Issue device (simple CA)
 */
typedef struct CKT_DEVICE_STR		CKT_DEVICE_STR;
typedef CK_METHOD(CK_RV, CKTD_CONTAINER)(CK_IN CKT_DEVICE_STR*, CK_IN char*, CK_OUT int*, CK_OUT char**);
typedef CK_METHOD(CK_RV, CKTD_KEYPAIR)(CK_INOUT CKT_DEVICE_STR*, CK_IN char*);
typedef CK_METHOD(CK_RV, CKTD_PUBKEY)(CK_IN CKT_DEVICE_STR*, CK_OUT NH_RSA_PUBKEY_HANDLER*);
typedef CK_METHOD(CK_RV, CKTD_GENCN)(CK_IN CKT_DEVICE_STR*, CK_IN int, char*, char*, char*, CK_OUT char**);
typedef CK_METHOD(CK_RV, CKTD_REQUEST)(CK_IN CKT_DEVICE_STR*, char*, char*, char*, CK_IN NH_RSA_PUBKEY_HANDLER, CK_OUT KP_BLOB*);
typedef CK_METHOD(CK_RV, CKTD_SERIAL)(CK_INOUT CKT_DEVICE_STR*, CK_OUT NH_BIG_INTEGER*);
typedef CK_METHOD(CK_RV, CKTD_TBS)(CK_IN CKT_DEVICE_STR*, char*, char*, char*, char*, CK_IN NH_BIG_INTEGER*, CK_IN KP_BLOB*, CK_IN CK_BBOOL, char*, CK_OUT NH_TBSCERT_ENCODER*);
typedef CK_METHOD(CK_RV, CKTD_SIGNCERT)(CK_IN CKT_DEVICE_STR*, CK_IN NH_TBSCERT_ENCODER, CK_OUT KP_BLOB*);
typedef CK_METHOD(CK_RV, CKTD_INSTALLCERT)(CK_IN CKT_DEVICE_STR*, CK_IN KP_BLOB*);
struct CKT_DEVICE_STR
{
	CK_FUNCTION_LIST_PTR		cryptoki;
	char*					szPIN;
	CK_SESSION_HANDLE			hSession;
	char*					szKeyAlias;
	int					iSerial;

	CKTD_CONTAINER			key_container;
	CKTD_KEYPAIR			generate_keys;
	CKTD_PUBKEY				get_pubkey;
	CKTD_GENCN				generate_cn;
	CKTD_REQUEST			cert_request;
	CKTD_SERIAL				get_serial;
	CKTD_TBS				cert_tbs;
	CKTD_SIGNCERT			cert_sign;
	CKTD_INSTALLCERT			cert_install;
};
typedef CKT_DEVICE_STR*			CKT_DEVICE;
CK_NEW(CKT_new_device)(CK_IN CK_FUNCTION_LIST_PTR, CK_IN CK_SLOT_ID, char*, CK_OUT CKT_DEVICE*);
CK_DELETE(CKT_delete_device)(CK_INOUT CKT_DEVICE);

CK_FUNCTION(CK_RV, CKT_chain_generation)(void);
CK_FUNCTION(CK_RV, CKT_chain_validation)(void);
CK_FUNCTION(CK_RV, CKT_check_winver)(void);
CK_FUNCTION(CK_RV, CKT_issue)(char*, KP_BLOB*);
CK_FUNCTION(CK_RV, CKT_sign)(KP_BLOB*, char*);
CK_FUNCTION(CK_RV, CKT_import)(char*);
extern char *__pPin;

#endif /* __TEST_H__ */