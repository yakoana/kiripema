#include "test.h"

int main(CK_UNUSED int argc, CK_UNUSED char** argv)
{
	CK_RV rv;
#ifdef _UNIX
	rv = CKT_chain_generation();
#else
	rv = CKT_check_winver();
#endif
	if (rv == CKR_OK) rv = CKT_chain_validation();
	return (int) rv;
}
