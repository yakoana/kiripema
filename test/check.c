#include "test.h"
#include <stdio.h>

static CK_RV __check_capabilities(CK_FUNCTION_LIST_PTR cryptoki, CK_SLOT_ID slotID)
{
	CK_RV rv;
	CK_MECHANISM_TYPE_PTR pList;
	CK_ULONG ulCount, i = 0;
	CK_BBOOL match = CK_FALSE;
	CK_MECHANISM_INFO info;
	CK_TOKEN_INFO token;

	printf("%s", "Kiripema cryptographic capabilities check... ");
	if
	(
		(rv = cryptoki->C_GetMechanismList(slotID, NULL, &ulCount)) == CKR_OK &&
		(rv = (pList = (CK_MECHANISM_TYPE_PTR) malloc(ulCount * sizeof(CK_MECHANISM_TYPE))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		rv = cryptoki->C_GetMechanismList(slotID, pList, &ulCount);
		while (rv == CKR_OK && i < ulCount && !match)
		{
			match =
			(
				pList[i] == CKM_RSA_PKCS &&
				(rv = cryptoki->C_GetMechanismInfo(slotID, pList[i], &info)) == CKR_OK &&
				(info.flags & CKF_SIGN)
			);
			++i;
		}
		if (rv == CKR_OK && !match) rv = CKR_FUNCTION_FAILED;
		free(pList);
	}
	if (rv == CKR_OK && (rv = cryptoki->C_GetTokenInfo(slotID, &token)) == CKR_OK)
	{
		if (!(match = (token.flags & CKF_LOGIN_REQUIRED) && (token.flags & CKF_USER_PIN_INITIALIZED))) rv = CKR_FUNCTION_FAILED;
	}
	if (rv == CKR_OK) printf("%s\n", "Succeded");
	else printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
CK_FUNCTION(CK_RV, CKT_check_winver)(void)
{
	CK_RV rv;
	CK_FUNCTION_LIST_PTR cryptoki;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList = NULL;
	CK_SLOT_ID slotID = CK_UNAVAILABLE_INFORMATION;

	if ((rv = C_GetFunctionList(&cryptoki)) == CKR_OK && (rv = cryptoki->C_Initialize(NULL)) == CKR_OK)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
			(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK) slotID = pSlotList[0];
			rv = __check_capabilities(cryptoki, slotID);
			free(pSlotList);
		}
		cryptoki->C_Finalize(NULL);
	}
	return rv;
}