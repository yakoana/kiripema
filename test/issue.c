#include "test.h"
#include <stdio.h>
#include <string.h>

static char* __pUserPIN		= "very poor secret";
static char* __pKeyLabel	= "Kiripema key container %d";
static char* __pDefaultO	= "Crypthing";
static char* __pDefaultOU	= "Test PKI";
static char* __pRootCA		= "Root CA %d";
static char* __pEndUserCA	= "End user CA %d";
static char* __pEndUser		= "End user %d";
static char* __pRootCDP		= "http://pki.crypthing.org/cdp/root/latest.crl";
static char* __pEndCDP		= "http://pki.crypthing.org/cdp/end/latest.crl";
static CK_ULONG __iKeySize	= 2048;


static CK_BBOOL __true = CK_TRUE, __false = CK_FALSE;
static CK_OBJECT_CLASS __privKey = CKO_PRIVATE_KEY, __cert = CKO_CERTIFICATE;
static CK_KEY_TYPE __rsaKey = CKK_RSA;
static CK_RV __check_capabilities(CK_IN CK_FUNCTION_LIST_PTR cryptoki, CK_IN CK_SLOT_ID slotID)
{
	CK_RV rv;
	CK_MECHANISM_TYPE_PTR pList;
	CK_ULONG ulCount, i = 0;
	int bitmap = 0;
	CK_MECHANISM_INFO info;

	printf("%s", "Kiripema cryptographic capabilities check... ");
	if
	(
		(rv = cryptoki->C_GetMechanismList(slotID, NULL, &ulCount)) == CKR_OK &&
		(rv = (pList = (CK_MECHANISM_TYPE_PTR) malloc(ulCount * sizeof(CK_MECHANISM_TYPE))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = cryptoki->C_GetMechanismList(slotID, pList, &ulCount)) == CKR_OK)
		{
			while (rv == CKR_OK && i < ulCount && bitmap != 15)
			{
				if (pList[i] == CKM_RSA_PKCS_KEY_PAIR_GEN)
				{
					bitmap |= 1;
					if ((rv = cryptoki->C_GetMechanismInfo(slotID, pList[i], &info)) == CKR_OK) if (info.ulMaxKeySize >= __iKeySize) bitmap |= 4;
				}
				if (pList[i] == CKM_RSA_PKCS)
				{
					bitmap |= 2;
					if ((rv = cryptoki->C_GetMechanismInfo(slotID, pList[i], &info)) == CKR_OK) if (info.flags & CKF_SIGN) bitmap |= 8;
				}
				++i;
			}
			if (rv == CKR_OK)
			{
				if (bitmap == 15) printf("%s\n", "Succeded");
				else
				{
					printf("%s\n", "Failed! Required mechanisms are not supported");
					rv = CKR_FUNCTION_FAILED;
				}
			}
		}
		free(pList);
	}
	if (rv != CKR_OK) printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
static CK_RV __init_token(CK_IN CK_FUNCTION_LIST_PTR cryptoki, CK_IN CK_SLOT_ID slotID)
{
	CK_RV rv;
	CK_TOKEN_INFO token;
	CK_SESSION_HANDLE hSession;

	printf("%s", "Kiripema token initialization... ");
	if ((rv = cryptoki->C_GetTokenInfo(slotID, &token)) == CKR_OK)
	{
		if (token.flags & CKF_LOGIN_REQUIRED)
		{
			if (!(token.flags & CKF_USER_PIN_INITIALIZED))
			{
				if ((rv = cryptoki->C_OpenSession(slotID,  CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession)) == CKR_OK)
				{
					if ((rv = cryptoki->C_InitPIN(hSession, (CK_UTF8CHAR_PTR) __pUserPIN, strlen(__pUserPIN))) == CKR_OK) printf("%s\n", "Succeeded!");
					else printf("Failed with error code 0x%08x", (unsigned int) rv);
					cryptoki->C_CloseSession(hSession);
				}
			}
			else printf("%s\n", "Token already initialized.");
		}
		else
		{
			printf("%s\n", "Token does not require authentication.");
			rv = CKR_FUNCTION_FAILED;
		}
	}
	else printf("Failure! Check error code 0x%08x\n", (unsigned int) rv);
	fflush(stdout);
	return rv;
}
CK_FUNCTION(CK_RV, CKT_chain_generation(void))
{
	CK_RV rv;
	CK_FUNCTION_LIST_PTR cryptoki;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList = NULL;
	CK_SLOT_ID slotID = CK_UNAVAILABLE_INFORMATION;
	CKT_DEVICE hRoot = NULL, hCA = NULL, hEndUser = NULL;
	char *szRootAlias = NULL, *szRootCN = NULL, *szCAAlias = NULL, *szCACN = NULL, *szEndUserAlias = NULL, *szEndUserCN = NULL;
	NH_RSA_PUBKEY_HANDLER hPubKey = NULL;
	KP_BLOB request = { NULL, 0UL }, cert = { NULL, 0UL };
	NH_BIG_INTEGER serial = { NULL, 0UL };
	NH_TBSCERT_ENCODER hTBS = NULL;
	int id;

	printf("%s\n", "CA chain generation check...");
	if ((rv = C_GetFunctionList(&cryptoki)) == CKR_OK && (rv = cryptoki->C_Initialize(NULL)) == CKR_OK)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
			(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK) slotID = pSlotList[0];
			free(pSlotList);
		}
		if (rv == CKR_OK) rv = __check_capabilities(cryptoki, slotID);
		if (rv == CKR_OK) rv = __init_token(cryptoki, slotID);
		printf("%s\n", "Root CA generation...");
		if (rv == CKR_OK && (rv = CKT_new_device(cryptoki, slotID, __pUserPIN, &hRoot)) == CKR_OK)
		{
			rv = hRoot->key_container(hRoot, __pKeyLabel, &id, &szRootAlias);
			if (rv == CKR_OK) rv = hRoot->generate_keys(hRoot, szRootAlias);
			if (rv == CKR_OK) rv = hRoot->get_pubkey(hRoot, &hPubKey);
			if (rv == CKR_OK) rv = hRoot->generate_cn(hRoot, id, __pDefaultO, __pDefaultOU, __pRootCA, &szRootCN);
			if (rv == CKR_OK) rv = hRoot->cert_request(hRoot, __pDefaultO, __pDefaultOU, szRootCN, hPubKey, &request);
			if (rv == CKR_OK) rv = hRoot->get_serial(hRoot, &serial);
			if (rv == CKR_OK) rv = hRoot->cert_tbs(hRoot, __pDefaultO, __pDefaultOU, szRootCN, szRootCN, &serial, &request, FALSE, __pRootCDP, &hTBS);
			if (rv == CKR_OK) rv = hRoot->cert_sign(hRoot, hTBS, &cert);
			if (rv == CKR_OK) rv = hRoot->cert_install(hRoot, &cert);

			if (hTBS) NH_delete_tbscert_encoder(hTBS);
			hTBS = NULL;
			if (hPubKey) NH_release_RSA_pubkey_handler(hPubKey);
			hPubKey = NULL;
			if (request.data) free(request.data);
			request.data = NULL;
			if (serial.data) free(serial.data);
			serial.data = NULL;
			if (cert.data) free(cert.data);
			cert.data = NULL;
		}
		printf("%s\n", "End user CA generation ...");
		if (rv == CKR_OK && (rv = CKT_new_device(cryptoki, slotID, __pUserPIN, &hCA)) == CKR_OK)
		{
			rv = hCA->key_container(hCA, __pKeyLabel, &id, &szCAAlias);
			if (rv == CKR_OK) rv = hCA->generate_keys(hCA, szCAAlias);
			if (rv == CKR_OK) rv = hCA->get_pubkey(hCA, &hPubKey);
			if (rv == CKR_OK) rv = hCA->generate_cn(hCA, id, __pDefaultO, __pDefaultOU, __pEndUserCA, &szCACN);
			if (rv == CKR_OK) rv = hCA->cert_request(hCA, __pDefaultO, __pDefaultOU, szCACN, hPubKey, &request);
			if (rv == CKR_OK) rv = hRoot->get_serial(hRoot, &serial);
			if (rv == CKR_OK) rv = hRoot->cert_tbs(hRoot, __pDefaultO, __pDefaultOU, szRootCN, szCACN, &serial, &request, FALSE, __pRootCDP, &hTBS);
			if (rv == CKR_OK) rv = hRoot->cert_sign(hRoot, hTBS, &cert);
			if (rv == CKR_OK) rv = hCA->cert_install(hCA, &cert);

			if (hTBS) NH_delete_tbscert_encoder(hTBS);
			hTBS = NULL;
			if (hPubKey) NH_release_RSA_pubkey_handler(hPubKey);
			hPubKey = NULL;
			if (request.data) free(request.data);
			request.data = NULL;
			if (serial.data) free(serial.data);
			serial.data = NULL;
			if (cert.data) free(cert.data);
			cert.data = NULL;
		}
		if (hRoot) CKT_delete_device(hRoot);
		if (szRootAlias) free(szRootAlias);
		if (szRootCN) free(szRootCN);
		printf("%s\n", "End user certificate generation...");
		if (rv == CKR_OK && (rv = CKT_new_device(cryptoki, slotID, __pUserPIN, &hEndUser)) == CKR_OK)
		{
			rv = hEndUser->key_container(hEndUser, __pKeyLabel, &id, &szEndUserAlias);
			if (rv == CKR_OK) rv = hEndUser->generate_keys(hEndUser, szEndUserAlias);
			if (rv == CKR_OK) rv = hEndUser->get_pubkey(hEndUser, &hPubKey);
			if (rv == CKR_OK) rv = hEndUser->generate_cn(hEndUser, id, __pDefaultO, __pDefaultOU, __pEndUser, &szEndUserCN);
			if (rv == CKR_OK) rv = hEndUser->cert_request(hEndUser, __pDefaultO, __pDefaultOU, szEndUserCN, hPubKey, &request);
			if (rv == CKR_OK) rv = hCA->get_serial(hCA, &serial);
			if (rv == CKR_OK) rv = hCA->cert_tbs(hCA, __pDefaultO, __pDefaultOU, szCACN, szEndUserCN, &serial, &request, TRUE, __pEndCDP, &hTBS);
			if (rv == CKR_OK) rv = hCA->cert_sign(hCA, hTBS, &cert);
			if (rv == CKR_OK) rv = hEndUser->cert_install(hEndUser, &cert);

			if (hTBS) NH_delete_tbscert_encoder(hTBS);
			if (hPubKey) NH_release_RSA_pubkey_handler(hPubKey);
			if (szEndUserCN) free(szEndUserCN);
			if (request.data) free(request.data);
			if (serial.data) free(serial.data);
			if (cert.data) free(cert.data);
		}

		if (hCA) CKT_delete_device(hCA);
		if (szCAAlias) free(szCAAlias);
		if (szCACN) free(szCACN);

		if (hEndUser) CKT_delete_device(hEndUser);
		if (szEndUserAlias) free(szEndUserAlias);

		cryptoki->C_Finalize(NULL);
	}
	if (rv == CKR_OK) printf("%s\n", "Chain check succeeded");
	else printf("Chain check failed\n");
	fflush(stdout);
	return rv;
}
static CK_RV __find_cert
(
	CK_FUNCTION_LIST_PTR cryptoki,
	CK_SESSION_HANDLE hSession,
	CK_ATTRIBUTE_TYPE ulType,
	CK_VOID_PTR pId,
	CK_ULONG ulIdLen,
	unsigned char **ppEncoding,
	size_t *pulEncodingLen
)
{
	CK_RV rv;
	CK_OBJECT_HANDLE pRet[4];
	CK_ULONG ulRet;
	CK_ATTRIBUTE cert[] =
	{
		{ 0UL, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__false, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) },
	};
	CK_ATTRIBUTE value[] = {{ CKA_VALUE, NULL, 0UL }};
	cert[0].type = ulType;
	cert[0].pValue = pId;
	cert[0].ulValueLen = ulIdLen;
	if
	(
		(rv = cryptoki->C_FindObjectsInit(hSession, cert, 4UL)) == CKR_OK &&
		(rv = cryptoki->C_FindObjects(hSession, pRet, 4UL, &ulRet)) == CKR_OK &&
		(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK &&
		(rv = ulRet == 1 ? CKR_OK : CKR_VENDOR_DEFINED) == CKR_OK &&
		(rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], value, 1UL)) == CKR_OK &&
		(rv = (value[0].pValue = malloc(value[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], value, 1UL)) == CKR_OK)
		{
			*ppEncoding = value[0].pValue;
			*pulEncodingLen = value[0].ulValueLen;
		}
		else free(value[0].pValue);
	}
	return rv;
}
static CK_RV __find_cert_by_id
(
	CK_FUNCTION_LIST_PTR cryptoki,
	CK_SESSION_HANDLE hSession,
	CK_VOID_PTR pId,
	CK_ULONG ulIdLen,
	unsigned char **ppEncoding,
	size_t *pulEncodingLen
)
{
	return __find_cert(cryptoki, hSession, CKA_ID, pId, ulIdLen, ppEncoding, pulEncodingLen);
}
static CK_RV __find_cert_by_subject
(
	CK_FUNCTION_LIST_PTR cryptoki,
	CK_SESSION_HANDLE hSession,
	CK_VOID_PTR pId,
	CK_ULONG ulIdLen,
	unsigned char **ppEncoding,
	size_t *pulEncodingLen
)
{
	return __find_cert(cryptoki, hSession, CKA_SUBJECT, pId, ulIdLen, ppEncoding, pulEncodingLen);
}
static CK_RV __validate_chain(CK_FUNCTION_LIST_PTR cryptoki, CK_SLOT_ID slotID, CK_VOID_PTR pValue, CK_ULONG ulValueLen)
{
	CK_RV rv;
	CK_SESSION_HANDLE hSession;
	unsigned char *pSonCert = NULL, *pCACert;
	size_t ulSonCertLen, ulCACertLen;
	NH_CERTIFICATE_HANDLER nhSonCert = NULL, nhCACert;

	if ((rv = cryptoki->C_OpenSession(slotID,  CKF_SERIAL_SESSION, NULL, NULL, &hSession)) == CKR_OK)
	{
		if ((rv = __find_cert_by_id(cryptoki, hSession, pValue, ulValueLen, &pSonCert, &ulSonCertLen)) == CKR_OK)
		{
			if ((rv = NH_parse_certificate((unsigned char*) pSonCert, ulSonCertLen, &nhSonCert) == NH_OK ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK)
			{
				while (rv == CKR_OK && strcmp(nhSonCert->subject->stringprep, nhSonCert->issuer->stringprep) != 0)
				{
					if 
					(
						(rv = __find_cert_by_subject(cryptoki, hSession, nhSonCert->issuer->node->identifier, (size_t) (nhSonCert->issuer->node->size + nhSonCert->issuer->node->contents - nhSonCert->issuer->node->identifier), &pCACert, &ulCACertLen)) == CKR_OK &&
						(rv = NH_parse_certificate(pCACert, ulCACertLen, &nhCACert) == NH_OK ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
						(rv = nhSonCert->verify(nhSonCert, nhCACert->pubkey) == NH_OK ? CKR_OK : CKR_VENDOR_DEFINED) == CKR_OK
					)
					{
						NH_release_certificate(nhSonCert);
						free(pSonCert);
						pSonCert = pCACert;
						nhSonCert = nhCACert;
						pCACert = NULL;
						nhCACert = NULL;
					}
				}
				if (nhSonCert) NH_release_certificate(nhSonCert);
			}
			if (pSonCert) free(pSonCert);
		}
		cryptoki->C_CloseSession(hSession);
	}
	return rv;
}
CK_FUNCTION(CK_RV, CKT_chain_validation)(void)
{
	CK_RV rv;
	CK_FUNCTION_LIST_PTR cryptoki;
	CK_ULONG ulCount, ulMaxCount, i;
	CK_SLOT_ID_PTR pSlotList = NULL;
	CK_SLOT_ID slotID = CK_UNAVAILABLE_INFORMATION;
	CK_SESSION_HANDLE hSession = CK_UNAVAILABLE_INFORMATION;
	CK_OBJECT_HANDLE pRet[16];
	CK_ULONG ulRet;
	CK_ATTRIBUTE privKey[] =
	{
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) },
		{ CKA_KEY_TYPE, &__rsaKey, sizeof(CK_KEY_TYPE) }
	},
	keyId[] =
	{
		{ CKA_ID, NULL, 0UL }
	};

	printf("%s\n", "Generated chains validation check...");
	if ((rv = C_GetFunctionList(&cryptoki)) == CKR_OK && (rv = cryptoki->C_Initialize(NULL)) == CKR_OK)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
			(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK)
			{
				slotID = pSlotList[0];
				if ((rv = cryptoki->C_OpenSession(slotID,  CKF_SERIAL_SESSION, NULL, NULL, &hSession)) == CKR_OK)
				{
					if ((rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) __pUserPIN, strlen(__pUserPIN))) == CKR_OK)
					{
						if ((rv = cryptoki->C_FindObjectsInit(hSession, privKey, 4UL)) == CKR_OK)
						{
							ulRet = 16UL;
							ulMaxCount = 16UL;
							while (rv == CKR_OK && ulRet == ulMaxCount)
							{
								if ((rv = cryptoki->C_FindObjects(hSession, pRet, ulMaxCount, &ulRet)))
									rv = ulRet >= 3 ? CKR_OK : CKR_VENDOR_DEFINED; 
								i = 0;
								while (rv == CKR_OK && i < ulRet)
								{
									if
									(
										(rv = cryptoki->C_GetAttributeValue(hSession, pRet[i], keyId, 1UL)) == CKR_OK &&
										(rv = (keyId[0].pValue = malloc(keyId[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
									)
									{
										if ((rv = cryptoki->C_GetAttributeValue(hSession, pRet[i], keyId, 1UL)) == CKR_OK)
											rv = __validate_chain(cryptoki, slotID, keyId[0].pValue, keyId[0].ulValueLen);
										free(keyId[0].pValue);
										keyId[0].pValue = NULL;
									}
									i++;
								}
							}
							cryptoki->C_FindObjectsFinal(hSession);
						}
						cryptoki->C_Logout(hSession);
					}
					cryptoki->C_CloseSession(hSession);
				}
			}
			free(pSlotList);
		}
		cryptoki->C_Finalize(NULL);
	}
	if (rv == CKR_OK) printf("%s\n", "Generated chains check succeeded");
	else printf("Generated chains check failed\n");
	fflush(stdout);
	return rv;
}