#include "test.h"
#include "kpapi.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#ifdef _UNIX
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <sys/wait.h>
#include <errno.h>
#else
	/* TODO Implement get_home under Windows */
#endif

#define COUNTRY_BR			"BR"
#define O_PKI				"PKI Brazil"
#define OU_PKI				"PKI Ruler for All Cats"
#define CN_PKI				"A cat user number %d"
char *__pPin				= "very poor secret";
static char *__eContents		= "Signed contents";
static unsigned int __c_oid[] 	= { 2, 5, 4, 6  };
static unsigned int __o_oid[]		= { 2, 5, 4, 10 };
static unsigned int __ou_oid[]	= { 2, 5, 4, 11 };
static unsigned int __cn_oid[]	= { 2, 5, 4, 3  };
static NH_OID_STR __pC_OID		= { __c_oid,  NHC_OID_COUNT(__c_oid)  };
static NH_OID_STR __pO_OID		= { __o_oid,  NHC_OID_COUNT(__o_oid)  };
static NH_OID_STR __pOU_OID		= { __ou_oid, NHC_OID_COUNT(__ou_oid) };
static NH_OID_STR __pCN_OID		= { __cn_oid, NHC_OID_COUNT(__cn_oid) };
static NH_NAME_STR __pC			= { &__pC_OID, COUNTRY_BR }, __pO = { &__pO_OID, O_PKI }, __pOU = { &__pOU_OID, OU_PKI };
static CK_BYTE __pubExp[]		= { 0x01, 0x00, 0x01 };
static KP_BLOB __pubExponent		= { __pubExp, sizeof(__pubExp) };
static int __iKeySize			= 2048;
static CK_MECHANISM_TYPE __sign	= CKM_SHA256_RSA_PKCS;

CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_home)(char szPath[CK_FILENAME_MAX])
{
	CK_RV rv = CKR_OK;
#ifdef _UNIX
	const char *homedir;

	if ((homedir = getenv("HOME")) == NULL) homedir = getpwuid(getuid())->pw_dir;
	strcpy(szPath, homedir);
	strcat(szPath, "/");
#else
	/* TODO Implement get_home under Windows */
	memset(szPath, 0, CK_FILENAME_MAX);
#endif
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __save_file)(char *szPath, char *szData)
{
	CK_RV rv;
	FILE *f;

	if ((rv = (f = fopen(szPath, "w")) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		fwrite(szData, 1, strlen(szData), f);
		fclose(f);
	}
	return rv;
}
#define ROUNDUP(x)	(--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
CK_INLINE CK_IMPLEMENTATION(CK_RV, __read_file)(char *szPath, char **szChain)
{
	CK_RV rv;
	char pBuffer[512], *pData = NULL;
	size_t i, max = 0, len = 0;
	FILE *f;
	
	
	if ((rv = (f = fopen(szPath, "r")) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		while ((i = fread(pBuffer, 1, sizeof(pBuffer), f)))
		{
			if (len + i + 1 > max)
			{
				max = len + i + 1;
				ROUNDUP(max);
				pData = (char*) realloc(pData, max);
			}
			memcpy(pData + len, pBuffer, i);
			len += i;
		}
		if (feof(f))
		{
			pData[len] = 0;
			*szChain = pData;
		}
		else
		{
			rv = CKR_FUNCTION_FAILED;
			if (pData) free(pData);
		}
		fclose(f);
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __is_CA_cert)(NH_CERTIFICATE_HANDLER hCert)
{
	NH_ASN1_PNODE eNode;
	return
	(
		NH_SUCCESS(hCert->basic_constraints(hCert, &eNode)) &&
		(eNode && (eNode = eNode->child) && ASN_IS_PRESENT(eNode) && *(unsigned char*) eNode->value)
	);
}
CK_IMPLEMENTATION(CK_RV, __get_subject)(KP_BLOB *pCMS, KP_BLOB *pSubject)
{
	CK_RV rv;
	NH_CMS_SD_PARSER hCMS;
	NH_ASN1_PNODE pNode;
	NH_CERTIFICATE_HANDLER hCert;
	CK_BBOOL match = CK_FALSE;

	if ((rv = NH_SUCCESS(NH_cms_parse_signed_data(pCMS->data, pCMS->length, &hCMS)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		pNode = hCMS->certificates->child;
		while (rv == CKR_OK && pNode && !match)
		{
			if ((rv = NH_SUCCESS(NH_parse_certificate(pNode->identifier, pNode->size + pNode->contents - pNode->identifier, &hCert)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
			{
				if (!__is_CA_cert(hCert))
				{
					pSubject->length = hCert->subject->node->size + (hCert->subject->node->contents - hCert->subject->node->identifier);
					if ((rv = (pSubject->data = (unsigned char*) malloc(pSubject->length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
					{
						memcpy(pSubject->data, hCert->subject->node->identifier, pSubject->length);
						match = CK_TRUE;
					}
				}
				NH_release_certificate(hCert);
			}
			pNode = pNode->next;
		}
		NH_cms_release_sd_parser(hCMS);
	}
	return rv;
}
CK_FUNCTION(CK_RV, CKT_issue)(char *szIssue, KP_BLOB *pSubject)
{
	CK_RV rv = CKR_OK;
	CK_TOKEN_INFO info;
	char szCN[64], *szRequest, *szPEM;
	NH_NAME pName[4];
	NH_NAME_STR pCN = { &__pCN_OID, NULL };
	KP_BLOB request = { NULL, 0UL }, pkcs = { NULL, 0UL };
	size_t ulRequest;
	char szHome[CK_FILENAME_MAX], szInput[CK_FILENAME_MAX], szCert[CK_FILENAME_MAX], szOutput[CK_FILENAME_MAX], szCMD[CK_FILENAME_MAX + CK_FILENAME_MAX];

	printf("%s\n", "Testing certificate issuing by OpenSSL...");
	if ((rv = KAPI_TokenInfo(&info)) == CKR_OK && !(info.flags & CKF_USER_PIN_INITIALIZED))
		rv = KAPI_InitToken(__pPin);
	srand (time(NULL));
	sprintf(szCN, CN_PKI, rand());
	pCN.szValue = szCN;
	pName[0] = &__pC;
	pName[1] = &__pO;
	pName[2] = &__pOU;
	pName[3] = &pCN;
	if
	(
		rv == CKR_OK &&
		(rv = KAPI_GenerateCSR(pName, 4UL, __iKeySize, &__pubExponent, __sign, __pPin, &request)) == CKR_OK
	)
	{
		if
		(
			(rv = KAPI_EncodePEM(&request, P10_FORMAT, NULL, &ulRequest)) == CKR_OK &&
			(rv = (szRequest = (char*) malloc(ulRequest)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if
			(
				(rv = KAPI_EncodePEM(&request, P10_FORMAT, szRequest, &ulRequest)) == CKR_OK &&
				(rv = __get_home(szHome)) == CKR_OK
			)
			{
				strcpy(szInput, szHome);
				strcat(szInput, "request.req");
				if ((rv = __save_file(szInput, szRequest)) == CKR_OK)
				{
					strcpy(szCMD, szIssue);
					strcat(szCMD, " ");
					strcat(szCMD, szInput);
					if ((rv = system(szCMD) == 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
					{
						strcpy(szCert, szHome);
						strcat(szCert, "signed.pem");
						strcpy(szOutput, szHome);
						strcat(szOutput, "signed.p7b");
						if ((rv = __read_file(szOutput, &szPEM)) == CKR_OK)
						{
							if
							(
								(rv = KAPI_DecodePEM(szPEM, &pkcs)) == CKR_OK &&
								(rv = (pkcs.data = (unsigned char*) malloc(pkcs.length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
							)
							{
								if ((rv = KAPI_DecodePEM(szPEM, &pkcs)) == CKR_OK)
								{
									if ((rv = KAPI_InstallCertificates(&pkcs, __pPin)) == CKR_OK)
										rv = __get_subject(&pkcs, pSubject);
								}
								free(pkcs.data);
							}
							free(szPEM);
						}
						remove(szCert);
						remove(szOutput);
					}
					remove(szInput);
				}
			}			
			free(szRequest);
		}
		free(request.data);
	}
	if (rv == CKR_OK) printf("%s\n", "Certificate issuing succeeded!");
	else printf("Certificate issuing failed with error code %d\n", (int) rv);
	return rv;
}

CK_FUNCTION(CK_RV, CKT_sign)(KP_BLOB *pCertSubject, char *szChain)
{
	CK_RV rv;
	KAPI_HANDLER pCtx = NULL;
	KAPI_CERTIFICATE pCert, pSigning = NULL;
	CK_BBOOL selected = CK_FALSE;
	KP_BLOB eContents = { (unsigned char*) __eContents, strlen(__eContents) }, pEncoded = { NULL, 0UL };
	char *szPKCS7;
	size_t ulLengh;
	char szHome[CK_FILENAME_MAX], szCMS[CK_FILENAME_MAX], szCMD[ARG_MAX];

	printf("%s\n", "Testing document signing with OpenSSL...");
	if ((rv = KAPI_EnumCertificatesInit(__pPin, &pCtx)) == CKR_OK)
	{
		while (!pSigning && (pCert = KAPI_EnumCertificates(pCtx)))
		{
			selected = !pCertSubject->data ?
				CK_TRUE :
				pCert->subject.length == pCertSubject->length &&
				memcmp(pCert->subject.data, pCertSubject->data, pCert->subject.length) == 0;
			if (selected) pSigning = KAPI_CloneCertificateContext(pCert);
		}
		KAPI_EnumCertificatesFinal(pCtx);
		if ((rv = pSigning ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
		{
			if ((rv = KAPI_Sign(pSigning, CKM_SHA256_RSA_PKCS, CAdES_BES, &eContents, CK_TRUE, __pPin, &pEncoded)) == CKR_OK)
			{
				if
				(
					(rv = KAPI_EncodePEM(&pEncoded, P7_FORMAT, NULL, &ulLengh)) == CKR_OK &&
					(rv = (szPKCS7 = (char*) malloc(ulLengh)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)
				{
					if
					(
						(rv = KAPI_EncodePEM(&pEncoded, P7_FORMAT, szPKCS7, &ulLengh)) == CKR_OK &&
						(rv = __get_home(szHome)) == CKR_OK
					)
					{
						strcpy(szCMS, szHome);
						strcat(szCMS, "cms.pem");
						if ((rv = __save_file(szCMS, szPKCS7)) == CKR_OK)
						{
							strcpy(szCMD, "openssl smime -verify -in ");
							strcat(szCMD, szCMS);
							strcat(szCMD, " -inform PEM -CAfile ");
							strcat(szCMD, szChain);
							rv = system(szCMD) == 0 ? CKR_OK : CKR_FUNCTION_FAILED;
							remove(szCMS);
						}
					}
					free(szPKCS7);
				}
				free(pEncoded.data);
			}
			KAPI_ReleaseCertificateContext(pSigning);
		}
	}
	if (rv == CKR_OK) printf("%s\n", "\nDocument signing succeeded!");
	else printf("\nDocument signing failed with error code %d\n", (int) rv);
	return rv;
}