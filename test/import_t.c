#include "test.h"
#include "kpapi.h"
#include <stdio.h>

static char *__szPFXSecret = "secret";
#define ROUNDUP(x)	(--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
CK_INLINE CK_IMPLEMENTATION(CK_RV, __read_file)(char *szPath, KP_BLOB *pOut)
{
	CK_RV rv;
	unsigned char pBuffer[512], *pData = NULL;
	size_t i, max = 0, len = 0;
	FILE *f;
	
	if ((rv = (f = fopen(szPath, "rb")) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		while ((i = fread(pBuffer, 1, sizeof(pBuffer), f)))
		{
			if (len + i > max)
			{
				max = len + i;
				ROUNDUP(max);
				pData = (unsigned char*) realloc(pData, max);
			}
			memcpy(pData + len, pBuffer, i);
			len += i;
		}
		if (feof(f))
		{
			pOut->data = pData;
			pOut->length = len;
		}
		else
		{
			rv = CKR_FUNCTION_FAILED;
			if (pData) free(pData);
		}
		fclose(f);
	}
	return rv;
}
static unsigned char __issuer[] =
{
	0x30, 0x72, 0x31, 0x0b, 0x30, 0x09, 0x06, 0x03, 0x55, 0x04, 0x06, 0x13,
	0x02, 0x42, 0x52, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x0a,
	0x13, 0x0a, 0x50, 0x4b, 0x49, 0x20, 0x42, 0x72, 0x61, 0x7a, 0x69, 0x6c,
	0x31, 0x1f, 0x30, 0x1d, 0x06, 0x03, 0x55, 0x04, 0x0b, 0x13, 0x16, 0x50,
	0x4b, 0x49, 0x20, 0x52, 0x75, 0x6c, 0x65, 0x72, 0x20, 0x66, 0x6f, 0x72,
	0x20, 0x41, 0x6c, 0x6c, 0x20, 0x43, 0x61, 0x74, 0x73, 0x31, 0x2d, 0x30,
	0x2b, 0x06, 0x03, 0x55, 0x04, 0x03, 0x13, 0x24, 0x43, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x20, 0x4e, 0x61, 0x6d, 0x65, 0x20, 0x66, 0x6f, 0x72, 0x20,
	0x41, 0x6c, 0x6c, 0x20, 0x43, 0x61, 0x74, 0x73, 0x20, 0x45, 0x6e, 0x64,
	0x20, 0x55, 0x73, 0x65, 0x72, 0x20, 0x43, 0x41
};
static unsigned char __serial[] = { 0x02, 0x01, 0x2c };
static char *__szLabel = "Imported from a foreign country";
static CK_BBOOL __true = CK_TRUE;
static CK_OBJECT_CLASS __privKey = CKO_PRIVATE_KEY, __cert = CKO_CERTIFICATE;
static CK_RV __verify_import(void)
{
	CK_RV rv;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;
	CK_ATTRIBUTE pkeySearch[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	},
	pkeyId[] =
	{
		{ CKA_ID, NULL, 0UL },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	},
	certSearch[] =
	{
		{ CKA_ID, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[2];
	CK_ULONG ulRet;

	pkeySearch[0].pValue = __szLabel;
	pkeySearch[0].ulValueLen = strlen(__szLabel);
	if(
		(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
		(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
			(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION, NULL, NULL, &hSession)) == CKR_OK
		)
		{
			if ((rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) __pPin, strlen(__pPin))) == CKR_OK)
			{
				if
				(
					(rv = cryptoki->C_FindObjectsInit(hSession, pkeySearch, 3UL)) == CKR_OK &&
					(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
					(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK &&
					(rv = ulRet == 1 ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK
				)
				{
					if
					(
						(rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], pkeyId, 2UL)) == CKR_OK &&
						(rv = (pkeyId[0].pValue = malloc(pkeyId[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
					)
					{
						if ((rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], pkeyId, 2UL)) == CKR_OK)
						{
							certSearch[0].pValue = pkeyId[0].pValue;
							certSearch[0].ulValueLen = pkeyId[0].ulValueLen;
							if
							(
								(rv = cryptoki->C_FindObjectsInit(hSession, certSearch, 3UL)) == CKR_OK &&
								(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
								(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK
							)	rv = ulRet == 1 ? CKR_OK : CKR_GENERAL_ERROR;
						}
						free(pkeyId[0].pValue);
					}
				}
				cryptoki->C_Logout(hSession);
			}
			cryptoki->C_CloseSession(hSession);
		}
		free(pSlotList);
	}
	return rv;
}
CK_FUNCTION(CK_RV, CKT_import)(char *szPFX)
{
	CK_RV rv;
	KP_BLOB pfx = { NULL, 0UL };
	KAPI_PFX pCtx;
	KAPI_X509CERT pSet;
	CK_BBOOL certFound = CK_FALSE;

	printf("%s... ", "Importing key and certificate from outside world");
	if ((rv = __read_file(szPFX, &pfx)) == CKR_OK)
	{
		if ((rv = KAPI_OpenPFX(pfx.data, pfx.length, __szPFXSecret, &pCtx)) == CKR_OK)
		{
			pSet = pCtx->pCertSet;
			while (!certFound && pSet)
			{
				certFound =
				(
					pSet->hCert->issuer->node->size + (pSet->hCert->issuer->node->contents - pSet->hCert->issuer->node->identifier) == sizeof(__issuer) &&
					memcmp(pSet->hCert->issuer->node->identifier, __issuer, sizeof(__issuer)) == 0 &&
					pSet->hCert->serialNumber->size + (pSet->hCert->serialNumber->contents - pSet->hCert->serialNumber->identifier) == sizeof(__serial) &&
					memcmp(pSet->hCert->serialNumber->identifier, __serial, sizeof(__serial)) == 0
				);
				pSet = pSet->pNext;
			}
			if ((rv = KAPI_ImportPFX(pCtx, __szLabel, __pPin)) == CKR_OK) rv = __verify_import();
			KAPI_ClosePFX(pCtx);
		}
		free(pfx.data);
	}
	if (rv == CKR_OK)
	{
		printf("%s\n", "Succeeded!");
		if (!certFound) printf("%s\n", "The key has been already imported");
	}
	else printf("Failed with error code %lu\n", rv);
	return rv;
}