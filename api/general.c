/**
 * @file general.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Kiripema High Level Cryptographic API general functions
 * 
 * @copyright Copyleft (c) 2019-2020 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kpapi.h"
#include "cencode.h"
#include "cdecode.h"
#include <stdlib.h>
#include <string.h>

CK_FUNCTION_LIST_PTR cryptoki = NULL;

#ifdef _WINDOWS
BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason, LPVOID lpReserved)
{
	BOOL ret = TRUE;

	switch (ul_reason)
	{
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_ATTACH:
		if (C_GetFunctionList(&cryptoki) != CKR_OK || cryptoki->C_Initialize(NULL) != CKR_OK) ret = FALSE;
		break;
	case DLL_PROCESS_DETACH:
		if (cryptoki) cryptoki->C_Finalize(NULL);
		break;
	}
	return ret;
}

#else
KAPI_CONSTRUCTOR KAPI_IMPLEMENTATION(void, __init)(void)
{
	if (C_GetFunctionList(&cryptoki) == CKR_OK) cryptoki->C_Initialize(NULL);
}
KAPI_DESTRUCTOR KAPI_IMPLEMENTATION(void, __destroy)(void)
{
	if (cryptoki) cryptoki->C_Finalize(NULL);
}
#endif

KAPI_EXPORT(CK_RV, KAPI_TokenInfo)(_INOUT_ CK_TOKEN_INFO_PTR pInfo)
{
	CK_RV rv;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;

	KAPI_ASSERT();
	if
	(
		(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
		(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK)
			rv = cryptoki->C_GetTokenInfo(pSlotList[0], pInfo);
		free(pSlotList);
	}
	return rv;
}

KAPI_EXPORT(CK_RV, KAPI_InitToken)(_IN_ char *szPIN)
{
	CK_RV rv;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;

	KAPI_ASSERT();
	if
	(
		(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
		(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
			(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession)) == CKR_OK
		)
		{
			rv = cryptoki->C_InitPIN(hSession, (CK_UTF8CHAR_PTR) szPIN, strlen(szPIN));
			cryptoki->C_CloseSession(hSession);
		}
		free(pSlotList);
	}
	return rv;
}

KAPI_FUNCTION(NH_RV, __signature_callback)
(
	_IN_ NH_BLOB *data,
	_UNUSED_ _IN_ CK_MECHANISM_TYPE mechanism,
	_IN_ void *params,
	_OUT_ unsigned char *signature,
	_INOUT_ size_t *sigSize
)
{
	NH_RV rv = NH_OK;
	NH_ASN1_ENCODER_HANDLE hEncoder;
	NH_BLOB hash = { NULL, 0UL };
	__SIGN_PARAMS p = (__SIGN_PARAMS) params;
	unsigned int *pHash;
	size_t ulHashLen;

	switch (p->ulMechanism)
	{
	case CKM_SHA1_RSA_PKCS:
		pHash = sha1_oid;
		ulHashLen = NHC_SHA1_OID_COUNT;
		break;
	case CKM_SHA256_RSA_PKCS:
		pHash = sha256_oid;
		ulHashLen = NHC_SHA256_OID_COUNT;
		break;
	case CKM_SHA384_RSA_PKCS:
		pHash = sha384_oid;
		ulHashLen = NHC_SHA384_OID_COUNT;
		break;
	case CKM_SHA512_RSA_PKCS:
		pHash = sha512_oid;
		ulHashLen = NHC_SHA512_OID_COUNT;
		break;	
	default:
		rv = NH_UNSUPPORTED_MECH_ERROR;
	}
	if (NH_SUCCESS(rv) && NH_SUCCESS(rv = NH_encode_digest_info(pHash, ulHashLen, data->data, data->length, &hEncoder)))
	{
		hash.length = hEncoder->encoded_size(hEncoder, hEncoder->root);
		if (NH_SUCCESS(rv = (hash.data = (unsigned char*) malloc(hash.length)) ? NH_OK : NH_OUT_OF_MEMORY_ERROR))
		{
			if
			(
				NH_SUCCESS(rv = hEncoder->encode(hEncoder, hEncoder->root, hash.data))
			)	rv = (cryptoki->C_Sign(p->hSession, hash.data, hash.length, signature, sigSize)) == CKR_OK ? NH_OK : NH_GENERAL_ERROR;
			free(hash.data);
		}
		NH_release_digest_encoder(hEncoder);
	}
	return rv;
}
#define BEGIN_P10_ARMOUR			"-----BEGIN CERTIFICATE REQUEST-----\n"
#define END_P10_ARMOUR				"-----END CERTIFICATE REQUEST-----"
#define BEGIN_CERT_ARMOUR			"-----BEGIN CERTIFICATE -----\n"
#define END_CERT_ARMOUR				"-----END CERTIFICATE -----"
#define BEGIN_P7_ARMOUR				"-----BEGIN PKCS7-----\n"
#define END_P7_ARMOUR				"-----END PKCS7-----"
KAPI_INLINE KAPI_IMPLEMENTATION(CK_ULONG,  __remove_PEM_armour)(char *szPEM, char **szStart, CK_ULONG_PTR ulNewLen)
{
	CK_ULONG idx = 0, nlen = 0, armourlen, ulSize;

	ulSize = strlen(szPEM);
	while (szPEM[idx] == 0x2D && idx < ulSize) idx++;
	if (idx == 0)
	{
		*szStart = szPEM;
		*ulNewLen = ulSize;
		return 0UL;
	}
	while (szPEM[idx] != 0x2D && idx < ulSize) idx++;
	while (szPEM[idx] == 0x2D && idx < ulSize) idx++;
	if (idx == 0 || idx == ulSize) return CK_UNAVAILABLE_INFORMATION;
	*szStart = &szPEM[idx];
	armourlen = idx;
	while (szPEM[idx] != 0x2D && idx < ulSize)
	{
		idx++;
		nlen++;
	}
	*ulNewLen = nlen;
	return armourlen;
}
KAPI_EXPORT(CK_RV, KAPI_DecodePEM)(_IN_ char *szInput, _OUT_ NH_BLOB *pOut)
{
	CK_RV rv;
	char *szStart = NULL;
	size_t ulLen, ulNewLen = 0UL;
	base64_decodestate state_in;

	if
	(
		(rv = szInput && pOut ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = __remove_PEM_armour((char*) szInput, &szStart, &ulNewLen) != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
	)
	{
		if (!pOut->data) pOut->length = ulNewLen;
		else
		{
			if (pOut->length >= ulNewLen)
			{
				base64_init_decodestate(&state_in);
				ulLen = base64_decode_block(szStart, ulNewLen, (char*) pOut->data, &state_in);
				pOut->length = ulLen;
			}
			else rv = CKR_BUFFER_TOO_SMALL;
		}
	}
	return rv;
}
KAPI_EXPORT(CK_RV, KAPI_EncodePEM)(_IN_ NH_BLOB *pInput, _IN_ KAPI_PEMFORMAT fmt, _OUT_ char *szOutput, _INOUT_ size_t *pulOutput)
{
	CK_RV rv = CKR_OK;
	CK_ULONG ulLen, ulBALen = 0UL, ulEALen = 0UL;
	char *szBeginArmour, *szEndArmour;
	base64_encodestate state_in;

	switch (fmt)
	{
	case P10_FORMAT:
		szBeginArmour = BEGIN_P10_ARMOUR;
		szEndArmour = END_P10_ARMOUR;
		break;
	case P7_FORMAT:
		szBeginArmour = BEGIN_P7_ARMOUR;
		szEndArmour = END_P7_ARMOUR;
		break;
	case CERT_FORMAT:
		szBeginArmour = BEGIN_CERT_ARMOUR;
		szEndArmour = END_CERT_ARMOUR;
		break;
	case FORMAT_NONE:
		szBeginArmour = NULL;
		szEndArmour = NULL;
		break;
	default:
		rv = CKR_ARGUMENTS_BAD;
		break;
	}
	if (rv == CKR_OK && (rv = pInput && pInput->data ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
	{
		if (szBeginArmour && szEndArmour)
		{
			ulBALen = strlen(szBeginArmour);
			ulEALen = strlen(szEndArmour);
		}
		ulLen = (((pInput->length + ((pInput->length % 3) ? (3 - (pInput->length % 3)) : 0)) / 3) * 4);
		ulLen += (ulLen / 72) + 3 + ulBALen + ulEALen + 2;
		if (!szOutput) *pulOutput = ulLen;
		else
		{
			if (*pulOutput >= ulLen)
			{
				if (szBeginArmour) memcpy(szOutput, szBeginArmour, ulBALen);
				base64_init_encodestate(&state_in);
				ulLen = base64_encode_block((char*) pInput->data, pInput->length, (szOutput + ulBALen), &state_in);
				ulLen += base64_encode_blockend((szOutput + ulBALen + ulLen), &state_in);
				if (szEndArmour) memcpy((szOutput + ulBALen + ulLen), szEndArmour, ulEALen);
				ulLen += ulBALen + ulEALen;
				szOutput[ulLen] = 0;
				*pulOutput = ulLen;
			}
			else rv = CKR_BUFFER_TOO_SMALL;
		}
	}
	return rv;
}
