/**
 * @file sign.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Signature API
 * 
 * @copyright Copyleft (c) 2019-2020 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kpapi.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>

KHASH_MAP_INIT_INT64(KOSET, CK_OBJECT_HANDLE)		/* Object handles hash set */

static CK_BBOOL __true = CK_TRUE;
static CK_OBJECT_CLASS __privKey = CKO_PRIVATE_KEY, __cert = CKO_CERTIFICATE;

KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __find_privKeys)(CK_SESSION_HANDLE hSession, khash_t(KOSET) **kOut)
{
	CK_RV rv;
	khash_t(KOSET) *kKeys;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[16];
	CK_ULONG ulRet = 16, ulMaxCount = 16, i;
	khiter_t k;
	int ret;

	if ((rv = (kKeys = kh_init(KOSET)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		rv = cryptoki->C_FindObjectsInit(hSession, search, 3UL);
		while (rv == CKR_OK && ulRet == ulMaxCount)
		{
			rv = cryptoki->C_FindObjects(hSession, pRet, ulMaxCount, &ulRet);
			i = 0;
			while (rv == CKR_OK && i < ulRet)
			{
				k = kh_put(KOSET, kKeys, pRet[i], &ret);
				if (ret > 0) kh_value(kKeys, k) = pRet[i];
				else rv = CKR_FUNCTION_FAILED;
				i++;
			}
		}
		rv = cryptoki->C_FindObjectsFinal(hSession);
		if (rv == CKR_OK) *kOut = kKeys;
		else kh_destroy(KOSET, kKeys);
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __find_cert)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hKey, CK_OBJECT_HANDLE_PTR phObject)
{
	CK_RV rv;
	CK_OBJECT_HANDLE pRet[2];
	CK_ULONG ulRet;
	CK_ATTRIBUTE id[] = {{ CKA_ID, NULL, 0UL }},
	cert[] =
	{
		{ CKA_ID, NULL, 0UL },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) }
	};

	if
	(
		(rv = cryptoki->C_GetAttributeValue(hSession, hKey, id, 1UL)) == CKR_OK &&
		(rv = (id[0].pValue = malloc(id[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		rv = cryptoki->C_GetAttributeValue(hSession, hKey, id, 1UL);
		cert[0].pValue = id[0].pValue;
		cert[0].ulValueLen = id[0].ulValueLen;

		if
		(
			(rv = cryptoki->C_FindObjectsInit(hSession, cert, 2UL)) == CKR_OK &&
			(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
			(rv = cryptoki->C_FindObjectsFinal(hSession) == CKR_OK) &&
			(rv = ulRet < 2 ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK
		)
		{
			if ((ulRet == 1)) *phObject = pRet[0];
			else *phObject = CK_UNAVAILABLE_INFORMATION;
		}
		free(id[0].pValue);
	}
	return rv;
}
KAPI_IMPLEMENTATION(void, __release_cert)(KAPI_CERTIFICATE pCert)
{
	if (pCert)
	{
		if (pCert->subject.data) free(pCert->subject.data);
		if (pCert->issuer.data) free(pCert->issuer.data);
		if (pCert->serial.data) free(pCert->serial.data);
		free(pCert);
	}
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __cert_to_sid)(NH_CERTIFICATE_HANDLER hCert, KAPI_CERTIFICATE *ppOut)
{
	CK_RV rv;
	KAPI_CERTIFICATE pCert = NULL;
	if ((rv = (pCert = (KAPI_CERTIFICATE) malloc(sizeof(KAPI_CERTIFICATE_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memset(pCert, 0, sizeof(KAPI_CERTIFICATE_STR));
		pCert->subject.length = hCert->subject->node->size + (hCert->subject->node->contents - hCert->subject->node->identifier);
		pCert->issuer.length = hCert->issuer->node->size + (hCert->issuer->node->contents - hCert->issuer->node->identifier);
		pCert->serial.length = hCert->serialNumber->valuelen;
		if
		(
			(rv = (pCert->subject.data = (unsigned char*) malloc(pCert->subject.length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = (pCert->issuer.data = (unsigned char*) malloc(pCert->issuer.length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = (pCert->serial.data = (unsigned char*) malloc(pCert->serial.length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			memcpy(pCert->subject.data, hCert->subject->node->identifier, pCert->subject.length);
			memcpy(pCert->issuer.data, hCert->issuer->node->identifier, pCert->issuer.length);
			memcpy(pCert->serial.data, hCert->serialNumber->value, pCert->serial.length);
			*ppOut = pCert;
		}
		else __release_cert(pCert);
	}
	return rv;
}
KAPI_IMPLEMENTATION(CK_RV, __get_cert_by_value)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hCert, NH_BLOB *pCert, NH_CERTIFICATE_HANDLER *phOut)
{
	CK_RV rv;
	CK_ATTRIBUTE cert[] = {{ CKA_VALUE, NULL, 0UL }};
	NH_CERTIFICATE_HANDLER hHandler = NULL;

	if
	(
		(rv = cryptoki->C_GetAttributeValue(hSession, hCert, cert, 1UL)) == CKR_OK &&
		(rv = cert[0].ulValueLen != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
		(rv = (cert[0].pValue = malloc(cert[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetAttributeValue(hSession, hCert, cert, 1UL)) == CKR_OK &&
			(rv = (pCert->data = (unsigned char*) malloc(cert[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			memcpy(pCert->data, cert[0].pValue, cert[0].ulValueLen);
			pCert->length = cert[0].ulValueLen;
			if ((rv = NH_SUCCESS(NH_parse_certificate(pCert->data, pCert->length, &hHandler)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK)
				*phOut = hHandler;
			else free(pCert->data);
		}
		free(cert[0].pValue);
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __get_cert)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hCert, KAPI_CERTIFICATE *ppOut)
{
	CK_RV rv;
	CK_ATTRIBUTE cert[] =
	{
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_ISSUER, NULL, 0UL },
		{ CKA_SERIAL_NUMBER, NULL, 0UL }
	};
	KAPI_CERTIFICATE pCert;
	NH_CERTIFICATE_HANDLER hHandler;
	NH_BLOB pEncoding = { NULL, 0UL };

	if ((rv = cryptoki->C_GetAttributeValue(hSession, hCert, cert, 3UL)) == CKR_OK)
	{
		if
		(
			cert[0].ulValueLen == CK_UNAVAILABLE_INFORMATION ||
			cert[1].ulValueLen == CK_UNAVAILABLE_INFORMATION ||
			cert[2].ulValueLen == CK_UNAVAILABLE_INFORMATION
		)
		{
			if ((rv = __get_cert_by_value(hSession, hCert, &pEncoding, &hHandler)) == CKR_OK)
			{
				rv = __cert_to_sid(hHandler, ppOut);
				NH_release_certificate(hHandler);
				free(pEncoding.data);
			}
		}
		else
		{
			if
			(
				(rv = (cert[0].pValue = malloc(cert[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
				(rv = (cert[1].pValue = malloc(cert[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
				(rv = (cert[2].pValue = malloc(cert[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
				(rv = cryptoki->C_GetAttributeValue(hSession, hCert, cert, 3UL)) == CKR_OK &&
				(rv = (pCert = (KAPI_CERTIFICATE) malloc(sizeof(KAPI_CERTIFICATE_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				memset(pCert, 0, sizeof(KAPI_CERTIFICATE_STR));
				if
				(
					(rv = (pCert->subject.data = (unsigned char*) malloc(cert[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK && 
					(rv = (pCert->issuer.data = (unsigned char*) malloc(cert[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
					(rv = (pCert->serial.data = (unsigned char*) malloc(cert[2].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)
				{
					memcpy(pCert->subject.data, cert[0].pValue, cert[0].ulValueLen);
					memcpy(pCert->issuer.data, cert[1].pValue, cert[1].ulValueLen);
					memcpy(pCert->serial.data, cert[2].pValue, cert[2].ulValueLen);
					pCert->subject.length = cert[0].ulValueLen;
					pCert->issuer.length = cert[1].ulValueLen;
					pCert->serial.length = cert[2].ulValueLen;
					*ppOut = pCert;
				}
				else
				{
					if (pCert->subject.data) free(pCert->subject.data);
					if (pCert->issuer.data) free(pCert->issuer.data);
					if (pCert->serial.data) free(pCert->serial.data);
					free(pCert);
				}
				
			}
			if (cert[0].pValue) free(cert[0].pValue);
			if (cert[1].pValue) free(cert[1].pValue);
			if (cert[2].pValue) free(cert[2].pValue);
		}
		
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_BBOOL, __check_validity)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hCert)
{
	CK_RV rv;
	CK_BBOOL ret = CK_FALSE;
	NH_CERTIFICATE_HANDLER hHandler = NULL;
	NH_BLOB pEncoding = { NULL, 0UL };
	time_t tCurrent;

	time(&tCurrent);
	if ((rv = __get_cert_by_value(hSession, hCert, &pEncoding, &hHandler)) == CKR_OK)
	{
		ret = NH_SUCCESS(hHandler->check_validity(hHandler, gmtime(&tCurrent)));
		NH_release_certificate(hHandler);
		if (pEncoding.data) free(pEncoding.data);
	}
	return ret;
}
KAPI_EXPORT(CK_RV, KAPI_EnumCertificatesInit)(_IN_ char *szPIN, _OUT_ KAPI_HANDLER *ppCtx)
{
	CK_RV rv;
	khash_t(KAPICERTMAP) *kCertificates;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;
	khash_t(KOSET) *kKeys;
	CK_OBJECT_HANDLE hCert = CK_UNAVAILABLE_INFORMATION;
	KAPI_CERTIFICATE pCert = NULL;
	khiter_t k, j;
	int ret;
	KAPI_CERTENUM pCtx;

	KAPI_ASSERT();
	if
	(
		(rv = szPIN ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (kCertificates = kh_init(KAPICERTMAP)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
			(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if
			(
				(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
				(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION, NULL, NULL, &hSession)) == CKR_OK
			)
			{
				if ((rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) szPIN, strlen(szPIN))) == CKR_OK)
				{
					if ((rv = __find_privKeys(hSession, &kKeys)) == CKR_OK)
					{
						j = kh_begin(kKeys);
						while (rv == CKR_OK && j != kh_end(kKeys))
						{
							if (kh_exist(kKeys, j))
							{
								if ((rv = __find_cert(hSession, kh_value(kKeys, j), &hCert)) == CKR_OK)
								{
									if
									(
										hCert != CK_UNAVAILABLE_INFORMATION &&
										(rv = __get_cert(hSession, hCert, &pCert)) == CKR_OK &&
										__check_validity(hSession, hCert)
									)
									{
										k = kh_put(KAPICERTMAP, kCertificates, hCert, &ret);
										if (ret > 0) kh_value(kCertificates, k) = pCert;
										else { __release_cert(pCert); rv = CKR_FUNCTION_FAILED; }
									}
								}
							}
							j++;
						}
						kh_destroy(KOSET, kKeys);
					}
					cryptoki->C_Logout(hSession);
				}
				cryptoki->C_CloseSession(hSession);
			}
			free(pSlotList);
		}
		if (rv == CKR_OK && (rv = (pCtx = malloc(sizeof(KAPI_CERTENUM_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			pCtx->k = kh_begin(kCertificates);
			pCtx->kCertificates = kCertificates;
			*ppCtx = pCtx;
		}
		if (rv != CKR_OK)
		{
			for (k = kh_begin(kCertificates); k != kh_end(kCertificates); k++) if (kh_exist(kCertificates, k)) __release_cert(kh_value(kCertificates, k));
			kh_destroy(KAPICERTMAP, kCertificates);
			kCertificates = NULL;
		}
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(size_t, __cert_size)(KAPI_CERTIFICATE pCert)
{
	return sizeof(KAPI_CERTIFICATE_STR) + pCert->subject.length + pCert->issuer.length + pCert->serial.length;
}
KAPI_EXPORT(KAPI_CERTIFICATE, KAPI_EnumCertificates)(_INOUT_ KAPI_HANDLER pCtx)
{
	KAPI_CERTENUM pEnum = (KAPI_CERTENUM) pCtx;
	KAPI_CERTIFICATE pCert = NULL;

	if (pEnum)
	{
		while (pEnum->k != kh_end(pEnum->kCertificates) && !kh_exist(pEnum->kCertificates, pEnum->k)) pEnum->k++;
		if (pEnum->k != kh_end(pEnum->kCertificates) && kh_exist(pEnum->kCertificates, pEnum->k)) pCert = kh_value(pEnum->kCertificates, pEnum->k++);
	}
	return pCert;
}
KAPI_EXPORT(void, KAPI_EnumCertificatesFinal)(_INOUT_ KAPI_HANDLER pCtx)
{
	khiter_t k;
	KAPI_CERTENUM pEnum = (KAPI_CERTENUM) pCtx;

	if (pEnum)
	{
		for (k = kh_begin(pEnum->kCertificates); k != kh_end(pEnum->kCertificates); k++) if (kh_exist(pEnum->kCertificates, k)) __release_cert(kh_value(pEnum->kCertificates, k));
		kh_destroy(KAPICERTMAP, pEnum->kCertificates);
		free(pEnum);
	}
}
KAPI_EXPORT(KAPI_CERTIFICATE, KAPI_CloneCertificateContext)(_IN_ KAPI_CERTIFICATE pCtx)
{
	KAPI_CERTIFICATE pOut = NULL;
	if (pCtx && (pOut = (KAPI_CERTIFICATE) malloc(sizeof(KAPI_CERTIFICATE_STR))))
	{
		memset(pOut, 0, sizeof(KAPI_CERTIFICATE_STR));
		if
		(
			(pOut->subject.data = (unsigned char*) malloc(pCtx->subject.length)) &&
			(pOut->issuer.data = (unsigned char*) malloc(pCtx->issuer.length)) &&
			(pOut->serial.data = (unsigned char*) malloc(pCtx->serial.length))
		)
		{
			pOut->subject.length = pCtx->subject.length;
			pOut->issuer.length = pCtx->issuer.length;
			pOut->serial.length = pCtx->serial.length;
			memcpy(pOut->subject.data, pCtx->subject.data, pCtx->subject.length);
			memcpy(pOut->issuer.data, pCtx->issuer.data, pCtx->issuer.length);
			memcpy(pOut->serial.data, pCtx->serial.data, pCtx->serial.length);
		}
		else
		{
			__release_cert(pOut);
			pOut = NULL;
		}
	}
	return pOut;
}
KAPI_EXPORT(void, KAPI_ReleaseCertificateContext)(_INOUT_ KAPI_CERTIFICATE pCtx)
{
	__release_cert(pCtx);
}


KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __add_cert_chain)(CK_SESSION_HANDLE hSession, NH_CERTIFICATE_HANDLER hCert, NH_CMS_SD_ENCODER hCMS)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] = {{ CKA_SUBJECT, NULL, 0UL }}, value[] = {{ CKA_VALUE, NULL, 0UL }};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;
	NH_CERTIFICATE_HANDLER hCA;
	CK_BBOOL finished = CK_FALSE;

	search[0].ulValueLen = hCert->issuer->node->size + hCert->issuer->node->contents - hCert->issuer->node->identifier;
	if ((rv = (search[0].pValue = malloc(search[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		memcpy(search[0].pValue, hCert->issuer->node->identifier, search[0].ulValueLen);
	while (rv == CKR_OK && !finished)
	{
		if
		(
			(rv = cryptoki->C_FindObjectsInit(hSession, search, 1UL)) == CKR_OK &&
			(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
			(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK &&
			!(finished = ulRet != 1 ? CK_TRUE : CK_FALSE)
		)
		{
			if
			(
				(rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], value, 1UL)) == CKR_OK &&
				(rv = value[0].ulValueLen != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
				(rv = (value[0].pValue = malloc(value[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				if
				(
					(rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], value, 1UL)) == CKR_OK &&
					(rv = NH_SUCCESS(NH_parse_certificate((unsigned char*) value[0].pValue, value[0].ulValueLen, &hCA)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK
				)
				{
					if ((rv = NH_SUCCESS(hCMS->add_cert(hCMS, hCA)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK)
					{
						free(search[0].pValue);
						search[0].ulValueLen = hCert->issuer->node->size + hCert->issuer->node->contents - hCert->issuer->node->identifier;
						if ((rv = (search[0].pValue = malloc(search[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
							memcpy(search[0].pValue, hCA->issuer->node->identifier, search[0].ulValueLen);
					}
					NH_release_certificate(hCA);
				}
			}
		}
	}
	if (search[0].pValue) free(search[0].pValue);
	if (value[0].pValue) free(value[0].pValue);
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __find_cert_by_sid)(CK_SESSION_HANDLE hSession, KAPI_CERTIFICATE pCert, NH_BLOB *pOut, NH_BLOB *pID)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_ISSUER, NULL, 0UL },
		{ CKA_SERIAL_NUMBER, NULL, 0UL },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) }
	},
	value[] =
	{
		{ CKA_VALUE, NULL, 0UL },
		{ CKA_ID, NULL, 0UL }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;

	search[0].pValue = pCert->subject.data;
	search[0].ulValueLen = pCert->subject.length;
	search[1].pValue = pCert->issuer.data;
	search[1].ulValueLen = pCert->issuer.length;
	search[2].pValue = pCert->serial.data;
	search[2].ulValueLen = pCert->serial.length;
	if
	(
		(rv = cryptoki->C_FindObjectsInit(hSession, search, 4UL)) == CKR_OK &&
		(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
		(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK &&
		(rv = ulRet == 1 ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], value, 2UL)) == CKR_OK &&
		(rv = value[0].ulValueLen != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
		(rv = value[1].ulValueLen != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
		(rv = (value[0].pValue = malloc(value[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = (value[1].pValue = malloc(value[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], value, 2UL)) == CKR_OK &&
			(rv = (pOut->data = (unsigned char*) malloc(value[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = (pID->data = (unsigned char*) malloc(value[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			memcpy(pOut->data, value[0].pValue, value[0].ulValueLen);
			memcpy(pID->data, value[1].pValue, value[1].ulValueLen);
			pOut->length = value[0].ulValueLen;
			pID->length = value[1].ulValueLen;
		}
	}
	if (value[0].pValue) free(value[0].pValue);
	if (value[1].pValue) free(value[1].pValue);
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __find_key_by_id)(CK_SESSION_HANDLE hSession, NH_BLOB *pID, CK_OBJECT_HANDLE_PTR phKey)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_ID, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[2];
	CK_ULONG ulRet;

	search[0].pValue = pID->data;
	search[0].ulValueLen = pID->length;
	if
	(
		(rv = cryptoki->C_FindObjectsInit(hSession, search, 4UL)) == CKR_OK &&
		(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
		(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK &&
		(rv = ulRet == 1 ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
	)	*phKey = pRet[0];
	return rv;
}
KAPI_IMPLEMENTATION(CK_RV, __sign_contents)
(
	CK_SESSION_HANDLE hSession,
	CK_MECHANISM_TYPE ulMechanism,
	KAPI_CERTIFICATE pSigningCert,
	NH_BLOB *pContents,
	CK_BBOOL attach,
	NH_BLOB *pOut
)
{
	CK_RV rv;
	NH_BLOB pCert = { NULL, 0UL }, pID = { NULL, 0UL }, pEncoded = { NULL, 0UL };
	CK_OBJECT_HANDLE hKey = CK_UNAVAILABLE_INFORMATION;
	CK_MECHANISM pMechanism = { CKM_RSA_PKCS, NULL, 0UL };
	NH_CERTIFICATE_HANDLER hCert;
	NH_CMS_SD_ENCODER hCMS;
	__SIGN_PARAMS_STR params = { 0UL, 0UL };

	if
	(
		(rv = __find_cert_by_sid(hSession, pSigningCert, &pCert, &pID)) == CKR_OK &&
		(rv = __find_key_by_id(hSession, &pID, &hKey)) == CKR_OK
	)
	{
		if ((rv = NH_SUCCESS(NH_parse_certificate(pCert.data, pCert.length, &hCert)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK)
		{
			params.hSession = hSession;
			params.ulMechanism = ulMechanism;
			if ((rv = NH_SUCCESS(NH_cms_encode_signed_data(pContents, &hCMS)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK)
			{
				if
				(
					(rv = hCMS->data_ctype(hCMS, attach)) == CKR_OK &&
					(rv = hCMS->add_cert(hCMS, hCert)) == CKR_OK &&
					(rv = __add_cert_chain(hSession, hCert, hCMS)) == CKR_OK &&
					(rv = cryptoki->C_SignInit(hSession, &pMechanism, hKey)) == CKR_OK &&
					(rv = hCMS->sign_cades_bes(hCMS, hCert, ulMechanism, __signature_callback, &params)) == CKR_OK
				)
				{
					pEncoded.length = hCMS->hEncoder->encoded_size(hCMS->hEncoder, hCMS->hEncoder->root);
					if ((rv = (pEncoded.data = (unsigned char*) malloc(pEncoded.length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
					{
						if ((rv = hCMS->hEncoder->encode(hCMS->hEncoder, hCMS->hEncoder->root, pEncoded.data)) == CKR_OK)
						{
							pOut->data = pEncoded.data;
							pOut->length = pEncoded.length;
						}
						else free(pEncoded.data);
					}
				}
				NH_cms_release_sd_encoder(hCMS);
			}
			NH_release_certificate(hCert);
		}
	}
	if (pCert.data) free(pCert.data);
	if (pID.data) free(pID.data);
	return rv;
}
KAPI_EXPORT(CK_RV, KAPI_Sign)
(
	_IN_ KAPI_CERTIFICATE pSigningCert,
	_IN_ CK_MECHANISM_TYPE ulMechanism,
	_IN_ KAPI_POLICY iPolicy,
	_IN_ NH_BLOB *pContents,
	_IN_ CK_BBOOL attach,
	_IN_ char *szPIN,
	_OUT_ NH_BLOB *pOut
)
{
	CK_RV rv;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;

	switch (ulMechanism)
	{
	case CKM_SHA1_RSA_PKCS:
	case CKM_SHA256_RSA_PKCS:
	case CKM_SHA384_RSA_PKCS:
	case CKM_SHA512_RSA_PKCS:
		switch (iPolicy)
		{
		case CAdES_BES:
			rv = CKR_OK;
			break;
		case CAdES_EPES:
		case CAdES_T:
		case CAdES_C:
		case CAdES_A:
		default:
			rv = CKR_ARGUMENTS_BAD;
		}
		break;
	default:
		rv = CKR_MECHANISM_INVALID;
		break;
	}
	if
	(
		(rv == CKR_OK) &&
		(rv = pSigningCert && pSigningCert->subject.data && pSigningCert->issuer.data && pSigningCert->serial.data &&
			pContents && pContents->data && szPIN ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
		(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
			(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION, NULL, NULL, &hSession)) == CKR_OK
		)
		{
			if ((rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) szPIN, strlen(szPIN))) == CKR_OK)
			{
				rv = __sign_contents(hSession, ulMechanism, pSigningCert, (NH_BLOB*) pContents, attach, pOut);
				cryptoki->C_Logout(hSession);
			}
			cryptoki->C_CloseSession(hSession);
		}
		free(pSlotList);
	}
	return rv;
}
