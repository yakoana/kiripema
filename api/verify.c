/**
 * @file verify.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Cryptographic signature validation API
 * 
 * @copyright Copyleft (c) 2019-2020 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kpapi.h"
#include <stdlib.h>
#include <string.h>

KAPI_EXPORT(CK_RV, KAPI_Verify)(_IN_ NH_BLOB *pCMS, _OUT_ CK_BBOOL *pbMatch)
{
	CK_RV rv;
	NH_CMS_SD_PARSER hCMS;
	NH_CMS_ISSUER_SERIAL pSID;
	NH_CERTIFICATE_HANDLER hCert;
	CK_BBOOL match;

	if
	(
		(rv = pCMS && pCMS->data && pbMatch ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = NH_cms_discover(pCMS->data, pCMS->length) == NH_SIGNED_DATA_CTYPE ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = NH_SUCCESS(NH_cms_parse_signed_data(pCMS->data, pCMS->length, &hCMS)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
	)
	{
		if
		(
			(rv = NH_SUCCESS(hCMS->get_sid(hCMS, 0UL, &pSID)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
			(rv = NH_SUCCESS(hCMS->get_cert(hCMS, pSID, &hCert)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
		)
		{
			if ((match = NH_SUCCESS(hCMS->verify(hCMS, 0UL, hCert->pubkey)))) match = NH_SUCCESS(hCMS->validate_attached(hCMS));
			*pbMatch = match;
			NH_release_certificate(hCert);
		}
		NH_cms_release_sd_parser(hCMS);
	}
	return rv;
}

static CK_OBJECT_CLASS __cert = CKO_CERTIFICATE;
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __check_issuer)(CK_SESSION_HANDLE hSession, NH_CERTIFICATE_HANDLER hCert, CK_BBOOL *pbMatch)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_ISSUER, NULL, 0UL },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) }
	},
	value[] = {{ CKA_VALUE, NULL, 0UL }};
	CK_OBJECT_HANDLE pRet[16];
	CK_ULONG ulRet = 16, ulMaxCount = 16, i;
	NH_CERTIFICATE_HANDLER hCA;
	CK_BBOOL match = CK_FALSE;

	rv = cryptoki->C_FindObjectsInit(hSession, search, 2UL);
	while (rv == CKR_OK && ulRet == ulMaxCount && !match)
	{
		rv = cryptoki->C_FindObjects(hSession, pRet, ulMaxCount, &ulRet);
		i = 0;
		while (rv == CKR_OK && i < ulRet && !match)
		{
			if
			(
				(rv = cryptoki->C_GetAttributeValue(hSession, pRet[i], value, 1UL)) == CKR_OK &&
				(rv = value[0].ulValueLen != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
				(rv = (value[0].pValue = malloc(value[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				if
				(
					(rv = cryptoki->C_GetAttributeValue(hSession, pRet[i], value, 1UL)) == CKR_OK &&
					(rv = NH_SUCCESS(NH_parse_certificate((unsigned char*) value[0].pValue, value[0].ulValueLen, &hCA)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK
				)
				{
					match = NH_SUCCESS(hCert->verify(hCert, hCA->pubkey));
					NH_release_certificate(hCA);
				}
				free(value[0].pValue);
			}
			i++;
		}
	}
	rv = cryptoki->C_FindObjectsFinal(hSession);
	if (rv == CKR_OK) *pbMatch = match;
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __find_issuer)(NH_CMS_SD_PARSER hCMS, NH_CERTIFICATE_HANDLER hCert, NH_CERTIFICATE_HANDLER *hIssuer)
{
	CK_RV rv;
	NH_ASN1_PNODE pNode;
	NH_CERTIFICATE_HANDLER hCur;

	rv = hCMS->certificates && (pNode = hCMS->certificates->child) ? CKR_OK : CKR_ARGUMENTS_BAD;
	while (rv == CKR_OK && pNode && !*hIssuer)
	{
		if ((rv = NH_SUCCESS(NH_parse_certificate(pNode->identifier, pNode->size + pNode->contents - pNode->identifier, &hCur)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
		{
			if (strcmp(hCur->subject->stringprep, hCert->issuer->stringprep) == 0) *hIssuer = hCur;
			else NH_release_certificate(hCur);
		}
		pNode = pNode->next;
	}
	return rv;
}
KAPI_EXPORT(CK_RV, KAPI_VerifySigningCertificate)(_IN_ NH_BLOB *pCMS, _OUT_ CK_BBOOL *pbMatch)
{
	CK_RV rv;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;
	NH_CMS_SD_PARSER hCMS;
	NH_CMS_ISSUER_SERIAL pSID;
	NH_CERTIFICATE_HANDLER hCert, hCA;
	CK_BBOOL match = CK_FALSE;

	if
	(
		(rv = pCMS && pCMS->data && pbMatch ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
		(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
			(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION, NULL, NULL, &hSession)) == CKR_OK
		)
		{
			if
			(
				(rv = NH_cms_discover(pCMS->data, pCMS->length) == NH_SIGNED_DATA_CTYPE ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
				(rv = NH_SUCCESS(NH_cms_parse_signed_data(pCMS->data, pCMS->length, &hCMS)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
			)
			{
				if
				(
					(rv = NH_SUCCESS(hCMS->get_sid(hCMS, 0UL, &pSID)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
					(rv = NH_SUCCESS(hCMS->get_cert(hCMS, pSID, &hCert)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
				)
				{
					while (rv == CKR_OK && !match)
					{
						rv = __check_issuer(hSession, hCert, &match);
						hCA = NULL;
						if (rv == CKR_OK && !match) rv = __find_issuer(hCMS, hCert, &hCA);
						NH_release_certificate(hCert);
						hCert = hCA;
					}
				}
				NH_cms_release_sd_parser(hCMS);
			}
			cryptoki->C_CloseSession(hSession);
		}
		free(pSlotList);
	}
	return rv;
}