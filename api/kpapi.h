/**
 * @file kpapi.h
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Kiripema High Level Cryptographic API
 * 
 * @copyright Copyleft (c) 2019-2020 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#ifndef __KPAPI_H__
#define __KPAPI_H__

/* Cryptoki definitions */
#ifndef NULL
#define NULL							((void*) 0)
#endif
#ifdef CK_EXPORT_SPEC
#undef CK_EXPORT_SPEC
#endif
#ifdef CK_IMPORT_SPEC
#undef CK_IMPORT_SPEC
#endif
#ifdef CK_CALL_SPEC
#undef CK_CALL_SPEC
#endif
#pragma pack(push, cryptoki, 1)
#if defined(_WINDOWS)
#define CK_IMPORT_SPEC						__declspec(dllimport)
#define CK_EXPORT_SPEC						CK_IMPORT_SPEC
#if defined (_WIN32)
#define CK_CALL_SPEC						__cdecl
#else
#define CK_CALL_SPEC
#endif
#else
#define CK_EXPORT_SPEC
#define CK_IMPORT_SPEC
#define CK_CALL_SPEC
#endif
#ifndef CK_PTR
#define CK_PTR 							*
#endif
#ifndef CK_DECLARE_FUNCTION
#define CK_DECLARE_FUNCTION(type, name)			type CK_EXPORT_SPEC CK_CALL_SPEC name
#endif
#ifndef CK_DECLARE_FUNCTION_POINTER
#define CK_DECLARE_FUNCTION_POINTER(type, name)		type CK_IMPORT_SPEC (CK_CALL_SPEC CK_PTR name)
#endif
#ifndef CK_CALLBACK_FUNCTION
#define CK_CALLBACK_FUNCTION(type, name)			type (CK_CALL_SPEC CK_PTR name)
#endif
#ifndef NULL_PTR
#define NULL_PTR 							NULL
#endif
#include "pkcs11.h"
#pragma pack(pop, cryptoki)	/* Cryptoki definitions */


/* Kiripema API call definitions */
#if defined(_WINDOWS)
#define KAPI_EXTERN						extern
#define KAPI_EXPORT_SPEC					__declspec(dllexport)
#if defined (_WIN32)
#define KAPI_CALL_SPEC						__cdecl
#else
#define KAPI_CALL_SPEC
#endif
#else
#define KAPI_EXTERN
#define KAPI_EXPORT_SPEC
#define KAPI_CALL_SPEC
#endif				/* Kiripema API call definitions */

/* Compiler attributes */
#if defined(_WINDOWS)
define KAPI_CONSTRUCTOR
#define KAPI_DESTRUCTOR
#define KAPI_UNUSED
#define KAPI_INLINE						__inline
#define KAPI_NOP							__noop
#define KAPI_HIDDEN
#else
#define KAPI_CONSTRUCTOR					__attribute__((constructor))
#define KAPI_DESTRUCTOR						__attribute__((destructor))
#define KAPI_UNUSED						__attribute__((unused))
#define KAPI_INLINE						__inline__
#define KAPI_HIDDEN						__attribute__((__visibility__("internal")))
#endif	/* Compiler attributes */


/**
 * @brief API types
 * 
 */
#define KAPI_EXPORT(type, name)				KAPI_EXTERN type KAPI_EXPORT_SPEC KAPI_CALL_SPEC name
#define KAPI_IMPLEMENTATION(type, name)			static type KAPI_CALL_SPEC name
#define KAPI_FUNCTION(type, name)				KAPI_HIDDEN type KAPI_CALL_SPEC name

#include "pfx.h"					/* Nharu Library */
#include "khash.h"				/* Hashtable implementation */
extern CK_FUNCTION_LIST_PTR cryptoki;	/* Cryptoki should be C_Initialized just once */
#define KAPI_ASSERT()				{ if (!cryptoki) return CKR_GENERAL_ERROR; }
#define KAPI_KEY_CONTAINER			"Kiripema key container %d"
#define KAPI_CONTAINER_SIZE			32
typedef struct KAPI_CERTIFICATE_STR		/* X.509 Certificate identification */
{
	NH_BLOB	subject;			/* DER encoded of Subject attribute */
	NH_BLOB	issuer;			/* DER encoded of Issuer attribute */
	NH_BLOB	serial;			/* DER encoded of SerialNumber attribute */

} KAPI_CERTIFICATE_STR, *KAPI_CERTIFICATE;
KHASH_MAP_INIT_INT64(KAPICERTMAP, KAPI_CERTIFICATE)
typedef struct KAPI_CERTENUM_STR
{
	khash_t(KAPICERTMAP)*	kCertificates;
	khiter_t			k;

} KAPI_CERTENUM_STR, *KAPI_CERTENUM;
#define KAPI_HANDLER				CK_VOID_PTR
typedef enum KAPI_POLICY			/* Electronic Signature Formats */
{
	CAdES_BES,					/* CAdES Basic Electronic Signature */
	CAdES_EPES,					/* CAdES Explicit Policy-based Electronic Signatures */
	CAdES_T,					/* Electronic Signature with Time */
	CAdES_C,					/* ES with Complete Validation Data References */
	CAdES_A					/* Archival Electronic Signature */

} KAPI_POLICY;
typedef struct __SIGN_PARAMS_STR
{
	CK_SESSION_HANDLE		hSession;
	CK_MECHANISM_TYPE		ulMechanism;

} __SIGN_PARAMS_STR, *__SIGN_PARAMS;

typedef enum KAPI_PEMFORMAT			/* Supported PEM (Privacy Enhanced Mail) formats */
{
	FORMAT_NONE,				/* Single Base64 document */
	P10_FORMAT,					/* Base64 encoded PKCS#10 */
	P7_FORMAT,					/* Base64 encoded PKCS#7 */
	CERT_FORMAT					/* Base64 encoded certificate */

} KAPI_PEMFORMAT;
typedef struct KAPI_RSAKEY_STR		KAPI_RSAKEY_STR;
struct KAPI_RSAKEY_STR
{
	NH_BLOB			buffer;
	NH_ASN1_PARSER_HANDLE	hPrivKey;
	NH_ASN1_PARSER_HANDLE	hRSAKey;
	KAPI_RSAKEY_STR*		pNext;
	KAPI_RSAKEY_STR*		pPrevious;
};
typedef struct KAPI_RSAKEY_STR*		KAPI_RSAKEY;
typedef struct KAPI_X509CERT_STR		KAPI_X509CERT_STR;
struct KAPI_X509CERT_STR
{
	NH_BLOB			buffer;
	NH_CERTIFICATE_HANDLER	hCert;
	KAPI_X509CERT_STR*	pNext;
	KAPI_X509CERT_STR*	pPrevious;
};
typedef struct KAPI_X509CERT_STR*		KAPI_X509CERT;
typedef struct KAPI_PFX_STR
{
	NH_CARGO_CONTAINER	hContainer;
	NH_BLOB			buffer;
	NH_PFX_PARSER		hParser;
	KAPI_RSAKEY			pKeySet;
	KAPI_X509CERT		pCertSet;

} KAPI_PFX_STR, *KAPI_PFX;


/**
 * @brief Utilities
 * 
 */
KAPI_FUNCTION(NH_RV, __signature_callback)(_IN_ NH_BLOB*, _IN_ CK_MECHANISM_TYPE, _IN_ void*, _OUT_ unsigned char*, _INOUT_ size_t*);
KAPI_FUNCTION(CK_RV, __install)(CK_SESSION_HANDLE, NH_CERTIFICATE_HANDLER, char*);
KAPI_FUNCTION(CK_BBOOL, __is_CA)(NH_CERTIFICATE_HANDLER);
KAPI_FUNCTION(CK_RV, __find_certiticate_by_pubkey)(CK_SESSION_HANDLE, NH_CERTIFICATE_HANDLER, CK_OBJECT_HANDLE_PTR);
KAPI_FUNCTION(CK_RV, __find_certiticate_by_value)(CK_SESSION_HANDLE, NH_CERTIFICATE_HANDLER, CK_OBJECT_HANDLE_PTR);
KAPI_FUNCTION(CK_RV, __certificate_exists)(CK_SESSION_HANDLE, NH_CERTIFICATE_HANDLER, CK_BBOOL*);

/**
 * @brief Error codes
 * 
 */
#define KAPI_ERROR				CKR_VENDOR_DEFINED
/* TODO: review errors
 */

/**
 * @brief The API itself
 * 
 */

#if defined(__cplusplus)
extern "C" {
#endif
KAPI_EXPORT(CK_RV, KAPI_TokenInfo)
							/* Obtains information about Kiripema softoken
							 */
(
	_INOUT_ CK_TOKEN_INFO_PTR		/* pInfo */
);
KAPI_EXPORT(CK_RV, KAPI_InitToken)
							/* Initializes softoken (all existing objects will be erased)
							 * This function must be called before any use.
							 * Otherwise, a CKR_USER_PIN_NOT_INITIALIZED will be returned from login.
							 */
(
	_IN_ char*					/* szPIN: user PIN , where 256 >= strlen(szPIN) >= 8*/
);
KAPI_EXPORT(CK_RV, KAPI_GenerateCSR)	
							/*
							 * Generates RSA key pair and a certificate request.
							 * Public keys attributes will be set as follows:
							 * 	CKA_TOKEN: CK_TRUE
							 * 	CKA_PRIVATE: CK_TRUE
							 * 	CKA_MODIFIABLE: CK_TRUE
							 * 	CKA_LABEL: KAPI_KEY_CONTAINER plus an incremented integer
							 * 	CKA_COPIABLE: CK_TRUE
							 * 	CKA_DESTROYABLE: CK_TRUE
							 * 	CKA_KEY_TYPE: CKK_RSA
							 * 	CKA_ID: CKA_MODULUS atribute
							 * 	CKA_DERIVE: CK_FALSE
							 * 	CKA_LOCAL: CK_TRUE
							 * 	CKA_KEY_GEN_MECHANISM: CKM_RSA_PKCS_KEY_PAIR_GEN
							 * 	CKA_ALLOWED_MECHANISMS: CKM_RSA_PKCS
							 * 	CKA_ENCRYPT: CK_FALSE
							 * 	CKA_VERIFY: CK_TRUE
							 * 	CKA_VERIFY_RECOVER: CK_FALSE
							 * 	CKA_WRAP: CK_FALSE
							 * 	CKA_TRUSTED: CK_FALSE
							 * 	CKA_PUBLIC_KEY_INFO: present
							 * Private keys attributes will be set as follows:
							 * 	CKA_TOKEN: CK_TRUE
							 * 	CKA_PRIVATE: CK_FALSE
							 * 	CKA_MODIFIABLE: CK_TRUE
							 * 	CKA_LABEL: KAPI_KEY_CONTAINER plus an incremented integer
							 * 	CKA_COPIABLE: CK_TRUE
							 * 	CKA_DESTROYABLE: CK_TRUE
							 * 	CKA_KEY_TYPE: CKK_RSA
							 * 	CKA_ID: CKA_MODULUS atribute
							 * 	CKA_DERIVE: CK_FALSE
							 * 	CKA_LOCAL: CK_TRUE
							 * 	CKA_KEY_GEN_MECHANISM: CKM_RSA_PKCS_KEY_PAIR_GEN
							 * 	CKA_ALLOWED_MECHANISMS: CKM_RSA_PKCS
							 * 	CKA_SENSITIVE: CK_TRUE
							 * 	CKA_DECRYPT: CK_FALSE
							 * 	CKA_SIGN: CK_FALSE
							 * 	CKA_SIGN_RECOVER: CK_FALSE
							 * 	CKA_UNWRAP: CK_FALSE
							 * 	CKA_EXTRACTABLE: CK_FALSE
							 * 	CKA_ALWAYS_SENSITIVE: CK_TRUE
							 * 	CKA_NEVER_EXTRACTABLE: CK_TRUE
							 * 	CKA_ALWAYS_AUTHENTICATE: CK_FALSE
							 * 	CKA_PUBLIC_KEY_INFO: present
							 */
( 
	_IN_ NH_NAME*,				/* ppName: Subject distinguished name */
	_IN_ CK_ULONG,				/* ulNameCount: pName count */
	_IN_ int,					/* iKeySize: RSA key size (in bits), where 4096 >= iKeySize >= 1024 */
	_IN_ NH_BLOB*,				/* pPubExponent: RSA key public exponent, any valid value */
	_IN_ CK_MECHANISM_TYPE,			/* ulMechanism: request signature mechanism, where: CKM_SHA1_RSA_PKCS || CKM_SHA256_RSA_PKCS || CKM_SHA384_RSA_PKCS || CKM_SHA512_RSA_PKCS */
	_IN_ char*,					/* szPIN: user PIN to log in Kiripema */
	_OUT_ NH_BLOB*				/* pRequest: DER encoded request */
);
KAPI_EXPORT(CK_RV, KAPI_InstallCertificates)
							/*
							 * Installs signed certificate and its chain, if necessary
							 * CMS Signed Data certificates will be validated as follows:
							 * 	If than one certificate is present, it must be a complete chain
							 * 	Otherwise, only user certificate must be present
							 * 	If a chain is present, there signatures are chacked
							 * 	If the certificate was installed before, it is not installed again
							 * Certificate attributes will be set as follows:
							 * 	CKA_TOKEN: CK_TRUE
							 * 	CKA_PRIVATE: CK_FALSE
							 * 	CKA_MODIFIABLE: CK_FALSE
							 * 	CKA_LABEL: public/private key CKA_LABEL, if user certificate; otherwise the stringprep of Subject
							 * 	CKA_COPIABLE: CK_TRUE
							 * 	CKA_DESTROYABLE: CK_TRUE
							 * 	CKA_CERTIFICATE_TYPE: CKC_X_509
							 * 	CKA_TRUSTED: CK_TRUE, if root certificate
							 * 	CKA_CERTIFICATE_CATEGORY: CK_CERTIFICATE_CATEGORY_UNSPECIFIED
							 * 	CKA_CHECK_VALUE: present
							 * 	CKA_PUBLIC_KEY_INFO: present
							 * 	CKA_SUBJECT: present
							 * 	CKA_ID: RSA modulus
							 * 	CKA_ISSUER: present
							 * 	CKA_SERIAL_NUMBER: present
							 * 	CKA_VALUE: present
							 * Public and private key attributes will be updated as follows:
							 * 	CKA_SUBJECT: CKA_SUBJECT of related CKO_CERTIFICAGTE
							 * 	CKA_MODIFIABLE: CK_FALSE
							 */
(
	_IN_ NH_BLOB*,				/* pCMS: DER encoded PKCS #7 with signed certificate and its chain */
	_IN_ char*					/* szPIN: user PIN to log in Kiripema */
);
KAPI_EXPORT(CK_RV, KAPI_EnumCertificatesInit)
							/* Enumerates all installed certificates that are associated to a private key
							 * The certificate must satisfy the following conditions:
							 * 	Its CKA_ID attribute must matches the CKA_ID attribute of an existing private key
							 * 	Their notAfter and notBefore fields must satisfy validity
							 * If pCertificate argument is NULL, the required buffer size will be returned in pulSize argument
							 * If last enumeration was not finished returns CKR_OPERATION_ACTIVE.
							 */
(
	_IN_ char*,					/* szPIN: user PIN */
	_OUT_ KAPI_HANDLER*			/* ppCtx: context enumerator*/
);
KAPI_EXPORT(KAPI_CERTIFICATE, KAPI_EnumCertificates)
							/* Returns next certificate in enumeration
							 * If returns NULL, there are no more certificates to iterate.
							 */
(
	_INOUT_ KAPI_HANDLER			/* pCtx: enumeration context */
);
KAPI_EXPORT(void, KAPI_EnumCertificatesFinal)
							/* Finalizes current enumeration and releases all resources.
							 */
(
	_INOUT_ KAPI_HANDLER			/* pCtx */
);
KAPI_EXPORT(KAPI_CERTIFICATE, KAPI_CloneCertificateContext)
							/* Duplicates certificate context. Must be used if context is needed]
							 * after KAPI_EnumCertificatesFinal() execution.
							 * Must be released by KAPI_ReleaseCertificateContext.
							 * May return NULL on an out of memory error
							 */
(
	_IN_ KAPI_CERTIFICATE			/* pCtx */
);
KAPI_EXPORT(void, KAPI_ReleaseCertificateContext)
							/* Releases a cloned certificate context.
							 */
(
	_INOUT_ KAPI_CERTIFICATE		/* pCtx */
);
KAPI_EXPORT(CK_RV, KAPI_Sign)
							/* Signs specified contents.
							 * The notAfter and notBefore fields of signing certificate must satisfy validity
							 * The signed content is attached according to attach argument
							 * The signing certificate is always attached to CMS Signed Data document
							 * If the CA chain of signing certificate is present, all those certificates are attached
							 */
(
	_IN_ KAPI_CERTIFICATE,			/* pSigningCert: signing certificate returned by KAPI_EnumerateCertificates */
	_IN_ CK_MECHANISM_TYPE,			/* ulMechanism: signing mechanism, where: CKM_SHA1_RSA_PKCS || CKM_SHA256_RSA_PKCS || CKM_SHA384_RSA_PKCS || CKM_SHA512_RSA_PKCS */
	_IN_ KAPI_POLICY,				/* iPolicy: RFC 5125 signature policy. Only CAdES_BES is supported */
	_IN_ NH_BLOB*,				/* pContents: contents to be signed */
	_IN_ CK_BBOOL,				/* attach: content attachment indicator */
	_IN_ char*,					/* SZPIN: user PIN */
	_OUT_ NH_BLOB*				/* DER encoded CMS Signed Data */
);
KAPI_EXPORT(CK_RV, KAPI_DecodePEM)
							/* Decodes any base64 encoded input text. Also supports PKCS#10, PKCS#7 and Certificate PEM formats.
							 * If pOut->data is NULL, pOut->length will return required
							 * pOut->data buffer size.
							 */
(
	_IN_ char*,					/* szInput: PEM document as a NULL terminated string */
	_OUT_ NH_BLOB*				/* pOut: output DER encoded */
);
KAPI_EXPORT(CK_RV, KAPI_EncodePEM)
							/* Encodes any binary input. Also supports PKCS#10, PKCS#7 and Certificate PEM formats.
							 * The szOutput lines will be at last 72 characters long.
							 * The szOutput buffer will receive a terminating NULL character.
							 * If szOutput is NULL, pulOutput will receive required buffer size, including NULL
							 */
(
	_IN_ NH_BLOB*,				/* pInput: binary input document Also supports DER encoded PEM documents */
	_IN_ KAPI_PEMFORMAT,			/* fmt: Base64 document format */
	_OUT_ char*,				/* szOutput: output Base64 encoded */
	_INOUT_ size_t*				/* pulOutput: szOutput buffer length */
);
KAPI_EXPORT(CK_RV, KAPI_Verify)
							/* Verifies CMS Signed Data document signature.
							 * Signing certificate must be embbeded.
							 */
(
	_IN_ NH_BLOB*,				/* pCMS: DER encoded CMS Signed Data document */
	_OUT_ CK_BBOOL*				/* pbMatch: ck_TRUE if signature matches, otherwise, CK_FALSE */
);
KAPI_EXPORT(CK_RV, KAPI_VerifySigningCertificate)
							/* Checks if embbeded signing certificate was issued by a trusted CA.
							 * Only Kiripema installed certificates are trusted.
							 */
(
	_IN_ NH_BLOB*,				/* pCMS: DER encoded CMS Signed Data document */
	_OUT_ CK_BBOOL*				/* pbMatch: ck_TRUE if signing certificate is signed by a trusted CA; otherwise, CK_FALSE */
);
KAPI_EXPORT(CK_RV, KAPI_OpenPFX)
(							/* Opens and parses a PKCS #12 document
							 * Currently supported features:
							 * 	The file must be hashed with sha1 HMAC
							 * 	Only pkcs8ShroudedKeyBag and certBag are supported
							 * 	The Only pkcs8ShroudedKeyBag must be encrypted with pbeWithSHA1And3-KeyTripleDES-CBC
							 * Encoding pBuffer is internally copied and may be released
							 */
	_IN_ CK_BYTE_PTR	,			/* pBuffer: DER encoded PKCS #12 document */
	_IN_ CK_ULONG,				/* ulSize: size of pBuffer */
	_IN_ char*,					/* szSecret: PBE secret */
	_OUT_ KAPI_PFX*				/* pCtx: PFX handler */
);
KAPI_EXPORT(CK_RV, KAPI_ImportPFX)
(							/* Imports keys and certificates embbeded in PKCS #12
							 * Rules:
							 * 	Only RSA keys and X.509 certificates are imported
							 * 	PKCS #12 must embbed the certificate associated to private key
							 * 	All other certificates must be a complete CA chain
							 * 	The leaf certificate in the chain must be the issuer of the subject certificate
							 * 	Key and certificates must not exists in Kiripema repository
							 */
	_IN_ KAPI_PFX,				/* pCtx: PFX handler */
	_IN_ char*,					/* szLabel: key and certificate label attributes */
	_IN_ char*					/* szPIN: user PIN */
);
KAPI_EXPORT(void, KAPI_ClosePFX)
(							/* Releases PKCS #12 handler resources */
	_INOUT_ KAPI_PFX				/* pCtx */
);
#if defined(__cplusplus)
}
#endif


#endif /* __KPAPI_H__ */