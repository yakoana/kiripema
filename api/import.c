/**
 * @file import.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief PKCS #12 keys and certificates import API
 * 
 * @copyright Copyleft (c) 2019-2020 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kpapi.h"
#include <stdlib.h>
#include <string.h>

KAPI_INLINE KAPI_IMPLEMENTATION(NH_RV, __extractRSAKey)
(
	_IN_ NH_PFX_PARSER hPFX,
	_IN_ NH_BLOB *key,
	_INOUT_ NH_ASN1_PARSER_HANDLE *hPrivKey,
	_INOUT_ NH_ASN1_PARSER_HANDLE *hRSA
)
{
	NH_RV rv;
	NH_ASN1_PNODE pNode;
	NH_BLOB pRSA = { NULL, 0UL };

	if (NH_SUCCESS(rv = hPFX->parse_privkey(key, hPrivKey)))
	{
		if
		(
			NH_SUCCESS(rv = (pNode = (*hPrivKey)->sail((*hPrivKey)->root, (NH_SAIL_SKIP_SOUTH << 16) | (NH_SAIL_SKIP_EAST << 8) | NH_SAIL_SKIP_SOUTH)) ? NH_OK : NH_PFX_INVALID_ENCODING_ERROR) &&
			NH_SUCCESS(rv = NH_match_oid((unsigned int*) pNode->value, pNode->valuelen, rsaEncryption_oid, NHC_RSA_ENCRYPTION_OID_COUNT) ? NH_OK : NH_PFX_UNSUPPORTED_KEY_ERROR) &&
			NH_SUCCESS(rv = (pNode = (*hPrivKey)->sail((*hPrivKey)->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 2))) ? NH_OK : NH_PFX_UNSUPPORTED_KEY_ERROR)
		)
		{
			pRSA.data = (unsigned char*) pNode->value;
			pRSA.length = pNode->valuelen;
			rv = hPFX->parse_rsa_key(&pRSA, hRSA);
		}
		if (NH_FAIL(rv)) NH_release_parser(*hPrivKey);
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __add_key)
(
	_IN_ NH_CARGO_CONTAINER hContainer,
	_IN_ NH_PFX_PARSER hParser,
	_IN_ NH_BLOB *pPrivKey,
	_INOUT_ KAPI_RSAKEY *ppKeySet
)
{
	CK_RV rv;
	KAPI_RSAKEY pKey, pLastKey = *ppKeySet;
			
	while (pLastKey && pLastKey->pNext) pLastKey = pLastKey->pNext;
	if
	(
		(rv = NH_SUCCESS(hContainer->bite_chunk(hContainer, sizeof(KAPI_RSAKEY_STR), (void*) &pKey)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = NH_SUCCESS(hContainer->bite_chunk(hContainer, pPrivKey->length, (void*) &pKey->buffer.data)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		memcpy(pKey->buffer.data, pPrivKey->data, pPrivKey->length);
		pKey->buffer.length = pPrivKey->length;
		pKey->pPrevious = pLastKey;
		pKey->pNext = NULL;
		if ((rv = NH_SUCCESS(__extractRSAKey(hParser, &pKey->buffer, &pKey->hPrivKey, &pKey->hRSAKey)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
		{
			if (!pLastKey) pLastKey = pKey;
			else pLastKey->pNext = pKey;
			while (pLastKey && pLastKey->pPrevious) pLastKey = pLastKey->pPrevious;
			*ppKeySet = pLastKey;
		}
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __add_cert)(_IN_ NH_CARGO_CONTAINER hContainer, _IN_ NH_BLOB *pContents, _INOUT_ KAPI_X509CERT *ppCertSet)
{
	CK_RV rv;
	KAPI_X509CERT pCert, pLastCert = *ppCertSet;

	while (pLastCert && pLastCert->pNext) pLastCert = pLastCert->pNext;
	if
	(
		(rv = NH_SUCCESS(hContainer->bite_chunk(hContainer, sizeof(KAPI_X509CERT_STR), (void*) &pCert)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = NH_SUCCESS(hContainer->bite_chunk(hContainer, pContents->length, (void*) &pCert->buffer.data)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		memcpy(pCert->buffer.data, pContents->data, pContents->length);
		pCert->buffer.length = pContents->length;
		pCert->pPrevious = pLastCert;
		pCert->pNext = NULL;
		if ((rv = NH_SUCCESS(NH_parse_certificate(pCert->buffer.data, pCert->buffer.length, &pCert->hCert)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
		{
			if (!pLastCert) pLastCert = pCert;
			else pLastCert->pNext = pCert;
			while (pLastCert && pLastCert->pPrevious) pLastCert = pLastCert->pPrevious;
			*ppCertSet = pLastCert;
		}
	}
	return rv;
}
KAPI_EXPORT(CK_RV, KAPI_OpenPFX)(_IN_ CK_BYTE_PTR pBuffer, _IN_ CK_ULONG ulSize, _IN_ char *szSecret, _OUT_ KAPI_PFX *pOut)
{
	CK_RV rv;
	KAPI_PFX pCtx;
	NH_PFX_QUERY_STR query = NH_PFX_INIT_QUERY(PFX_pkcs8ShroudedKeyBag);
	NH_BLOB privkey = { NULL, 0UL };
	KAPI_RSAKEY pKeySet = NULL;
	KAPI_X509CERT pCertSet = NULL;

	if
	(
		(rv = pBuffer && szSecret ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pCtx = (KAPI_PFX) malloc(sizeof(KAPI_PFX_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		memset(pCtx, 0, sizeof(KAPI_PFX_STR));
		if
		(
			(rv = NH_SUCCESS(NH_freight_container(16384, &pCtx->hContainer)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = NH_SUCCESS(pCtx->hContainer->bite_chunk(pCtx->hContainer, ulSize, (void*) &pCtx->buffer.data)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			memcpy(pCtx->buffer.data, pBuffer, ulSize);
			pCtx->buffer.length = ulSize;
			rv = NH_SUCCESS(NHFX_new_pfx_parser(pCtx->buffer.data, pCtx->buffer.length, szSecret, &pCtx->hParser)) ? CKR_OK : CKR_ARGUMENTS_BAD;
		}
		while
		(
			rv == CKR_OK &&
			(rv = NH_SUCCESS(pCtx->hParser->next_bag(pCtx->hParser, &query)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
			query.pResult &&
			(rv = NH_SUCCESS(pCtx->hParser->unpack_key(pCtx->hParser, query.pResult, szSecret, &privkey)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
		)
		{
			rv = __add_key(pCtx->hContainer, pCtx->hParser, &privkey, &pKeySet);
			NH_safe_zeroize(privkey.data, privkey.length);
			free(privkey.data);
			privkey.data = NULL;
		}
		query.bagType = PFX_certBag;
		while
		(
			rv == CKR_OK &&
			(rv = NH_SUCCESS(pCtx->hParser->next_bag(pCtx->hParser, &query)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
			query.pResult &&
			NH_match_oid(query.pResult->bag.certBag->certType.pIdentifier, query.pResult->bag.certBag->certType.uCount, pkcs9_x509_certificate_oid, PKCS9_X509_CERTIFICATE_OID_COUNT)
		)	rv = __add_cert(pCtx->hContainer, &query.pResult->bag.certBag->contents, &pCertSet);
		if (rv == CKR_OK)
		{
			pCtx->pKeySet = pKeySet;
			pCtx->pCertSet = pCertSet;
			*pOut = pCtx;
		}
		else KAPI_ClosePFX(pCtx);
	}
	return rv;
}
KAPI_EXPORT(void, KAPI_ClosePFX)(_INOUT_ KAPI_PFX pCtx)
{
	KAPI_RSAKEY pKeySet;
	KAPI_X509CERT pCertSet;

	if (pCtx)
	{
		pCertSet = pCtx->pCertSet;
		while (pCertSet)
		{
			NH_release_certificate(pCertSet->hCert);
			pCertSet = pCertSet->pNext;
		}
		pKeySet = pCtx->pKeySet;
		while (pKeySet)
		{
			NH_release_parser(pKeySet->hRSAKey);
			NH_release_parser(pKeySet->hPrivKey);
			NH_safe_zeroize(pKeySet->buffer.data, pKeySet->buffer.length);
			pKeySet = pKeySet->pNext;
		}
		NHFX_delete_pfx_parser(pCtx->hParser);
		NH_release_container(pCtx->hContainer);
		free(pCtx);
	}
}

static CK_BBOOL __true = CK_TRUE, __false = CK_FALSE;
static CK_OBJECT_CLASS __privKey = CKO_PRIVATE_KEY, __pubKey = CKO_PUBLIC_KEY, __cert = CKO_CERTIFICATE;
static CK_CERTIFICATE_TYPE __certType = CKC_X_509;
static CK_KEY_TYPE __rsa = CKK_RSA;
KAPI_INLINE KAPI_IMPLEMENTATION(CK_BBOOL, __is_trusted)(NH_CERTIFICATE_HANDLER hCert)
{
	return (__is_CA(hCert) && (strcmp(hCert->issuer->stringprep, hCert->subject->stringprep) == 0));
}
KAPI_FUNCTION(CK_RV, __install)(CK_SESSION_HANDLE hSession, NH_CERTIFICATE_HANDLER hCert, char *szLabel)
{
	CK_ATTRIBUTE attrs[] =
	{
		{ CKA_PUBLIC_KEY_INFO, NULL, 0UL },
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_ID, NULL, 0UL },
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_ISSUER, NULL, 0UL },
		{ CKA_SERIAL_NUMBER, NULL, 0UL },
		{ CKA_VALUE, NULL, 0UL },
		{ CKA_TRUSTED, NULL, sizeof(CK_BBOOL) },
		{ CKA_CERTIFICATE_TYPE, &__certType, sizeof(CK_CERTIFICATE_TYPE) },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) }
	};
	NH_ASN1_PNODE pModulus = RSA_PUBKEY_GET_MODULUS(hCert);
	CK_OBJECT_HANDLE hIgnored;

	attrs[0].pValue = hCert->pubkey->identifier;
	attrs[0].ulValueLen = hCert->pubkey->size + hCert->pubkey->contents - hCert->pubkey->identifier;
	attrs[1].pValue = hCert->subject->node->identifier;
	attrs[1].ulValueLen = hCert->subject->node->size + hCert->subject->node->contents - hCert->subject->node->identifier;
	attrs[2].pValue = pModulus->value;
	attrs[2].ulValueLen = pModulus->valuelen;
	attrs[3].pValue = szLabel;
	attrs[3].ulValueLen = strlen(szLabel);
	attrs[4].pValue = hCert->issuer->node->identifier;
	attrs[4].ulValueLen = hCert->issuer->node->size + hCert->issuer->node->contents - hCert->issuer->node->identifier;
	attrs[5].pValue = hCert->serialNumber->identifier;
	attrs[5].ulValueLen = hCert->serialNumber->size + hCert->serialNumber->contents - hCert->serialNumber->identifier;
	attrs[6].pValue = hCert->hParser->root->identifier;
	attrs[6].ulValueLen = hCert->hParser->root->size + hCert->hParser->root->contents - hCert->hParser->root->identifier;
	attrs[7].pValue = __is_trusted(hCert) ? &__true : &__false;
	return cryptoki->C_CreateObject(hSession, attrs, 12UL, &hIgnored);
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __key_exists)(CK_SESSION_HANDLE hSession, KAPI_RSAKEY pKey, CK_BBOOL *match)
{
	CK_RV rv;
	NH_ASN1_PNODE pModulus;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;

	if ((rv = (pModulus = pKey->hRSAKey->sail(pKey->hRSAKey->root, (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
	{
		search[0].pValue = pModulus->value;
		search[0].ulValueLen = pModulus->valuelen;
		if
		(
			(rv = cryptoki->C_FindObjectsInit(hSession, search, 2UL)) == CKR_OK &&
			(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
			(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK
		)
		{
			switch (ulRet)
			{
			case 0:
				*match = CK_FALSE;
				break;
			case 1:
				*match = CK_TRUE;
				break;
			default:
				rv = CKR_GENERAL_ERROR;
				break;
			}
		}
	}
	return rv;

}
KAPI_IMPLEMENTATION(CK_RV, __import_key)(CK_SESSION_HANDLE hSession, KAPI_RSAKEY pKey, NH_CERTIFICATE_HANDLER hCert, char *szLabel)
{
	CK_RV rv;
	NH_ASN1_PNODE pModulus, pPubExponent, pPrivExponent, pPrime1, pPrime2, pExponent1, pExponent2, pCoefficient;
	CK_ATTRIBUTE privkey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_ID, NULL, 0UL },
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_PUBLIC_EXPONENT, NULL, 0UL },
		{ CKA_PRIVATE_EXPONENT, NULL, 0UL },
		{ CKA_PRIME_1, NULL, 0UL },
		{ CKA_PRIME_2, NULL, 0UL },
		{ CKA_EXPONENT_1, NULL, 0UL },
		{ CKA_EXPONENT_2, NULL, 0UL },
		{ CKA_COEFFICIENT, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) },
		{ CKA_EXTRACTABLE, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) },
		{ CKA_KEY_TYPE, &__rsa, sizeof(CK_KEY_TYPE) }
	},
	pubkey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_ID, NULL, 0UL },
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_PUBLIC_EXPONENT, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__pubKey, sizeof(CK_OBJECT_CLASS) },
		{ CKA_KEY_TYPE, &__rsa, sizeof(CK_KEY_TYPE) }
	};
	CK_OBJECT_HANDLE hIgnored;

	if
	(
		(rv = (pModulus = pKey->hRSAKey->sail(pKey->hRSAKey->root, (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pPubExponent = pModulus->next) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pPrivExponent = pPubExponent->next) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pPrime1 = pPrivExponent->next) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pPrime2 = pPrime1->next) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pExponent1 = pPrime2->next) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pExponent2 = pExponent1->next) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = (pCoefficient = pExponent2->next) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
	)
	{
		privkey[0].pValue = szLabel;
		privkey[0].ulValueLen = strlen(szLabel);
		privkey[1].pValue = pModulus->value;
		privkey[1].ulValueLen = pModulus->valuelen;
		privkey[2].pValue = hCert->subject->node->identifier;
		privkey[2].ulValueLen = hCert->subject->node->size + hCert->subject->node->contents - hCert->subject->node->identifier;
		privkey[3].pValue = pModulus->value;
		privkey[3].ulValueLen = pModulus->valuelen;
		privkey[4].pValue = pPubExponent->value;
		privkey[4].ulValueLen = pPubExponent->valuelen;
		privkey[5].pValue = pPrivExponent->value;
		privkey[5].ulValueLen = pPrivExponent->valuelen;
		privkey[6].pValue = pPrime1->value;
		privkey[6].ulValueLen = pPrime1->valuelen;
		privkey[7].pValue = pPrime2->value;
		privkey[7].ulValueLen = pPrime2->valuelen;
		privkey[8].pValue = pExponent1->value;
		privkey[8].ulValueLen = pExponent1->valuelen;
		privkey[9].pValue = pExponent2->value;
		privkey[9].ulValueLen = pExponent2->valuelen;
		privkey[10].pValue = pCoefficient->value;
		privkey[10].ulValueLen = pCoefficient->valuelen;
		if ((rv = cryptoki->C_CreateObject(hSession, privkey, 16UL, &hIgnored)) == CKR_OK)
		{
			pubkey[0].pValue = szLabel;
			pubkey[0].ulValueLen = strlen(szLabel);
			pubkey[1].pValue = pModulus->value;
			pubkey[1].ulValueLen = pModulus->valuelen;
			pubkey[2].pValue = hCert->subject->node->identifier;
			pubkey[2].ulValueLen = hCert->subject->node->size + hCert->subject->node->contents - hCert->subject->node->identifier;
			pubkey[3].pValue = pModulus->value;
			pubkey[3].ulValueLen = pModulus->valuelen;
			pubkey[4].pValue = pPubExponent->value;
			pubkey[4].ulValueLen = pPubExponent->valuelen;
			rv = cryptoki->C_CreateObject(hSession, pubkey, 9UL, &hIgnored);
		}
	}
	return rv;	
}
KAPI_INLINE KAPI_IMPLEMENTATION(KAPI_X509CERT, __find_related_cert)(KAPI_PFX pCtx, KAPI_RSAKEY pKey)
{
	NH_ASN1_PNODE pSource = pKey->hRSAKey->sail(pKey->hRSAKey->root, (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST), pTarget;
	KAPI_X509CERT pSet = pCtx->pCertSet, pRelated = NULL;

	while (pSource && pSet && !pRelated)
	{
		if ((pTarget = RSA_PUBKEY_GET_MODULUS(pSet->hCert))) pRelated = pSet;
		pSet = pSet->pNext;
	}
	return pRelated;
}
KAPI_INLINE KAPI_IMPLEMENTATION(KAPI_X509CERT, __find_issuer)(KAPI_PFX pCtx, KAPI_X509CERT pCert)
{
	KAPI_X509CERT pSet = pCtx->pCertSet, pIssuer = NULL;

	while (pSet && !pIssuer)
	{
		if (strcmp(pSet->hCert->subject->stringprep, pCert->hCert->issuer->stringprep) == 0) pIssuer = pSet;
		pSet = pSet->pNext;
	}
	return pIssuer;
}
typedef enum __CHAIN_STATUS { NO_CHAIN, INCOMPLETE_CHAIN, COMPLETE_CHAIN } __CHAIN_STATUS;
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __get_chain)(KAPI_PFX pCtx, KAPI_X509CERT pUserCert, KAPI_X509CERT *pChain, int iLen)
{
	CK_RV rv = CKR_OK;
	int i = 0;
	KAPI_X509CERT pIssuer = __find_issuer(pCtx, pUserCert), pCert = pUserCert;
	__CHAIN_STATUS status;

	memset(pChain, 0, sizeof(KAPI_X509CERT) * iLen);
	pChain[i++] = pUserCert;
	status = pIssuer ? INCOMPLETE_CHAIN : NO_CHAIN;
	while
	(
		status == INCOMPLETE_CHAIN &&
		i < iLen &&
		pIssuer &&
		(rv = NH_SUCCESS(pCert->hCert->verify(pCert->hCert, pIssuer->hCert->pubkey)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK
	)
	{
		pChain[i++] = pIssuer;
		if (strcmp(pIssuer->hCert->subject->stringprep, pIssuer->hCert->issuer->stringprep) == 0) status = COMPLETE_CHAIN;
		pCert = pIssuer;
		pIssuer = __find_issuer(pCtx, pCert);
	}
	if (rv == CKR_OK && status == INCOMPLETE_CHAIN) rv = CKR_ARGUMENTS_BAD;
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __import_all)(CK_SESSION_HANDLE hSession, KAPI_PFX pCtx, char *szLabel)
{
	int iChain = 0, i;
	KAPI_X509CERT pSet = pCtx->pCertSet, *pChain, pCert;
	CK_RV rv;
	KAPI_RSAKEY pKey = pCtx->pKeySet;
	CK_BBOOL exists;

	while (pSet) { iChain++; pSet = pSet->pNext; };
	if ((rv = (pChain = (KAPI_X509CERT*) malloc(sizeof(KAPI_X509CERT) * iChain)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		while (rv == CKR_OK && pKey && (rv = (pCert = __find_related_cert(pCtx, pKey)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
		{
			if ((rv = __get_chain(pCtx, pCert, pChain, iChain)) == CKR_OK)
			{
				if ((rv = __key_exists(hSession, pKey, &exists)) == CKR_OK && !exists)
					rv = __import_key(hSession, pKey, pCert->hCert, szLabel);
				i = 0;
				while (rv == CKR_OK && i < iChain && pChain[i])
				{
					if ((rv = __certificate_exists(hSession, pChain[i]->hCert, &exists)) == CKR_OK && !exists)
						rv = __install(hSession, pChain[i]->hCert, szLabel);
					i++;
				}
			}
			pKey = pKey->pNext;
		}
		free(pChain);
	}
	return rv;
}
KAPI_EXPORT(CK_RV, KAPI_ImportPFX)(_IN_ KAPI_PFX pCtx, _IN_ char *szLabel, _IN_ char *szPIN)
{
	CK_RV rv;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;

	if
	(
		(rv = pCtx && szLabel && szPIN ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK &&
		(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
		(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
			(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession)) == CKR_OK
		)
		{
			if ((rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) szPIN, strlen(szPIN))) == CKR_OK)
			{
				rv = __import_all(hSession, pCtx, (char*) szLabel);
				cryptoki->C_Logout(hSession);
			}
			cryptoki->C_CloseSession(hSession);
		}
		free(pSlotList);
	}
	return rv;
}