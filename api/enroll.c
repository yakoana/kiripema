/**
 * @file enroll.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Enrollment API
 * 
 * @copyright Copyleft (c) 2019-2020 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kpapi.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>

static CK_BBOOL __true = CK_TRUE, __false = CK_FALSE;
static CK_OBJECT_CLASS __privKey = CKO_PRIVATE_KEY, __pubKey = CKO_PUBLIC_KEY, __cert = CKO_CERTIFICATE;

KAPI_IMPLEMENTATION(CK_RV, __key_container)(CK_SESSION_HANDLE hSession, char **szContainer)
{
	CK_RV rv = CKR_OK;
	CK_BBOOL match = CK_FALSE;
	int id = 0;
	char szLabel[KAPI_CONTAINER_SIZE], *szRet;
	CK_ATTRIBUTE privKey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__true, sizeof(CK_BBOOL) },
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL };
	CK_ULONG ulRet;

	while (rv == CKR_OK && !match)
	{
		ulRet = 0;
		memset(szLabel, 0, sizeof(szLabel));
		sprintf(szLabel, KAPI_KEY_CONTAINER, ++id);
		privKey[0].pValue = szLabel;
		privKey[0].ulValueLen = strlen(szLabel);
		if
		(
			(rv = cryptoki->C_FindObjectsInit(hSession, privKey, 4UL)) == CKR_OK &&
			(rv = cryptoki->C_FindObjects(hSession, pRet, 1UL, &ulRet)) == CKR_OK &&
			(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK
		)	match = ulRet == 0;
	}
	if (rv == CKR_OK && (rv = (szRet = (char*) malloc(strlen(szLabel) + 1)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		strcpy(szRet, szLabel);
		*szContainer = szRet;
	}
	return rv;
}
KAPI_IMPLEMENTATION(CK_RV, __generate_keys)
(
	CK_SESSION_HANDLE hSession,
	int iKeySize,
	NH_BLOB *pPubExponent,
	char *szLabel,
	CK_OBJECT_HANDLE_PTR phKey
)
{
	CK_RV rv;
	CK_ATTRIBUTE pubKey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_MODULUS_BITS, 0, sizeof(int) },
		{ CKA_PUBLIC_EXPONENT, NULL, 0UL },
		{ CKA_VERIFY, &__true, sizeof(CK_BBOOL) }
	},
	privKey[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_TOKEN, &__true, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, &__true, sizeof(CK_BBOOL) },
		{ CKA_SENSITIVE, &__true, sizeof(CK_BBOOL) },
		{ CKA_SIGN, &__true, sizeof(CK_BBOOL) }
	},
	modulus[] = {{ CKA_MODULUS, NULL, 0UL }},
	id[] = {{ CKA_ID, NULL, 0UL }};
	CK_MECHANISM mechanism = { CKM_RSA_PKCS_KEY_PAIR_GEN, NULL, 0 };
	CK_OBJECT_HANDLE hPubKey, hPrivKey;

	pubKey[0].pValue = szLabel;
	pubKey[0].ulValueLen = strlen(szLabel);
	pubKey[1].pValue = (CK_VOID_PTR) &iKeySize;
	pubKey[2].pValue = pPubExponent->data;
	pubKey[2].ulValueLen = pPubExponent->length;
	privKey[0].pValue = (CK_VOID_PTR) szLabel;
	privKey[0].ulValueLen = strlen(szLabel);
	if
	(
		(rv = cryptoki->C_GenerateKeyPair(hSession, &mechanism, pubKey, 4UL, privKey, 5UL, &hPubKey, &hPrivKey)) == CKR_OK &&
		(rv = cryptoki->C_GetAttributeValue(hSession, hPubKey, modulus, 1UL)) == CKR_OK &&
		(rv = (modulus[0].pValue = malloc(modulus[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = cryptoki->C_GetAttributeValue(hSession, hPubKey, modulus, 1UL)) == CKR_OK)
		{
			id[0].pValue = modulus[0].pValue;
			id[0].ulValueLen = modulus[0].ulValueLen;
			if
			(
				(rv = cryptoki->C_SetAttributeValue(hSession, hPubKey, id, 1UL)) == CKR_OK &&
				(rv = cryptoki->C_SetAttributeValue(hSession, hPrivKey, id, 1UL)) == CKR_OK
			)	*phKey = hPrivKey;
		}
		free(modulus[0].pValue);
	}
	return rv;
}
KAPI_IMPLEMENTATION(CK_RV, __get_pubkey)(CK_SESSION_HANDLE hSession, char *szLabel, NH_RSA_PUBKEY_HANDLER *hHandler)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_LABEL, NULL, 0UL },
		{ CKA_CLASS, &__pubKey, sizeof(CK_OBJECT_CLASS) }
	},
	attrs[] =
	{
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_PUBLIC_EXPONENT, NULL, 0UL }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;
	NH_BIG_INTEGER n = { NULL, 0 }, e = { NULL, 0 };
	NH_RSA_PUBKEY_HANDLER hPubKey;

	search[0].pValue = szLabel;
	search[0].ulValueLen = strlen(szLabel);
	if
	(
		(rv = cryptoki->C_FindObjectsInit(hSession, search, 2UL)) == CKR_OK &&
		(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
		(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK &&
		(rv = ulRet == 1 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
		(rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], attrs, 2UL)) == CKR_OK &&
		(rv = (attrs[0].pValue = malloc(attrs[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = (attrs[1].pValue = malloc(attrs[1].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			if ((rv = cryptoki->C_GetAttributeValue(hSession, pRet[0], attrs, 2UL)) == CKR_OK)
			{
				n.data = (unsigned char*) attrs[0].pValue;
				n.length = attrs[0].ulValueLen;
				e.data = (unsigned char*) attrs[1].pValue;
				e.length = attrs[1].ulValueLen;
				if ((rv = NH_new_RSA_pubkey_handler(&hPubKey)) == CKR_OK)
				{
					if ((rv = hPubKey->create(hPubKey, &n, &e)) == CKR_OK)*hHandler = hPubKey;
					else NH_release_RSA_pubkey_handler(hPubKey);
				}
			}
			free(attrs[1].pValue);
		}
		free(attrs[0].pValue);
	}
	return rv;
}
KAPI_IMPLEMENTATION(CK_RV, __sign_request)
(
	CK_SESSION_HANDLE hSession,
	NH_NAME *ppName,
	CK_ULONG ulNameCount,
	NH_RSA_PUBKEY_HANDLER hPubKey,
	CK_OBJECT_HANDLE hKey,
	CK_MECHANISM_TYPE ulMechanism,
	NH_BLOB *pRequest
)
{
	CK_RV rv;
	NH_CREQUEST_ENCODER hEncoder;
	CK_MECHANISM mech = { CKM_RSA_PKCS, NULL, 0UL };
	__SIGN_PARAMS_STR params = { 0UL, 0UL };

	if ((rv = NH_SUCCESS(NH_new_certificate_request(&hEncoder)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		if
		(
			(rv = NH_SUCCESS(hEncoder->put_version(hEncoder, 0)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
			(rv = NH_SUCCESS(hEncoder->put_subject(hEncoder, ppName, ulNameCount)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
			(rv = NH_SUCCESS(hEncoder->put_pubkey(hEncoder, hPubKey)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
			(rv = cryptoki->C_SignInit(hSession, &mech, hKey)) == CKR_OK
		)
		{
			params.hSession = hSession;
			params.ulMechanism = ulMechanism;
			if
			(
				(rv = NH_SUCCESS(hEncoder->sign(hEncoder, ulMechanism, __signature_callback, &params)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
				(rv = NH_SUCCESS(hEncoder->encode(hEncoder, NULL, &pRequest->length)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK &&
				(rv = (pRequest->data = (unsigned char*) malloc(pRequest->length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				if ((rv = NH_SUCCESS(hEncoder->encode(hEncoder, pRequest->data, &pRequest->length)) ? CKR_OK : CKR_GENERAL_ERROR) != CKR_OK)
					free(pRequest->data);
			}
		}
		NH_delete_certificate_request(hEncoder);
	}
	return rv;
}
KAPI_EXPORT(CK_RV, KAPI_GenerateCSR)
(
	_IN_ NH_NAME *ppName,
	_IN_ CK_ULONG ulNameCount,
	_IN_ int iKeySize,
	_IN_ NH_BLOB *pPubExponent,
	_IN_ CK_MECHANISM_TYPE ulMechanism,
	_IN_ char *szPIN,
	_OUT_ NH_BLOB *pRequest
)
{
	CK_RV rv;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;
	char *szContainer = NULL;
	CK_OBJECT_HANDLE hKey;
	NH_RSA_PUBKEY_HANDLER hPubKey;

	KAPI_ASSERT();
	if (!ppName || !pPubExponent || !pPubExponent->data || !szPIN || !pRequest) return CKR_ARGUMENTS_BAD;
	if
	(
		(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
		(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
			(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession)) == CKR_OK
		)
		{
			if ((rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) szPIN, strlen(szPIN))) == CKR_OK)
			{
				if ((rv = __key_container(hSession, &szContainer)) == CKR_OK)
				{
					if
					(
						(rv = __generate_keys(hSession, iKeySize, (NH_BLOB*) pPubExponent, szContainer, &hKey)) == CKR_OK &&
						(rv = __get_pubkey(hSession, szContainer, &hPubKey)) == CKR_OK
					)
					{
						rv = __sign_request( hSession, (NH_NAME*) ppName, ulNameCount, hPubKey, hKey, ulMechanism, pRequest);
						NH_release_RSA_pubkey_handler(hPubKey);
					}
					free(szContainer);
				}
				cryptoki->C_Logout(hSession);
			}
			cryptoki->C_CloseSession(hSession);
		}
		free(pSlotList);
	}
	return rv;
}

KAPI_FUNCTION(CK_BBOOL, __is_CA)(NH_CERTIFICATE_HANDLER hCert)
{
	NH_ASN1_PNODE eNode;
	return
	(
		NH_SUCCESS(hCert->basic_constraints(hCert, &eNode)) &&
		(eNode && (eNode = eNode->child) && ASN_IS_PRESENT(eNode) && *(unsigned char*) eNode->value)
	);
}
KAPI_INLINE KAPI_IMPLEMENTATION(int, __find_user_certificate)(NH_CERTIFICATE_HANDLER chain[], int iCount)
{
	int i;
	for (i = 0; i < iCount; i++) if (!__is_CA(chain[i])) return i;
	return -1;
}
KAPI_INLINE KAPI_IMPLEMENTATION(int, __find_signer_certificate)(NH_CERTIFICATE_HANDLER chain[], int iCount, int idx)
{
	int i;
	for (i = 0; i < iCount; i++) if (strcmp(chain[idx]->issuer->stringprep, chain[i]->subject->stringprep) == 0) return i;
	return -1;
}
KAPI_IMPLEMENTATION(CK_RV, __get_certificates)(NH_CMS_SD_PARSER hCMS, NH_CERTIFICATE_HANDLER **chain, int *piCount)
{
	CK_RV rv;
	NH_ASN1_PNODE pNode;
	NH_CERTIFICATE_HANDLER *hChain = NULL, hCert;
	int iCount = 0, i = 0;

	if (!hCMS->certificates || !(pNode = hCMS->certificates->child)) return CKR_ARGUMENTS_BAD;
	while (pNode) { iCount++; pNode = pNode->next; }
	rv = (hChain = (NH_CERTIFICATE_HANDLER*) malloc(iCount * sizeof(NH_CERTIFICATE_HANDLER))) ? CKR_OK : CKR_HOST_MEMORY;
	pNode = hCMS->certificates->child;
	while (rv == CKR_OK && pNode)
	{
		rv = NH_SUCCESS(NH_parse_certificate(pNode->identifier, pNode->size + pNode->contents - pNode->identifier, &hCert)) ? CKR_OK : CKR_ARGUMENTS_BAD;
		if (NH_SUCCESS(rv)) hChain[i++] = hCert;
		pNode = pNode->next;
	}
	if (rv == CKR_OK)
	{
		*chain = hChain;
		*piCount = iCount;
	}
	else if (hChain) for (i = 0; i < iCount; i++) if (hChain[i]) NH_release_certificate(hChain[i]);
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_BBOOL, __check_chain)(NH_CERTIFICATE_HANDLER chain[], int iCount)
{
	CK_BBOOL checked;
	NH_RV rv = NH_OK;
	int i = 0, iLeaf, iCA;

	checked = iCount > 0 && (iLeaf = __find_user_certificate(chain, iCount)) > -1 ? CK_TRUE : CK_FALSE;
	while (rv == NH_OK && i < iCount && checked)
	{
		iCA = __find_signer_certificate(chain, iCount, iLeaf);
		if (iCA > -1) rv = chain[iLeaf]->verify(chain[iLeaf], chain[iCA]->pubkey);
		else checked = CK_FALSE;
		iLeaf = iCA;
		i++;
	}
	if (NH_FAIL(rv)) checked = CK_FALSE;
	return checked;
}
KAPI_FUNCTION(CK_RV, __find_certiticate_by_pubkey)(CK_SESSION_HANDLE hSession, NH_CERTIFICATE_HANDLER hCert, CK_OBJECT_HANDLE_PTR phObject)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] =
	{
		{ CKA_PUBLIC_KEY_INFO, NULL, 0UL },
		{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) }
	};
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;

	search[0].pValue = hCert->pubkey->identifier;
	search[0].ulValueLen = hCert->pubkey->size + hCert->pubkey->contents - hCert->pubkey->identifier;
	if
	(
		(rv = cryptoki->C_FindObjectsInit(hSession, search, 2UL)) == CKR_OK &&
		(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
		(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK
	)
	{
		switch (ulRet)
		{
		case 0:
			*phObject = CK_UNAVAILABLE_INFORMATION;
			break;
		case 1:
			*phObject = pRet[0];
			break;
		default:
			rv = CKR_GENERAL_ERROR;
			break;
		}
	}
	return rv;
}
KAPI_FUNCTION(CK_RV, __find_certiticate_by_value)(CK_SESSION_HANDLE hSession, NH_CERTIFICATE_HANDLER hCert, CK_OBJECT_HANDLE_PTR phObject)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] = {{ CKA_CLASS, &__cert, sizeof(CK_OBJECT_CLASS) }}, cert[] = {{ CKA_VALUE, NULL, 0UL }};
	CK_OBJECT_HANDLE pRet[16];
	CK_ULONG ulRet = 16, ulMaxCount = 16, i;
	NH_CERTIFICATE_HANDLER hFound;
	int hCertSize;

	*phObject = CK_UNAVAILABLE_INFORMATION;
	hCertSize = hCert->pubkey->size + hCert->pubkey->contents - hCert->pubkey->identifier;
	memset(pRet, 0, 16 * sizeof(CK_OBJECT_HANDLE));
	rv = cryptoki->C_FindObjectsInit(hSession, search, 1UL);
	while (rv == CKR_OK && ulRet == ulMaxCount)
	{
		rv = cryptoki->C_FindObjects(hSession, pRet, ulMaxCount, &ulRet);
		i = 0;
		while (rv == CKR_OK && i < ulRet && *phObject == CK_UNAVAILABLE_INFORMATION)
		{
			if
			(
				(rv = cryptoki->C_GetAttributeValue(hSession, pRet[i], cert, 1UL)) == CKR_OK &&
				(rv = (cert[0].pValue = malloc(cert[0].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				if
				(
					(rv = cryptoki->C_GetAttributeValue(hSession, pRet[i], cert, 1UL)) == CKR_OK &&
					(rv = NH_SUCCESS(NH_parse_certificate((unsigned char*) cert[0].pValue, cert[0].ulValueLen, &hFound)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
				)
				{
					if
					(
						hCertSize == hFound->pubkey->size + hFound->pubkey->contents - hFound->pubkey->identifier &&
						memcmp(hCert->pubkey->identifier, hFound->pubkey->identifier, hCertSize) == 0
					)	*phObject = pRet[i];
					NH_release_certificate(hFound);
				}
				free(cert[0].pValue);
				cert[0].pValue = NULL;
				cert[0].ulValueLen = 0UL;
			}
			i++;
		}
	}
	cryptoki->C_FindObjectsFinal(hSession);
	return rv;
}
KAPI_FUNCTION(CK_RV, __certificate_exists)(CK_SESSION_HANDLE hSession, NH_CERTIFICATE_HANDLER hCert, CK_BBOOL *match)
{
	CK_OBJECT_HANDLE hObject;
	CK_RV rv = __find_certiticate_by_pubkey(hSession, hCert, &hObject);
	if (rv == CKR_OK && hObject == CK_UNAVAILABLE_INFORMATION) rv = __find_certiticate_by_value(hSession, hCert, &hObject);
	if (rv == CKR_OK) *match = hObject != CK_UNAVAILABLE_INFORMATION;
	return rv;
}
KAPI_IMPLEMENTATION(CK_RV, __find_related_key)
(
	CK_SESSION_HANDLE hSession,
	NH_CERTIFICATE_HANDLER hCert,
	CK_OBJECT_CLASS ulClass,
	CK_OBJECT_HANDLE_PTR phObject
)
{
	CK_RV rv;
	CK_ATTRIBUTE search[] = 
	{
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_CLASS, NULL, sizeof(CK_OBJECT_CLASS) }
	};
	NH_ASN1_PNODE pModulus = RSA_PUBKEY_GET_MODULUS(hCert);
	CK_OBJECT_HANDLE pRet[] = { 0UL, 0UL };
	CK_ULONG ulRet;

	if ((rv = pModulus ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		search[0].pValue = pModulus->value;
		search[0].ulValueLen = pModulus->valuelen;
		search[1].pValue = &ulClass;
		if
		(
			(rv = cryptoki->C_FindObjectsInit(hSession, search, 2UL)) == CKR_OK &&
			(rv = cryptoki->C_FindObjects(hSession, pRet, 2UL, &ulRet)) == CKR_OK &&
			(rv = cryptoki->C_FindObjectsFinal(hSession)) == CKR_OK
		)
		{
			switch (ulRet)
			{
			case 0:
				*phObject = CK_UNAVAILABLE_INFORMATION;
				break;
			case 1:
				*phObject = pRet[0];
				break;
			default: rv = CKR_GENERAL_ERROR;
			}
		}
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __get_label)
(
	CK_SESSION_HANDLE hSession,
	CK_OBJECT_HANDLE hObject,
	char **szLabel
)
{
	CK_RV rv;
	CK_ATTRIBUTE attrs[] = {{ CKA_LABEL, NULL, 0UL }};

	if
	(
		(rv = cryptoki->C_GetAttributeValue(hSession, hObject, attrs, 1UL)) == CKR_OK &&
		(rv = (attrs[0].pValue = malloc(attrs[0].ulValueLen + 1)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		memset(attrs[0].pValue, 0, attrs[0].ulValueLen + 1);
		if ((rv = cryptoki->C_GetAttributeValue(hSession, hObject, attrs, 1UL)) == CKR_OK) *szLabel = attrs[0].pValue;
		else free(attrs[0].pValue);
	}
	return rv;
}
KAPI_INLINE KAPI_IMPLEMENTATION(CK_RV, __update_keys)
(
	CK_SESSION_HANDLE hSession,
	NH_CERTIFICATE_HANDLER hCert,
	CK_OBJECT_HANDLE hPubKey,
	CK_OBJECT_HANDLE hPrivKey
)
{
	CK_RV rv;
	CK_ATTRIBUTE pubKeyAttrs[] =
	{
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) }
	},
	privKeyAttrs[] =
	{
		{ CKA_SUBJECT, NULL, 0UL },
		{ CKA_MODIFIABLE, &__false, sizeof(CK_BBOOL) }
	};

	pubKeyAttrs[0].pValue = hCert->subject->node->identifier;
	pubKeyAttrs[0].ulValueLen = hCert->subject->node->size + hCert->subject->node->contents - hCert->subject->node->identifier;
	privKeyAttrs[0].pValue = hCert->subject->node->identifier;
	privKeyAttrs[0].ulValueLen = hCert->subject->node->size + hCert->subject->node->contents - hCert->subject->node->identifier;
	if ((rv = cryptoki->C_SetAttributeValue(hSession, hPubKey, pubKeyAttrs, 2UL)) == CKR_OK)
		rv = cryptoki->C_SetAttributeValue(hSession, hPrivKey, privKeyAttrs, 2UL);
	return rv;
}
KAPI_EXPORT(CK_RV, KAPI_InstallCertificates)(_IN_ NH_BLOB *pCMS, _IN_ char *szPIN)
{
	CK_RV rv;
	NH_CMS_SD_PARSER hCMS;
	NH_CERTIFICATE_HANDLER *hChain;
	int iCount, iUser, i;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SESSION_HANDLE hSession;
	CK_OBJECT_HANDLE hPubKey, hPrivKey;
	char *szLabel;
	CK_BBOOL match = CK_FALSE;

	KAPI_ASSERT();
	if ((rv = pCMS && pCMS->data && NH_SUCCESS(NH_cms_parse_signed_data(pCMS->data, pCMS->length, &hCMS)) ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK)
	{
		if ((rv = __get_certificates(hCMS, &hChain, &iCount)) == CKR_OK)
		{
			rv = (iUser = __find_user_certificate(hChain, iCount)) > -1 ? CKR_OK : CKR_ARGUMENTS_BAD;
			rv = __check_chain(hChain, iCount) || (iCount == 1 && iUser == 0) ? CKR_OK : CKR_ARGUMENTS_BAD;
			if
			(
				(rv = cryptoki->C_GetSlotList(CK_TRUE, NULL, &ulCount)) == CKR_OK &&
				(rv = (pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				if
				(
					(rv = cryptoki->C_GetSlotList(CK_TRUE, pSlotList, &ulCount)) == CKR_OK &&
					(rv = cryptoki->C_OpenSession(pSlotList[0],  CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession)) == CKR_OK
				)
				{
					if ((rv = cryptoki->C_Login(hSession, CKU_USER, (CK_UTF8CHAR_PTR) szPIN, strlen(szPIN))) == CKR_OK)
					{
						rv = __certificate_exists(hSession, hChain[iUser], &match);
						if (rv == CKR_OK && !match)
						{
							if
							(
								(rv = __find_related_key(hSession, hChain[iUser], CKO_PUBLIC_KEY, &hPubKey)) == CKR_OK &&
								(rv = __find_related_key(hSession, hChain[iUser], CKO_PRIVATE_KEY, &hPrivKey)) == CKR_OK &&
								(rv = __get_label(hSession, hPrivKey, &szLabel)) == CKR_OK
							)
							{
								if ((rv = __install(hSession, hChain[iUser], szLabel)) == CKR_OK)
									rv = __update_keys(hSession, hChain[iUser], hPubKey, hPrivKey);
								free(szLabel);
							}
						}
						i = 0;
						while (rv == CKR_OK && i < iCount)
						{
							if (i != iUser)
							{
								match = CK_FALSE;
								rv = __certificate_exists(hSession, hChain[i], &match);
								if (rv == CKR_OK && !match) rv = __install(hSession, hChain[i], hChain[i]->subject->stringprep);
							}
							i++;
						}
						cryptoki->C_Logout(hSession);
					}
					cryptoki->C_CloseSession(hSession);
				}
				free(pSlotList);
			}
			for (i = 0; i < iCount; i++) NH_release_certificate(hChain[i]);
			free(hChain);
		}
		NH_cms_release_sd_parser(hCMS);
	}
	return rv;
}