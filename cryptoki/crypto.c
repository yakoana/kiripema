#include "kiripema.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#ifdef _WINDOWS
#include "winob.h"
#include <ncrypt.h>
#else
#include "uxob.h"
#include "openssl/rsa.h"
#include <openssl/bn.h>
#endif

/* TODO: PBE encryption initialization vector must be ciphered by secret - see PKCS#12 */
CK_MECHANISM_TYPE __pSignMechanisms[] = { CKM_RSA_PKCS };

CK_IMPLEMENTATION(CK_RV, __list)(CK_IN KCRYPTO_HANDLER_STR *self, CK_OUT CK_MECHANISM_TYPE_PTR pMechanismList, CK_INOUT CK_ULONG_PTR pulCount)
{
	CK_RV rv = CKR_OK;
	khiter_t k;
	int i;

	if (!pMechanismList) *pulCount = kh_size(self->kMechanisms);
	else
	{
		if (*pulCount >= kh_size(self->kMechanisms))
		{
			for (k = kh_begin(self->kMechanisms), i = 0; k != kh_end(self->kMechanisms); ++k)
				if (kh_exist(self->kMechanisms, k))
					pMechanismList[i++] = kh_value(self->kMechanisms, k)->ulType;
			*pulCount = kh_size(self->kMechanisms);
		}
		else rv = CKR_BUFFER_TOO_SMALL;
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __info)(CK_IN KCRYPTO_HANDLER_STR *self, CK_IN CK_MECHANISM_TYPE ulMechanism, CK_INOUT CK_MECHANISM_INFO_PTR pInfo)
{
	CK_RV rv = CKR_OK;
	khiter_t k;

	assert(pInfo);
	if ((k = kh_get(KMMAP, self->kMechanisms, ulMechanism)) != kh_end(self->kMechanisms)) memcpy(pInfo, &kh_value(self->kMechanisms, k)->info, sizeof(CK_MECHANISM_INFO));
	else rv = CKR_MECHANISM_INVALID;
	return rv;
}
CK_IMPLEMENTATION(CK_BBOOL, __is_allowed)(CK_IN KCRYPTO_HANDLER_STR *self, CK_IN CK_MECHANISM_TYPE ulMechanism)
{
	return (kh_get(KMMAP, self->kMechanisms, ulMechanism) != kh_end(self->kMechanisms));
}
CK_IMPLEMENTATION(CK_RV, __sign_init)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_INOUT KSESSION_STR *pSession,
	CK_INOUT KOBJECT_STR *pObject,
	CK_IN CK_MECHANISM_TYPE ulMechanism
)
{
	CK_RV rv = CKR_OK;
#ifdef _UNIX
	KUX_OBJECT pUX = UX_LOADER(pObject);
	KRSAPRIVKEY_PARSER hPrivKey = (KRSAPRIVKEY_PARSER) pUX->hParser;
	NH_BIG_INTEGER n = { NULL, 0UL }, e = { NULL, 0UL }, d = { NULL, 0UL }, p = { NULL, 0UL },
	q = { NULL, 0UL }, dmp = { NULL, 0UL }, dmq = { NULL, 0UL }, qmp = { NULL, 0UL };
	NH_RV _ret;
	NH_RSA_PRIVKEY_HANDLER pKey;
#endif
	if (!self->is_allowed(self, ulMechanism)) return CKR_MECHANISM_INVALID;
#ifdef _UNIX

	assert(pObject->hClass == CKO_PRIVATE_KEY && pUX && hPrivKey);
	if (!pUX->key)
	{
		if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
		{

			while (!self->isInited) pthread_cond_wait(self->pCondition, self->pMutex);
			if
			(
				(rv = pUX->hRSA->get_modulus(pUX->hRSA, &n)) == CKR_OK &&
				(rv = pUX->hRSA->get_publicExponent(pUX->hRSA, &e)) == CKR_OK &&
				(rv = pUX->hRSA->get_privateExponent(pUX->hRSA, &d)) == CKR_OK
			)
			{
				rv = pUX->hRSA->get_prime1(pUX->hRSA, &p);
				if (rv == CKR_KP_ATTRIBUTE_UNAVAILABLE) rv = CKR_OK;
				if (rv == CKR_OK)
				{
					rv = pUX->hRSA->get_prime2(pUX->hRSA, &q);
					if (rv == CKR_KP_ATTRIBUTE_UNAVAILABLE) rv = CKR_OK;
				}
				if (rv == CKR_OK)
				{
					rv = pUX->hRSA->get_exponent1(pUX->hRSA, &dmp);
					if (rv == CKR_KP_ATTRIBUTE_UNAVAILABLE) rv = CKR_OK;
				}
				if (rv == CKR_OK)
				{
					rv = pUX->hRSA->get_exponent2(pUX->hRSA, &dmq);
					if (rv == CKR_KP_ATTRIBUTE_UNAVAILABLE) rv = CKR_OK;
				}
				if (rv == CKR_OK)
				{
					rv = pUX->hRSA->get_coefficient(pUX->hRSA, &qmp);
					if (rv == CKR_KP_ATTRIBUTE_UNAVAILABLE) rv = CKR_OK;
				}
			}
			if (rv == CKR_OK)
			{
				rv = (_ret = NH_new_RSA_privkey_handler(&pKey)) == NH_OK ? CKR_OK : CKR_KEY_HANDLE_INVALID;
				NHARU_ASSERT(_ret, "NH_new_RSA_privkey_handler");
				if (rv == CKR_OK)
				{
					_ret = pKey->create(pKey, &n, &e, &d, p.data ? &p : NULL, q.data ? &q : NULL, dmp.data ? &dmp : NULL, dmq.data ? &dmq : NULL, qmp.data ? &qmp : NULL);
					NHARU_ASSERT(_ret, "NH_RSA_PRIVKEY_HANDLER->create");
					if ((rv = _ret == NH_OK ? CKR_OK : CKR_KEY_HANDLE_INVALID) == CKR_OK)
					{
						if ((rv = pObject->hMutex->LockMutex(pObject->pMutex)) == CKR_OK)
						{
							pUX->key = pKey;
							rv = pObject->hMutex->UnlockMutex(pObject->pMutex);
						}
					}
					else NH_release_RSA_privkey_handler(pKey);
				}
			}
			if (n.data) free(n.data);
			if (e.data) free(e.data);
			if (d.data)
			{
				OPENSSL_cleanse(d.data, d.length);
				free(d.data);
			}
			if (p.data)
			{
				OPENSSL_cleanse(p.data, p.length);
				free(p.data);
			}
			if (q.data)
			{
				OPENSSL_cleanse(q.data, q.length);
				free(q.data);
			}
			if (dmp.data)
			{
				OPENSSL_cleanse(dmp.data, dmp.length);
				free(dmp.data);
			}
			if (dmq.data)
			{
				OPENSSL_cleanse(dmq.data, dmq.length);
				free(dmq.data);
			}
			if (qmp.data)
			{
				OPENSSL_cleanse(qmp.data, qmp.length);
				free(qmp.data);
			}
			self->hMutex->UnlockMutex(self->pMutex);
		}
	}
#endif
	if (rv == CKR_OK) rv = pSession->init_crypto(pSession, ulMechanism, pObject);
	return rv;
}
#define SSL3_MD5_HASH_SIZE				16
#define SSL3_SHA_HASH_SIZE				20
#define SSL3_SHAMD5_HASH_SIZE 			(SSL3_MD5_HASH_SIZE + SSL3_SHA_HASH_SIZE)
#ifdef _WINDOWS
CK_IMPLEMENTATION(CK_RV, __win_sign)(CK_UNUSED CK_IN KCRYPTO_HANDLER_STR *self, CK_IN KOBJECT pObject, CK_IN CK_MECHANISM_TYPE hHash, CK_IN CK_BYTE_PTR pHash, CK_IN CK_ULONG ulHashLen, CK_OUT CK_BYTE_PTR pSignature, CK_INOUT CK_ULONG_PTR pulSignatureLen)
{
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	DWORD dwFlags = CRYPT_ACQUIRE_COMPARE_KEY_FLAG, dwOldFlags = AT_SIGNATURE;
	HCRYPTPROV_OR_NCRYPT_KEY_HANDLE hKey;
	DWORD dwKeySpec, cbResult;
	BOOL mustFree;
	ALG_ID uiAlgId;
	SECURITY_STATUS stat;
	HCRYPTHASH hash;
	BCRYPT_PKCS1_PADDING_INFO paddingInfo = { NULL };
	BOOL ret;

	assert(pObject && pHash && pulSignatureLen);
	if (hHash != CK_UNAVAILABLE_INFORMATION) dwFlags |= CRYPT_ACQUIRE_PREFER_NCRYPT_KEY_FLAG;
	WINAPI_ASSERT((ret = CryptAcquireCertificatePrivateKey(pWin->pCtx, dwFlags, NULL, &hKey, &dwKeySpec, &mustFree)), "CryptAcquireCertificatePrivateKey");
	if (!ret) return CKR_FUNCTION_FAILED;
	if (dwKeySpec == CERT_NCRYPT_KEY_SPEC)
	{
		switch (hHash)
		{
		case CKM_SHA_1:
			paddingInfo.pszAlgId = NCRYPT_SHA1_ALGORITHM;
			break;
		case CKM_SHA256:
			paddingInfo.pszAlgId = NCRYPT_SHA256_ALGORITHM;
			break;
		case CKM_SHA384:
			paddingInfo.pszAlgId = NCRYPT_SHA384_ALGORITHM;
			break;
		case CKM_SHA512:
			paddingInfo.pszAlgId = NCRYPT_SHA512_ALGORITHM;
			break;
		}
		rv = (stat = NCryptSignHash(hKey, &paddingInfo, pHash, ulHashLen, NULL, 0, &cbResult, NCRYPT_PAD_PKCS1_FLAG)) == ERROR_SUCCESS ? CKR_OK : CKR_FUNCTION_FAILED;
		if (stat != ERROR_SUCCESS) __pLog->log(__pLog, "NCryptSignHash returned following error: 0x%08x\n", stat);
		if (rv == CKR_OK)
		{
			if (!pSignature) *pulSignatureLen = cbResult;
			else
			{
				rv = (*pulSignatureLen >= cbResult) ? CKR_OK : CKR_BUFFER_TOO_SMALL;
				if (rv == CKR_OK)
				{
					rv = (stat = NCryptSignHash(hKey, &paddingInfo, pHash, ulHashLen, pSignature, *pulSignatureLen, &cbResult, NCRYPT_PAD_PKCS1_FLAG)) == ERROR_SUCCESS ? CKR_OK : CKR_FUNCTION_FAILED;
					if (stat != ERROR_SUCCESS) __pLog->log(__pLog, "NCryptSignHash returned following error: 0x%08x\n", stat);
				}
				if (rv == CKR_OK) *pulSignatureLen = cbResult;
			}
		}
	}
	else
	{
		if (hHash != CK_UNAVAILABLE_INFORMATION)
		{
			switch (hHash)
			{
			case CKM_SHA_1:
				uiAlgId = CALG_SHA1;
				break;
			case CKM_SHA256:
				uiAlgId = CALG_SHA_256;
				break;
			case CKM_SHA384:
				uiAlgId = CALG_SHA_384;
				break;
			case CKM_SHA512:
				uiAlgId = CALG_SHA_512;
				break;
			}
		}
		else uiAlgId = CALG_SSL3_SHAMD5;
		WINAPI_ASSERT((ret = CryptCreateHash(hKey, uiAlgId, 0, 0, &hash)), "CryptCreateHash");
		if (ret)
		{
			WINAPI_ASSERT((ret = CryptSetHashParam(hash, HP_HASHVAL, pHash, 0)), "CryptSetHashParam");
			if (ret)
			{
				WINAPI_ASSERT((ret = CryptSignHash(hash, dwOldFlags, NULL, 0, NULL, &cbResult)), "CryptSignHash");
				if (ret)
				{
					if (!pSignature) *pulSignatureLen = cbResult;
					else
					{
						rv = (*pulSignatureLen >= cbResult) ? CKR_OK : CKR_BUFFER_TOO_SMALL;
						if (rv == CKR_OK)
						{
							WINAPI_ASSERT((ret = CryptSignHash(hash, dwOldFlags, NULL, 0, pSignature, &cbResult)), "CryptSignHash");
							if (ret) *pulSignatureLen = cbResult;
						}
					}
				}
			}
		}
		if (!ret) rv = CKR_FUNCTION_FAILED;
		CryptDestroyHash(hHash);
	}
	if (mustFree)
	{
		if (dwKeySpec == CERT_NCRYPT_KEY_SPEC) NCryptFreeObject(hKey);
		else CryptReleaseContext(hKey, 0);
	}
	return rv;
}
#define __platform_sign					__win_sign
#else
CK_IMPLEMENTATION(CK_RV, __ux_sign)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN KOBJECT pObject,
	CK_IN CK_MECHANISM_TYPE hHash,
	CK_IN CK_BYTE_PTR pHash,
	CK_IN CK_ULONG ulHashLen,
	CK_OUT CK_BYTE_PTR pSignature,
	CK_INOUT CK_ULONG_PTR pulSignatureLen
)
{
	CK_RV rv = CKR_OK;
	KUX_OBJECT pUX = UX_LOADER(pObject);
	CK_MECHANISM_TYPE hMech;
	CK_ULONG ulMD5Len = SSL3_MD5_HASH_SIZE, ulSHALen = SSL3_SHA_HASH_SIZE;
	assert(pUX && pHash && pulSignatureLen);
	switch (hHash)
	{
	case CKM_SHA_1:
		hMech = CKM_SHA1_RSA_PKCS;
		break;
	case CKM_SHA256:
		hMech = CKM_SHA256_RSA_PKCS;
		break;
	case CKM_SHA384:
		hMech = CKM_SHA384_RSA_PKCS;
		break;
	case CKM_SHA512:
		hMech = CKM_SHA512_RSA_PKCS;
		break;
	case CK_UNAVAILABLE_INFORMATION:
		break;
	default: rv = CKR_DATA_INVALID;
	}
	if (rv == CKR_OK)
	{
		if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
		{

			while (!self->isInited) pthread_cond_wait(self->pCondition, self->pMutex);
			if (hHash != CK_UNAVAILABLE_INFORMATION)
			{
				rv = pUX->key->sign(pUX->key, hMech, pHash, ulHashLen, pSignature, pulSignatureLen);
				NHARU_ASSERT(rv, "NH_new_RSA_privkey_handler->sign");
				rv = rv == NH_OK ? CKR_OK : CKR_FUNCTION_FAILED;
			}
			else
			{
				if (!pSignature) *pulSignatureLen = SSL3_SHAMD5_HASH_SIZE;
				else
				{
					if (*pulSignatureLen >= SSL3_SHAMD5_HASH_SIZE)
					{
						rv = pUX->key->sign(pUX->key, CKM_MD5, pHash, ulMD5Len, pSignature, &ulMD5Len);
						NHARU_ASSERT(rv, "NH_new_RSA_privkey_handler->sign");
						if ((rv = rv == NH_OK ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
						{
							rv = pUX->key->sign(pUX->key, CKM_SHA_1, pHash + ulMD5Len, ulSHALen, pSignature + ulMD5Len, &ulSHALen);
							NHARU_ASSERT(rv, "NH_new_RSA_privkey_handler->sign");
							if ((rv = rv == NH_OK ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK) *pulSignatureLen = ulMD5Len + ulSHALen;
						}

					}
					else rv = CKR_BUFFER_TOO_SMALL;
				}
			}
			self->hMutex->UnlockMutex(self->pMutex);
		}
	}
	return rv;
}
#define __platform_sign					__ux_sign
#endif
CK_IMPLEMENTATION(CK_RV, __sign)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_INOUT KSESSION pSession,
	CK_IN CK_BYTE_PTR pData,
	CK_IN CK_ULONG ulDataLen,
	CK_OUT CK_BYTE_PTR pSignature,
	CK_INOUT CK_ULONG_PTR pulSignatureLen
)
{
	CK_RV rv = CKR_OK;
	CK_MECHANISM_TYPE hHash;
	NH_ASN1_PARSER_HANDLE hParser = NULL;
	CK_BYTE_PTR pHash = NULL;
	CK_ULONG ulHashLen = 0UL;
	NH_ASN1_PNODE pNode;

	assert(pSession && pData && pulSignatureLen);
	if (ulDataLen == SSL3_SHAMD5_HASH_SIZE)
	{
		hHash = CK_UNAVAILABLE_INFORMATION;
		pHash = pData;
		ulHashLen = ulDataLen;
	}
	else
	{
		rv = NH_parse_digest_info(pData, ulDataLen, &hParser);
		NHARU_ASSERT(rv, "NH_parse_digest_info");
		if
		(
			(rv = rv == NH_OK ? CKR_OK : CKR_DATA_INVALID) == CKR_OK &&
			(rv = (pNode = hParser->sail(hParser->root, NH_PARSE_SOUTH | 2)) ? CKR_OK : CKR_DATA_INVALID) == CKR_OK &&
			((rv = (hHash = NH_oid_to_mechanism((unsigned int*) pNode->value, pNode->valuelen)) != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_DATA_INVALID) == CKR_OK) &&
			((rv = (pNode = hParser->sail(hParser->root, (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST)) ? CKR_OK : CKR_DATA_INVALID) == CKR_OK)
		)
		{
			pHash = pNode->value;
			ulHashLen = pNode->valuelen;
		}
	}
	if (rv == CKR_OK)
	{
		rv = __platform_sign(self, pSession->Operation.kCrypto.hKey, hHash, pHash, ulHashLen, pSignature, pulSignatureLen);
		if (rv != CKR_OK || pSignature) pSession->finish_crypto(pSession);
	}
	if (hParser) NH_release_digest_parser(hParser);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __keypair_templates)
(
	CK_UNUSED CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN CK_MECHANISM_TYPE ulType,
	CK_IN CK_ATTRIBUTE_PTR pPublicKeyTemplate,
	CK_IN CK_ULONG ulPublicKeyAttributeCount,
	CK_IN CK_ATTRIBUTE_PTR pPrivateKeyTemplate,
	CK_IN CK_ULONG ulPrivateKeyAttributeCount
)
{
#ifdef _WINDOWS
	return CKR_FUNCTION_NOT_SUPPORTED;
#else
	CK_ULONG i = 0;
	CK_KEY_TYPE ulKey = CK_UNAVAILABLE_INFORMATION;

	if (ulType != CKM_RSA_PKCS_KEY_PAIR_GEN) return CKR_MECHANISM_INVALID;
	while (ulKey == CK_UNAVAILABLE_INFORMATION && i < ulPublicKeyAttributeCount)
	{
		if (pPublicKeyTemplate[i].type == CKA_KEY_TYPE)
		{
			if (pPublicKeyTemplate[i].ulValueLen != sizeof(CK_KEY_TYPE)) return CKR_ATTRIBUTE_VALUE_INVALID;
			ulKey = *(CK_KEY_TYPE*) pPublicKeyTemplate[i].pValue;
		}
		i++;
	}
	if (ulKey == CK_UNAVAILABLE_INFORMATION) ulKey = CKK_RSA;
	if (ulKey != CKK_RSA) return CKR_MECHANISM_INVALID;
	if (pPrivateKeyTemplate)
	{
		i = 0;
		while (i < ulPrivateKeyAttributeCount)
		{
			if (pPrivateKeyTemplate[i].type == CKA_KEY_TYPE)
			{
				if (pPrivateKeyTemplate[i].ulValueLen != sizeof(CK_KEY_TYPE)) return CKR_ATTRIBUTE_VALUE_INVALID;
				if (*(CK_KEY_TYPE*) pPrivateKeyTemplate[i].pValue != ulKey) return CKR_TEMPLATE_INCONSISTENT;
			}
			i++;
		}
	}
	return CKR_OK;
#endif
}
CK_IMPLEMENTATION(CK_RV, __keypair_parameters)
(
	CK_UNUSED CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_OUT CK_ULONG_PTR pulBits,
	CK_OUT CK_ULONG_PTR pulExponent
)
{
#ifdef _WINDOWS
	return CKR_FUNCTION_NOT_SUPPORTED;
#else
	CK_ULONG ulBits = 0UL, i = 0UL, ulRoll = 0UL, ulExponent = 0UL;
	CK_BYTE_PTR pContents;
	CK_RV rv;
	while ((!ulExponent || !ulBits) && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		case CKA_MODULUS_BITS:
			if (pTemplate[i].ulValueLen > sizeof(CK_ULONG)) return CKR_ATTRIBUTE_VALUE_INVALID;
			ulBits = *(CK_ULONG_PTR) pTemplate[i].pValue;
			break;
		case CKA_PUBLIC_EXPONENT:
			pContents = (CK_BYTE_PTR) pTemplate[i].pValue;
			switch (pTemplate[i].ulValueLen)
			{
			case 4:
				ulExponent = *(pContents + 3) << ulRoll;
				ulRoll += 0x08;
			case 3:
				ulExponent |= *(pContents + 2) << ulRoll;
				ulRoll += 0x08;
			case 2:
				ulExponent |= *(pContents + 1) << ulRoll;
				ulRoll += 0x08;
			case 1:
				ulExponent |= *pContents << ulRoll;
				break;
			default:
				return CKR_ATTRIBUTE_VALUE_INVALID;
			}
			break;
		}
		i++;
	}
	if ((rv = ulBits && ulExponent ? CKR_OK : CKR_TEMPLATE_INCOMPLETE) == CKR_OK)
	{
		*pulBits = ulBits;
		*pulExponent = ulExponent;
	}
	return rv;
#endif
}
#ifdef _UNIX
CK_IMPLEMENTATION(CK_RV, __encode_bignum)(CK_IN BIGNUM *n, CK_OUT KP_BLOB *bn)
{
	int iBytes, iOffset;
	assert(bn);
	iBytes = BN_num_bytes(n);
	iOffset = (BN_num_bits(n) == iBytes * 8) ? 1 : 0;
	if (!(bn->data = (unsigned char*) malloc(iBytes + iOffset))) return CKR_HOST_MEMORY;
	memset(bn->data, 0, iBytes + iOffset);
	BN_bn2bin(n, bn->data + iOffset);
	bn->length = iBytes + iOffset;
	return CKR_OK;
}
static CK_OBJECT_CLASS __pubKey		= CKO_PUBLIC_KEY;
static CK_OBJECT_CLASS __privKey		= CKO_PRIVATE_KEY;
static CK_KEY_TYPE __rsaKey			= CKK_RSA;
static CK_MECHANISM_TYPE __rsaKeygen	= CKM_RSA_PKCS_KEY_PAIR_GEN;
static CK_BBOOL __true				= CK_TRUE;
static CK_BBOOL __false				= CK_FALSE;
#define CKA_TOKEN_IDX				1
#define CKA_MODULUS_IDX				4
#define CKA_PUBLIC_EXPONENT_IDX		5
#define CKA_PRIVATE_EXPONENT_IDX		6
#define CKA_PRIME_1_IDX				7
#define CKA_PRIME_2_IDX				8
#define CKA_EXPONENT_1_IDX			9
#define CKA_EXPONENT_2_IDX			10
#define CKA_COEFFICIENT_IDX			11
CK_IMPLEMENTATION(CK_RV, __to_pubkey)(CK_IN KTOKEN_STR *pToken, CK_IN CK_BBOOL isSessionObject, CK_IN RSA *pKey, CK_OUT KOBJECT *pOut)
{
	CK_ATTRIBUTE pTemplate[] =
	{
		{ CKA_CLASS, &__pubKey, sizeof(CK_OBJECT_CLASS) },
		{ CKA_TOKEN, NULL, sizeof(CK_BBOOL) },
		{ CKA_KEY_TYPE, &__rsaKey, sizeof(CK_KEY_TYPE) },
		{ CKA_KEY_GEN_MECHANISM, &__rsaKeygen, sizeof(CK_MECHANISM_TYPE) },
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_PUBLIC_EXPONENT, NULL, 0UL }
	};
	BIGNUM *n;
	BIGNUM *e;
	CK_RV rv;
	KP_BLOB bn = { NULL, 0UL }, be = { NULL, 0UL };
	CK_BBOOL isToken = !isSessionObject;
	KOBJECT pPubKey;

	RSA_get0_key(pKey, (const BIGNUM **) &n, (const BIGNUM **) &e, NULL);
	if
	(
		(rv = __encode_bignum(n, &bn)) == CKR_OK &&
		(rv = __encode_bignum(e, &be)) == CKR_OK
	)
	{
		pTemplate[CKA_TOKEN_IDX].pValue = &isToken;
		pTemplate[CKA_MODULUS_IDX].pValue = bn.data;
		pTemplate[CKA_MODULUS_IDX].ulValueLen = bn.length;
		pTemplate[CKA_PUBLIC_EXPONENT_IDX].pValue = be.data;
		pTemplate[CKA_PUBLIC_EXPONENT_IDX].ulValueLen = be.length;
		rv = CKO_uxnew_object(pToken, pTemplate, CKA_PUBLIC_EXPONENT_IDX + 1, &pPubKey);
	}
	if (bn.data) free(bn.data);
	if (be.data) free(be.data);
	if (rv == CKR_OK) *pOut = pPubKey;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __to_privkey)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN CK_BBOOL isSessionObject,
	CK_IN RSA *pKey,
	CK_OUT KOBJECT *pOut
)
{
	CK_ATTRIBUTE pTemplate[] =
	{
		{ CKA_CLASS, &__privKey, sizeof(CK_OBJECT_CLASS) },
		{ CKA_TOKEN, NULL, sizeof(CK_BBOOL) },
		{ CKA_KEY_TYPE, &__rsaKey, sizeof(CK_KEY_TYPE) },
		{ CKA_KEY_GEN_MECHANISM, &__rsaKeygen, sizeof(CK_MECHANISM_TYPE) },
		{ CKA_MODULUS, NULL, 0UL },
		{ CKA_PUBLIC_EXPONENT, NULL, 0UL },
		{ CKA_PRIVATE_EXPONENT, NULL, 0UL },
		{ CKA_PRIME_1, NULL, 0UL },
		{ CKA_PRIME_2, NULL, 0UL },
		{ CKA_EXPONENT_1, NULL, 0UL },
		{ CKA_EXPONENT_2, NULL, 0UL },
		{ CKA_COEFFICIENT, NULL, 0UL },
		{ CKA_SENSITIVE, &__true, sizeof(CK_BBOOL) },
		{ CKA_EXTRACTABLE, &__false, sizeof(CK_BBOOL) }
	};
	BIGNUM *n;
	BIGNUM *e;
	BIGNUM *d;
	BIGNUM *p;
	BIGNUM *q;
	BIGNUM *dmp1;
	BIGNUM *dmq1;
	BIGNUM *iqmp;
	CK_RV rv;
	KP_BLOB bn = { NULL, 0UL }, be = { NULL, 0UL }, bd = { NULL, 0UL }, bp = { NULL, 0UL },
	bq = { NULL, 0UL }, bdmp1 = { NULL, 0UL }, bdmq1 = { NULL, 0UL }, biqmp = { NULL, 0UL };
	CK_BBOOL isToken = !isSessionObject;
	KOBJECT pPrivKey;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{

		while (!self->isInited) pthread_cond_wait(self->pCondition, self->pMutex);
		RSA_get0_key(pKey, (const BIGNUM **) &n, (const BIGNUM **) &e, (const BIGNUM **) &d);
		RSA_get0_factors(pKey,(const BIGNUM **) &p, (const BIGNUM **) &q);
		RSA_get0_crt_params(pKey, (const BIGNUM **) &dmp1, (const BIGNUM **) &dmq1, (const BIGNUM **) &iqmp);
		self->hMutex->UnlockMutex(self->pMutex);
	}
	if
	(
		(rv = __encode_bignum(n, &bn)) == CKR_OK &&
		(rv = __encode_bignum(e, &be)) == CKR_OK &&
		(rv = __encode_bignum(d, &bd)) == CKR_OK &&
		(rv = __encode_bignum(p, &bp)) == CKR_OK &&
		(rv = __encode_bignum(q, &bq)) == CKR_OK &&
		(rv = __encode_bignum(dmp1, &bdmp1)) == CKR_OK &&
		(rv = __encode_bignum(dmq1, &bdmq1)) == CKR_OK &&
		(rv = __encode_bignum(iqmp, &biqmp)) == CKR_OK
	)
	{
		pTemplate[CKA_TOKEN_IDX].pValue = &isToken;
		pTemplate[CKA_MODULUS_IDX].pValue = bn.data;
		pTemplate[CKA_MODULUS_IDX].ulValueLen = bn.length;
		pTemplate[CKA_PUBLIC_EXPONENT_IDX].pValue = be.data;
		pTemplate[CKA_PUBLIC_EXPONENT_IDX].ulValueLen = be.length;
		pTemplate[CKA_PRIVATE_EXPONENT_IDX].pValue = bd.data;
		pTemplate[CKA_PRIVATE_EXPONENT_IDX].ulValueLen = bd.length;
		pTemplate[CKA_PRIME_1_IDX].pValue = bp.data;
		pTemplate[CKA_PRIME_1_IDX].ulValueLen = bp.length;
		pTemplate[CKA_PRIME_2_IDX].pValue = bq.data;
		pTemplate[CKA_PRIME_2_IDX].ulValueLen = bq.length;
		pTemplate[CKA_EXPONENT_1_IDX].pValue = bdmp1.data;
		pTemplate[CKA_EXPONENT_1_IDX].ulValueLen = bdmp1.length;
		pTemplate[CKA_EXPONENT_2_IDX].pValue = bdmq1.data;
		pTemplate[CKA_EXPONENT_2_IDX].ulValueLen = bdmq1.length;
		pTemplate[CKA_COEFFICIENT_IDX].pValue = biqmp.data;
		pTemplate[CKA_COEFFICIENT_IDX].ulValueLen = biqmp.length;
		rv = CKO_uxnew_object(self->pToken, pTemplate, CKA_COEFFICIENT_IDX + 3, &pPrivKey);
	}
	if (bn.data) free(bn.data);
	if (be.data) free(be.data);
	if (bd.data)
	{
		OPENSSL_cleanse(bd.data,  bd.length);
		free(bd.data);
	}
	if (bp.data)
	{
		OPENSSL_cleanse(bp.data, bp.length);
		free(bp.data);
	}
	if (bq.data)
	{
		OPENSSL_cleanse(bq.data, bq.length);
		free(bq.data);
	}
	if (bdmp1.data)
	{
		OPENSSL_cleanse(bdmp1.data, bdmp1.length);
		free(bdmp1.data);
	}
	if (bdmq1.data)
	{
		OPENSSL_cleanse(bdmq1.data, bdmq1.length);
		free(bdmq1.data);
	}
	if (biqmp.data)
	{
		OPENSSL_cleanse(biqmp.data, biqmp.length);
		free(biqmp.data);
	}
	if (rv == CKR_OK) *pOut = pPrivKey;
	return rv;
}
#endif
CK_IMPLEMENTATION(CK_RV, __generate_keypair)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN CK_ULONG ulBits,
	CK_IN CK_ULONG ulPublicExponent,
	CK_IN CK_BBOOL isSessionObject,
	CK_OUT KOBJECT *pPubKey,
	CK_OUT KOBJECT *pPrivKey
)
{
#ifdef _WINDOWS
	return CKR_FUNCTION_NOT_SUPPORTED;
#else

	NH_RSA_PUBKEY_HANDLER hPubKey;
	NH_RSA_PRIVKEY_HANDLER hPrivKey;
	CK_RV rv;
	KOBJECT pRSAPub, pRSAPriv;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		while (!self->isInited) pthread_cond_wait(self->pCondition, self->pMutex);
		rv = NH_generate_RSA_keys((int) ulBits, ulPublicExponent, &hPubKey, &hPrivKey);
		self->hMutex->UnlockMutex(self->pMutex);
		NHARU_ASSERT(rv, "NH_generate_RSA_keys");
		rv = rv == NH_OK ? CKR_OK : CKR_FUNCTION_FAILED;
	}
	if ( rv == CKR_OK && (rv = __to_pubkey(self->pToken, isSessionObject, hPubKey->key, &pRSAPub)) == CKR_OK)
	{
		if ((rv = __to_privkey(self, isSessionObject, hPrivKey->key, &pRSAPriv)) == CKR_OK)
		{
			*pPubKey = pRSAPub;
			*pPrivKey = pRSAPriv;
		}
		else CKO_uxdelete_object(pRSAPub);
	}
	NH_release_RSA_pubkey_handler(hPubKey);
	NH_release_RSA_privkey_handler(hPrivKey);
	return rv;

#endif
}
#ifdef _UNIX
CK_IMPLEMENTATION(CK_RV, __rand)(CK_IN KCRYPTO_HANDLER_STR *self, CK_OUT unsigned char *pBuffer, CK_IN int iSize)
{
	CK_RV rv;

	assert(pBuffer);
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{

		while (!self->isInited) pthread_cond_wait(self->pCondition, self->pMutex);
		RAND_bytes(pBuffer, iSize);
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __pbe)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN KP_BLOB *pPin,
	CK_IN KP_BLOB *pSalt,
	CK_OUT unsigned char *pBuffer,
	CK_IN int iSize
)
{
	CK_RV rv;
	int ret;

	assert(pPin && pPin->data && pSalt && pSalt->data && pBuffer);
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{

		while (!self->isInited) pthread_cond_wait(self->pCondition, self->pMutex);
		ret = PKCS5_PBKDF2_HMAC_SHA1((char*) pPin->data, (int) pPin->length, pSalt->data, pSalt->length, PBE_ITERATION_COUNT, iSize, pBuffer);
		self->hMutex->UnlockMutex(self->pMutex);
		OPENSSL_ASSERT(ret != 0, "PKCS5_PBKDF2_HMAC_SHA1");
		rv = ret != 0 ? CKR_OK : CKR_GENERAL_ERROR;
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __pbe_cipher)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN CK_BYTE_PTR pKey,
	CK_IN CK_BYTE_PTR pIV,
	CK_IN CK_BYTE_PTR pData,
	CK_IN CK_ULONG ulData,
	CK_IN int enc,
	CK_OUT CK_BYTE_PTR *ppOut,
	CK_OUT CK_ULONG_PTR pulOut
)
{
	CK_RV rv;
	int ret;
	CK_BYTE_PTR pBuffer;
	int iBufLen, iTempLen;
	EVP_CIPHER_CTX *ctx;

	assert(pKey && pIV && pData && pulOut);
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{

		while (!self->isInited) pthread_cond_wait(self->pCondition, self->pMutex);
		if ((rv = (ctx = EVP_CIPHER_CTX_new()) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			ret = EVP_CipherInit_ex(ctx, PBE_CIPHER_MECHANISM, NULL, NULL, NULL, enc);
			OPENSSL_ASSERT(ret != 0, "EVP_CipherInit_ex");
			if
			(
				(rv = ret != 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
				(rv = EVP_CIPHER_CTX_key_length(ctx) == PBE_KEY_LEN ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
				(rv = EVP_CIPHER_CTX_iv_length(ctx) == PBE_IV_LEN ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)
			{
				ret = EVP_CipherInit_ex(ctx, NULL, NULL, pKey, pIV, enc);
				OPENSSL_ASSERT(ret != 0, "EVP_CipherInit_ex");
				if
				(
					(rv = ret != 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
					(iBufLen = EVP_CIPHER_CTX_block_size(ctx) + ulData) &&
					(rv = (pBuffer = (CK_BYTE_PTR) malloc(iBufLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)
				{
					ret = EVP_CipherUpdate(ctx, pBuffer, &iBufLen, pData, (int) ulData);
					OPENSSL_ASSERT(ret != 0, "EVP_CipherUpdate");
					if ((rv = ret != 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
					{
						ret = EVP_CipherFinal_ex(ctx, pBuffer + iBufLen, &iTempLen);
						OPENSSL_ASSERT(ret != 0, "EVP_CipherFinal_ex");
						if ((rv = ret != 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
						{
							*ppOut = pBuffer;
							*pulOut = iBufLen + iTempLen;
						}
					}
					if (rv != CKR_OK) free(pBuffer);
				}
			}
			EVP_CIPHER_CTX_free(ctx);
		}
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __encrypt)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN CK_BYTE_PTR pKey,
	CK_IN CK_BYTE_PTR pIV,
	CK_IN CK_BYTE_PTR pData,
	CK_IN CK_ULONG ulData,
	CK_OUT CK_BYTE_PTR *ppOut,
	CK_OUT CK_ULONG_PTR pulOut
)
{
	return __pbe_cipher(self, pKey, pIV, pData,ulData, 1, ppOut, pulOut);
}
CK_IMPLEMENTATION(CK_RV, __decrypt)
(
	CK_IN KCRYPTO_HANDLER_STR *self,
	CK_IN CK_BYTE_PTR pKey,
	CK_IN CK_BYTE_PTR pIV,
	CK_IN CK_BYTE_PTR pData,
	CK_IN CK_ULONG ulData,
	CK_OUT CK_BYTE_PTR *ppOut,
	CK_OUT CK_ULONG_PTR pulOut
)
{
	return __pbe_cipher(self, pKey, pIV, pData,ulData, 0, ppOut, pulOut);
}
CK_IMPLEMENTATION(CK_VOID_PTR, __initPRNG)(void *pArgs)
{
	KCRYPTO_HANDLER self = (KCRYPTO_HANDLER) pArgs;
	NH_NOISE_HANDLER hNoise;
	NH_RV rv;

	assert(pArgs);
	if (self->hMutex->LockMutex(self->pMutex) == CKR_OK)
	{
		rv = NH_new_noise_device(&hNoise);
		OPENSSL_ASSERT(rv == NH_OK, "NH_new_noise_device");
		if (NH_SUCCESS(rv)) NH_release_noise_device(hNoise);
		self->isInited = CK_TRUE;
		pthread_cond_broadcast(self->pCondition);
		self->hMutex->UnlockMutex(self->pMutex);
	}
	else
	{
		__pLog->log(__pLog, "Could not initialize PRNG due to locking error");
		self->isInited = CK_TRUE;	/* And pray to Lord... */
	}
	return NULL;
}
#endif
static KCRYPTO_HANDLER_STR __default_crypto_handler =
{
	NULL,			/* kMechanisms*/
	NULL,			/* pToken */

	__list,
	__info,
	__is_allowed,
	__sign_init,
	__sign,
	__keypair_templates,
	__keypair_parameters,
	__generate_keypair

#ifdef _UNIX
	,
	NULL,			/* pCondition */
	CK_FALSE,		/* isInited */
	NULL,			/* pMutex */
	NULL,			/* hMutex */

	__rand,
	__pbe,
	__encrypt,
	__decrypt
#endif
};
CK_NEW(CK_new_crypto)(KTOKEN_STR *pToken, CK_OUT KCRYPTO_HANDLER *hOut)
{
	CK_RV rv;
	KCRYPTO_HANDLER pHandler = NULL;
	KC_MECHANISM pMech;
	khiter_t k;
	int ret;
#ifdef _UNIX
	pthread_attr_t attr;
	pthread_t tid;
#endif

	if ((rv = (pHandler = (KCRYPTO_HANDLER) malloc(sizeof(KCRYPTO_HANDLER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(pHandler, &__default_crypto_handler, sizeof(KCRYPTO_HANDLER_STR));
		pHandler->pToken = pToken;
		
		if
		(
			(rv = (pHandler->kMechanisms = kh_init(KMMAP)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = (pMech = (KC_MECHANISM) malloc(sizeof(KC_MECHANISM_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			pMech->ulType = CKM_RSA_PKCS;
			pMech->info.ulMinKeySize = 0;
			pMech->info.ulMaxKeySize = 0;
			pMech->info.flags = CKF_SIGN;
			k = kh_put(KMMAP, pHandler->kMechanisms, CKM_RSA_PKCS, &ret);
			if (ret > 0) kh_value(pHandler->kMechanisms, k) = pMech;
			else rv = CKR_FUNCTION_FAILED;
		}
#ifdef _UNIX
		if (rv == CKR_OK && (rv = (pMech = (KC_MECHANISM) malloc(sizeof(KC_MECHANISM_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			pMech->ulType = CKM_RSA_PKCS_KEY_PAIR_GEN;
			pMech->info.ulMinKeySize = 512;
			pMech->info.ulMaxKeySize = 5096;
			pMech->info.flags = CKF_GENERATE_KEY_PAIR;
			k = kh_put(KMMAP, pHandler->kMechanisms, CKM_RSA_PKCS_KEY_PAIR_GEN, &ret);
			if (ret > 0) kh_value(pHandler->kMechanisms, k) = pMech;
			else rv = CKR_FUNCTION_FAILED;
		}
		if
		(
			rv == CKR_OK &&
			(rv = pToken->hMutex->clone(pToken->hMutex, &pHandler->hMutex)) == CKR_OK &&
			(rv = pHandler->hMutex->CreateMutex(&pHandler->pMutex)) == CKR_OK &&
			(rv = (pHandler->pCondition = (pthread_cond_t*) malloc(sizeof(pthread_cond_t))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			OS_ASSERT((ret = pthread_cond_init((pthread_cond_t*) pHandler->pCondition, NULL)), "pthread_cond_init");
			if ((rv = ret == 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
			{
				OS_ASSERT((ret = pthread_attr_init(&attr)), "pthread_attr_init");
				if ((rv = ret == 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
				{
					OS_ASSERT((ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)), "pthread_attr_setdetachstate");
					if ((rv = ret == 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
					{
						OS_ASSERT((ret = pthread_create(&tid, &attr, &__initPRNG, pHandler)), "pthread_create");
						rv = ret == 0 ? CKR_OK : CKR_FUNCTION_FAILED;
					}
					pthread_attr_destroy(&attr);
				}
			}
		}
#endif
	}
	if (rv == CKR_OK) *hOut = pHandler;
	else CK_release_crypto(pHandler);
	return rv;
}
CK_DELETE(CK_release_crypto)(CK_INOUT KCRYPTO_HANDLER pHandler)
{
	khiter_t k;

	if (pHandler)
	{
		if (pHandler->kMechanisms)
		{
			for
			(
				k = kh_begin(pHandler->kMechanisms);
				k != kh_end(pHandler->kMechanisms);
				++k
			)	if (kh_exist(pHandler->kMechanisms, k))
			{
				free(kh_value(pHandler->kMechanisms, k));
				kh_del(KMMAP, pHandler->kMechanisms, k);
			}
			kh_destroy(KMMAP, pHandler->kMechanisms);
		}
#ifdef _UNIX
		if (pHandler->pCondition)
		{
			pthread_cond_destroy((pthread_cond_t*) pHandler->pCondition);
			free(pHandler->pCondition);
		}
		if (pHandler->hMutex)
		{
			pHandler->hMutex->DestroyMutex(pHandler->pMutex);
			CK_release_mutex_handler(pHandler->hMutex);
		}
#endif
		free(pHandler);
	}
}
