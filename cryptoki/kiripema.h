/**
 * @file kiripema.h
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Multiplatform PKCS #11 softoken implementation 
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#ifndef __KIRIPEMA_H__
#define __KIRIPEMA_H__

/* OS and device check */
#if (defined(linux) || defined(__linux) || defined(__linux__) || defined(__gnu_linux))
#ifndef _UNIX
#define _UNIX
#endif
#ifndef _UX
#define _UX
#endif

#elif (defined(_WIN32) || defined(_WIN64))
#ifndef _WINDOWS
#define _WINDOWS
#endif

#elif (defined(ANDROID) || defined(__ANDROID__))
#ifndef _UNIX
#define _UNIX
#endif
#ifndef _DROID
#define _DROID
#endif

#elif defined(__APPLE__)
#ifndef _UNIX
#define _UNIX
#endif
#ifndef _APPLE
#define _APPLE
#endif

#else
#error Unsupported operating system
#endif	/* OS and device check */


/* Compiler attributes */
#if defined(_UNIX)
#include <linux/limits.h>
#define CK_IN							const
#define CK_FILENAME_MAX						PATH_MAX
#define CK_OUT
#define CK_INOUT
#define CK_CONSTRUCTOR						__attribute__((constructor))
#define CK_DESTRUCTOR						__attribute__((destructor))
#define CK_UNUSED							__attribute__((unused))
#define CK_HIDDEN							__attribute__((__visibility__("internal")))
#define CK_INLINE							__inline__
#define CK_NOP							__asm__("nop");

#elif defined(_WINDOWS)
#include <windows.h>
#include <SDKDDKVer.h>
#include <sal.h>
#define CK_FILENAME_MAX						FILENAME_MAX
#define CK_IN							_In_ const
#define CK_OUT							_Out_
#define CK_INOUT							_Inout_
#define CK_CONSTRUCTOR
#define CK_DESTRUCTOR
#define CK_UNUSED
#define CK_HIDDEN
#define CK_INLINE							__inline
#define CK_NOP							__noop
#endif	/* Compiler attributes */


/* Cryptoki definitions */
#ifndef NULL
#define NULL							((void*) 0)
#endif
#ifdef CK_EXPORT_SPEC
#undef CK_EXPORT_SPEC
#endif
#ifdef CK_IMPORT_SPEC
#undef CK_IMPORT_SPEC
#endif
#ifdef CK_CALL_SPEC
#undef CK_CALL_SPEC
#endif
#pragma pack(push, cryptoki, 1)

#if defined(_WINDOWS)
#define CK_IMPORT_SPEC						__declspec(dllimport)
#ifdef CRYPTOKI_EXPORTS
#define CK_EXPORT_SPEC						__declspec(dllexport)
#else
#define CK_EXPORT_SPEC						CK_IMPORT_SPEC
#endif
#if defined (_WIN32)
#define CK_CALL_SPEC						__cdecl
#else
#define CK_CALL_SPEC
#endif

#else
#define CK_EXPORT_SPEC
#define CK_IMPORT_SPEC
#define CK_CALL_SPEC
#endif

#ifndef CK_PTR
#define CK_PTR 							*
#endif
#ifndef CK_DECLARE_FUNCTION
#define CK_DECLARE_FUNCTION(type, name)			type CK_EXPORT_SPEC CK_CALL_SPEC name
#endif
#ifndef CK_DECLARE_FUNCTION_POINTER
#define CK_DECLARE_FUNCTION_POINTER(type, name)		type CK_IMPORT_SPEC (CK_CALL_SPEC CK_PTR name)
#endif
#ifndef CK_CALLBACK_FUNCTION
#define CK_CALLBACK_FUNCTION(type, name)			type (CK_CALL_SPEC CK_PTR name)
#endif
#ifndef NULL_PTR
#define NULL_PTR 							NULL
#endif
#include "pkcs11.h"
#pragma pack(pop, cryptoki)	/* Cryptoki definitions */


#define MANUFACTURER_ID						"kiripema@crypthing.org          "
#define LIBRARY_DESCRIPTION					"Kiripema Cryptoki               "
#define SLOT_DESCRIPTION					"Kiripema Cryptoki Soft Token Default Slot                       "
#define TOKEN_DESCRIPTION					"Kiripema Cryptoki Default Token "
#define SLOT_TOKEN_MODEL					"2.0             "
#define SLOT_TOKEN_SERIAL					"2.0             "
#define DEFAULT_UTC_TIME					"                "
#define DEFAULT_SLOT_ID						0xFFFF
#define LIBRARY_VERSION_MAJOR					0x01
#define LIBRARY_VERSION_MINOR					0x00
#define HARDWARE_VERSION_MAJOR				0x00
#define HARDWARE_VERSION_MINOR				0x00
#define FIRMWARE_VERSION_MAJOR				0x00
#define FIRMWARE_VERSION_MINOR				0x00
#define MAX_PIN_LEN						256
#define MIN_PIN_LEN						8

#define CK_IMPLEMENTATION(type, name)			static type CK_CALL_SPEC name
#define CK_METHOD							CK_CALLBACK_FUNCTION
#define CK_FUNCTION(type, name)				CK_HIDDEN type CK_CALL_SPEC name
#define CK_NEW(name)						CK_FUNCTION(CK_RV, name)
#define CK_DELETE(name)						CK_FUNCTION(void, name)


#include "pfx.h"							/* Nharu Library */
#include "khash.h"						/* Hashtable implementation */

/**
 * @brief Concurrency facility
 *
 */
typedef struct  KMUTEX_HANDLER_STR				KMUTEX_HANDLER_STR;
typedef CK_METHOD(CK_RV, KM_CLONE)				/* Clone this instance */
(
	CK_IN  KMUTEX_HANDLER_STR*,				/* self */
	CK_OUT KMUTEX_HANDLER_STR**				/* pOut */
);
struct KMUTEX_HANDLER_STR
{
	CK_CREATEMUTEX	CreateMutex;
	CK_LOCKMUTEX	LockMutex;
	CK_UNLOCKMUTEX	UnlockMutex;
	CK_DESTROYMUTEX	DestroyMutex;

	KM_CLONE		clone;
};
typedef KMUTEX_HANDLER_STR*					KMUTEX_HANDLER;


/**
 * @brief Virtual file system facility
 * 
 */
typedef enum KSPECIAL_FOLDER
{
	KSF_HOME = 0,						/* Kiripema data location */
	KSF_LOG = 0,						/* Kiripema log location, if DEBUG */
	KSF_TOKEN = 1,						/* Kiripema objects location */
	KSF_PUB_OBJECTS = 1,					/* Kiripema public objects (certificates and public keys) location */
	KSF_PRIV_OBJECTS = 2,					/* Kiripema private objects (private keys) location */
	KSF_AUTH_TOKEN = 2					/* Kiripema authorization token location */

} KSPECIAL_FOLDER;

typedef struct KFILE_SYSTEM_STR				KFILE_SYSTEM_STR;
typedef CK_CALLBACK_FUNCTION(CK_RV, KFS_READDIR)	/* Folder iteraction callback */
(
	CK_IN char*,						/* Target directory */
	CK_IN char*,						/* Existing filename */
	CK_VOID_PTR							/* Callback user argument */
);
typedef CK_METHOD(char*, KFS_SPEC_FOLDER)			/* Gets file system location of folder */
(
	CK_IN KFILE_SYSTEM_STR*,				/* self */
	CK_IN KSPECIAL_FOLDER					/* iFolder */
);
typedef CK_METHOD(CK_BBOOL, KFS_EXISTS)			/* Checks if objects exists in file system */
(
	CK_IN KFILE_SYSTEM_STR*,				/* self */
	CK_IN KSPECIAL_FOLDER,					/* iFolder */
	CK_IN  char*						/* szObject */
);
typedef CK_METHOD(CK_RV, KFS_LIST)				/* Iterates through token special folders */
(
	CK_IN KFILE_SYSTEM_STR*,				/* self */
	CK_IN KSPECIAL_FOLDER,					/* iFolder */
	KFS_READDIR,						/* callback */
	CK_VOID_PTR							/* pArgument */
);
typedef CK_METHOD(CK_RV, KFS_LOAD)				/* Loads specified object into memory */
(
	CK_IN  KFILE_SYSTEM_STR*,				/* self */
	CK_IN  KSPECIAL_FOLDER,					/* iFolder */
	CK_IN  char*,						/* szObject */
	CK_OUT CK_BYTE_PTR*,					/* pEncoding */
	CK_OUT CK_ULONG_PTR					/* pulEncoding */
);
typedef CK_METHOD(CK_RV, KSF_LOADPATH)			/* Loads specified object into memory */
(
	CK_IN  KFILE_SYSTEM_STR*,				/* self */
	CK_IN  char*,						/* szPath */
	CK_OUT CK_BYTE_PTR*,					/* pEncoding */
	CK_OUT CK_ULONG_PTR					/* pulEncoding */
);
typedef CK_METHOD(CK_RV, KFS_STORE)				/* Stores specified object in virtual file system */
(
	CK_IN KFILE_SYSTEM_STR*,				/* self */
	CK_IN CK_BYTE_PTR,					/* pEncoding */
	CK_IN CK_ULONG,						/* ulEncoding */
	CK_IN  KSPECIAL_FOLDER,					/* iFolder */
	CK_IN  char*						/* szObject */
);
typedef CK_METHOD(CK_RV, KFS_STOREPATH)			/* Stores specified object in virtual file system */
(
	CK_IN KFILE_SYSTEM_STR*,				/* self */
	CK_IN CK_BYTE_PTR,					/* pEncoding */
	CK_IN CK_ULONG,						/* ulEncoding */
	CK_IN  char*						/* szPath */
);
typedef CK_METHOD(CK_RV, KFS_OPEN)				/* Opens specified object to write */
(
	CK_IN  KFILE_SYSTEM_STR*,				/* self */
	CK_IN  KSPECIAL_FOLDER,					/* iFolder */
	CK_IN  char*,						/* szObject */
	CK_IN  char*,						/* szMode */
	CK_OUT CK_VOID_PTR_PTR					/* fHandle */
);
typedef CK_METHOD(void, KFS_WRITE)				/* Writes data to opened file */
(
	CK_IN KFILE_SYSTEM_STR*,				/* self */
	CK_IN CK_VOID_PTR,					/* fHandle */
	CK_IN CK_BYTE_PTR,					/* pData */
	CK_IN CK_ULONG						/* ulData */
);
typedef CK_METHOD(void, KFS_CLOSE)				/* Closes opened file */
(
	CK_IN KFILE_SYSTEM_STR*,				/* self */
	CK_IN CK_VOID_PTR						/* fHandle */
);
struct KFILE_SYSTEM_STR
{
	CK_VOID_PTR		pSlot;				/* Any object */
	KFS_SPEC_FOLDER	get_folder;				/* Gets special folder name */
	KFS_EXISTS		exists;				/* Checks if file exists */
	KFS_LIST		list;					/* Iterates through system folder */
	KFS_LOAD		load;					/* Loads object */
	KSF_LOADPATH	load_path;				/* Loads object from path */
	KFS_STORE		store;				/* Stores object */
	KFS_STOREPATH	store_path;				/* Stores path to file system */
	KFS_EXISTS		erase;				/* Deletes object */
	KFS_OPEN		open;					/* Opens file for write */
	KFS_WRITE		write;				/* Writes data into file */
	KFS_CLOSE		close;				/* Closes file */
};
typedef KFILE_SYSTEM_STR*					KFILE_SYSTEM;

#if defined(_UX)
typedef struct KUX_FOLDER_STR
{
	KSPECIAL_FOLDER	iFolder;				/* Special folder id */
	char			szFolder[CK_FILENAME_MAX];	/* Folder location */

} KUX_FOLDER_STR, *KUX_FOLDER;
KHASH_MAP_INIT_INT(KFMAP, KUX_FOLDER)
typedef struct KFUX_FILE_SYSTEM_STR
{
	char			szAuth[CK_FILENAME_MAX];	/* Path to authorization token */
	khash_t(KFMAP)*	kFolders;				/* Map of special folders */

} KFUX_FILE_SYSTEM_STR, *KFUX_FILE_SYSTEM;

#elif defined(_DROID)
/* TODO: Android implementation */
#endif


/**
 * @brief Log and other debug facilities
 * 
 */
KHASH_MAP_INIT_INT64(KOSET, CK_OBJECT_HANDLE)		/* Object handles hash set */
typedef struct KLOG_HANDLER_STR				KLOG_HANDLER_STR;
typedef CK_METHOD(CK_RV, KS_INIT_MUTEX)			/* Initializes mutex device */
(
	CK_INOUT KLOG_HANDLER_STR*,				/* self */
	CK_IN KMUTEX_HANDLER					/* hToClone */
);
typedef CK_METHOD(void, KS_LOG)				/* Logs message */
(
	CK_IN KLOG_HANDLER_STR*,				/* self */
	CK_IN char*,						/* szMsg */
	...								/* any */
);
typedef CK_METHOD(void, KS_LOG_TEMPLATE)			/* Logs PKCS #11 attributes */
(
	CK_IN KLOG_HANDLER_STR*,				/* self */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG,						/* ulCount */
	CK_IN char*,						/*szMsg */
	CK_IN CK_BBOOL						/* printValue */
);
typedef CK_METHOD(void, KS_LOG_HANDLES)			/* Logs set of PKCS #11 handles */
(
	CK_IN KLOG_HANDLER_STR*,				/* self */
	CK_IN char*,						/* szMsg */
	CK_IN khash_t(KOSET)*					/* kSet */
);
typedef CK_METHOD(void, KS_LOG_OBJECTS)			/* Logs list of PKCS #11 object handles */
(
	CK_IN KLOG_HANDLER_STR*,				/* self */
	CK_IN char*,						/* szMsg */
	CK_IN CK_OBJECT_HANDLE_PTR,				/* phObject */
	CK_IN CK_ULONG						/* ulObjectCount */
);
typedef CK_METHOD(void, KS_LOG_END)				/* Finalizes log system. It releases only file system  resources */
(
	CK_INOUT KLOG_HANDLER_STR*				/* self */
);
struct KLOG_HANDLER_STR
{
	CK_VOID_PTR			pMutex;			/* The mutex itself */
	KMUTEX_HANDLER		hMutex;			/* Mutex facility */
	CK_VOID_PTR			fHandle;			/* Handle to log file */
	KFILE_SYSTEM		hFS;				/* Virtual file system */

	KS_INIT_MUTEX		init_mutex;
	KS_LOG			log;				/* Basic log function */
	KS_LOG_TEMPLATE		log_template;		/* Logs a PKCS #11 template contents */
	KS_LOG_HANDLES		log_handles;		/* Logs PKCS #11 handles */
	KS_LOG_OBJECTS		log_objects;		/* Logs PKCS #11 objects handles list */
};
typedef KLOG_HANDLER_STR*					KLOG_HANDLER;

 #include <openssl/err.h>
#include <errno.h>
#ifdef _UNIX
#define __FUNCTION__						__func__
#define TEXT(_t)							(_t)
#endif
#define LOG_IS_ON							TEXT("1")
#define ENV_LOG_ENTRY						TEXT("KIRIPEMA_LOG")
extern KLOG_HANDLER __pLog;
#define KENTER(_f)						{ __pLog->log(__pLog, "%s: called", _f); }
#define KRETURN(_ret)						{ CK_RV _rv = _ret; __pLog->log(__pLog, "0x%08x returned", _rv); return _rv; }
#define KASSERT(_cond, _err, _ret)				{ if (!(_cond)) { __pLog->log(__pLog, _err); KRETURN(_ret); }}
#define KAPIASSERT(_cond, _f, _ret)				{ if (!(_cond)) { __pLog->log(__pLog, "System API error 0x%08x", _f); KRETURN(_ret); }}
#define NHARU_ASSERT(_rv, _f)					{ if (_rv != NH_OK) __pLog->log(__pLog, "Nharu %s function error code 0x%08x", _f, _rv); }
#define OPENSSL_ASSERT(_cond, _f)				{ if (!(_cond)) { __pLog->log(__pLog, "OpenSSL error code %lu on function %s", ERR_get_error(), _f); }}
#ifdef _WINDOWS
#define WINAPI_ASSERT(_cond, _f)				{ if ((_cond)) __pLog->log(__pLog, "%s function returned following error: 0x%08x\n", _f, GetLastError()); }
#else
#define OS_ASSERT(_rv, _f)					{ if ((_rv) != 0) __pLog->log(__pLog, "OS error code %d returned on function %s", _rv, _f); }
#endif
#define IO_ASSERT(_rv, _f)					{ if (_rv != 0) __pLog->log(__pLog, "OS I/O error %d occurrend on function %s", errno, _f); }

#ifdef _DEBUG
int saveToFile							(unsigned char*, size_t, char*);
void kprintASNTree						(NH_ASN1_PNODE, int);
#endif
typedef NH_BLOB							KP_BLOB;
typedef CK_VOID_PTR						KP_OBJ_HANDLER;


/**
 * @brief Cryptographic facility
 * 
 */
#ifdef _UNIX
#include <pthread.h>
#endif
typedef struct KSESSION_STR					KSESSION_STR;
typedef struct KOBJECT_STR					KOBJECT_STR;
typedef struct KTOKEN_STR					KTOKEN_STR;
typedef struct KC_MECHANISM_STR
{
	CK_MECHANISM_TYPE		ulType;
	CK_MECHANISM_INFO		info;

} KC_MECHANISM_STR, *KC_MECHANISM;
KHASH_MAP_INIT_INT64(KMMAP, KC_MECHANISM)

typedef struct KCRYPTO_HANDLER_STR				KCRYPTO_HANDLER_STR;
typedef CK_METHOD(CK_RV, KC_LIST)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_OUT CK_MECHANISM_TYPE_PTR,				/* pMechanismList */
	CK_INOUT CK_ULONG_PTR					/* pulCount */
);
typedef CK_METHOD(CK_RV, KC_INFO)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_IN CK_MECHANISM_TYPE,				/* ulMechanism */
	CK_INOUT CK_MECHANISM_INFO_PTR			/* pInfo */
);
typedef CK_METHOD(CK_BBOOL, KC_IS_ALLOWED)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_IN CK_MECHANISM_TYPE					/* ulMechanism */
);
typedef CK_METHOD(CK_RV, KC_SIGN_INIT)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_INOUT KSESSION_STR*,					/* pSession */
	CK_INOUT KOBJECT_STR*,					/* pObject */
	CK_IN CK_MECHANISM_TYPE					/* ulMechanism */
);
typedef CK_METHOD(CK_RV, KC_SIGN)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_INOUT KSESSION_STR*,					/* pSession */
	CK_IN CK_BYTE_PTR,					/* pData */
	CK_IN CK_ULONG,						/* ulDataLen */
	CK_OUT CK_BYTE_PTR,					/* pSignature */
	CK_INOUT CK_ULONG_PTR					/* pulSignatureLen */
);
typedef CK_METHOD(CK_RV, KC_KEYPAIR_TEMPLATE)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_IN CK_MECHANISM_TYPE,				/* ulType */
	CK_IN CK_ATTRIBUTE_PTR,					/* pPublicKeyTemplate */
	CK_IN CK_ULONG,						/* ulPublicKeyAttributeCount */
	CK_IN CK_ATTRIBUTE_PTR,					/* pPrivateKeyTemplate */
	CK_IN CK_ULONG						/* ulPrivateKeyAttributeCount */
);
typedef CK_METHOD(CK_RV, KC_KEYPAIR_PARAMS)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG,						/* ulCount */
	CK_OUT CK_ULONG_PTR,					/* pulBits */
	CK_OUT CK_ULONG_PTR					/* pulExponent */
);
typedef CK_METHOD(CK_RV, KC_GEN_KEYPAIR)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_IN CK_ULONG,						/* ulBits */
	CK_IN CK_ULONG,						/* ulPublicExponent */
	CK_IN CK_BBOOL,						/* isSessionObject */
	CK_OUT KOBJECT_STR**,					/* pPubKey */
	CK_OUT KOBJECT_STR**					/* pPrivKey */
);
#ifdef _UNIX
typedef CK_METHOD(CK_RV, KC_RAND)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_OUT unsigned char*,					/* pBuffer */
	CK_IN int							/* iSize */
);
typedef CK_METHOD(CK_RV, KC_PBE)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_IN KP_BLOB*,						/* pPin */
	CK_IN KP_BLOB*,						/* pSalt */
	CK_OUT unsigned char*,					/* pBuffer */
	CK_IN int							/* iSize */
);
typedef CK_METHOD(CK_RV, KC_CIPHER)
(
	CK_IN KCRYPTO_HANDLER_STR*,				/* self */
	CK_IN CK_BYTE_PTR,					/* pKey */
	CK_IN CK_BYTE_PTR,					/* pIV */
	CK_IN CK_BYTE_PTR,					/* pData */
	CK_IN CK_ULONG,						/* ulData*/
	CK_OUT CK_BYTE_PTR*,					/* ppOut */
	CK_OUT CK_ULONG_PTR					/* pulOut */
);
#endif
struct KCRYPTO_HANDLER_STR
{
	khash_t(KMMAP)*		kMechanisms;		/* Supported mechanisms */
	KTOKEN_STR*			pToken;			/* Token reference (for encryption) */

	KC_LIST			list;				/* List all supported mechanisms */
	KC_INFO			info;				/* Gets specified mechanism info */
	KC_IS_ALLOWED		is_allowed;			/* Checks if specified mechanism is supported */
	KC_SIGN_INIT		sign_init;			/* Inits signature */
	KC_SIGN			sign;				/* Signs */
	KC_KEYPAIR_TEMPLATE	keypair_templates;	/* Checks key pair generation templates */
	KC_KEYPAIR_PARAMS		keypair_parameters;	/* Gets key pair generation parameters from template */
	KC_GEN_KEYPAIR		generate_keypair;		/* Generates specified key pair */

#ifdef _UNIX
	pthread_cond_t*		pCondition;			/* Condition to signal OpenSSL initialization */
	CK_BBOOL			isInited;			/* PRNG initialialization signal */
	CK_VOID_PTR			pMutex;			/* The mutex itself */
	KMUTEX_HANDLER		hMutex;			/* Mutex facility */

	KC_RAND			rand;
	KC_PBE			pbe_derive;
	KC_CIPHER			encrypt;
	KC_CIPHER			decrypt;
#endif
};
typedef KCRYPTO_HANDLER_STR*					KCRYPTO_HANDLER;
extern CK_MECHANISM_TYPE __pSignMechanisms[];
#define KP_SIGN_MECHANISMS_COUNT				1
#ifdef _UNIX
#define KCKM_SIGN_MECHANISMS					1
#define PBE_SALT_LEN						8
#define PBE_IV_LEN						8
#define PBE_KEY_LEN						24
#define PBE_CIPHER_MECHANISM					EVP_des_ede3_cbc()
#define PBE_ITERATION_COUNT					1000
#endif


/**
 * @brief PKCS #11 objects
 * 
 */
KHASH_MAP_INIT_INT64(KOMAP, KOBJECT_STR*)			/* <CK_OBJECT_HANDLE, KOBJECT> hash map  */
typedef CK_METHOD(CK_BBOOL, KO_BFUNCTION)
(
	CK_IN KOBJECT_STR*,					/* self */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG,						/* ulCount */
	CK_IN KP_BLOB*						/* pPin */
);
typedef CK_METHOD(CK_RV, KO_RFUNCTION)
(
	CK_INOUT KOBJECT_STR*,					/* self */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG,						/* ulCount */
	CK_IN KP_BLOB*						/* pPin */
);
typedef CK_METHOD(CK_RV, KO_CREATE)
(
	CK_INOUT KOBJECT_STR*,					/* self */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG,						/* ulCount */
	CK_IN KTOKEN_STR*						/* pToken */
);
struct KOBJECT_STR
{
	CK_OBJECT_HANDLE		hObject;			/* Object handle */
	CK_OBJECT_CLASS		hClass;			/* Object class */
	KP_BLOB			hash;				/* Object hash */
	KP_OBJ_HANDLER		hPersistence;		/* Object loader (platform dependent) */
#ifdef _UNIX
	CK_VOID_PTR			pMutex;			/* The mutex itself */
	KMUTEX_HANDLER		hMutex;			/* Mutex facility */
#endif
	KO_BFUNCTION		match;			/* Object matcher (against template) */
	KO_RFUNCTION		get_value;			/* Object attributes getter */
	KO_CREATE			create;			/* Object creator (from template). May not be implemented */
};
typedef KOBJECT_STR*						KOBJECT;
typedef struct  KOBJECT_CACHE_STR				KOBJECT_CACHE_STR;
typedef struct KOB_ITERATOR_STR
{
	int				iMatch;
	khiter_t			k;

} KOB_ITERATOR_STR, *KOB_ITERATOR;
typedef enum KOC_LOADED
{
	KOC_CACHE_EMPTY,
	KOC_PUBLIC_OBJECTS,
	KOC_PRIVATE_OBJECTS

} KOC_LOADED;

typedef CK_METHOD(CK_RV, KOC_LOAD)
(
	CK_INOUT KOBJECT_CACHE_STR*,				/* self */
	KTOKEN_STR*,						/* pToken */
	CK_IN KOC_LOADED						/* status */
);
typedef CK_METHOD(CK_RV, KOC_CLEAR)
(
	CK_INOUT KOBJECT_CACHE_STR*				/* self */
);
typedef CK_METHOD(CK_RV, KOC_ADD)
(
	CK_INOUT KOBJECT_CACHE_STR*,				/* self */
	CK_IN KOBJECT						/* pObject */
);
typedef CK_METHOD(CK_RV, KOC_REMOVE)
(
	CK_INOUT KOBJECT_CACHE_STR*,				/* self */
	CK_IN CK_OBJECT_HANDLE					/* hObject */
);
typedef CK_METHOD(KOBJECT, KOC_GET)
(
	CK_INOUT KOBJECT_CACHE_STR*,				/* self */
	CK_IN CK_OBJECT_HANDLE					/* hObject */
);
typedef CK_METHOD(CK_RV, KOC_ITER)
(
	CK_INOUT KOBJECT_CACHE_STR*,				/* self */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG,						/* ulCount */
	CK_INOUT KOB_ITERATOR					/* hIt */
);
typedef CK_METHOD(KOBJECT, KOC_NEXT)
(
	CK_IN KOBJECT_CACHE_STR*,				/* self */
	CK_INOUT KOB_ITERATOR					/* hIt */
);
struct KOBJECT_CACHE_STR					/* Objects long term objects cache interface */
{
	CK_VOID_PTR			pMutex;			/* The mutex itself */
	KMUTEX_HANDLER		hMutex;			/* Mutex facility */
	khash_t(KOMAP)*		kObjects;			/* The cache itself */
	KOC_LOADED			loaded;			/* Cache status */

	KOC_LOAD			load;				/* Loads all objects from file system */
	KOC_CLEAR			clear;			/* Clears cache and destroys all objects */
	KOC_ADD			add;				/* Adds a new object to cache */
	KOC_REMOVE			remove;			/* Removes (and destroys) object from cache */
	KOC_GET			get;				/* Gets specified object */
	KOC_ITER			iterator;			/* Initializes cache iterator */
	KOC_NEXT			next;				/* Gets next object or NULL if iterator is empty */
};
typedef KOBJECT_CACHE_STR*					KOBJECT_CACHE;



/**
 * @brief PKCS #11 sessions
 * 
 */
typedef struct KOBJECT_SEARCH_STR
{
	khash_t(KOSET)*		kSearch;			/* Found objects */
	khiter_t			k;				/* Current object to return */

} KOBJECT_SEARCH_STR, *KOBJECT_SEARCH;

typedef struct KCRYPTO_STR					/* Cryptographic operation */
{
	CK_MECHANISM_TYPE		hMechanism;			/* Mechanism */
	KOBJECT			hKey;				/* Object */
} KCRYPTO;
typedef struct KOPERATION_STR					/* Active operation */
{
	KOBJECT_SEARCH		pSearch;			/* C_FindObjects operation */
	KCRYPTO			kCrypto;			/* Cryptographic operation */

} KOPERATION;
typedef CK_METHOD(CK_STATE, KS_GET_STATE)
(
	CK_IN KSESSION_STR*,					/* self */
	CK_IN CK_BBOOL						/* isLogged */
);
typedef CK_METHOD(CK_RV, KS_INIT_CRYPTO)
(
	CK_INOUT KSESSION_STR*,					/* self */
	CK_IN CK_MECHANISM_TYPE,				/* ulMechanism */
	CK_IN KOBJECT						/* pObject */
);
typedef CK_METHOD(CK_RV, KS_OPERATION)
(
	CK_INOUT KSESSION_STR*					/* self */
);
typedef CK_METHOD(CK_RV, KS_NEXT)
(
	CK_IN KSESSION_STR*,					/* self */
	CK_OUT CK_OBJECT_HANDLE_PTR				/* phNext */
);
typedef CK_METHOD(CK_RV, KS_FOUND)
(
	CK_IN KSESSION_STR*,					/* self */
	CK_OUT CK_OBJECT_HANDLE					/* hNext */
);
struct KSESSION_STR
{
	CK_VOID_PTR			pMutex;			/* The mutex itself */
	KMUTEX_HANDLER		hMutex;			/* Mutex facility */
	khash_t(KOSET)*		kObjects;			/* Session objects */
	CK_SESSION_HANDLE		hHandle;			/* Session handle */
	CK_BBOOL			isRW;				/* R/W session indicator */
	CK_ULONG			ulDeviceError;		/* Last error */
	KOPERATION			Operation;			/* Active operation */

	KS_GET_STATE		get_state;			/* Session state */
	KS_INIT_CRYPTO		init_crypto;		/* Starts a cryptographic operation */
	KS_OPERATION		finish_crypto;		/* Finishes cryptographic operation */
	KS_OPERATION		init_search;		/* Starts a search object operation */
	KS_NEXT			next_object;		/* Gets next object in search resultset */
	KS_FOUND			found_object;		/* Adds object to search resultset */
	KS_OPERATION		finish_search;		/* Finishes search operation */
};
typedef KSESSION_STR*						KSESSION;
KHASH_MAP_INIT_INT64(KSMAP, KSESSION)			/* <CK_SESSION_HANDLE, KSESSION> hash map */

typedef struct KSESSION_HANDLER_STR				KSESSION_HANDLER_STR;
typedef CK_METHOD(CK_BBOOL, KS_CHECK)
(
	CK_IN KSESSION_HANDLER_STR*,				/* self */
	CK_IN CK_SESSION_HANDLE					/* hSession */
);
typedef CK_METHOD(KSESSION, KS_GET_SESSION)
(
	CK_IN KSESSION_HANDLER_STR*,				/* self */
	CK_IN CK_SESSION_HANDLE					/* hSession */
);
typedef CK_METHOD(CK_RV, KS_NEW_SESSION)
(
	CK_INOUT KSESSION_HANDLER_STR*,			/* self */
	CK_IN CK_BBOOL,						/* isRW */
	CK_OUT CK_SESSION_HANDLE_PTR				/* phSession */
);
typedef CK_METHOD(CK_RV, KS_DEL_SESSION)
(
	CK_IN KSESSION_HANDLER_STR*,				/* self */
	CK_IN CK_SESSION_HANDLE					/* hSession */
);
typedef CK_METHOD(CK_RV, KS_CLEAR)
(
	CK_IN KSESSION_HANDLER_STR*				/* self */
);
typedef CK_METHOD(CK_RV, KS_ALT_OBJECT)
(
	CK_IN KSESSION_HANDLER_STR*,				/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_OBJECT_HANDLE					/* hObject */
);
struct KSESSION_HANDLER_STR
{
	CK_VOID_PTR			pMutex;			/* The mutex itself */
	KMUTEX_HANDLER		hMutex;			/* Mutex facility */
	CK_SESSION_HANDLE		hSessionCount;		/* Session handle counter */
	khash_t(KSMAP)*		kSessions;			/* Sessions map */

	KS_CHECK			is_open;			/* Checks if handle is valid session handle */
	KS_GET_SESSION		get_session;		/* Returns session object specified by handle */
	KS_NEW_SESSION		new_session;		/* Creates a new session */
	KS_DEL_SESSION		delete_session;		/* Destroys session specified by handle */
	KS_CLEAR			delete_all;			/* Destroys all sessions */

	KS_ALT_OBJECT		add_object;			/* Stores specified object handle as a session object */
	KS_ALT_OBJECT		remove_object;		/* Destroys session object handle */
	KS_CLEAR			remove_all_objects;	/* Destroys all session objects */

	KS_CHECK			search_is_active;		/* Checks if a search is already active */
	KS_CHECK			crypto_is_active;		/* Checks if a cryptographic operation is active */
};
typedef KSESSION_HANDLER_STR*					KSESSION_HANDLER;



/**
 * @brief PKCS #11 token
 * 
 */
#ifdef _UNIX
#define KP_AUTH_TOKEN		"auth.tok"			/* Authentication token file name */
#define KP_CERT_ID		".crt"			/* PKCS #11 certificate object file identifier */
#define KP_PUBKEY_ID		".pub"			/* PKCS #11 public key object file identifier */
#define KP_PRIVKEY_ID		".prv"			/* PKCS #11 private key object file identifier */
#define KP_KEY_ID			".key"			/* PKCS #11 secret key object file identifier */
#endif
typedef CK_METHOD(CK_RV, KT_SLOT_LIST)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_OUT CK_SLOT_ID_PTR,					/* pSlotList */
	CK_INOUT CK_ULONG_PTR					/* pulCount */
);
typedef CK_METHOD(CK_RV, KT_SLOT_INFO)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SLOT_ID,						/* slotID */
	CK_INOUT CK_SLOT_INFO_PTR				/* pInfo */
);
typedef CK_METHOD(CK_RV, KT_TOKEN_INFO)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SLOT_ID,						/* slotID */
	CK_INOUT CK_TOKEN_INFO_PTR				/* pInfo */
);
typedef CK_METHOD(CK_RV, KT_SLOT_EVENT)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_FLAGS,						/* flags */
	CK_IN CK_SLOT_ID_PTR					/* pSlot */
);
typedef CK_METHOD(CK_RV, KT_MECHANISM_LIST)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SLOT_ID,						/* slotID */
	CK_OUT CK_MECHANISM_TYPE_PTR,				/* pMechanismList */
	CK_INOUT CK_ULONG_PTR					/* pulCount */
);
typedef CK_METHOD(CK_RV, KT_MECHANISM_INFO)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SLOT_ID,						/* slotID */
	CK_IN CK_MECHANISM_TYPE,				/* type */
	CK_INOUT CK_MECHANISM_INFO_PTR			/* pInfo */
);
typedef CK_METHOD(CK_RV, KT_INIT_PIN)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_UTF8CHAR_PTR,					/* pPin */
	CK_IN CK_ULONG						/* ulPinLen */
);
typedef CK_METHOD(CK_RV, KT_SET_PIN)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_UTF8CHAR_PTR,					/* pOldPin */
	CK_IN CK_ULONG,						/* ulOldLen */
	CK_IN CK_UTF8CHAR_PTR,					/* pNewPin */
	CK_IN CK_ULONG						/* ulNewLen */
);
typedef CK_METHOD(CK_RV, KT_OPEN_SESSION)(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SLOT_ID,						/* slotID */
	CK_IN CK_FLAGS,						/* flags */
	CK_IN CK_VOID_PTR,					/* pApplication */
	CK_IN CK_NOTIFY,						/* Notify */
	CK_OUT CK_SESSION_HANDLE_PTR				/* phSession */
);
typedef CK_METHOD(CK_RV, KT_CLOSE_SESSION)(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE					/* hSession */
);
typedef CK_METHOD(CK_RV, KT_CLOSE_ALL_SESSIONS)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SLOT_ID						/* slotID */
);
typedef CK_METHOD(CK_RV, KT_SESSION_INFO)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_OUT CK_SESSION_INFO_PTR				/* pInfo */
);
typedef CK_METHOD(CK_RV, KT_LOGIN)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_UTF8CHAR_PTR,					/* pPin */
	CK_IN CK_ULONG						/* ulPinLen */
);
typedef CK_METHOD(CK_RV, KT_LOGOUT)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE					/* hSession */
);
typedef CK_METHOD(CK_RV, KT_CREATE_OBJECT)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG,						/* ulCount */
	CK_OUT CK_OBJECT_HANDLE_PTR				/* phObject */
);
typedef CK_METHOD(CK_RV, KT_FIND_INIT)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG						/* ulCount */
);
typedef CK_METHOD(CK_RV, KT_FIND)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_INOUT CK_OBJECT_HANDLE_PTR,			/* phObject */
	CK_IN CK_ULONG,						/* ulMaxObjectCount */
	CK_IN CK_ULONG_PTR					/* pulObjectCount */
);
typedef CK_METHOD(CK_RV, KT_FIND_FINAL)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE					/* hSession */
);
typedef CK_METHOD(CK_RV, KT_GET_ATTRIBUTE_VALUE)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_OBJECT_HANDLE,					/* hObject */
	CK_INOUT CK_ATTRIBUTE_PTR,				/* pTemplate */
	CK_IN CK_ULONG						/* ulCount */
);
typedef CK_METHOD(CK_RV, KT_SIGN_INIT)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_MECHANISM_PTR,					/* pMechanism */
	CK_IN CK_OBJECT_HANDLE					/* hKey */
);
typedef CK_METHOD(CK_RV, KT_SIGN)
(
	CK_IN KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_BYTE_PTR,					/* pData */
	CK_IN CK_ULONG,						/* ulDataLen */
	CK_OUT CK_BYTE_PTR,					/* pSignature */
	CK_INOUT CK_ULONG_PTR					/* pulSignatureLen */
);
typedef CK_METHOD(CK_RV, KT_DESTROY_OBJECT)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_OBJECT_HANDLE					/* hObject */
);
typedef CK_METHOD(CK_RV, KT_GEN_KEY_PAIR)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_MECHANISM_TYPE,				/* ulType */
	CK_IN CK_ATTRIBUTE_PTR,					/* pPublicKeyTemplate */
	CK_IN CK_ULONG,						/* ulPublicKeyAttributeCount */
	CK_IN CK_ATTRIBUTE_PTR,					/* pPrivateKeyTemplate */
	CK_IN CK_ULONG,						/* ulPrivateKeyAttributeCount */
	CK_OUT CK_OBJECT_HANDLE_PTR,				/* phPublicKey */
	CK_OUT CK_OBJECT_HANDLE_PTR				/* phPrivateKey */
);
typedef CK_METHOD(CK_RV, KT_SET_ATTRIBUTE_VALUE)
(

	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_SESSION_HANDLE,				/* hSession */
	CK_IN CK_OBJECT_HANDLE,					/* hObject */
	CK_IN CK_ATTRIBUTE_PTR,					/* pTemplate */
	CK_IN CK_ULONG						/* ulCount */
);
typedef CK_METHOD(CK_RV, KT_GET_HANDLE)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_OUT CK_ULONG_PTR					/* phHandle */
);
typedef CK_METHOD(CK_RV, KT_COUNT_SESSION)
(
	CK_INOUT KTOKEN_STR*,					/* self */
	CK_IN CK_BBOOL						/* isRW */
);

struct KTOKEN_STR
{
	CK_VOID_PTR			pMutex;			/* The mutex object */
	KMUTEX_HANDLER		hMutex;			/* Mutex facility */
	KCRYPTO_HANDLER		hCrypto;			/* Cryptographic facility */
	CK_OBJECT_HANDLE		ulHandleCount;		/* Handles counter */
	CK_ULONG			ulSessionCount;		/* Opened sessions */
	CK_ULONG			ulRwSessionCount;		/* Opened R/W sessions */
	KP_BLOB			pin;				/* User PIN (if logged) */
	KFILE_SYSTEM		pFS;				/* PKCS #11 objects persistence layer */
	KSESSION_HANDLER		hSessions;			/* Sessions handler */
	KOBJECT_CACHE		hCache;			/* Objects cache */

	KT_SLOT_LIST		slot_list;			/* C_GetSlotList implementation */
	KT_SLOT_INFO		slot_info;			/* C_GetSlotInfo implementation */
	KT_TOKEN_INFO		token_info;			/* C_GetTokenInfo implementation */
	KT_SLOT_EVENT		wait;				/* C_WaitForSlotEvent implementation */
	KT_MECHANISM_LIST		mechanism_list;		/* C_GetMechanismList implementation */
	KT_MECHANISM_INFO		mechanism_info;		/* C_GetMechanismInfo implementation */
#ifdef _UNIX
	KT_INIT_PIN			init_pin;			/* C_InitPIN implementation */
	KT_SET_PIN			set_pin;			/* C_SetPIN implementation */
#endif
	KT_OPEN_SESSION		open_session;		/* C_OpenSession implementation */
	KT_CLOSE_SESSION		close_session;		/* C_CloseSession implementation */
	KT_CLOSE_ALL_SESSIONS	close_all_sessions;	/* C_CloseAllSessions implementation */
	KT_SESSION_INFO		session_info;		/* C_GetSessionInfo */
	KT_LOGIN			login;			/* C_Login implementation */
	KT_LOGOUT			logout;			/* C_Logout implementation */
#ifdef _UNIX
	KT_CREATE_OBJECT		create_object;		/* C_CreateObject implementation */
#endif
	KT_DESTROY_OBJECT		destroy_object;		/* C_DestroyObject implementation (may not be implemented) */
	KT_FIND_INIT		find_objects_init;	/* C_FindObjects implementation */
	KT_FIND			find_objects;		/* C_FindObjects implementation */
	KT_FIND_FINAL		find_objects_final;	/* C_FindObjectsFinal implementation */
	KT_GET_ATTRIBUTE_VALUE	get_attribute_value;	/* C_GetAttributeValue implementation */
#ifdef _UNIX
	KT_SET_ATTRIBUTE_VALUE	set_attribute_value;	/* C_SetAttributeValue implementation */
	KT_GEN_KEY_PAIR		generate_keypair;		/* C_GenerateKeyPair implementation */
#endif
	KT_SIGN_INIT		sign_init;			/* C_SignInit implementation */
	KT_SIGN			sign;				/* C_Sign */

	KT_GET_HANDLE		get_handle;			/* Gets a new handle */
	KT_COUNT_SESSION		inc_session;		/* Increments session counter */
	KT_COUNT_SESSION		dec_session;		/* Decrements session counter */
};
typedef KTOKEN_STR*						KTOKEN;


/**
 * @brief Facilities
 * 
 */
CK_NEW(CK_new_mutex_handler)					/* Creates a new mutex handler */
(
	CK_IN CK_C_INITIALIZE_ARGS_PTR,			/* pArgs */
	CK_OUT KMUTEX_HANDLER*					/* pOut */
);
CK_DELETE(CK_release_mutex_handler)				/* Releases mutex handler */
(
	CK_INOUT KMUTEX_HANDLER					/* hHandler */
);
CK_NEW(CK_new_log)						/* Creates a new Log object */
(
	CK_IN CK_BBOOL,						/* isOn */
	CK_OUT KLOG_HANDLER*					/* hHandler */
);
CK_DELETE(CK_delete_log)					/* Releases log object */
(
	CK_INOUT KLOG_HANDLER					/* hHandler */
);
#if defined(_UX)
CK_NEW(__new_ux_fs)						/* Creates a new Unix virtual file system */
(
	CK_OUT KFILE_SYSTEM*					/* hFS */
);
CK_DELETE(__release_us_fs)					/* Releases Unix virtual file system */
(
	CK_INOUT KFILE_SYSTEM					/* hFS */
);
#define CKO_new_fs						__new_ux_fs
#define CKO_release_fs						__release_us_fs
#elif defined(_WINDOWS)
CK_NEW(__new_win_fs)						/* Creates a new Windows virtual file system */
(
	CK_OUT KFILE_SYSTEM*					/* hFS */
);
CK_DELETE(__delete_win_fs)					/* Releases Windows virtual file system */
(
	CK_INOUT KFILE_SYSTEM					/* hFS */
);
#define CKO_new_fs						__new_win_fs
#define CKO_release_fs						__delete_win_fs
#endif
CK_NEW(CK_new_object_cache)					/* Creates a new object cache */
(
	CK_IN KMUTEX_HANDLER,					/* pMutex */
	CK_OUT KOBJECT_CACHE*					/* hCache */
);
CK_DELETE(CK_release_object_cache)				/* Releases object cache */
(
	CK_INOUT KOBJECT_CACHE					/* hCache */
);
CK_NEW(CK_new_crypto)						/* Creates a new cryptographic handler */
(
	KTOKEN_STR*,						/* pToken */
	CK_OUT KCRYPTO_HANDLER*					/* hOut */
);
CK_DELETE(CK_release_crypto)					/* Releases cryptographic handler */
(
	CK_INOUT KCRYPTO_HANDLER pHandler			/* pHandler */
);
CK_NEW(CK_new_session_handler)
(
	CK_IN KMUTEX_HANDLER,					/* pMutex */
	CK_OUT KSESSION_HANDLER*				/* hOut */
);
CK_DELETE(CK_release_session_handler)
(
	CK_INOUT KSESSION_HANDLER				/* hHandler */
);

CK_NEW(CK_new_token)						/* Initializes token resources */
(
	CK_C_INITIALIZE_ARGS_PTR,				/* pArgs */
	CK_OUT KTOKEN*						/* pToken */
);
CK_DELETE(CK_release_token)					/* Releases token resources */
(
	CK_INOUT KTOKEN						/* pToken */
);
CK_FUNCTION(char*, CK_bin2hex)				/* Gets hexadecimal string representation of specified binary data */
(
	CK_IN unsigned char*,					/* bin */
	CK_IN CK_ULONG						/* len */
);

#endif  /* __KIRIPEMA_H__ */