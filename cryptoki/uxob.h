/**
 * @file uxob.h
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Unix Cryptographic Services PKCS#11 implementation 
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#ifndef __UXOB_H__
#define __UXOB_H__

#include "tokenio.h"

#ifdef _UNIX
#define UXOBJ_UUID_LEN		41					/* Object filename length */
KHASH_MAP_INIT_STR(KFSET, char*)
typedef struct KUX_OBJECT_STR						/* Unix object persistence loader */
{
	char				szFilename[UXOBJ_UUID_LEN];	/* Filename in objects repository. Must be NULL if session object */
	KP_BLOB			encoding;				/* Object DER encoding */
	KP_OBJ_HANDLER		hEncoder;				/* Encoding handler (according to object class) */
	KP_OBJ_HANDLER		hParser;				/* Parsing handler (according to object class) */
	KRSAKEY_HANDLER		hRSA;					/* RSA private key handler */
	KP_BLOB			material;				/* RSA key encoded material */
	NH_RSA_PRIVKEY_HANDLER	key;					/* OpenSSL version of private keys */

} KUX_OBJECT_STR, *KUX_OBJECT;
#define UX_LOADER(_p)		((KUX_OBJECT)_p->hPersistence)/* Object typecast shortcut */


typedef CK_CALLBACK_FUNCTION(CK_RV, CKO_READDIR)
(
	CK_IN char*,							/* Target directory */
	CK_IN char*,							/* Existing filename */
	CK_VOID_PTR								/* Callback user argument */
);
CK_NEW(CKO_uxnew_object)
(										/* Creates an object from template */
	CK_IN KTOKEN_STR*,						/* pToken */
	CK_IN CK_ATTRIBUTE_PTR,						/* pTemplate */
	CK_IN CK_ULONG,							/* ulCount */
	CK_INOUT KOBJECT*							/* hOut */
);
CK_NEW(CKO_uxload_object)
(										/* Loads an object from persistence */
	CK_IN KTOKEN,							/* pToken */
	CK_IN CK_BYTE_PTR,						/* pEncoding */
	CK_IN CK_ULONG,							/* ulSize */
	CK_OUT KOBJECT*							/* hOut */
);
CK_DELETE(CKO_uxdelete_object)
(										/* Releases object */
	CK_INOUT KOBJECT							/* hHandler */
);
CK_FUNCTION(CK_OBJECT_CLASS, CKO_get_class)
(										/* Gets object class, if any */
	CK_IN CK_ATTRIBUTE_PTR,						/* pTemplate */
	CK_IN CK_ULONG							/* ulCount */
);
CK_FUNCTION(CK_RV, CKO_willbe_session_object)
(										/* Checks if object is not CKA_TOKEN */
	CK_IN CK_ATTRIBUTE_PTR,						/* pTemplate */
	CK_IN CK_ULONG,							/* ulCount */
	CK_OUT CK_BBOOL*							/* pIs */
);
CK_FUNCTION(CK_RV, CKO_check_create)
(										/* Validates template for object creation */
	CK_IN CK_OBJECT_CLASS,						/* ulClass */
	CK_IN CK_MECHANISM_TYPE,					/* ulType */
	CK_IN CK_ATTRIBUTE_PTR,						/* pTemplate */
	CK_IN CK_ULONG							/* ulCount */
);
CK_FUNCTION(CK_RV, CKO_set_attributes)
(										/* Sets object attributes values */
	CK_INOUT KOBJECT,							/* pObject */
	CK_IN CK_ATTRIBUTE_PTR,						/* pTemplate */
	CK_IN CK_ULONG,							/* ulCount */
	CK_INOUT KTOKEN_STR*						/* pToken */
);
CK_FUNCTION(CK_BBOOL, CKO_is_modifiable)
(										/* Check if object is modifiable */
	CK_IN KOBJECT 							/* pObject */
);
CK_FUNCTION(CK_BBOOL, CKO_is_session_object)
(										/* Check if object is a session object */
	CK_IN KSESSION, 							/* pSession */
	CK_IN CK_OBJECT_HANDLE						/* hObject */
);
CK_FUNCTION(CK_RV, CKO_check_set)
(										/* Validates template for object set */
	CK_IN KOBJECT,							/* pObject */
	CK_IN CK_ATTRIBUTE_PTR,						/* pTemplate */
	CK_ULONG								/* ulCount */
);
CK_FUNCTION(CK_RV, CKO_encode_object)
(										/* Shortcut to ASN.1 encoding */
	CK_IN NH_ASN1_ENCODER_HANDLE,					/* hEncoder */
	CK_INOUT KP_BLOB*							/* pOut */
);
#define CKO_delete_object						CKO_uxdelete_object

#endif /* _UNIX */
#endif /* __UXOB_H__ */