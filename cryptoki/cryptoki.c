/**
 * @file cryptoki.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Multiplatform PKCS #11 softoken implementation 
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kiripema.h"



/*
 * General-purpose functions
 */
static KTOKEN __pToken		= NULL;
#define IS_INITED()		(__pToken && __pLog)
CK_IMPLEMENTATION(CK_RV, C_Initialize)(CK_VOID_PTR pInitArgs)
{
	CK_RV rv;
	CK_C_INITIALIZE_ARGS_PTR args = (CK_C_INITIALIZE_ARGS_PTR) pInitArgs;

	KENTER(__FUNCTION__);
	KASSERT(!IS_INITED(), "Invalid function call", CKR_CRYPTOKI_ALREADY_INITIALIZED);
	if (args)
	{
		CK_BBOOL bSupplied = (args->CreateMutex && args->DestroyMutex && args->LockMutex && args->UnlockMutex);
		KASSERT(!args->pReserved && ((!args->CreateMutex && !args->DestroyMutex && !args->LockMutex&& !args->UnlockMutex) || bSupplied), "Invalid function call", CKR_ARGUMENTS_BAD);
		__pLog->log(__pLog, "Mutex supplied: %d; flags value: %lu", bSupplied, args->flags);
		if (!(!(args->flags & CKF_OS_LOCKING_OK) && bSupplied)) args = NULL;
	}
	if ((rv = CK_new_token(args, &__pToken)) == CKR_OK) rv = __pLog->init_mutex(__pLog, __pToken->hMutex);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_Finalize)(CK_VOID_PTR pReserved)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(!pReserved, "Invalid function call: pReserved is not NULL", CKR_ARGUMENTS_BAD);
	CK_release_token(__pToken);
	__pToken = NULL;
	KRETURN(CKR_OK);
}
static CK_INFO __kLibInfo = 
{
	{
		CRYPTOKI_VERSION_MAJOR,
		CRYPTOKI_VERSION_MINOR
	},
	MANUFACTURER_ID,
	0UL,
	LIBRARY_DESCRIPTION,
	{
		LIBRARY_VERSION_MAJOR,
		LIBRARY_VERSION_MINOR
	}
};
CK_IMPLEMENTATION(CK_RV, C_GetInfo)(CK_INFO_PTR pInfo)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pInfo, "Invalid function call: pInfo is NULL", CKR_ARGUMENTS_BAD);
	memcpy(pInfo, &__kLibInfo, sizeof(CK_INFO));
	KRETURN(CKR_OK);
}
CK_IMPLEMENTATION(CK_RV, ___C_GetFunctionList)(CK_FUNCTION_LIST_PTR_PTR ppFunctionList) { return C_GetFunctionList(ppFunctionList); }

/**
 * Slot and token management functions
 */
CK_IMPLEMENTATION(CK_RV, C_GetSlotList)(CK_UNUSED CK_BBOOL tokenPresent, CK_SLOT_ID_PTR pSlotList, CK_ULONG_PTR pulCount)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pulCount, "Invalid function call: pulCount is NULL", CKR_ARGUMENTS_BAD);
	rv = __pToken->slot_list(__pToken, pSlotList, pulCount);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_GetSlotInfo)(CK_SLOT_ID slotID, CK_SLOT_INFO_PTR pInfo)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pInfo, "Invalid function call: pInfo is NULL", CKR_ARGUMENTS_BAD);
	rv = __pToken->slot_info(__pToken, slotID, pInfo);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_GetTokenInfo)(CK_SLOT_ID slotID, CK_TOKEN_INFO_PTR pInfo)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pInfo, "Invalid function call: pInfo is NULL", CKR_ARGUMENTS_BAD);
	rv = __pToken->token_info(__pToken, slotID, pInfo);
	__pLog->log(__pLog, "Session count: %lu; RW session count: %lu; flags: %lu", pInfo->ulSessionCount, pInfo->ulRwSessionCount, pInfo->flags);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_WaitForSlotEvent)(CK_FLAGS flags, CK_SLOT_ID_PTR pSlot, CK_VOID_PTR pRserved)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(!pRserved && pSlot, "Invalid function call: arguments invalid", CKR_ARGUMENTS_BAD);
	rv = __pToken->wait(__pToken, flags, pSlot);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_GetMechanismList)(CK_SLOT_ID slotID, CK_MECHANISM_TYPE_PTR pMechanismList, CK_ULONG_PTR pulCount)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pulCount, "Invalid function call: pulCount is NULL", CKR_ARGUMENTS_BAD);
	rv = __pToken->mechanism_list(__pToken, slotID, pMechanismList, pulCount);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_GetMechanismInfo)(CK_SLOT_ID slotID, CK_MECHANISM_TYPE type, CK_MECHANISM_INFO_PTR pInfo)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pInfo, "Invalid function call: pInfo is NULL", CKR_ARGUMENTS_BAD);
	rv = __pToken->mechanism_info(__pToken, slotID, type, pInfo);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_InitToken)(CK_UNUSED CK_SLOT_ID slotID, CK_UNUSED CK_UTF8CHAR_PTR pPin, CK_UNUSED CK_ULONG ulPinLen, CK_UNUSED CK_UTF8CHAR_PTR pLabel)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}

/**
 * Session management functions
 */
CK_IMPLEMENTATION(CK_RV, C_OpenSession)(CK_SLOT_ID slotID, CK_FLAGS flags, CK_VOID_PTR pApplication, CK_NOTIFY Notify, CK_SESSION_HANDLE_PTR phSession)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(phSession, "Invalid function call: phSession is NULL", CKR_ARGUMENTS_BAD);
	KASSERT(flags & CKF_SERIAL_SESSION, "Invalid function call: CKF_SERIAL_SESSION flag not set", CKR_SESSION_PARALLEL_NOT_SUPPORTED);
	__pLog->log(__pLog, "Flags argument: %lu; pApplication: %s; Notify: %s", flags, pApplication ? "not NULL" : "NULL", Notify ? "not NULL" : "NULL");
	rv = __pToken->open_session(__pToken, slotID, flags, pApplication, Notify, phSession);
	if (rv == CKR_OK) __pLog->log(__pLog, "Session handle created: %lu", *phSession);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_CloseSession)(CK_SESSION_HANDLE hSession)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	__pLog->log(__pLog, "Session handle received: %lu", hSession);
	rv = __pToken->close_session(__pToken, hSession);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_CloseAllSessions)(CK_SLOT_ID slotID)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	rv = __pToken->close_all_sessions(__pToken, slotID);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_GetSessionInfo)(CK_SESSION_HANDLE hSession, CK_SESSION_INFO_PTR pInfo)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pInfo, "Invalid function call: pInfo is NULL", CKR_ARGUMENTS_BAD);
	__pLog->log(__pLog, "Session handle received: %lu", hSession);
	rv = __pToken->session_info(__pToken, hSession, pInfo);
	if (rv == CKR_OK) __pLog->log(__pLog, "Session state: %lu; flags: %lu; device error: %lu", pInfo->state, pInfo->flags, pInfo->ulDeviceError);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_GetOperationState)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pOperationState, CK_UNUSED CK_ULONG_PTR pulOperationStateLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_SetOperationState)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pOperationState, CK_UNUSED CK_ULONG ulOperationStateLen, CK_UNUSED CK_OBJECT_HANDLE hEncryptionKey, CK_UNUSED CK_OBJECT_HANDLE hAuthenticationKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_InitPIN)(CK_SESSION_HANDLE hSession, CK_UTF8CHAR_PTR pPin, CK_ULONG ulPinLen)
{
	CK_RV rv = CKR_FUNCTION_NOT_SUPPORTED;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
#ifdef _UNIX
	KASSERT(pPin, "Invalid function call: pPin is NULL", CKR_ARGUMENTS_BAD);
	KASSERT(ulPinLen >= MIN_PIN_LEN && ulPinLen <= MAX_PIN_LEN, "Invalid function call: ulPinLen out of range", CKR_PIN_LEN_RANGE);
	rv = __pToken->init_pin(__pToken, hSession, pPin, ulPinLen);
#endif
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_SetPIN)(CK_SESSION_HANDLE hSession, CK_UTF8CHAR_PTR pOldPin, CK_ULONG ulOldLen, CK_UTF8CHAR_PTR pNewPin, CK_ULONG ulNewLen)
{
	CK_RV rv = CKR_FUNCTION_NOT_SUPPORTED;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
#ifdef _UNIX
	KASSERT(pOldPin && pNewPin, "Invalid function call: arguments must not be NULL", CKR_ARGUMENTS_BAD);
	KASSERT(ulNewLen >= MIN_PIN_LEN && ulNewLen <= MAX_PIN_LEN, "Invalid function call: ulNewLen out of range", CKR_PIN_LEN_RANGE);
	rv = __pToken->set_pin(__pToken, hSession, pOldPin, ulOldLen, pNewPin, ulNewLen);
#endif
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_Login)(CK_SESSION_HANDLE hSession, CK_USER_TYPE userType, CK_UTF8CHAR_PTR pPin, CK_ULONG ulPinLen)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(userType == CKU_USER, "Invalid function call: SO not supported", CKR_USER_TYPE_INVALID);
	KASSERT(pPin, "Invalid function call: pPin is NULL", CKR_ARGUMENTS_BAD);
	rv = __pToken->login(__pToken, hSession, pPin, ulPinLen);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_Logout)(CK_SESSION_HANDLE hSession)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	rv = __pToken->logout(__pToken, hSession);
	KRETURN(rv);
}


/*
 * Object management functions
 */
CK_IMPLEMENTATION(CK_RV, C_CreateObject)(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount, CK_OBJECT_HANDLE_PTR phObject)
{
	CK_RV rv = CKR_FUNCTION_NOT_SUPPORTED;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
#ifdef _UNIX
	KASSERT(pTemplate && phObject, "Invalid function call: arguments must not be NULL", CKR_ARGUMENTS_BAD);
	__pLog->log_template(__pLog, pTemplate, ulCount, "Object template", CK_TRUE);
	rv = __pToken->create_object(__pToken, hSession, pTemplate, ulCount, phObject);
	if (rv == CKR_OK) __pLog->log_objects(__pLog, "Created following object handle:", phObject, 1);
#endif
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_CopyObject)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_OBJECT_HANDLE hObject, CK_UNUSED CK_ATTRIBUTE_PTR pTemplate, CK_UNUSED CK_ULONG ulCount, CK_UNUSED CK_OBJECT_HANDLE_PTR phNewObject)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DestroyObject)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	rv = __pToken->destroy_object(__pToken, hSession, hObject);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_GetObjectSize)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_OBJECT_HANDLE hObject, CK_UNUSED CK_ULONG_PTR pulSize)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_GetAttributeValue)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pTemplate, "Invalid funciton call: pTemplate is NULL", CKR_ARGUMENTS_BAD);
	__pLog->log(__pLog, "Argument hSession received: %lu; argument hObject: %lu", hSession, hObject);
	__pLog->log_template(__pLog, pTemplate, ulCount, "Attributes to get", CK_FALSE);
	rv = __pToken->get_attribute_value(__pToken, hSession, hObject, pTemplate, ulCount);
	if (rv == CKR_OK) __pLog->log_template(__pLog, pTemplate, ulCount, "Attribute values found", CK_TRUE);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_SetAttributeValue)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	CK_RV rv = CKR_FUNCTION_NOT_SUPPORTED;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
#ifdef _UNIX
	KASSERT(pTemplate, "Invalid funciton call: pTemplate is NULL", CKR_ARGUMENTS_BAD);
	__pLog->log_template(__pLog, pTemplate, ulCount, "Attributes to get", CK_TRUE);
	rv = __pToken->set_attribute_value(__pToken, hSession, hObject, pTemplate, ulCount);
#endif
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_FindObjectsInit)(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	__pLog->log(__pLog, "Argument hSession received: %lu", hSession);
	__pLog->log_template(__pLog, pTemplate, ulCount, "Parameters template", CK_TRUE);
	rv = __pToken->find_objects_init(__pToken, hSession, pTemplate, ulCount);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_FindObjects)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE_PTR phObject, CK_ULONG ulMaxObjectCount, CK_ULONG_PTR pulObjectCount)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(phObject && pulObjectCount, "Invalid function call: out arguments not initialized", CKR_ARGUMENTS_BAD);
	__pLog->log(__pLog, "Argument hSession received: %lu", hSession);
	__pLog->log(__pLog, "Argument ulMaxObjectCount: %lu", ulMaxObjectCount);
	rv = __pToken->find_objects(__pToken, hSession, phObject, ulMaxObjectCount, pulObjectCount);
	if (rv == CKR_OK) __pLog->log_objects(__pLog, "Give back following object handles:", phObject, *pulObjectCount);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_FindObjectsFinal)(CK_SESSION_HANDLE hSession)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	rv = __pToken->find_objects_final(__pToken, hSession);
	KRETURN(rv);
}


/*
 * Symmetric cryptography functions
 */
CK_IMPLEMENTATION(CK_RV, C_EncryptInit)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_OBJECT_HANDLE hKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_Encrypt)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pData, CK_UNUSED CK_ULONG ulDataLen, CK_UNUSED CK_BYTE_PTR pEncryptedData, CK_UNUSED CK_ULONG_PTR pulEncryptedDataLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_EncryptUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG ulPartLen, CK_UNUSED CK_BYTE_PTR pEncryptedPart, CK_UNUSED CK_ULONG_PTR pulEncryptedPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_EncryptFinal)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pLastEncryptedPart, CK_UNUSED CK_ULONG_PTR pulLastEncryptedPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DecryptInit)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_OBJECT_HANDLE hKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_Decrypt)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pEncryptedData, CK_UNUSED CK_ULONG ulEncryptedDataLen, CK_UNUSED CK_BYTE_PTR pData, CK_UNUSED CK_ULONG_PTR pulDataLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DecryptUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pEncryptedPart, CK_UNUSED CK_ULONG ulEncryptedPartLen, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG_PTR pulPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DecryptFinal)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pLastPart, CK_UNUSED CK_ULONG_PTR pulLastPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}


/*
 * Message digesting functions
 */
CK_IMPLEMENTATION(CK_RV, C_DigestInit)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_Digest)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pData, CK_UNUSED CK_ULONG ulDataLen, CK_UNUSED CK_BYTE_PTR pDigest, CK_UNUSED CK_ULONG_PTR pulDigestLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DigestUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG ulPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DigestKey)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_OBJECT_HANDLE hKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DigestFinal)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pDigest, CK_UNUSED CK_ULONG_PTR pulDigestLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}


/**
 * Signing functions
 */
CK_IMPLEMENTATION(CK_RV, C_SignInit)(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
	CK_RV rv;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pMechanism, "Invalid function call: pMechanism cannot be NULL", CKR_ARGUMENTS_BAD);
	__pLog->log(__pLog, "Argument hSession: %lu; argument mechanism type: %lu; argument hKey: %lu", hSession, pMechanism->mechanism, hKey);
	rv = __pToken->sign_init(__pToken, hSession, pMechanism, hKey);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_Sign)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pSignature, CK_ULONG_PTR pulSignatureLen)
{
	CK_RV rv;
#ifdef _DEBUG
	char *szInput;
#endif
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KASSERT(pData && pulSignatureLen, "Invalid function call: pData and pulSignatureLen cannot be NULL", CKR_ARGUMENTS_BAD);
	__pLog->log(__pLog, "Argument hSession received: %lu", hSession);
#ifdef _DEBUG
	if ((szInput = CK_bin2hex(pData, ulDataLen)))
	{
		__pLog->log(__pLog, "Argument pData: %s", szInput);
		free(szInput);
	}
#endif
	rv = __pToken->sign(__pToken, hSession, pData, ulDataLen, pSignature, pulSignatureLen);
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_SignUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG ulPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_SignFinal)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pSignature, CK_UNUSED CK_ULONG_PTR pulSignatureLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_SignRecoverInit)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_OBJECT_HANDLE hKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_SignRecover)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pData, CK_UNUSED CK_ULONG ulDataLen, CK_UNUSED CK_BYTE_PTR pSignature, CK_UNUSED CK_ULONG_PTR pulSignatureLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}


/**
 * Verify functions
 */
CK_IMPLEMENTATION(CK_RV, C_VerifyInit)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_OBJECT_HANDLE hKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_Verify)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pData, CK_UNUSED CK_ULONG ulDataLen, CK_UNUSED CK_BYTE_PTR pSignature, CK_UNUSED CK_ULONG ulSignatureLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_VerifyUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG ulPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_VerifyFinal)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pSignature, CK_UNUSED CK_ULONG ulSignatureLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_VerifyRecoverInit)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_OBJECT_HANDLE hKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_VerifyRecover)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pSignature, CK_UNUSED CK_ULONG ulSignatureLen, CK_UNUSED CK_BYTE_PTR pData, CK_UNUSED CK_ULONG_PTR pulDataLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}


CK_IMPLEMENTATION(CK_RV, C_DigestEncryptUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG ulPartLen, CK_UNUSED CK_BYTE_PTR pEncryptedPart, CK_UNUSED CK_ULONG_PTR pulEncryptedPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DecryptDigestUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pEncryptedPart, CK_UNUSED CK_ULONG ulEncryptedPartLen, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG_PTR pulPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_SignEncryptUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_UNUSED CK_ULONG ulPartLen, CK_UNUSED CK_BYTE_PTR pEncryptedPart, CK_UNUSED CK_ULONG_PTR pulEncryptedPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DecryptVerifyUpdate)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pEncryptedPart, CK_UNUSED CK_ULONG ulEncryptedPartLen, CK_UNUSED CK_BYTE_PTR pPart, CK_UNUSED CK_ULONG_PTR pulPartLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_GenerateKey)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_ATTRIBUTE_PTR pTemplate, CK_UNUSED CK_ULONG ulCount, CK_UNUSED CK_OBJECT_HANDLE_PTR phKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_GenerateKeyPair)
(
	CK_SESSION_HANDLE hSession,
	CK_MECHANISM_PTR pMechanism,
	CK_ATTRIBUTE_PTR pPublicKeyTemplate,
	CK_ULONG ulPublicKeyAttributeCount,
	CK_ATTRIBUTE_PTR pPrivateKeyTemplate,
	CK_ULONG ulPrivateKeyAttributeCount,
	CK_OBJECT_HANDLE_PTR phPublicKey,
	CK_OBJECT_HANDLE_PTR phPrivateKey
)
{
	CK_RV rv = CKR_FUNCTION_NOT_SUPPORTED;
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
#ifdef _UNIX
	KASSERT
	(
		pMechanism && pPublicKeyTemplate && phPublicKey && phPrivateKey,
		"Invalid function call: pMechanism, pPublicKeyTemplate, phPublicKey and phPrivateKey cannot be NULL",
		CKR_ARGUMENTS_BAD
	);
	__pLog->log(__pLog, "Argument hSession: %lu; argument mechanism type: %lu", hSession, pMechanism->mechanism);
	__pLog->log_template(__pLog, pPublicKeyTemplate, ulPublicKeyAttributeCount, "Public key template argument", CK_TRUE);
	__pLog->log_template(__pLog, pPrivateKeyTemplate, ulPrivateKeyAttributeCount, "Private key template argument", CK_TRUE);
	rv = __pToken->generate_keypair
	(
		__pToken,
		hSession,
		pMechanism->mechanism,
		pPublicKeyTemplate,
		ulPublicKeyAttributeCount,
		pPrivateKeyTemplate,
		ulPrivateKeyAttributeCount,
		phPublicKey,
		phPrivateKey
	);
	if (rv == CKR_OK)
	{
		__pLog->log_objects(__pLog, "Generated public key object handle:", phPublicKey, 1);
		__pLog->log_objects(__pLog, "Generated private key object handle:", phPrivateKey, 1);
	}
#endif
	KRETURN(rv);
}
CK_IMPLEMENTATION(CK_RV, C_WrapKey)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR  pMechanism, CK_UNUSED CK_OBJECT_HANDLE  hWrappingKey, CK_UNUSED CK_OBJECT_HANDLE  hKey, CK_UNUSED CK_BYTE_PTR bpWrappedKey, CK_UNUSED CK_ULONG_PTR pulWrappedKeyLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_UnwrapKey)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_OBJECT_HANDLE hUnwrappingKey, CK_UNUSED CK_BYTE_PTR pWrappedKey, CK_UNUSED CK_ULONG ulWrappedKeyLen, CK_UNUSED CK_ATTRIBUTE_PTR pTemplate, CK_UNUSED CK_ULONG ulAttributeCount, CK_UNUSED CK_OBJECT_HANDLE_PTR phKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_DeriveKey)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_MECHANISM_PTR pMechanism, CK_UNUSED CK_OBJECT_HANDLE hBaseKey, CK_UNUSED CK_ATTRIBUTE_PTR pTemplate, CK_UNUSED CK_ULONG ulAttributeCount, CK_UNUSED CK_OBJECT_HANDLE_PTR phKey)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_SeedRandom)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR pSeed, CK_UNUSED CK_ULONG ulSeedLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_GenerateRandom)(CK_UNUSED CK_SESSION_HANDLE hSession, CK_UNUSED CK_BYTE_PTR RandomData, CK_UNUSED CK_ULONG ulRandomLen)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_SUPPORTED);
}
CK_IMPLEMENTATION(CK_RV, C_GetFunctionStatus)(CK_UNUSED CK_SESSION_HANDLE hSession)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_PARALLEL);
}
CK_IMPLEMENTATION(CK_RV, C_CancelFunction)(CK_UNUSED CK_SESSION_HANDLE hSession)
{
	KENTER(__FUNCTION__);
	KASSERT(IS_INITED(), "Invalid function call", CKR_CRYPTOKI_NOT_INITIALIZED);
	KRETURN(CKR_FUNCTION_NOT_PARALLEL);
}
static CK_FUNCTION_LIST __kfList = {
	{ CRYPTOKI_VERSION_MAJOR, CRYPTOKI_VERSION_MINOR },
	C_Initialize,
	C_Finalize,
	C_GetInfo,
	___C_GetFunctionList,
	C_GetSlotList,
	C_GetSlotInfo,
	C_GetTokenInfo,
	C_GetMechanismList,
	C_GetMechanismInfo,
	C_InitToken,
	C_InitPIN,
	C_SetPIN,
	C_OpenSession,
	C_CloseSession,
	C_CloseAllSessions,
	C_GetSessionInfo,
	C_GetOperationState,
	C_SetOperationState,
	C_Login,
	C_Logout,
	C_CreateObject,
	C_CopyObject,
	C_DestroyObject,
	C_GetObjectSize,
	C_GetAttributeValue,
	C_SetAttributeValue,
	C_FindObjectsInit,
	C_FindObjects,
	C_FindObjectsFinal,
	C_EncryptInit,
	C_Encrypt,
	C_EncryptUpdate,
	C_EncryptFinal,
	C_DecryptInit,
	C_Decrypt,
	C_DecryptUpdate,
	C_DecryptFinal,
	C_DigestInit,
	C_Digest,
	C_DigestUpdate,
	C_DigestKey,
	C_DigestFinal,
	C_SignInit,
	C_Sign,
	C_SignUpdate,
	C_SignFinal,
	C_SignRecoverInit,
	C_SignRecover,
	C_VerifyInit,
	C_Verify,
	C_VerifyUpdate,
	C_VerifyFinal,
	C_VerifyRecoverInit,
	C_VerifyRecover,
	C_DigestEncryptUpdate,
	C_DecryptDigestUpdate,
	C_SignEncryptUpdate,
	C_DecryptVerifyUpdate,
	C_GenerateKey,
	C_GenerateKeyPair,
	C_WrapKey,
	C_UnwrapKey,
	C_DeriveKey,
	C_SeedRandom,
	C_GenerateRandom,
	C_GetFunctionStatus,
	C_CancelFunction,
	C_WaitForSlotEvent
};
CK_DECLARE_FUNCTION(CK_RV, C_GetFunctionList)(CK_FUNCTION_LIST_PTR_PTR ppFunctionList)
{
	CK_RV rv;
	if ((rv = ppFunctionList ? CKR_OK : CKR_ARGUMENTS_BAD) == CKR_OK) *ppFunctionList = &__kfList;
	return rv;
}