/**
 * @file uxobject.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Unix Cryptographic Services PKCS#11 implementation 
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "uxob.h"
#ifdef _UNIX
#include <assert.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <libgen.h>

#define __SEARCH_ONLY_SESSION_OBJECT	(1)						/* Look for session objects */
#define __SEARCH_ONLY_TOKEN_OBJECT		(__SEARCH_ONLY_SESSION_OBJECT << 1)	/* Look for token objects */
#define __SEARCH_TOKEN_SESSION_OBJECTS	(__SEARCH_ONLY_SESSION_OBJECT << 2)	/* Look for token and session objects */
#define __SEARCH_CERT_OBJECT			(__SEARCH_ONLY_SESSION_OBJECT << 3)	/* Look for certificates */
#define __SEARCH_PUBKEY_OBJECT		(__SEARCH_ONLY_SESSION_OBJECT << 4)	/* Look for public keys */
#define __SEARCH_PRIVKEY_OBJECT		(__SEARCH_ONLY_SESSION_OBJECT << 5)	/* Look for private keys */
#define __SEARCH_KEY_OBJECT			(__SEARCH_ONLY_SESSION_OBJECT << 6)	/* Look for secret keys */
#define __SEARCH_ALL_OBJECTS			(0x0F)

CK_INLINE CK_IMPLEMENTATION(CK_KEY_TYPE, __get_key_type)(CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_KEY_TYPE ulRet = CK_UNAVAILABLE_INFORMATION;
	CK_ULONG i = 0UL;
	CK_BBOOL found = CK_FALSE;
	while (!found && i < ulCount)
	{
		if (pTemplate[i].type == CKA_KEY_TYPE)
		{
			found = CK_TRUE;
			if (pTemplate[i].ulValueLen == sizeof(CK_KEY_TYPE))
			{
				ulRet = *(CK_KEY_TYPE*) pTemplate[i].pValue;
				switch (ulRet)
				{
				case CKK_RSA:
					break;
				default:
					ulRet = CK_UNAVAILABLE_INFORMATION;
				}
			}
		}
		i++;
	}
	return ulRet;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __new_ux_object)
(
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_OBJECT_CLASS ulClass,
	CK_IN KTOKEN_STR *pToken,
	CK_INOUT KUX_OBJECT *hObject
)
{
	CK_RV rv;
	KUX_OBJECT hUX;
	KX509CERT_ENCODER hCertEncoder = NULL;
	KX09CERT_PARSER hCertParser = NULL;
	KRSAPUBKEY_ENCODER hPubEncoder = NULL;
	KRSAPUBKEY_PARSER hPubParser = NULL;
	KRSAPRIVKEY_ENCODER hPrivEncoder = NULL;
	KRSAPRIVKEY_PARSER hPrivParser = NULL;
	KRSAKEY_HANDLER hRSA = NULL;

	assert(ulClass != CK_UNAVAILABLE_INFORMATION);
	if ((rv = (hUX = (KUX_OBJECT) malloc(sizeof(KUX_OBJECT_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memset(hUX, 0, sizeof(KUX_OBJECT_STR));
		switch (ulClass)
		{
		case CKO_CERTIFICATE:
			if
			(
				(rv = CKIO_new_certificate_encoder(&hCertEncoder)) == CKR_OK &&
				(rv = hCertEncoder->from(hCertEncoder, pTemplate, ulCount)) == CKR_OK &&
				(rv = CKO_encode_object(hCertEncoder->hEncoder, &hUX->encoding)) == CKR_OK &&
				(rv = CKIO_new_certificate_parser(hUX->encoding.data, hUX->encoding.length, &hCertParser)) == CKR_OK
			)
			{
				hUX->hEncoder = hCertEncoder;
				hUX->hParser = hCertParser;
			}
			if(rv != CKR_OK)
			{
				if (hCertEncoder) CKIO_release_certificate_encoder(hCertEncoder);
				if (hCertParser) CKIO_release_certificate_parser(hCertParser);
				if (hUX->encoding.data) free (hUX->encoding.data);
			}
			break;
		case CKO_PUBLIC_KEY:
			if
			(
				(rv = __get_key_type(pTemplate, ulCount) == CKK_RSA ? CKR_OK : CKR_TEMPLATE_INCOMPLETE) == CKR_OK &&
				(rv = CKIO_new_rsapubkey_encoder(&hPubEncoder)) == CKR_OK &&
				(rv = hPubEncoder->from(hPubEncoder, pTemplate, ulCount)) == CKR_OK &&
				(rv = CKO_encode_object(hPubEncoder->hEncoder, &hUX->encoding)) == CKR_OK &&
				(rv = CKIO_new_rsapubkey_parser(hUX->encoding.data, hUX->encoding.length, &hPubParser)) == CKR_OK
			)
			{
				hUX->hEncoder = hPubEncoder;
				hUX->hParser = hPubParser;
			}
			if(rv != CKR_OK)
			{
				if (hPubEncoder) CKIO_release_rsapubkey_encoder(hPubEncoder);
				if (hPubParser) CKIO_release_rsapubkey_parser(hPubParser);
				if (hUX->encoding.data) free (hUX->encoding.data);
			}
			break;
		case CKO_PRIVATE_KEY:
			if
			(
				(rv = __get_key_type(pTemplate, ulCount) == CKK_RSA ? CKR_OK : CKR_TEMPLATE_INCOMPLETE) == CKR_OK &&
				(rv = CKIO_new_rsaprivkey_encoder(&hPrivEncoder)) == CKR_OK &&
				(rv = hPrivEncoder->from(hPrivEncoder, pToken, pTemplate, ulCount)) == CKR_OK &&
				(rv = CKO_encode_object(hPrivEncoder->hEncoder, &hUX->encoding)) == CKR_OK &&
				(rv = CKIO_new_rsaprivkey_parser(hUX->encoding.data, hUX->encoding.length, &hPrivParser)) == CKR_OK &&
				(rv = hPrivParser->get_sensitive_material(hPrivParser, pToken, &hUX->material.data, &hUX->material.length)) == CKR_OK
			)
			{
				if
				(
					(rv = CKIO_new_rsa_handler(&hRSA)) == CKR_OK &&
					(rv = hRSA->parse(hRSA, hUX->material.data, hUX->material.length)) == CKR_OK
				)
				{
					hUX->hEncoder = hPrivEncoder;
					hUX->hParser = hPrivParser;
					hUX->hRSA = hRSA;
				}
			}
			if(rv != CKR_OK)
			{
				if (hPrivEncoder) CKIO_release_rsaprivkey_encoder(hPrivEncoder);
				if (hPrivParser) CKIO_release_rsaprivkey_parser(hPrivParser);
				if (hUX->encoding.data) free (hUX->encoding.data);
				if (hUX->material.data)
				{
					OPENSSL_cleanse(hUX->material.data, hUX->material.length);
					free(hUX->material.data);
				}
				if (hRSA) CKIO_release_rsa_handler(hRSA);
			}
			break;
		default: rv = CKR_TEMPLATE_INCOMPLETE;
		}
		if (rv == CKR_OK) *hObject = hUX;
		else free(hUX);
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(void, __release_ux_object)(CK_INOUT KUX_OBJECT pUX, CK_IN CK_OBJECT_CLASS ulClass)
{
	if (pUX)
	{
		switch (ulClass)
		{
		case CKO_CERTIFICATE:
			if (pUX->hEncoder) CKIO_release_certificate_encoder((KX509CERT_ENCODER) pUX->hEncoder);
			if (pUX->hParser) CKIO_release_certificate_parser((KX09CERT_PARSER) pUX->hParser);
			break;
		case CKO_PUBLIC_KEY:
			if (pUX->hEncoder) CKIO_release_rsapubkey_encoder((KRSAPUBKEY_ENCODER) pUX->hEncoder);
			if (pUX->hParser) CKIO_release_rsapubkey_parser((KRSAPUBKEY_PARSER) pUX->hParser);
			break;
		case CKO_PRIVATE_KEY:
			if (pUX->hEncoder) CKIO_release_rsaprivkey_encoder((KRSAPRIVKEY_ENCODER) pUX->hEncoder);
			if (pUX->hParser) CKIO_release_rsaprivkey_parser((KRSAPRIVKEY_PARSER) pUX->hParser);
			if (pUX->hRSA) CKIO_release_rsa_handler(pUX->hRSA);
			if (pUX->key) NH_release_RSA_privkey_handler(pUX->key);
			if (pUX->material.data)
			{
				OPENSSL_cleanse(pUX->material.data, pUX->material.length);
				free(pUX->material.data);
			}
		}
		if (pUX->encoding.data) free (pUX->encoding.data);
		free(pUX);
	}
}
CK_IMPLEMENTATION(CK_BBOOL, __match_ux_object)(CK_IN KOBJECT_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_IN KP_BLOB *pPin)
{
	KUX_OBJECT pUX = UX_LOADER(self);
	assert(pPin && pUX);
	switch (self->hClass)
	{
	case CKO_CERTIFICATE: return ((KX09CERT_PARSER) pUX->hParser)->match(((KX09CERT_PARSER) pUX->hParser), pTemplate, ulCount);
	case CKO_PUBLIC_KEY: return ((KRSAPUBKEY_PARSER) pUX->hParser)->match(((KRSAPUBKEY_PARSER) pUX->hParser), pTemplate, ulCount);
	case CKO_PRIVATE_KEY: return ((KRSAPRIVKEY_PARSER) pUX->hParser)->match(((KRSAPRIVKEY_PARSER) pUX->hParser), pUX->hRSA, pTemplate, ulCount, pPin);
	}
	return CK_FALSE;
}
CK_IMPLEMENTATION(CK_RV, __get_ux_object)(CK_INOUT KOBJECT_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_IN KP_BLOB *pPin)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	KUX_OBJECT pUX = UX_LOADER(self);
	assert(pTemplate && pPin && pUX);
	switch (self->hClass)
	{
	case CKO_CERTIFICATE: return ((KX09CERT_PARSER) pUX->hParser)->attributes(((KX09CERT_PARSER) pUX->hParser), pTemplate, ulCount);
	case CKO_PUBLIC_KEY: return ((KRSAPUBKEY_PARSER) pUX->hParser)->attributes(((KRSAPUBKEY_PARSER) pUX->hParser), pTemplate, ulCount);
	case CKO_PRIVATE_KEY: return ((KRSAPRIVKEY_PARSER) pUX->hParser)->attributes(((KRSAPRIVKEY_PARSER) pUX->hParser), pUX->hRSA, pTemplate, ulCount, pPin);
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __hash_object)(CK_IN KUX_OBJECT pObject, CK_IN CK_OBJECT_CLASS ulClass, CK_INOUT KP_BLOB *pOut)
{
	NH_RV rv;
	NH_HASH_HANDLER hHash;
	unsigned char *pHash ;
	size_t ulHash;

	assert(pObject && ulClass != CK_UNAVAILABLE_INFORMATION && pOut);
	if (NH_SUCCESS(rv = NH_new_hash(&hHash)))
	{
		if
		(
			NH_SUCCESS(rv = hHash->init(hHash, CKM_SHA_1)) &&
			NH_SUCCESS(rv = hHash->update(hHash, pObject->encoding.data, pObject->encoding.length)) &&
			NH_SUCCESS(rv = hHash->update(hHash, (unsigned char*) &ulClass, sizeof(CK_OBJECT_CLASS))) &&
			NH_SUCCESS(rv = hHash->finish(hHash, NULL, &ulHash)) &&
			NH_SUCCESS(rv = (pHash = (unsigned char*) malloc(ulHash)) ? NH_OK : NH_OUT_OF_MEMORY_ERROR)
		)
		{
			if (NH_SUCCESS(rv = hHash->finish(hHash, pHash, &ulHash)))
			{
				pOut->data = pHash;
				pOut->length = ulHash;
			}
			else free(pHash);
		}
		NH_release_hash(hHash);
	}
	NHARU_ASSERT(rv, "__hash_object");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_RV, __create_ux_object)
(
	CK_INOUT KOBJECT_STR *self,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_IN KTOKEN_STR *pToken
)
{
	CK_RV rv;
	CK_OBJECT_CLASS ulClass = CK_UNAVAILABLE_INFORMATION;
	KUX_OBJECT pObject = NULL;
	assert(pTemplate && pToken && !self->hPersistence && !self->hash.data);
	if
	(
		(rv = (ulClass = CKO_get_class(pTemplate, ulCount)) != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_TEMPLATE_INCOMPLETE) == CKR_OK &&
		(rv = __new_ux_object(pTemplate, ulCount, ulClass, pToken, &pObject)) == CKR_OK &&
		(rv = __hash_object(pObject, ulClass, &self->hash)) == CKR_OK
	)
	{
		self->hClass = ulClass;
		self->hPersistence = pObject;
	}
	else __release_ux_object(pObject, ulClass);
	return rv;
}
static KOBJECT_STR __default_ux_object =
{
	0UL,
	0UL,
	{ NULL, 0UL },
	NULL,
	NULL,
	NULL,

	__match_ux_object,
	__get_ux_object,
	__create_ux_object
};
CK_IMPLEMENTATION(CK_RV, __new_object)(CK_IN KTOKEN_STR *pToken, CK_OUT KOBJECT *pOut)
{
	CK_RV rv;
	KOBJECT pRet;

	assert(pToken);
	if ((rv = (pRet = (KOBJECT) malloc(sizeof(KOBJECT_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(pRet, &__default_ux_object, sizeof(KOBJECT_STR));
		if
		(
			(rv = pToken->hMutex->clone(pToken->hMutex, &pRet->hMutex)) == CKR_OK &&
			(rv = pRet->hMutex->CreateMutex(&pRet->pMutex)) == CKR_OK
		)	*pOut = pRet;
		if (rv != CKR_OK) free(pRet);
	}
	return rv;
}
CK_NEW(CKO_uxnew_object)(CK_IN KTOKEN_STR *pToken, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_INOUT KOBJECT *hOut)
{
	CK_RV rv;
	KOBJECT pRet;
	assert(pToken && pTemplate);
	if ((rv = __new_object(pToken, &pRet)) == CKR_OK)
	{
		if ((rv = pRet->create(pRet, pTemplate, ulCount, pToken)) == CKR_OK) *hOut = pRet;
		else CKO_uxdelete_object(pRet);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __template_from_cert)(CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulSize, CK_OUT CK_ATTRIBUTE_PTR *pTemplate, CK_OUT CK_ULONG_PTR pulCount)
{
	CK_RV rv;
	KX09CERT_PARSER hCert;
	NH_ASN1_PNODE pNode;
	CK_ULONG ulCount = 0, i = 0;
	CK_ATTRIBUTE_PTR pOut;

	if ((rv = CKIO_new_certificate_parser(pEncoding, ulSize, &hCert)) == CKR_OK)
	{
		/* StorageAttributes */
		if (ASN_IS_PARSED((pNode = hCert->get_class(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_token(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_private(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_modifiable(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_label(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_copyable(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_destroyable(hCert)))) ulCount++;

		if (ASN_IS_PARSED((pNode = hCert->get_type(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_trusted(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_category(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_check_value(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_start_date(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_end_date(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_pubkey(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_subject(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_id(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_issuer(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_serial_number(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_url(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_value(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_subject_pubkey(hCert)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hCert->get_issuer_pubkey(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_java_midp(hCert)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hCert->get_hash_algorithm(hCert)))) ulCount++;
	
		if ((rv = (pOut = (CK_ATTRIBUTE_PTR) malloc(ulCount * sizeof(CK_ATTRIBUTE))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			memset(pOut, 0, ulCount * sizeof(CK_ATTRIBUTE));

			/* StorageAttributes */
			if (ASN_IS_PARSED((pNode = hCert->get_class(hCert))))
			{
				pOut[i].type = CKA_CLASS;
				if
				(
					(rv = hCert->attr_class(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_class(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_token(hCert))))
			{
				pOut[i].type = CKA_TOKEN;
				if
				(
					(rv = hCert->attr_token(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_token(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_private(hCert))))
			{
				pOut[i].type = CKA_PRIVATE;
				if
				(
					(rv = hCert->attr_private(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_private(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_modifiable(hCert))))
			{
				pOut[i].type = CKA_MODIFIABLE;
				if
				(
					(rv = hCert->attr_modifiable(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_modifiable(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_label(hCert))))
			{
				pOut[i].type = CKA_LABEL;
				if
				(
					(rv = hCert->attr_label(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_label(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_copyable(hCert))))
			{
				pOut[i].type = CKA_COPYABLE;
				if
				(
					(rv = hCert->attr_copyable(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_copyable(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_destroyable(hCert))))
			{
				pOut[i].type = CKA_DESTROYABLE;
				if
				(
					(rv = hCert->attr_destroyable(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_destroyable(hCert, &pOut[i++]);
			}


			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_type(hCert))))
			{
				pOut[i].type = CKA_CERTIFICATE_TYPE;
				if
				(
					(rv = hCert->attr_type(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_type(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_trusted(hCert))))
			{
				pOut[i].type = CKA_TRUSTED;
				if
				(
					(rv = hCert->attr_trusted(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_trusted(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_category(hCert))))
			{
				pOut[i].type = CKA_CERTIFICATE_CATEGORY;
				if
				(
					(rv = hCert->attr_category(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_category(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_check_value(hCert))))
			{
				pOut[i].type = CKA_CHECK_VALUE;
				if
				(
					(rv = hCert->attr_check_value(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_check_value(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_start_date(hCert))))
			{
				pOut[i].type = CKA_START_DATE;
				if
				(
					(rv = hCert->attr_start_date(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_start_date(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_end_date(hCert))))
			{
				pOut[i].type = CKA_END_DATE;
				if
				(
					(rv = hCert->attr_end_date(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_end_date(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_pubkey(hCert))))
			{
				pOut[i].type = CKA_PUBLIC_KEY_INFO;
				if
				(
					(rv = hCert->attr_pubkey(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_pubkey(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_subject(hCert))))
			{
				pOut[i].type = CKA_SUBJECT;
				if
				(
					(rv = hCert->attr_subject(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_subject(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_id(hCert))))
			{
				pOut[i].type = CKA_ID;
				if
				(
					(rv = hCert->attr_id(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_id(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_issuer(hCert))))
			{
				pOut[i].type = CKA_ISSUER;
				if
				(
					(rv = hCert->attr_issuer(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_issuer(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_serial_number(hCert))))
			{
				pOut[i].type = CKA_SERIAL_NUMBER;
				if
				(
					(rv = hCert->attr_serial_number(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_serial_number(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_url(hCert))))
			{
				pOut[i].type = CKA_URL;
				if
				(
					(rv = hCert->attr_url(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_url(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_value(hCert))))
			{
				pOut[i].type = CKA_VALUE;
				if
				(
					(rv = hCert->attr_value(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_value(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_subject_pubkey(hCert))))
			{
				pOut[i].type = CKA_HASH_OF_SUBJECT_PUBLIC_KEY;
				if
				(
					(rv = hCert->attr_subject_pubkey(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_subject_pubkey(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hCert->get_issuer_pubkey(hCert))))
			{
				pOut[i].type = CKA_HASH_OF_ISSUER_PUBLIC_KEY;
				if
				(
					(rv = hCert->attr_issuer_pubkey(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_issuer_pubkey(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_java_midp(hCert))))
			{
				pOut[i].type = CKA_JAVA_MIDP_SECURITY_DOMAIN;
				if
				(
					(rv = hCert->attr_java_midp(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_java_midp(hCert, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hCert->get_hash_algorithm(hCert))))
			{
				pOut[i].type = CKA_NAME_HASH_ALGORITHM;
				if
				(
					(rv = hCert->attr_hash_algorithm(hCert, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hCert->attr_hash_algorithm(hCert, &pOut[i]);
			}

			if (rv == CKR_OK)
			{
				*pTemplate = pOut;
				*pulCount = ulCount;
			}
			else
			{
				for (i = 0; i < ulCount; i++) if (pOut[i].pValue) free(pOut[i].pValue);
				free(pOut);
			}
		}
		CKIO_release_certificate_parser(hCert);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __template_from_pubkey)(CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulSize, CK_OUT CK_ATTRIBUTE_PTR *pTemplate, CK_OUT CK_ULONG_PTR pulCount)
{
	CK_RV rv;
	KRSAPUBKEY_PARSER hPubKey;
	NH_ASN1_PNODE pNode;
	CK_ULONG ulCount = 0, i = 0;
	CK_ATTRIBUTE_PTR pOut;

	if ((rv = CKIO_new_rsapubkey_parser(pEncoding, ulSize, &hPubKey)) == CKR_OK)
	{
		/* StorageAttributes */
		if (ASN_IS_PARSED((pNode = hPubKey->get_class(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_token(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_private(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_modifiable(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_label(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_copyable(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_destroyable(hPubKey)))) ulCount++;

		/* KeyAttributes */
		if (ASN_IS_PARSED((pNode = hPubKey->get_keytype(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_id(hPubKey)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hPubKey->get_start_date(hPubKey)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hPubKey->get_end_date(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_derive(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_local(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_keyGenMech(hPubKey)))) ulCount++;
		if (ASN_IS_PRESENT((pNode = hPubKey->get_allowedMech(hPubKey)))) ulCount++;

		if (ASN_IS_PRESENT((pNode = hPubKey->get_subject(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_encrypt(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_verify(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_verifyRec(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_wrap(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_trusted(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_keyinfo(hPubKey)))) ulCount++;

		if (ASN_IS_PARSED((pNode = hPubKey->get_modulus(hPubKey)))) ulCount++;
		if (ASN_IS_PARSED((pNode = hPubKey->get_pubexponent(hPubKey)))) ulCount++;

		if ((rv = (pOut = (CK_ATTRIBUTE_PTR) malloc(ulCount * sizeof(CK_ATTRIBUTE))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			memset(pOut, 0, ulCount * sizeof(CK_ATTRIBUTE));

			/* StorageAttributes */
			if (ASN_IS_PARSED((pNode = hPubKey->get_class(hPubKey))))
			{
				pOut[i].type = CKA_CLASS;
				if
				(
					(rv = hPubKey->attr_class(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_class(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_token(hPubKey))))
			{
				pOut[i].type = CKA_TOKEN;
				if
				(
					(rv = hPubKey->attr_token(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_token(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_private(hPubKey))))
			{
				pOut[i].type = CKA_PRIVATE;
				if
				(
					(rv = hPubKey->attr_private(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_private(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_modifiable(hPubKey))))
			{
				pOut[i].type = CKA_MODIFIABLE;
				if
				(
					(rv = hPubKey->attr_modifiable(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_modifiable(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_label(hPubKey))))
			{
				pOut[i].type = CKA_LABEL;
				if
				(
					(rv = hPubKey->attr_label(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_label(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_copyable(hPubKey))))
			{
				pOut[i].type = CKA_COPYABLE;
				if
				(
					(rv = hPubKey->attr_copyable(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_copyable(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_destroyable(hPubKey))))
			{
				pOut[i].type = CKA_DESTROYABLE;
				if
				(
					(rv = hPubKey->attr_destroyable(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_destroyable(hPubKey, &pOut[i++]);
			}

			/* KeyAttributes */
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_keytype(hPubKey))))
			{
				pOut[i].type = CKA_KEY_TYPE;
				if
				(
					(rv = hPubKey->attr_keytype(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_keytype(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_id(hPubKey))))
			{
				pOut[i].type = CKA_ID;
				if
				(
					(rv = hPubKey->attr_id(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_id(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPubKey->get_start_date(hPubKey))))
			{
				pOut[i].type = CKA_START_DATE;
				if
				(
					(rv = hPubKey->attr_start_date(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_start_date(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPubKey->get_end_date(hPubKey))))
			{
				pOut[i].type = CKA_END_DATE;
				if
				(
					(rv = hPubKey->attr_end_date(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_end_date(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_derive(hPubKey))))
			{
				pOut[i].type = CKA_DERIVE;
				if
				(
					(rv = hPubKey->attr_derive(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_derive(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_local(hPubKey))))
			{
				pOut[i].type = CKA_LOCAL;
				if
				(
					(rv = hPubKey->attr_local(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_local(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_keyGenMech(hPubKey))))
			{
				pOut[i].type = CKA_KEY_GEN_MECHANISM;
				if
				(
					(rv = hPubKey->attr_keyGenMech(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_keyGenMech(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPubKey->get_allowedMech(hPubKey))))
			{
				pOut[i].type = CKA_ALLOWED_MECHANISMS;
				if
				(
					(rv = hPubKey->attr_allowedMech(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_allowedMech(hPubKey, &pOut[i++]);
			}

			if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPubKey->get_subject(hPubKey))))
			{
				pOut[i].type = CKA_SUBJECT;
				if
				(
					(rv = hPubKey->attr_subject(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_subject(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_encrypt(hPubKey))))
			{
				pOut[i].type = CKA_ENCRYPT;
				if
				(
					(rv = hPubKey->attr_encrypt(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_encrypt(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_verify(hPubKey))))
			{
				pOut[i].type = CKA_VERIFY;
				if
				(
					(rv = hPubKey->attr_verify(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_verify(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_verifyRec(hPubKey))))
			{
				pOut[i].type = CKA_VERIFY_RECOVER;
				if
				(
					(rv = hPubKey->attr_verifyRec(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_verifyRec(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_wrap(hPubKey))))
			{
				pOut[i].type = CKA_WRAP;
				if
				(
					(rv = hPubKey->attr_wrap(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_wrap(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_trusted(hPubKey))))
			{
				pOut[i].type = CKA_TRUSTED;
				if
				(
					(rv = hPubKey->attr_trusted(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_trusted(hPubKey, &pOut[i++]);
			}
			if (ASN_IS_PARSED((pNode = hPubKey->get_keyinfo(hPubKey))))
			{
				pOut[i].type = CKA_PUBLIC_KEY_INFO;
				if
				(
					(rv = hPubKey->attr_keyinfo(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_keyinfo(hPubKey, &pOut[i++]);
			}

			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_modulus(hPubKey))))
			{
				pOut[i].type = CKA_MODULUS;
				if
				(
					(rv = hPubKey->attr_modulus(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_modulus(hPubKey, &pOut[i++]);
			}
			if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPubKey->get_pubexponent(hPubKey))))
			{
				pOut[i].type = CKA_PUBLIC_EXPONENT;
				if
				(
					(rv = hPubKey->attr_pubexponent(hPubKey, &pOut[i])) == CKR_OK &&
					(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
				)	rv = hPubKey->attr_pubexponent(hPubKey, &pOut[i]);
			}

			if (rv == CKR_OK)
			{
				*pTemplate = pOut;
				*pulCount = ulCount;
			}
			else
			{
				for (i = 0; i < ulCount; i++) if (pOut[i].pValue) free(pOut[i].pValue);
				free(pOut);
			}
		}
		CKIO_release_rsapubkey_parser(hPubKey);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __template_from_privkey)
(
	CK_IN KTOKEN pToken,
	CK_IN CK_BYTE_PTR pEncoding,
	CK_IN CK_ULONG ulSize,
	CK_OUT CK_ATTRIBUTE_PTR *pTemplate,
	CK_OUT CK_ULONG_PTR pulCount
)
{
	CK_RV rv;
	KRSAPRIVKEY_PARSER hPrivKey;
	NH_ASN1_PNODE pNode;
	CK_ULONG ulCount = 0, i = 0;
	CK_ATTRIBUTE *pOut, pAttr = { 0UL, NULL, 0UL };
	KP_BLOB key = { NULL, 0 };
	KRSAKEY_HANDLER hKey = NULL;

	if ((rv = CKIO_new_rsaprivkey_parser(pEncoding, ulSize, &hPrivKey)) == CKR_OK)
	{
		if ((rv = hPrivKey->get_sensitive_material(hPrivKey, pToken, &key.data, &key.length)) == CKR_OK)
		{
			if ((rv = CKIO_new_rsa_handler(&hKey)) == CKR_OK)
			{
				rv = hKey->parse(hKey, key.data, key.length);

				/* StorageAttributes */
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_class(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_token(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_private(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_modifiable(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_label(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_copyable(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_destroyable(hPrivKey)))) ulCount++;

				/* KeyAttributes */
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_keytype(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_id(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_start_date(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_end_date(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_derive(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_local(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_keyGenMech(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_allowedMech(hPrivKey)))) ulCount++;

				if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_subject(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_sensitive(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_decrypt(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_sign(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_signRecover(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_unwrap(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_extractable(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_alwaysSensitive(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_neverExtractable(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_wrapWithTrusted(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_alwaysAuth(hPrivKey)))) ulCount++;
				if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_keyinfo(hPrivKey)))) ulCount++;

				pAttr.type = CKA_MODULUS;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;
				pAttr.type = CKA_PUBLIC_EXPONENT;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;
				pAttr.type = CKA_PRIVATE_EXPONENT;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;
				pAttr.type = CKA_PRIME_1;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;
				pAttr.type = CKA_PRIME_2;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;
				pAttr.type = CKA_EXPONENT_1;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;
				pAttr.type = CKA_EXPONENT_2;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;
				pAttr.type = CKA_COEFFICIENT;
				if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK) ulCount++;

				if (rv == CKR_OK && (rv = (pOut = (CK_ATTRIBUTE_PTR) malloc(ulCount * sizeof(CK_ATTRIBUTE))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
				{
					memset(pOut, 0, ulCount * sizeof(CK_ATTRIBUTE));

					/* StorageAttributes */
					if (ASN_IS_PARSED((pNode = hPrivKey->get_class(hPrivKey))))
					{
						pOut[i].type = CKA_CLASS;
						if
						(
							(rv = hPrivKey->attr_class(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_class(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_token(hPrivKey))))
					{
						pOut[i].type = CKA_TOKEN;
						if
						(
							(rv = hPrivKey->attr_token(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_token(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_private(hPrivKey))))
					{
						pOut[i].type = CKA_PRIVATE;
						if
						(
							(rv = hPrivKey->attr_private(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_private(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_modifiable(hPrivKey))))
					{
						pOut[i].type = CKA_MODIFIABLE;
						if
						(
							(rv = hPrivKey->attr_modifiable(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_modifiable(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_label(hPrivKey))))
					{
						pOut[i].type = CKA_LABEL;
						if
						(
							(rv = hPrivKey->attr_label(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_label(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_copyable(hPrivKey))))
					{
						pOut[i].type = CKA_COPYABLE;
						if
						(
							(rv = hPrivKey->attr_copyable(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_copyable(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_destroyable(hPrivKey))))
					{
						pOut[i].type = CKA_DESTROYABLE;
						if
						(
							(rv = hPrivKey->attr_destroyable(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_destroyable(hPrivKey, &pOut[i++]);
					}

					/* KeyAttributes */
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_keytype(hPrivKey))))
					{
						pOut[i].type = CKA_KEY_TYPE;
						if
						(
							(rv = hPrivKey->attr_keytype(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_keytype(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_id(hPrivKey))))
					{
						pOut[i].type = CKA_ID;
						if
						(
							(rv = hPrivKey->attr_id(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_id(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_start_date(hPrivKey))))
					{
						pOut[i].type = CKA_START_DATE;
						if
						(
							(rv = hPrivKey->attr_start_date(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_start_date(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_end_date(hPrivKey))))
					{
						pOut[i].type = CKA_END_DATE;
						if
						(
							(rv = hPrivKey->attr_end_date(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_end_date(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_derive(hPrivKey))))
					{
						pOut[i].type = CKA_DERIVE;
						if
						(
							(rv = hPrivKey->attr_derive(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_derive(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_local(hPrivKey))))
					{
						pOut[i].type = CKA_LOCAL;
						if
						(
							(rv = hPrivKey->attr_local(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_local(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_keyGenMech(hPrivKey))))
					{
						pOut[i].type = CKA_KEY_GEN_MECHANISM;
						if
						(
							(rv = hPrivKey->attr_keyGenMech(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_keyGenMech(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_allowedMech(hPrivKey))))
					{
						pOut[i].type = CKA_ALLOWED_MECHANISMS;
						if
						(
							(rv = hPrivKey->attr_allowedMech(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_allowedMech(hPrivKey, &pOut[i++]);
					}

					if (rv == CKR_OK && ASN_IS_PRESENT((pNode = hPrivKey->get_subject(hPrivKey))))
					{
						pOut[i].type = CKA_SUBJECT;
						if
						(
							(rv = hPrivKey->attr_subject(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_subject(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_sensitive(hPrivKey))))
					{
						pOut[i].type = CKA_SENSITIVE;
						if
						(
							(rv = hPrivKey->attr_sensitive(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_sensitive(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_decrypt(hPrivKey))))
					{
						pOut[i].type = CKA_DECRYPT;
						if
						(
							(rv = hPrivKey->attr_decrypt(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_decrypt(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_sign(hPrivKey))))
					{
						pOut[i].type = CKA_SIGN;
						if
						(
							(rv = hPrivKey->attr_sign(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_sign(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_signRecover(hPrivKey))))
					{
						pOut[i].type = CKA_SIGN_RECOVER;
						if
						(
							(rv = hPrivKey->attr_signRecover(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_signRecover(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_unwrap(hPrivKey))))
					{
						pOut[i].type = CKA_UNWRAP;
						if
						(
							(rv = hPrivKey->attr_unwrap(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_unwrap(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_extractable(hPrivKey))))
					{
						pOut[i].type = CKA_EXTRACTABLE;
						if
						(
							(rv = hPrivKey->attr_extractable(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_extractable(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_alwaysSensitive(hPrivKey))))
					{
						pOut[i].type = CKA_ALWAYS_SENSITIVE;
						if
						(
							(rv = hPrivKey->attr_alwaysSensitive(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_alwaysSensitive(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_neverExtractable(hPrivKey))))
					{
						pOut[i].type = CKA_NEVER_EXTRACTABLE;
						if
						(
							(rv = hPrivKey->attr_neverExtractable(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_neverExtractable(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_wrapWithTrusted(hPrivKey))))
					{
						pOut[i].type = CKA_WRAP_WITH_TRUSTED;
						if
						(
							(rv = hPrivKey->attr_wrapWithTrusted(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_wrapWithTrusted(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_alwaysAuth(hPrivKey))))
					{
						pOut[i].type = CKA_ALWAYS_AUTHENTICATE;
						if
						(
							(rv = hPrivKey->attr_alwaysAuth(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_alwaysAuth(hPrivKey, &pOut[i++]);
					}
					if (rv == CKR_OK && ASN_IS_PARSED((pNode = hPrivKey->get_keyinfo(hPrivKey))))
					{
						pOut[i].type = CKA_PUBLIC_KEY_INFO;
						if
						(
							(rv = hPrivKey->attr_keyinfo(hPrivKey, &pOut[i])) == CKR_OK &&
							(rv = (pOut[i].pValue = malloc(pOut[i].ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hPrivKey->attr_keyinfo(hPrivKey, &pOut[i++]);
					}

					pAttr.type = CKA_MODULUS;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_MODULUS;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}
					pAttr.type = CKA_PUBLIC_EXPONENT;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_PUBLIC_EXPONENT;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}
					pAttr.type = CKA_PRIVATE_EXPONENT;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_PRIVATE_EXPONENT;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}
					pAttr.type = CKA_PRIME_1;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_PRIME_1;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}
					pAttr.type = CKA_PRIME_2;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_PRIME_2;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}
					pAttr.type = CKA_EXPONENT_1;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_EXPONENT_1;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}
					pAttr.type = CKA_EXPONENT_2;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_EXPONENT_2;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}
					pAttr.type = CKA_COEFFICIENT;
					if (rv == CKR_OK && (rv = hKey->attribute(hKey, &pAttr)) == CKR_OK)
					{
						pOut[i].type = CKA_COEFFICIENT;
						pOut[i].ulValueLen = pAttr.ulValueLen;
						if
						(
							(rv = (pOut[i].pValue = malloc(pAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
						)	rv = hKey->attribute(hKey, &pOut[i++]);
					}

					if (rv == CKR_OK)
					{
						*pTemplate = pOut;
						*pulCount = ulCount;
					}
					else
					{
						for (i = 0; i < ulCount; i++) if (pOut[i].pValue) free(pOut[i].pValue);
						free(pOut);
					}
				}
				CKIO_release_rsa_handler(hKey);
			}
			OPENSSL_cleanse(key.data, key.length);
			free(key.data);
		}
		CKIO_release_rsaprivkey_parser(hPrivKey);
	}
	return rv;
}
CK_NEW(CKO_uxload_object)(CK_IN KTOKEN pToken, CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulSize, CK_OUT KOBJECT* hOut)
{
	CK_RV rv;
	CK_OBJECT_CLASS ulClass;
	CK_ATTRIBUTE_PTR pTemplate = NULL;
	CK_ULONG ulCount, i;
	KOBJECT pRet = NULL;

	assert(pToken && pEncoding);
	if ((rv = (ulClass = CKIO_discover(pEncoding, ulSize)) != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		switch (ulClass)
		{
		case CKO_CERTIFICATE:
			rv = __template_from_cert(pEncoding, ulSize, &pTemplate, &ulCount);
			break;
		case CKO_PUBLIC_KEY:
			rv = __template_from_pubkey(pEncoding, ulSize, &pTemplate, &ulCount);
			break;
		case CKO_PRIVATE_KEY:
			rv = __template_from_privkey(pToken, pEncoding, ulSize, &pTemplate, &ulCount);
			break;
		default:
			rv = CKR_FUNCTION_FAILED;
		}
		if (rv == CKR_OK && (rv = CKO_uxnew_object(pToken, pTemplate, ulCount, &pRet)) == CKR_OK) *hOut = pRet;
	}
	if (pTemplate)
	{
		for (i = 0; i < ulCount; i++) if (pTemplate[i].pValue) free(pTemplate[i].pValue);
		free(pTemplate);
	}
	return rv;
}
CK_DELETE(CKO_uxdelete_object)(CK_INOUT KOBJECT hHandler)
{
	if (hHandler)
	{
		if (hHandler->hMutex) hHandler->hMutex->DestroyMutex(hHandler->pMutex);
		CK_release_mutex_handler(hHandler->hMutex);
		__release_ux_object((KUX_OBJECT) hHandler->hPersistence, hHandler->hClass);
		if (hHandler->hash.data) free(hHandler->hash.data);
		free(hHandler);
	}
}

#define CERT_CKA_CERTIFICATE_TYPE_PRESENT			(1)
#define CERT_CKA_SUBJECT_PRESENT				(CERT_CKA_CERTIFICATE_TYPE_PRESENT << 1)
#define CERT_CKA_VALUE_PRESENT				(CERT_CKA_CERTIFICATE_TYPE_PRESENT << 2)
#define CERT_CKA_URL_PRESENT					(CERT_CKA_CERTIFICATE_TYPE_PRESENT << 3)
#define CERT_CKA_HASH_OF_SUBJECT_PUBLIC_KEY_PRESENT	(CERT_CKA_CERTIFICATE_TYPE_PRESENT << 4)
#define CERT_CKA_HASH_OF_ISSUER_PUBLIC_KEY_PRESENT	(CERT_CKA_CERTIFICATE_TYPE_PRESENT << 5)
#define CERT_ATTR_TEMPLATE					(CERT_CKA_CERTIFICATE_TYPE_PRESENT | CERT_CKA_SUBJECT_PRESENT)
#define CERT_URL_TEMPLATE					(CERT_CKA_URL_PRESENT | CERT_CKA_HASH_OF_SUBJECT_PUBLIC_KEY_PRESENT | CERT_CKA_HASH_OF_ISSUER_PUBLIC_KEY_PRESENT)
#define CERT_VALUE_TEMPLATE_COMPLETE			(CERT_ATTR_TEMPLATE | CERT_CKA_VALUE_PRESENT)
#define CERT_URL_TEMPLATE_COMPLETE				(CERT_ATTR_TEMPLATE | CERT_URL_TEMPLATE)
CK_INLINE CK_IMPLEMENTATION(CK_RV, __check_cert_create)(CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	int iTemplate = 0;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:
		case CKA_TRUSTED:
		case CKA_CERTIFICATE_CATEGORY:
		case CKA_CHECK_VALUE:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_PUBLIC_KEY_INFO:
		case CKA_ID:
		case CKA_ISSUER:
		case CKA_SERIAL_NUMBER:
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
		case CKA_NAME_HASH_ALGORITHM:
			break;
		case CKA_CERTIFICATE_TYPE:
			iTemplate |= CERT_CKA_CERTIFICATE_TYPE_PRESENT;
			break;
		case CKA_SUBJECT:
			iTemplate |= CERT_CKA_SUBJECT_PRESENT;
			break;
		case CKA_VALUE:
			iTemplate |= CERT_CKA_VALUE_PRESENT;
			break;
		case CKA_URL:
			iTemplate |=  CERT_CKA_URL_PRESENT;
			break;
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
			iTemplate |= CERT_CKA_HASH_OF_SUBJECT_PUBLIC_KEY_PRESENT;
			break;
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
			iTemplate |= CERT_CKA_HASH_OF_ISSUER_PUBLIC_KEY_PRESENT;
			break;
		default: return CKR_TEMPLATE_INCONSISTENT;
		}
	}
	return (iTemplate & CERT_CKA_VALUE_PRESENT) || (iTemplate & CERT_URL_TEMPLATE_COMPLETE) ? CKR_OK : CKR_TEMPLATE_INCOMPLETE;
}
#define PUBKEY_CKA_KEY_TYPE_PRESENT			(1)
#define PUBKEY_CKA_MODULUS_PRESENT			(PUBKEY_CKA_KEY_TYPE_PRESENT << 1)
#define PUBKEY_CKA_PUBLIC_EXPONENT_PRESENT	(PUBKEY_CKA_KEY_TYPE_PRESENT << 2)
#define PUBKEY_CKA_MODULUS_BITS_PRESENT		(PUBKEY_CKA_KEY_TYPE_PRESENT << 3)
#define PUBKEY_TEMPLATE_COMPLETE			0x07
CK_INLINE CK_IMPLEMENTATION(CK_RV, __check_key_create)(CK_IN CK_MECHANISM_TYPE ulType, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_ID:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_DERIVE:
		case CKA_ALLOWED_MECHANISMS:
			break;
		case CKA_KEY_TYPE:
			if (pTemplate[i].ulValueLen != sizeof(CK_KEY_TYPE)) return CKR_ATTRIBUTE_VALUE_INVALID;
			if (ulType != CK_UNAVAILABLE_INFORMATION)
			{
				switch (*(CK_KEY_TYPE*) pTemplate[i].pValue)
				{
				case CKK_RSA:
					if (ulType != CKM_RSA_PKCS_KEY_PAIR_GEN) return CKR_TEMPLATE_INCONSISTENT;
					break;
				default:
					return CKR_ATTRIBUTE_VALUE_INVALID;
				}
			}
			break;
		case CKA_LOCAL:
		case CKA_KEY_GEN_MECHANISM: return CKR_ATTRIBUTE_READ_ONLY;
		}
	}
	return CKR_OK;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __check_rsa_pubkey_create)(CK_IN CK_MECHANISM_TYPE ulType, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	int iTemplate = 0;
	CK_RV rv = __check_key_create(ulType, pTemplate, ulCount);
	if (rv != CKR_OK) return rv;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:

		case CKA_ID:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_DERIVE:
		case CKA_LOCAL:
		case CKA_KEY_GEN_MECHANISM:
		case CKA_ALLOWED_MECHANISMS:

		case CKA_SUBJECT:
		case CKA_ENCRYPT:
		case CKA_VERIFY:
		case CKA_VERIFY_RECOVER:
		case CKA_WRAP:
		case CKA_WRAP_TEMPLATE:
		case CKA_TRUSTED:
		case CKA_PUBLIC_KEY_INFO:
			break;

		case CKA_KEY_TYPE:
			if (ulType == CK_UNAVAILABLE_INFORMATION) iTemplate |= PUBKEY_CKA_KEY_TYPE_PRESENT;
			break;
		case CKA_MODULUS:
			if (ulType != CK_UNAVAILABLE_INFORMATION) return CKR_ATTRIBUTE_READ_ONLY;
			iTemplate |= PUBKEY_CKA_MODULUS_PRESENT;
			break;
		case CKA_MODULUS_BITS:
			iTemplate |= PUBKEY_CKA_MODULUS_BITS_PRESENT;
			break;
		case CKA_PUBLIC_EXPONENT:
			iTemplate |= PUBKEY_CKA_PUBLIC_EXPONENT_PRESENT;
			break;
		default: return CKR_TEMPLATE_INCONSISTENT;
		}
	}
	if (ulType == CK_UNAVAILABLE_INFORMATION) rv = iTemplate == PUBKEY_TEMPLATE_COMPLETE ? CKR_OK : CKR_TEMPLATE_INCOMPLETE;
	else if (ulType == CKM_RSA_PKCS_KEY_PAIR_GEN) rv = (iTemplate & PUBKEY_CKA_MODULUS_BITS_PRESENT) && (iTemplate & PUBKEY_CKA_PUBLIC_EXPONENT_PRESENT) ? CKR_OK : CKR_TEMPLATE_INCOMPLETE;
	else rv = CKR_TEMPLATE_INCOMPLETE;
	return rv;
}
#define PRIVKEY_CKA_KEY_TYPE_PRESENT		(1)
#define PRIVKEY_CKA_MODULUS_PRESENT			(PRIVKEY_CKA_KEY_TYPE_PRESENT << 1)
#define PRIVKEY_CKA_PUBLIC_EXPONENT_PRESENT	(PRIVKEY_CKA_KEY_TYPE_PRESENT << 2)
#define PRIVKEY_CKA_PRIVATE_EXPONENT_PRESENT	(PRIVKEY_CKA_KEY_TYPE_PRESENT << 3)
#define PRIVKEY_TEMPLATE_COMPLETE			0x0F
CK_INLINE CK_IMPLEMENTATION(CK_RV, __check_rsa_privkey_create)(CK_IN CK_MECHANISM_TYPE ulType, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	int iTemplate = 0;
	CK_RV rv = __check_key_create(ulType, pTemplate, ulCount);
	if (rv != CKR_OK) return rv;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:

		case CKA_ID:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_DERIVE:
		case CKA_LOCAL:
		case CKA_KEY_GEN_MECHANISM:
		case CKA_ALLOWED_MECHANISMS:

		case CKA_SUBJECT:
		case CKA_SENSITIVE:
		case CKA_DECRYPT:
		case CKA_SIGN:
		case CKA_SIGN_RECOVER:
		case CKA_UNWRAP:
		case CKA_EXTRACTABLE:
		case CKA_WRAP_WITH_TRUSTED:
		case CKA_UNWRAP_TEMPLATE:
		case CKA_ALWAYS_AUTHENTICATE:
		case CKA_PUBLIC_KEY_INFO:
			break;

		case CKA_KEY_TYPE:
			if (ulType == CK_UNAVAILABLE_INFORMATION) iTemplate |= PRIVKEY_CKA_KEY_TYPE_PRESENT;
			break;
		case CKA_ALWAYS_SENSITIVE:
		case CKA_NEVER_EXTRACTABLE: return CKR_ATTRIBUTE_READ_ONLY;

		case CKA_MODULUS:
			if (ulType != CK_UNAVAILABLE_INFORMATION) return CKR_ATTRIBUTE_READ_ONLY;
			iTemplate |= PRIVKEY_CKA_MODULUS_PRESENT;
			break;
		case CKA_PUBLIC_EXPONENT:
			if (ulType != CK_UNAVAILABLE_INFORMATION) return CKR_ATTRIBUTE_READ_ONLY;
			iTemplate |= PRIVKEY_CKA_PUBLIC_EXPONENT_PRESENT;
			break;
		case CKA_PRIVATE_EXPONENT:
			if (ulType != CK_UNAVAILABLE_INFORMATION) return CKR_ATTRIBUTE_READ_ONLY;
			iTemplate |= PRIVKEY_CKA_PRIVATE_EXPONENT_PRESENT;
			break;
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			if (ulType != CK_UNAVAILABLE_INFORMATION) return CKR_ATTRIBUTE_READ_ONLY;
			break;
		default: return CKR_TEMPLATE_INCONSISTENT;
		}
	}
	return (ulType == CK_UNAVAILABLE_INFORMATION && iTemplate != PRIVKEY_TEMPLATE_COMPLETE) ? CKR_TEMPLATE_INCOMPLETE : CKR_OK;
}
CK_INLINE CK_FUNCTION(CK_RV, CKO_check_create)(CK_IN CK_OBJECT_CLASS ulClass, CK_IN CK_MECHANISM_TYPE ulType, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	assert(pTemplate);
	switch (ulClass)
	{
	case CKO_CERTIFICATE: return __check_cert_create(pTemplate, ulCount);
	case CKO_PUBLIC_KEY:  return __check_rsa_pubkey_create(ulType, pTemplate, ulCount);
	case CKO_PRIVATE_KEY: return __check_rsa_privkey_create(ulType, pTemplate, ulCount);
	}
	return CKR_TEMPLATE_INCOMPLETE;
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __set_cert_attributes)
(
	CK_INOUT KOBJECT pObject,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount
)
{
	KUX_OBJECT pUX = UX_LOADER(pObject);
	KX509CERT_ENCODER hEncoder = (KX509CERT_ENCODER) pUX->hEncoder;
	KX09CERT_PARSER hParser;
	CK_RV rv;
	KP_BLOB encoding = { NULL, 0UL };

	assert(pObject);
	if ((rv = pObject->hMutex->LockMutex(pObject->pMutex)) == CKR_OK)
	{
		rv = hEncoder->from(hEncoder, pTemplate, ulCount);
		pObject->hMutex->UnlockMutex(pObject->pMutex);
	}
	if
	(
		rv == CKR_OK &&
		(rv = CKO_encode_object(hEncoder->hEncoder, &encoding)) == CKR_OK &&
		(rv = CKIO_new_certificate_parser(encoding.data, encoding.length, &hParser)) == CKR_OK
	)
	{
		if ((rv = pObject->hMutex->LockMutex(pObject->pMutex)) == CKR_OK)
		{
			CKIO_release_certificate_parser((KX09CERT_PARSER) pUX->hParser);
			pUX->hParser = hParser;
			if (pUX->encoding.data) free(pUX->encoding.data);
			pUX->encoding.data = encoding.data;
			pUX->encoding.length = encoding.length;
			rv = pObject->hMutex->UnlockMutex(pObject->pMutex);
		}
	}
	if (rv != CKR_OK && encoding.data) free(encoding.data);
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __set_pubkey_attributes)
(
	CK_INOUT KOBJECT pObject,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount
)
{
	KUX_OBJECT pUX = UX_LOADER(pObject);
	KRSAPUBKEY_ENCODER hEncoder = (KRSAPUBKEY_ENCODER) pUX->hEncoder;
	KRSAPUBKEY_PARSER hParser;
	CK_RV rv;
	KP_BLOB encoding = { NULL, 0UL };

	assert(pObject);
	if ((rv = pObject->hMutex->LockMutex(pObject->pMutex)) == CKR_OK)
	{
		rv = hEncoder->from(hEncoder, pTemplate, ulCount);
		pObject->hMutex->UnlockMutex(pObject->pMutex);
	}
	if
	(
		rv == CKR_OK &&
		(rv = CKO_encode_object(hEncoder->hEncoder, &encoding)) == CKR_OK &&
		(rv = CKIO_new_rsapubkey_parser(encoding.data, encoding.length, &hParser)) == CKR_OK
	)
	{
		if ((rv = pObject->hMutex->LockMutex(pObject->pMutex)) == CKR_OK)
		{
			CKIO_release_rsapubkey_parser((KRSAPUBKEY_PARSER) pUX->hParser);
			pUX->hParser = hParser;
			if (pUX->encoding.data) free(pUX->encoding.data);
			pUX->encoding.data = encoding.data;
			pUX->encoding.length = encoding.length;
			rv = pObject->hMutex->UnlockMutex(pObject->pMutex);
		}
	}
	if (rv != CKR_OK && encoding.data) free(encoding.data);
	return rv;
}
static CK_BBOOL __true	= CK_TRUE;
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __is_sensitive)(CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	for (i = 0; i < ulCount; i++)
	{
		if (pTemplate[i].type == CKA_SENSITIVE)
		{
			if (pTemplate[i].ulValueLen != sizeof(CK_BBOOL)) return CK_FALSE;
			return *(CK_BBOOL*) pTemplate[i].pValue;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __is_extractable)(CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	for (i = 0; i < ulCount; i++)
	{
		if (pTemplate[i].type == CKA_EXTRACTABLE)
		{
			if (pTemplate[i].ulValueLen != sizeof(CK_BBOOL)) return CK_FALSE;
			return *(CK_BBOOL*) pTemplate[i].pValue;
		}
	}
	return CK_FALSE;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __set_privkey_attributes)
(
	CK_INOUT KOBJECT pObject,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_INOUT KTOKEN_STR *pToken
)
{
	KUX_OBJECT pUX = UX_LOADER(pObject);
	KRSAPRIVKEY_ENCODER hEncoder = (KRSAPRIVKEY_ENCODER) pUX->hEncoder;
	KRSAPRIVKEY_PARSER hParser = NULL;
	KRSAKEY_HANDLER hKey = NULL;
	CK_RV rv;
	KP_BLOB encoding = { NULL, 0UL }, key = { NULL, 0 };
	CK_ATTRIBUTE isSensitive = { CKA_SENSITIVE, &__true, sizeof(CK_BBOOL) },
	isExtractable = { CKA_EXTRACTABLE, &__true, sizeof(CK_BBOOL) };

	if ((rv = pObject->hMutex->LockMutex(pObject->pMutex)) == CKR_OK)
	{
		rv = hEncoder->from(hEncoder, pToken, pTemplate, ulCount);
		if
		(
			((KRSAPRIVKEY_PARSER) pUX->hParser)->match_sensitive(((KRSAPRIVKEY_PARSER) pUX->hParser), &isSensitive) &&
			__is_sensitive(pTemplate, ulCount)
		)	rv = hEncoder->set_alwaysSensitive(hEncoder, CK_TRUE);
		if
		(
			!((KRSAPRIVKEY_PARSER) pUX->hParser)->match_extractable(((KRSAPRIVKEY_PARSER) pUX->hParser), &isExtractable) &&
			!__is_extractable(pTemplate, ulCount)
		)	rv = hEncoder->set_neverExtractable(hEncoder, CK_TRUE);
		pObject->hMutex->UnlockMutex(pObject->pMutex);
	}
	if
	(
		rv == CKR_OK &&
		(rv = CKO_encode_object(hEncoder->hEncoder, &encoding)) == CKR_OK &&
		(rv = CKIO_new_rsaprivkey_parser(encoding.data, encoding.length, &hParser)) == CKR_OK &&
		(rv = hParser->get_sensitive_material(hParser, pToken, &key.data, &key.length)) == CKR_OK &&
		(rv = CKIO_new_rsa_handler(&hKey)) == CKR_OK &&
		(rv = hKey->parse(hKey, key.data, key.length)) == CKR_OK
	)
	{
		if ((rv = pObject->hMutex->LockMutex(pObject->pMutex)) == CKR_OK)
		{
			CKIO_release_rsaprivkey_parser((KRSAPRIVKEY_PARSER) pUX->hParser);
			CKIO_release_rsa_handler(pUX->hRSA);
			if (pUX->key) NH_release_RSA_privkey_handler(pUX->key);
			if (pUX->encoding.data) free(pUX->encoding.data);
			if (pUX->material.data)
			{
				OPENSSL_cleanse(pUX->material.data, pUX->material.length);
				free(pUX->material.data);
			}
			pUX->hParser = hParser;
			pUX->hRSA = hKey;
			pUX->material.data = key.data;
			pUX->material.length = key.length;
			pUX->key = NULL;
			pUX->encoding.data = encoding.data;
			pUX->encoding.length = encoding.length;
			rv = pObject->hMutex->UnlockMutex(pObject->pMutex);
		}
	}
	if (rv != CKR_OK)
	{
		if (encoding.data) free(encoding.data);
		if (key.data)
		{
			OPENSSL_cleanse(key.data, key.length);
			free(key.data);
		}
	}
	return rv;
}
CK_FUNCTION(CK_RV, CKO_set_attributes)
(
	CK_INOUT KOBJECT pObject,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_INOUT KTOKEN_STR *pToken
)
{
	assert(pObject && pTemplate);
	switch (pObject->hClass)
	{
	case CKO_CERTIFICATE:	return __set_cert_attributes(pObject, pTemplate, ulCount);
	case CKO_PUBLIC_KEY:	return __set_pubkey_attributes(pObject, pTemplate, ulCount);
	case CKO_PRIVATE_KEY:	return __set_privkey_attributes(pObject, pTemplate, ulCount, pToken);
	case CKO_SECRET_KEY:
		break;
	}
	return CKR_TEMPLATE_INCOMPLETE;
}
CK_INLINE CK_FUNCTION(CK_BBOOL, CKO_is_modifiable)(CK_IN KOBJECT pObject)
{
	CK_BBOOL is = CK_FALSE;
	CK_ATTRIBUTE isModifiable = { CKA_MODIFIABLE, &__true, sizeof(CK_BBOOL) };
	switch (pObject->hClass)
	{
	case CKO_CERTIFICATE:
		is = ((KX09CERT_PARSER) UX_LOADER(pObject)->hParser)->match_modifiable((KX09CERT_PARSER) UX_LOADER(pObject)->hParser, &isModifiable);
		break;
	case CKO_PUBLIC_KEY:
		is = ((KRSAPUBKEY_PARSER) UX_LOADER(pObject)->hParser)->match_modifiable((KRSAPUBKEY_PARSER) UX_LOADER(pObject)->hParser, &isModifiable);
		break;
	case CKO_PRIVATE_KEY:
		is = ((KRSAPRIVKEY_PARSER) UX_LOADER(pObject)->hParser)->match_modifiable((KRSAPRIVKEY_PARSER) UX_LOADER(pObject)->hParser, &isModifiable);
		break;
	}
	return is;
}
CK_INLINE CK_FUNCTION(CK_BBOOL, CKO_is_session_object)(CK_IN KSESSION pSession, CK_IN CK_OBJECT_HANDLE hObject)
{
	CK_BBOOL is = CK_FALSE;
	khiter_t k;
	k = kh_get(KOSET, pSession->kObjects, hObject);
	if (k != kh_end(pSession->kObjects) && kh_exist(pSession->kObjects, k)) is = CK_TRUE;
	return is;
}
static CK_ATTRIBUTE __isPrivate = { CKA_PRIVATE, &__true, sizeof(CK_BBOOL) };
static CK_ATTRIBUTE __isModifiable = { CKA_MODIFIABLE, &__true, sizeof(CK_BBOOL) };
static CK_ATTRIBUTE __isCopyable = { CKA_COPYABLE, &__true, sizeof(CK_BBOOL) };
static CK_ATTRIBUTE __isSensitive = { CKA_SENSITIVE, &__true, sizeof(CK_BBOOL) };
static CK_ATTRIBUTE __isExtractable = { CKA_EXTRACTABLE, &__true, sizeof(CK_BBOOL) };
#define CHECK_STORAGE_SET_ATTRIBUTES(_h, _p)									\
{																\
	case CKA_TOKEN:													\
	case CKA_LABEL:													\
	case CKA_DESTROYABLE:												\
		break;													\
	case CKA_PRIVATE:													\
		if (_p.ulValueLen != sizeof(CK_BBOOL)) return CKR_ATTRIBUTE_VALUE_INVALID;		\
		if (_h->match_private(_h, &__isPrivate)) return CKR_ATTRIBUTE_READ_ONLY;		\
		break;													\
	case CKA_MODIFIABLE:												\
		if (_p.ulValueLen != sizeof(CK_BBOOL)) return CKR_ATTRIBUTE_VALUE_INVALID;		\
		if (!_h->match_modifiable(_h, &__isModifiable)) return CKR_ATTRIBUTE_VALUE_INVALID;	\
		break;													\
	case CKA_COPYABLE:												\
		if (_p.ulValueLen != sizeof(CK_BBOOL)) return CKR_ATTRIBUTE_VALUE_INVALID;		\
		if (!_h->match_copyable(_h, &__isCopyable)) return CKR_ATTRIBUTE_READ_ONLY;		\
		break;													\
}
#define CHECK_KEY_SET_ATTRIBUTES()			\
{								\
	case CKA_KEY_TYPE:				\
	case CKA_LOCAL:					\
	case CKA_KEY_GEN_MECHANISM:			\
		return CKR_ATTRIBUTE_READ_ONLY;	\
	case CKA_ID:					\
	case CKA_START_DATE:				\
	case CKA_END_DATE:				\
	case CKA_DERIVE:					\
	case CKA_ALLOWED_MECHANISMS:			\
		break;					\
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __check_cert_set)(CK_IN KOBJECT pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	CK_RV rv = CKR_OK;
	CK_ULONG i;
	KX09CERT_PARSER hParser = (KX09CERT_PARSER) UX_LOADER(pObject)->hParser;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		CHECK_STORAGE_SET_ATTRIBUTES(hParser, pTemplate[i])

		case CKA_CERTIFICATE_TYPE:
		case CKA_TRUSTED:
		case CKA_CERTIFICATE_CATEGORY:
			return CKR_ATTRIBUTE_READ_ONLY;
		case CKA_CHECK_VALUE:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_PUBLIC_KEY_INFO:
			break;

		case CKA_SUBJECT:
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
		case CKA_VALUE:
		case CKA_URL:
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
		case CKA_NAME_HASH_ALGORITHM:
			return CKR_ATTRIBUTE_READ_ONLY;
		case CKA_ID:
		case CKA_ISSUER:
		case CKA_SERIAL_NUMBER:
			break;

		default: return CKR_TEMPLATE_INCONSISTENT;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __check_pubkey_set)(CK_IN KOBJECT pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	CK_RV rv = CKR_OK;
	CK_ULONG i;
	KRSAPUBKEY_PARSER hParser = (KRSAPUBKEY_PARSER) UX_LOADER(pObject)->hParser;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		CHECK_STORAGE_SET_ATTRIBUTES(hParser, pTemplate[i])
		CHECK_KEY_SET_ATTRIBUTES()

		case CKA_SUBJECT:
		case CKA_ENCRYPT:
		case CKA_VERIFY:
		case CKA_VERIFY_RECOVER:
		case CKA_WRAP:
		case CKA_TRUSTED:
		case CKA_WRAP_TEMPLATE:
		case CKA_PUBLIC_KEY_INFO:
			break;

		case CKA_MODULUS_BITS:
		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
			return CKR_ATTRIBUTE_READ_ONLY;

		default: return CKR_TEMPLATE_INCONSISTENT;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __check_privkey_set)(CK_IN KOBJECT pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	CK_RV rv = CKR_OK;
	CK_ULONG i;
	KRSAPRIVKEY_PARSER hParser = (KRSAPRIVKEY_PARSER) UX_LOADER(pObject)->hParser;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		CHECK_STORAGE_SET_ATTRIBUTES(hParser, pTemplate[i])
		CHECK_KEY_SET_ATTRIBUTES()

		case CKA_SUBJECT:
		case CKA_DECRYPT:
		case CKA_SIGN:
		case CKA_SIGN_RECOVER:
		case CKA_UNWRAP:
		case CKA_WRAP_WITH_TRUSTED:
		case CKA_UNWRAP_TEMPLATE:
		case CKA_ALWAYS_AUTHENTICATE:
		case CKA_PUBLIC_KEY_INFO:
			break;
		case CKA_SENSITIVE:
			if (hParser->match_sensitive(hParser, &__isSensitive)) return CKR_ATTRIBUTE_READ_ONLY;
			break;
		case CKA_EXTRACTABLE:
			if (!hParser->match_extractable(hParser, &__isExtractable)) return CKR_ATTRIBUTE_READ_ONLY;
			break;

		case CKA_ALWAYS_SENSITIVE:
		case CKA_NEVER_EXTRACTABLE:
		case CKA_MODULUS_BITS:
		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
		case CKA_PRIVATE_EXPONENT:
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			return CKR_ATTRIBUTE_READ_ONLY;

		default: return CKR_TEMPLATE_INCONSISTENT;
		}
	}
	return rv;
}
CK_INLINE CK_FUNCTION(CK_RV, CKO_check_set)(CK_IN KOBJECT pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	switch (pObject->hClass)
	{
	case CKO_CERTIFICATE:	return __check_cert_set(pObject, pTemplate, ulCount);
	case CKO_PUBLIC_KEY:	return __check_pubkey_set(pObject, pTemplate, ulCount);
	case CKO_PRIVATE_KEY:	return __check_privkey_set(pObject, pTemplate, ulCount);
	}
	return CKR_TEMPLATE_INCONSISTENT;
}
#endif /* _UNIX */