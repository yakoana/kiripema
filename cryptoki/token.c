#include "kiripema.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WINDOWS
#include "winob.h"
#define CK_SLEEP					Sleep
#define TIMEOUT					1000
#else
#include <unistd.h>
#include <uuid/uuid.h>
#include "uxob.h"
#define CK_SLEEP					sleep
#define TIMEOUT					1
#endif

/**
 * @brief Token management
 * 
 */
CK_IMPLEMENTATION(CK_RV, __slot_list)(CK_UNUSED CK_IN KTOKEN_STR *self, CK_OUT CK_SLOT_ID_PTR pSlotList, CK_INOUT CK_ULONG_PTR pulCount)
{
	CK_RV rv = CKR_OK;

	assert(pulCount);
	if (!pSlotList) *pulCount = 1;
	else
	{
		if ((rv = *pulCount >= 1 ? CKR_OK : CKR_BUFFER_TOO_SMALL) == CKR_OK)
		{
			*pulCount = 1;
			pSlotList[0] = DEFAULT_SLOT_ID;
		}
	}
	return rv;
}
static CK_SLOT_INFO __kSlotInfo =
{
	SLOT_DESCRIPTION,
	MANUFACTURER_ID,
	CKF_TOKEN_PRESENT,
	{
		HARDWARE_VERSION_MAJOR,
		HARDWARE_VERSION_MINOR
	},
	{
		FIRMWARE_VERSION_MAJOR,
		FIRMWARE_VERSION_MINOR
	}
};
CK_IMPLEMENTATION(CK_RV, __slot_info)(CK_UNUSED CK_IN KTOKEN_STR *self, CK_IN CK_SLOT_ID slotID, CK_INOUT CK_SLOT_INFO_PTR pInfo)
{
	CK_RV rv;

	assert(pInfo);
	if ((rv = (slotID == DEFAULT_SLOT_ID) ? CKR_OK : CKR_SLOT_ID_INVALID) == CKR_OK) memcpy(pInfo, &__kSlotInfo, sizeof(CK_SLOT_INFO));
	return rv;
}
static CK_TOKEN_INFO __kTokenInfo =
{
	TOKEN_DESCRIPTION,
	MANUFACTURER_ID,
	SLOT_TOKEN_MODEL,
	SLOT_TOKEN_SERIAL,
	CKF_TOKEN_INITIALIZED | CKF_LOGIN_REQUIRED,
	CK_EFFECTIVELY_INFINITE,
	0,
	CK_EFFECTIVELY_INFINITE,
	0,
	MAX_PIN_LEN,
	MIN_PIN_LEN,
	CK_EFFECTIVELY_INFINITE,
	CK_EFFECTIVELY_INFINITE,
	CK_EFFECTIVELY_INFINITE,
	CK_EFFECTIVELY_INFINITE,
	{
		HARDWARE_VERSION_MAJOR,
		HARDWARE_VERSION_MINOR
	},
	{
		FIRMWARE_VERSION_MAJOR,
		FIRMWARE_VERSION_MINOR
	},
	DEFAULT_UTC_TIME
};
CK_IMPLEMENTATION(CK_RV, __token_info)(CK_IN KTOKEN_STR *self, CK_IN CK_SLOT_ID slotID, CK_INOUT CK_TOKEN_INFO_PTR pInfo)
{
	CK_RV rv;

	assert(pInfo);
	if ((rv = slotID == DEFAULT_SLOT_ID ? CKR_OK : CKR_SLOT_ID_INVALID) == CKR_OK)
	{
		memcpy(pInfo, &__kTokenInfo, sizeof(CK_TOKEN_INFO));
		pInfo->ulSessionCount = self->ulSessionCount;
		pInfo->ulRwSessionCount = self->ulRwSessionCount;
#ifdef _WINDOWS
		pInfo->flags |= CKF_USER_PIN_INITIALIZED;
#else
		if (self->pFS->exists(self->pFS, KSF_AUTH_TOKEN, KP_AUTH_TOKEN)) pInfo->flags |= CKF_USER_PIN_INITIALIZED;
#endif	
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __wait_slot_event)(CK_UNUSED CK_IN KTOKEN_STR *self, CK_IN CK_FLAGS flags, CK_IN CK_SLOT_ID_PTR pSlot)
{
	CK_RV rv;

	assert(pSlot);
	if ((rv = *pSlot == DEFAULT_SLOT_ID ? CKR_NO_EVENT : CKR_SLOT_ID_INVALID) == CKR_OK) if (!(flags & CKF_DONT_BLOCK)) CK_SLEEP(TIMEOUT);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __mechanism_list)(CK_UNUSED CK_IN KTOKEN_STR *self, CK_IN CK_SLOT_ID slotID, CK_OUT CK_MECHANISM_TYPE_PTR pMechanismList, CK_INOUT CK_ULONG_PTR pulCount)
{
	CK_RV rv;

	assert(pulCount);
	if ((rv = slotID == DEFAULT_SLOT_ID ? CKR_OK : CKR_SLOT_ID_INVALID) == CKR_OK) rv = self->hCrypto->list(self->hCrypto, pMechanismList, pulCount);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __mechanism_info)(CK_UNUSED CK_IN KTOKEN_STR *self, CK_IN CK_SLOT_ID slotID, CK_IN CK_MECHANISM_TYPE type, CK_INOUT CK_MECHANISM_INFO_PTR pInfo)
{
	CK_RV rv;

	assert(pInfo);
	if ((rv = slotID == DEFAULT_SLOT_ID ? CKR_OK : CKR_SLOT_ID_INVALID) == CKR_OK) rv = self->hCrypto->info(self->hCrypto, type, pInfo);
	return rv;
}

/**
 * @brief Session management
 * 
 */
CK_IMPLEMENTATION(CK_RV, __hash_pin)(CK_IN CK_UTF8CHAR_PTR pPin, CK_IN CK_ULONG ulPinLen, CK_INOUT KP_BLOB *pHash)
{
	NH_RV rv;
	NH_HASH_HANDLER hHash;

	assert(pPin && pHash);
	if ((rv = NH_new_hash(&hHash)) == CKR_OK)
	{
		if
		(
			(rv = hHash->init(hHash, CKM_SHA_1)) == CKR_OK &&
			(rv = hHash->update(hHash, pPin, ulPinLen)) == CKR_OK &&
			(rv = hHash->finish(hHash, NULL, &pHash->length)) == CKR_OK &&
			(rv = (pHash->data = (unsigned char*) malloc(pHash->length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)	rv = hHash->finish(hHash, pHash->data, &pHash->length);
		NH_release_hash(hHash);
	}
	NHARU_ASSERT(rv, "__hash_pin");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}

CK_IMPLEMENTATION(CK_RV, __check_pin)(CK_INOUT KTOKEN pToken, CK_UTF8CHAR_PTR pPin, CK_ULONG ulPin)
{
	CK_RV rv = CKR_OK;
#ifdef _UNIX
	KP_BLOB hash = { NULL, 0UL }, cur = { NULL, 0UL };
	
	if ((rv = pToken->pFS->load(pToken->pFS, KSF_AUTH_TOKEN, KP_AUTH_TOKEN, &cur.data, &cur.length) == CKR_OK ? CKR_OK : CKR_USER_PIN_NOT_INITIALIZED) == CKR_OK)
	{
		if ((rv = __hash_pin(pPin, ulPin, &hash)) == CKR_OK)
		{
			rv = (cur.length == hash.length && memcmp(cur.data, hash.data, cur.length) == 0) ? CKR_OK : CKR_PIN_INCORRECT;
			free(hash.data);
		}
		free(cur.data);
	}
#endif
	if (rv == CKR_OK)
	{
		if ((rv = pToken->hMutex->LockMutex(pToken->pMutex)) == CKR_OK)
		{
			if ((rv = (pToken->pin.data = (unsigned char*) malloc(ulPin)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
			{
				memcpy(pToken->pin.data, pPin, ulPin);
				pToken->pin.length = ulPin;
				rv = pToken->hMutex->UnlockMutex(pToken->pMutex);
			}
		}
	}
	return rv;
}
CK_IMPLEMENTATION(void, __release_pin)(CK_INOUT KTOKEN pToken)
{
	if (pToken->pin.data)
	{
		OPENSSL_cleanse(pToken->pin.data, pToken->pin.length);
		free(pToken->pin.data);
	}
	pToken->pin.data = NULL;
	pToken->pin.length = 0UL;
}
CK_IMPLEMENTATION(CK_RV, __open_session)
(
	CK_INOUT KTOKEN_STR *self,
	CK_IN CK_SLOT_ID slotID,
	CK_IN CK_FLAGS flags,
	CK_UNUSED CK_IN CK_VOID_PTR pApplication,
	CK_UNUSED CK_IN CK_NOTIFY Notify,
	CK_OUT CK_SESSION_HANDLE_PTR phSession
)
{
	CK_RV rv;

	if
	(
		(rv = slotID == DEFAULT_SLOT_ID ? CKR_OK : CKR_SLOT_ID_INVALID) == CKR_OK &&
		(rv = self->hSessions->new_session(self->hSessions, (flags & CKF_RW_SESSION), phSession)) == CKR_OK
	)	rv = self->inc_session(self, (flags & CKF_RW_SESSION));
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __close_session)(CK_INOUT KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	CK_RV rv;
	KSESSION pSession;
	CK_BBOOL isRW;

	if ((rv = ((pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID)) == CKR_OK)
	{
		isRW = pSession->isRW;
		if ((rv = self->hSessions->delete_session(self->hSessions, hSession)) == CKR_OK) rv = self->dec_session(self, isRW);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __close_all_sessions)(CK_INOUT KTOKEN_STR *self, CK_IN CK_SLOT_ID slotID)
{
	CK_RV rv;

	if
	(
		(rv = slotID == DEFAULT_SLOT_ID ? CKR_OK : CKR_SLOT_ID_INVALID) == CKR_OK &&
		(rv = self->hSessions->delete_all(self->hSessions)) == CKR_OK &&
		(rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK
	)
	{
		self->ulSessionCount = 0;
		self->ulRwSessionCount = 0;
		__release_pin(self);
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __session_info)(CK_IN KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_OUT CK_SESSION_INFO_PTR pInfo)
{
	CK_RV rv;
	KSESSION pSession;

	assert(pInfo);
	if ((rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK)
	{
		pInfo->slotID = DEFAULT_SLOT_ID;
		pInfo->state = pSession->get_state(pSession, (self->pin.data != NULL));
		pInfo->flags = pSession->isRW ? CKF_SERIAL_SESSION | CKF_RW_SESSION : CKF_SERIAL_SESSION;
		pInfo->ulDeviceError = pSession->ulDeviceError;
	}
	return rv;
}

/**
 * @brief Object management
 * 
 */
#ifdef _UNIX
CK_IMPLEMENTATION(CK_RV, __erase_objects)(CK_IN char *szFolder, CK_IN  char *szObject, CK_VOID_PTR pArgument)
{
	KFILE_SYSTEM pFS = (KFILE_SYSTEM) pArgument;
	char szFile[CK_FILENAME_MAX];

	if (!pFS) return CKR_ARGUMENTS_BAD;
	if
	(
		strstr(szObject, KP_AUTH_TOKEN) ||
		strstr(szObject, KP_CERT_ID) ||
		strstr(szObject, KP_PUBKEY_ID) ||
		strstr(szObject, KP_PRIVKEY_ID) ||
		strstr(szObject, KP_KEY_ID)
	)
	{
		strcpy(szFile, szFolder);
		strcat(szFile, szObject);
		IO_ASSERT(unlink(szFile), "__erase_objects");
	}
	return CKR_OK;
}
CK_IMPLEMENTATION(CK_RV, __init_pin)(CK_INOUT KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_IN CK_UTF8CHAR_PTR pPin, CK_IN CK_ULONG ulPinLen)
{
	CK_RV rv;
	KSESSION pSession;
	KP_BLOB hash = { NULL, 0UL };

	assert(pPin);
	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = pSession->isRW ? CKR_OK : CKR_SESSION_READ_ONLY) == CKR_OK
	)
	{
		self->hSessions->remove_all_objects(self->hSessions);
		self->hCache->clear(self->hCache);
		if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
		{
			if
			(
				(rv = self->pFS->list(self->pFS, KSF_PUB_OBJECTS, __erase_objects, self->pFS)) == CKR_OK &&
				(rv = self->pFS->list(self->pFS, KSF_PRIV_OBJECTS, __erase_objects, self->pFS)) == CKR_OK &&
				(rv = __hash_pin(pPin, ulPinLen, &hash)) == CKR_OK
			)
			{
				rv = self->pFS->store(self->pFS, hash.data, hash.length, KSF_AUTH_TOKEN, KP_AUTH_TOKEN);
				free(hash.data);
			}
			self->hMutex->UnlockMutex(self->pMutex);
		}
		pSession->ulDeviceError = rv;
	}
	return rv;
}
typedef struct __reload_params_str
{
	KTOKEN	pToken;
	KP_BLOB*	pNewPin;

} __reload_params_str, *__reload_params;
CK_IMPLEMENTATION(CK_RV, __reload_objects)(CK_IN char *szFolder, CK_IN  char *szObject, CK_VOID_PTR pArgument)
{
	__reload_params pArgs = (__reload_params) pArgument;
	CK_RV rv = CKR_OK;
	char szPath[CK_FILENAME_MAX];
	KP_BLOB encoding = { NULL, 0UL }, material = { NULL, 0UL }, newEncoding = { NULL, 0UL };
	KOBJECT pObject;
	KUX_OBJECT pUX;
	KRSAPRIVKEY_PARSER hParser, hNewParser;
	KRSAPRIVKEY_ENCODER hEncoder;
	KRSAKEY_HANDLER hRSAKey;
	KP_BLOB pinSave = { NULL, 0UL };

	assert(pArgs);
	if (strstr(szObject, KP_PRIVKEY_ID))
	{
		pinSave.data = pArgs->pToken->pin.data;
		pinSave.length = pArgs->pToken->pin.length;
		pArgs->pToken->pin.data = pArgs->pNewPin->data;
		pArgs->pToken->pin.length = pArgs->pNewPin->length;
		strcpy(szPath, szFolder);
		strcat(szPath, szObject);
		if ((rv = pArgs->pToken->pFS->load_path(pArgs->pToken->pFS, szPath, &encoding.data, &encoding.length)) == CKR_OK)
		{
			if ((rv = CKO_uxload_object(pArgs->pToken, encoding.data, encoding.length, &pObject)) == CKR_OK)
			{
				pUX = UX_LOADER(pObject);
				hParser = (KRSAPRIVKEY_PARSER) pUX->hParser;
				hEncoder = (KRSAPRIVKEY_ENCODER) pUX->hEncoder;
				if ((rv = hParser->get_sensitive_material(hParser, pArgs->pToken, &material.data, &material.length)) == CKR_OK)
				{
					if ((rv = CKIO_new_rsa_handler(&hRSAKey)) == CKR_OK)
					{
						if
						(
							(rv = hRSAKey->parse(hRSAKey, material.data, material.length)) == CKR_OK &&
							(rv = CKIO_encrypt_material(hEncoder, hRSAKey, pArgs->pToken)) == CKR_OK &&
							(rv = CKO_encode_object(hEncoder->hEncoder, &newEncoding)) == CKR_OK
						)
						{
							CKIO_release_rsaprivkey_parser(hParser);
							pUX->hParser = NULL;
							if ((rv = CKIO_new_rsaprivkey_parser(newEncoding.data, newEncoding.length, &hNewParser)) == CKR_OK)
							{
								pUX->hParser = hNewParser;
								if (pUX->encoding.data) free(pUX->encoding.data);
								if ((rv = (pUX->encoding.data = (unsigned char*) malloc(newEncoding.length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
								{
									memcpy(pUX->encoding.data, newEncoding.data, newEncoding.length);
									pUX->encoding.length = newEncoding.length;
									strcpy(pUX->szFilename, szObject);
									rv = pArgs->pToken->pFS->store_path(pArgs->pToken->pFS, pUX->encoding.data, pUX->encoding.length, szPath);
								}
							}
							free(newEncoding.data);
						}
						CKIO_release_rsa_handler(hRSAKey);
					}
					OPENSSL_cleanse(material.data, material.length);
					free(material.data);
				}
				CKO_delete_object(pObject);
			}
			free(encoding.data);
		}
		pArgs->pToken->pin.data = pinSave.data;
		pArgs->pToken->pin.length = pinSave.length;
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __set_pin)
(
	CK_INOUT KTOKEN_STR *self,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_IN CK_UTF8CHAR_PTR pOldPin,
	CK_IN CK_ULONG ulOldLen,
	CK_IN CK_UTF8CHAR_PTR pNewPin,
	CK_IN CK_ULONG ulNewLen
)
{
	CK_RV rv = CKR_OK;
	KSESSION pSession;
	__reload_params_str params;
	KP_BLOB hash = { NULL, 0UL }, pin = { NULL, 0UL };

	assert(pOldPin && pNewPin);
	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = pSession->isRW ? CKR_OK : CKR_SESSION_READ_ONLY) == CKR_OK
	)
	{
		if (self->pin.data) self->logout(self, hSession);
		if ((rv = self->login(self, hSession, pOldPin, ulOldLen)) == CKR_OK)
		{
			self->hSessions->remove_all_objects(self->hSessions);
			self->hCache->clear(self->hCache);
			if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
			{
				pin.data = pNewPin;
				pin.length = ulNewLen;
				params.pToken = self;
				params.pNewPin = &pin;
				if ((rv = self->pFS->list(self->pFS, KSF_PRIV_OBJECTS, __reload_objects, &params)) == CKR_OK)
				{
					self->logout(self, hSession);
					if ((rv = __hash_pin(pNewPin, ulNewLen, &hash)) == CKR_OK)
					{
						rv = self->pFS->store(self->pFS, hash.data, hash.length, KSF_AUTH_TOKEN, KP_AUTH_TOKEN);
						free(hash.data);
					}
				}
				self->hMutex->UnlockMutex(self->pMutex);
			}
		}
		if (rv == CKR_OK)
		{
			self->hCache->load(self->hCache, self, KSF_PUB_OBJECTS);
			self->hCache->load(self->hCache, self, KSF_PRIV_OBJECTS);
		}
		pSession->ulDeviceError = rv;
	}
	return rv;
}
#endif

CK_IMPLEMENTATION(CK_RV, __login)(CK_INOUT KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_IN CK_UTF8CHAR_PTR pPin, CK_IN CK_ULONG ulPinLen)
{
	CK_RV rv;
	
	assert(pPin);
	if
	(
		(rv = self->hSessions->is_open(self->hSessions, hSession) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = !self->pin.data ? CKR_OK : CKR_USER_ALREADY_LOGGED_IN) == CKR_OK
	)	rv = __check_pin(self, pPin, ulPinLen);
	if (rv == CKR_OK) self->hCache->load(self->hCache, self, KOC_PRIVATE_OBJECTS);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __logout)(CK_INOUT KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	CK_RV rv;

	if
	(
		(rv = self->hSessions->is_open(self->hSessions, hSession) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = self->pin.data ? CKR_OK : CKR_USER_NOT_LOGGED_IN) == CKR_OK
	)
	{
		if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
		{
			__release_pin(self);
			rv = self->hMutex->UnlockMutex(self->pMutex);
		}
	}
	return rv;
}

#ifdef _UNIX
CK_INLINE CK_IMPLEMENTATION(void, __uuid)(CK_INOUT KOBJECT pObject)
{
	KUX_OBJECT pUX = UX_LOADER(pObject);
	uuid_t uu;

	uuid_generate(uu);
	uuid_unparse(uu, pUX->szFilename);
	switch (pObject->hClass)
	{
	case CKO_CERTIFICATE:
		strcat(pUX->szFilename, KP_CERT_ID);
		break;
	case CKO_PUBLIC_KEY:
		strcat(pUX->szFilename, KP_PUBKEY_ID);
		break;
	case CKO_PRIVATE_KEY:
		strcat(pUX->szFilename, KP_PRIVKEY_ID);
		break;
	case CKO_SECRET_KEY:
		strcat(pUX->szFilename, KP_KEY_ID);
		break;
	}
}
CK_IMPLEMENTATION(CK_RV, __persist_new_object)(CK_INOUT KTOKEN_STR *pToken, CK_INOUT KSESSION pSession, CK_INOUT KOBJECT pObject)
{
	CK_RV rv = CKR_OK;
	KUX_OBJECT pUX = UX_LOADER(pObject);
	KSPECIAL_FOLDER iTarget = KSF_PUB_OBJECTS;

	if (pSession) rv = pToken->hSessions->add_object(pToken->hSessions, pSession->hHandle, pObject->hObject);
	else
	{
		__uuid(pObject);
		switch (pObject->hClass)
		{
		case CKO_CERTIFICATE:
		case CKO_PUBLIC_KEY:
			break;
		case CKO_PRIVATE_KEY:
		case CKO_SECRET_KEY:
			iTarget = KSF_PRIV_OBJECTS;
			break;
		default: return CKR_ARGUMENTS_BAD;
		}
		rv = pToken->pFS->store(pToken->pFS, pUX->encoding.data, pUX->encoding.length, iTarget, pUX->szFilename);
	}
	;
	if (rv == CKR_OK && (rv = pToken->get_handle(pToken, &pObject->hObject)) == CKR_OK) rv = pToken->hCache->add(pToken->hCache, pObject);
	else if (!pSession) rv = pToken->pFS->erase(pToken->pFS, iTarget, pUX->szFilename) ? CKR_OK : CKR_GENERAL_ERROR;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __create_object)
(
	CK_INOUT KTOKEN_STR *self,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_OUT CK_OBJECT_HANDLE_PTR phObject
)
{
	CK_RV rv;
	CK_BBOOL isSessionObject;
	KSESSION pSession = NULL;
	CK_OBJECT_CLASS ulClass;
	KOBJECT pObject = NULL;
	assert(pTemplate && phObject);

	rv = CKO_willbe_session_object(pTemplate, ulCount, &isSessionObject);
	if (rv == CK_UNAVAILABLE_INFORMATION) isSessionObject = CK_TRUE;
	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		!pSession->isRW
	)	rv = isSessionObject ? CKR_OK : CKR_SESSION_READ_ONLY;
	if
	(
		rv == CKR_OK &&
		(rv = (ulClass = CKO_get_class(pTemplate, ulCount)) != CK_UNAVAILABLE_INFORMATION ? CKR_OK : CKR_TEMPLATE_INCOMPLETE) == CKR_OK &&
		(rv = ((ulClass == CKO_PRIVATE_KEY || ulClass == CKO_SECRET_KEY) && self->pin.data) || ulClass == CKO_CERTIFICATE || ulClass == CKO_PUBLIC_KEY ? CKR_OK : CKR_USER_NOT_LOGGED_IN) == CKR_OK &&
		(rv = CKO_check_create(ulClass, CK_UNAVAILABLE_INFORMATION, pTemplate, ulCount)) == CKR_OK &&
		(rv = CKO_uxnew_object(self, pTemplate, ulCount, &pObject)) == CKR_OK
	)
	{
		if ((rv = __persist_new_object(self, isSessionObject ? pSession : NULL, pObject)) == CKR_OK) *phObject = pObject->hObject;
		else self->hCache->remove(self->hCache, pObject->hObject);
	}
	if (rv != CKR_OK && pObject) CKO_delete_object(pObject);
	if (pSession) pSession->ulDeviceError = rv;
	return rv;
}
#endif
CK_IMPLEMENTATION(CK_RV, __destroy_object)(CK_INOUT KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_IN CK_OBJECT_HANDLE hObject)
{
	CK_RV rv = CKR_FUNCTION_NOT_SUPPORTED;
#ifdef _UNIX
	KSESSION pSession;
	KOBJECT pObject;
	KUX_OBJECT pUX;
	KSPECIAL_FOLDER iTarget;

	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = (pObject = self->hCache->get(self->hCache, hObject)) ? CKR_OK : CKR_OBJECT_HANDLE_INVALID) == CKR_OK
	)
	{
		switch (pObject->hClass)
		{
		case CKO_CERTIFICATE:
		case CKO_PUBLIC_KEY:
			iTarget = KSF_PUB_OBJECTS;
			break;
		case CKO_PRIVATE_KEY:
		case CKO_SECRET_KEY:
			iTarget = KSF_PRIV_OBJECTS;
			break;
		default: return CKR_ARGUMENTS_BAD;
		}
		pUX = UX_LOADER(pObject);
		if (pUX->szFilename)
		{
			if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
			{
				self->pFS->erase(self->pFS, iTarget, pUX->szFilename);
				rv = self->hMutex->UnlockMutex(self->pMutex);
			}
		}
		if (rv == CKR_OK && (rv = self->hSessions->remove_object(self->hSessions, hSession, hObject)) == CKR_OK) rv = self->hCache->remove(self->hCache, hObject);
		pSession->ulDeviceError = rv;
	}
#endif
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __find_objects_init)
(
	CK_INOUT KTOKEN_STR *self,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount
)
{
	CK_RV rv;
	KSESSION pSession;
	KOB_ITERATOR_STR hIt = { 0, 0 };
	KOBJECT pObject;

	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = !self->hSessions->search_is_active(self->hSessions, hSession) ? CKR_OK : CKR_OPERATION_ACTIVE) == CKR_OK &&
		(rv = pSession->init_search(pSession)) == CKR_OK &&
		(rv = self->hCache->iterator(self->hCache, pTemplate, ulCount, &hIt)) == CKR_OK
	)
	{
		while (rv == CKR_OK && (pObject = self->hCache->next(self->hCache, &hIt)))
			if (pObject->match(pObject, pTemplate, ulCount, &self->pin))
				rv = pSession->found_object(pSession, pObject->hObject);
		if (rv != CKR_OK) pSession->finish_search(pSession);
		pSession->ulDeviceError = rv;
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __find_objects)
(
	CK_INOUT KTOKEN_STR *self,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_INOUT CK_OBJECT_HANDLE_PTR phObject,
	CK_IN CK_ULONG ulMaxObjectCount,
	CK_IN CK_ULONG_PTR pulObjectCount
)
{
	CK_RV rv;
	CK_ULONG i = 0;
	KSESSION pSession;
	CK_OBJECT_HANDLE hFound;

	assert(phObject && pulObjectCount);
	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = self->hSessions->search_is_active(self->hSessions, hSession) ? CKR_OK : CKR_OPERATION_NOT_INITIALIZED) == CKR_OK
	)
	{
		while
		(
			(rv = pSession->next_object(pSession, &hFound)) == CKR_OK &&
			hFound != CK_UNAVAILABLE_INFORMATION &&
			i < ulMaxObjectCount
		)	phObject[i++] = hFound;
		if (rv == CKR_OK) *pulObjectCount = i;
		pSession->ulDeviceError = rv;
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __find_objects_final)(CK_INOUT KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	CK_RV rv;
	KSESSION pSession;

	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = self->hSessions->search_is_active(self->hSessions, hSession) ? CKR_OK : CKR_OPERATION_NOT_INITIALIZED) == CKR_OK
	)
	{
		rv = pSession->finish_search(pSession);
		pSession->ulDeviceError = rv;
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __get_attribute_value)(CK_IN KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_IN CK_OBJECT_HANDLE hObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_RV rv;
	KOBJECT pObject;

	assert(pTemplate);
	if
	(
		(rv = self->hSessions->is_open(self->hSessions, hSession) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = (pObject = self->hCache->get(self->hCache, hObject)) ? CKR_OK : CKR_OBJECT_HANDLE_INVALID) == CKR_OK
	)	rv = pObject->get_value(pObject, pTemplate, ulCount, &self->pin);
	return rv;
}

#ifdef _UNIX
CK_IMPLEMENTATION(CK_RV, __set_attribute_value)
(
	CK_INOUT KTOKEN_STR *self,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_IN CK_OBJECT_HANDLE hObject,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount
)
{
	CK_RV rv;
	KSESSION pSession = NULL;
	KOBJECT pObject;
	CK_BBOOL  isNowSessionObject, willBeSessionObject;
	KSPECIAL_FOLDER iTarget;
	KUX_OBJECT pUX;

	assert(pTemplate);
	if
	(
		(rv = (pObject = self->hCache->get(self->hCache, hObject)) ? CKR_OK : CKR_OBJECT_HANDLE_INVALID) == CKR_OK &&
		(rv = CKO_is_modifiable(pObject) ? CKR_OK : CKR_ACTION_PROHIBITED) == CKR_OK &&
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK
	)
	{
		switch (pObject->hClass)
		{
		case CKO_CERTIFICATE:
		case CKO_PUBLIC_KEY:
			iTarget = KSF_PUB_OBJECTS;
			break;
		case CKO_PRIVATE_KEY:
		case CKO_SECRET_KEY:
			iTarget = KSF_PRIV_OBJECTS;
			rv = self->pin.data ? CKR_OK : CKR_USER_NOT_LOGGED_IN;
			break;
		default:
			rv = CKR_TEMPLATE_INCONSISTENT;
			break;
		}
		if (rv == CKR_OK)
		{
			isNowSessionObject = CKO_is_session_object(pSession, hObject);
			rv = (pSession->isRW || (!pSession->isRW && isNowSessionObject)) ? CKR_OK : CKR_SESSION_READ_ONLY;
		}
	}
	if (rv == CKR_OK)
	{
		rv = CKO_willbe_session_object(pTemplate, ulCount, &willBeSessionObject);
		if (rv == CK_UNAVAILABLE_INFORMATION) willBeSessionObject = CK_FALSE;
		if
		(
			(rv = CKO_check_set(pObject, pTemplate, ulCount)) == CKR_OK &&
			(rv = CKO_set_attributes(pObject, pTemplate, ulCount, self)) == CKR_OK
		)
		{
			pUX = UX_LOADER(pObject);
			if (!isNowSessionObject && willBeSessionObject)
			{
				if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
				{
					rv = self->pFS->erase(self->pFS, iTarget, pUX->szFilename);
					self->hMutex->UnlockMutex(self->pMutex);
				}
				if (rv == CKR_OK)
				{
					memset(pUX->szFilename, 0, UXOBJ_UUID_LEN);
					rv = self->hSessions->add_object(self->hSessions, hSession, hObject);
				}
			}
			else if (isNowSessionObject && !willBeSessionObject)
			{
				if ((rv = self->hSessions->remove_object(self->hSessions, hSession, hObject)) == CKR_OK) __uuid(pObject);
			}
			if (!willBeSessionObject)
			{
				if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
				{
					rv = self->pFS->store(self->pFS, pUX->encoding.data, pUX->encoding.length, iTarget, pUX->szFilename);
					self->hMutex->UnlockMutex(self->pMutex);
				}
			}
		}
	}
	if (pSession) pSession->ulDeviceError = rv;
	return rv;
}

/**
 * @brief Cryptographic functions
 * 
 */
CK_IMPLEMENTATION(CK_RV, __generate_keypair)
(
	CK_INOUT KTOKEN_STR *self,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_IN CK_MECHANISM_TYPE ulType,
	CK_IN CK_ATTRIBUTE_PTR pPublicKeyTemplate,
	CK_IN CK_ULONG ulPublicKeyAttributeCount,
	CK_IN CK_ATTRIBUTE_PTR pPrivateKeyTemplate,
	CK_IN CK_ULONG ulPrivateKeyAttributeCount,
	CK_OUT CK_OBJECT_HANDLE_PTR phPublicKey,
	CK_OUT CK_OBJECT_HANDLE_PTR phPrivateKey
)
{
	CK_RV rv, isPubRV, isPrivRV = CK_UNAVAILABLE_INFORMATION;
	KSESSION pSession = NULL;
	CK_BBOOL isPubSession, isPrivSession, isSessionObject;
	CK_OBJECT_CLASS ulClass;
	CK_ULONG ulBits, ulPublicExponent;
	KOBJECT pPubKey = NULL, pPrivKey = NULL;

	assert(pPublicKeyTemplate && phPublicKey && phPrivateKey);
	if (ulType != CKM_RSA_PKCS_KEY_PAIR_GEN) return CKR_MECHANISM_INVALID;
	
	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = pSession->isRW ? CKR_OK : CKR_SESSION_READ_ONLY) == CKR_OK &&
		(rv = self->pin.data ? CKR_OK : CKR_USER_NOT_LOGGED_IN) == CKR_OK &&
		(rv = (ulClass = CKO_get_class(pPublicKeyTemplate, ulPublicKeyAttributeCount)) == CK_UNAVAILABLE_INFORMATION || ulClass == CKO_PUBLIC_KEY ? CKR_OK : CKR_TEMPLATE_INCONSISTENT) == CKR_OK &&
		(rv = CKO_check_create(CKO_PUBLIC_KEY, ulType, pPublicKeyTemplate, ulPublicKeyAttributeCount)) == CKR_OK
	)
	{
		isPubRV = CKO_willbe_session_object(pPublicKeyTemplate, ulPublicKeyAttributeCount, &isPubSession);
		if (pPrivateKeyTemplate)
		{
			rv = (ulClass = CKO_get_class(pPrivateKeyTemplate, ulPrivateKeyAttributeCount)) == CK_UNAVAILABLE_INFORMATION || ulClass == CKO_PRIVATE_KEY ? CKR_OK : CKR_TEMPLATE_INCONSISTENT;
			rv = CKO_check_create(CKO_PRIVATE_KEY, ulType, pPrivateKeyTemplate, ulPrivateKeyAttributeCount);
			isPrivRV = CKO_willbe_session_object(pPrivateKeyTemplate, ulPrivateKeyAttributeCount, &isPrivSession);
		}
		if      (isPubRV == CK_UNAVAILABLE_INFORMATION && isPrivRV == CK_UNAVAILABLE_INFORMATION) isSessionObject = CK_TRUE;
		else if (isPubRV == CK_UNAVAILABLE_INFORMATION && isPrivRV == CKR_OK) isSessionObject = isPrivSession;
		else if (isPubRV == CKR_OK && isPrivRV == CK_UNAVAILABLE_INFORMATION) isSessionObject = isPubSession;
		else
		{
			isSessionObject = isPubRV;
			rv = isPubRV == isPrivRV ? CKR_OK : CKR_TEMPLATE_INCONSISTENT;
		}
	}
	if
	(
		rv == CKR_OK &&
		(rv = self->hCrypto->keypair_templates(self->hCrypto, ulType, pPublicKeyTemplate, ulPublicKeyAttributeCount, pPrivateKeyTemplate, ulPrivateKeyAttributeCount)) == CKR_OK &&
		(rv = self->hCrypto->keypair_parameters(self->hCrypto, pPublicKeyTemplate, ulPublicKeyAttributeCount, &ulBits, &ulPublicExponent)) == CKR_OK
	)	rv = self->hCrypto->generate_keypair(self->hCrypto, ulBits, ulPublicExponent, isSessionObject, &pPubKey, &pPrivKey);
	if (rv == CKR_OK && (rv = CKO_set_attributes(pPubKey, pPublicKeyTemplate, ulPublicKeyAttributeCount, self)) == CKR_OK)
	{
		if (pPrivateKeyTemplate) rv = CKO_set_attributes(pPrivKey, pPrivateKeyTemplate, ulPrivateKeyAttributeCount, self);
		if
		(
			rv == CKR_OK &&
			(rv = __persist_new_object(self, isSessionObject ? pSession : NULL, pPubKey)) == CKR_OK &&
			(rv = __persist_new_object(self, isSessionObject ? pSession : NULL, pPrivKey)) == CKR_OK
		)
		{
			*phPublicKey = pPubKey->hObject;
			*phPrivateKey = pPrivKey->hObject;
		}
	}
	if (pSession) pSession->ulDeviceError = rv;
	return rv;
}
#endif
CK_IMPLEMENTATION(CK_RV, __sign_init)
(
	CK_IN KTOKEN_STR *self,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_IN CK_MECHANISM_PTR pMechanism,
	CK_IN CK_OBJECT_HANDLE hKey
)
{
	CK_RV rv;
	KSESSION pSession = NULL;
	KOBJECT pObject;

	assert(pMechanism);
	
	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = !self->hSessions->crypto_is_active(self->hSessions, hSession) ? CKR_OK : CKR_OPERATION_ACTIVE) == CKR_OK &&
		(rv = (pObject = self->hCache->get(self->hCache, hKey)) ? CKR_OK : CKR_OBJECT_HANDLE_INVALID) == CKR_OK &&
		(rv = pObject->hClass == CKO_PRIVATE_KEY ? CKR_OK : CKR_KEY_TYPE_INCONSISTENT) == CKR_OK &&
		(rv = self->hCrypto->is_allowed(self->hCrypto, pMechanism->mechanism) ? CKR_OK : CKR_MECHANISM_INVALID) == CKR_OK
	)	rv = self->hCrypto->sign_init(self->hCrypto, pSession, pObject, pMechanism->mechanism);
	if (pSession) pSession->ulDeviceError = rv;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __sign)(CK_IN KTOKEN_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_IN CK_BYTE_PTR pData, CK_IN CK_ULONG ulDataLen, CK_OUT CK_BYTE_PTR pSignature, CK_INOUT CK_ULONG_PTR pulSignatureLen)
{
	CK_RV rv;
	KSESSION pSession = NULL;

	assert(pData && pulSignatureLen);
	if
	(
		(rv = (pSession = self->hSessions->get_session(self->hSessions, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK &&
		(rv = self->hSessions->crypto_is_active(self->hSessions, hSession) ? CKR_OK : CKR_OPERATION_NOT_INITIALIZED) == CKR_OK
	)
	{
		rv = self->hCrypto->sign(self->hCrypto, pSession, pData, ulDataLen, pSignature, pulSignatureLen);
		pSession->ulDeviceError = rv;
	}
	if (pSession) pSession->ulDeviceError = rv;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __get_handle)(CK_INOUT KTOKEN_STR *self, CK_OUT CK_ULONG_PTR phHandle)
{
	CK_RV rv;
	CK_ULONG hHandle;
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		hHandle = ++self->ulHandleCount;
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	if (rv == CKR_OK) *phHandle = hHandle;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __inc_session)(CK_INOUT KTOKEN_STR *self, CK_IN CK_BBOOL isRW)
{
	CK_RV rv;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		self->ulSessionCount++;
		if (isRW) self->ulRwSessionCount++;
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __dec_session)(CK_INOUT KTOKEN_STR *self, CK_IN CK_BBOOL isRW)
{
	CK_RV rv;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		self->ulSessionCount--;
		if (isRW) self->ulRwSessionCount--;
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}

static KTOKEN_STR __defToken =
{
	NULL,			/* pMutex */
	NULL,			/* hMutex */
	NULL,			/* hCrypto */
	0UL,			/* ulHandleCount*/
	0UL,			/* ulSessionCount*/
	0UL,			/* ulRwSessionCount */
	{ NULL, 0UL },	/* pin */
	NULL,			/* pFS */
	NULL,			/* hSessions */
	NULL,			/* hCache */

	__slot_list,
	__slot_info,
	__token_info,
	__wait_slot_event,
	__mechanism_list,
	__mechanism_info,
#ifdef _UNIX
	__init_pin,
	__set_pin,
#endif
	__open_session,
	__close_session,
	__close_all_sessions,
	__session_info,
	__login,
	__logout,
#ifdef _UNIX
	__create_object,
#endif
	__destroy_object,
	__find_objects_init,
	__find_objects,
	__find_objects_final,
	__get_attribute_value,
#ifdef _UNIX
	__set_attribute_value,
	__generate_keypair,
#endif
	__sign_init,
	__sign,
	__get_handle,
	__inc_session,
	__dec_session
};
CK_NEW(CK_new_token)(CK_C_INITIALIZE_ARGS_PTR pArgs, CK_OUT KTOKEN *pToken)
{
	CK_RV rv;
	KTOKEN hOut = NULL;
	if ((rv = (hOut = (KTOKEN) malloc(sizeof(KTOKEN_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		
		memcpy(hOut, &__defToken, sizeof(KTOKEN_STR));
		if
		(
			(rv = CK_new_mutex_handler(pArgs, &hOut->hMutex)) == CKR_OK &&
			(rv = hOut->hMutex->CreateMutex(&hOut->pMutex)) == CKR_OK &&
			(rv = CK_new_crypto(hOut, &hOut->hCrypto)) == CKR_OK &&
			(rv = CKO_new_fs(&hOut->pFS)) == CKR_OK &&
			(rv = CK_new_object_cache(hOut->hMutex, &hOut->hCache)) == CKR_OK &&
			(rv = CK_new_session_handler(hOut->hMutex, &hOut->hSessions)) == CKR_OK &&
			(rv = hOut->hCache->load(hOut->hCache, hOut, KOC_PUBLIC_OBJECTS)) == CKR_OK
		)	*pToken = hOut;
	}
	if (rv != CKR_OK) CK_release_token(hOut);
	return rv;
}
CK_DELETE(CK_release_token)(CK_INOUT KTOKEN pToken)
{
	if (pToken)
	{
		if (pToken->hMutex)
		{
			if (pToken->pMutex) pToken->hMutex->DestroyMutex(pToken->pMutex);
			CK_release_mutex_handler(pToken->hMutex);
		}
		if (pToken->hCrypto) CK_release_crypto(pToken->hCrypto);
		__release_pin(pToken);
		CKO_release_fs(pToken->pFS);
		CK_release_session_handler(pToken->hSessions);
		CK_release_object_cache(pToken->hCache);
		free(pToken);
	}
}
