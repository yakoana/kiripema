/**
 * @file winob.h
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Windows Cryptographic Services PKCS#11 implementation 
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#ifndef __WINOB_H__
#define __WINOB_H__

#include "kiripema.h"

#ifdef _WINDOWS
typedef struct KWIN_OBJECT_STR				/* Windows object persistence loader */
{
	NH_CERTIFICATE_HANDLER		hCert;		/* Handler to Nharu certificate parser */
	PCCERT_CONTEXT			pCtx;			/* Handler to Windows CryptoAPI certificate context */

} KWIN_OBJECT_STR, *KWIN_OBJECT;
CK_NEW(CKO_wnew_object)						/* Create new windows object loader */
(
	CK_IN PCCERT_CONTEXT,					/* pCtx */
	CK_IN CK_OBJECT_CLASS,					/* hClass */
	CK_OUT KOBJECT*						/* hOut */
);
CK_DELETE(CKO_wdelete_object)					/* Releases object loader */
(
	CK_INOUT KOBJECT						/* hHandler */
);
#define CKO_delete_object					CKO_wdelete_object

#endif
#endif /* __WINOB_H__ */