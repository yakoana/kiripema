/**
 * @file vfs.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Platform independent file system implementation
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kiripema.h"
#include <assert.h>

#if defined(_UX)
#include <dirent.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/stat.h>

CK_IMPLEMENTATION(char*, __get_special_folder)(CK_IN KFUX_FILE_SYSTEM pFs, CK_IN KSPECIAL_FOLDER iFolder)
{
	char *szFolder = NULL;
	khiter_t k = kh_get(KFMAP, pFs->kFolders, iFolder);
	if (k != kh_end(pFs->kFolders) && kh_exist(pFs->kFolders, k)) szFolder = kh_value(pFs->kFolders, k)->szFolder;
	return szFolder;
}
CK_IMPLEMENTATION(char*, __get_folder)(CK_IN KFILE_SYSTEM_STR * self, CK_IN KSPECIAL_FOLDER iFolder)
{
	KFUX_FILE_SYSTEM fs = (KFUX_FILE_SYSTEM) self->pSlot;
	assert(fs);
	return __get_special_folder(fs, iFolder);
}
CK_IMPLEMENTATION(CK_BBOOL, __exists)(CK_IN KFILE_SYSTEM_STR *self, CK_IN KSPECIAL_FOLDER iFolder, CK_IN  char *szObject)
{
	CK_BBOOL hasAccess = CK_FALSE;
	char szFile[CK_FILENAME_MAX], *szFolder = self->get_folder(self, iFolder);
	if (szFolder)
	{
		strcpy(szFile, szFolder);
		strcat(szFile, szObject);
		hasAccess = !access(szFile, F_OK);
	}
	return hasAccess;
}
CK_IMPLEMENTATION(CK_RV, __list)(CK_IN KFILE_SYSTEM_STR *self, CK_IN KSPECIAL_FOLDER iFolder, KFS_READDIR callback, CK_VOID_PTR pArgument)
{
	CK_RV rv;
	DIR *d;
	struct dirent *dir;
	KFUX_FILE_SYSTEM fs = (KFUX_FILE_SYSTEM) self->pSlot;
	char *szFolder;

	assert(fs && callback);
	if ((szFolder = __get_special_folder(fs, iFolder)))
	{
		if ((rv = (d = opendir(szFolder)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
		{
			while (rv == CKR_OK && (dir = readdir(d))) rv = callback(szFolder, dir->d_name, pArgument);
			closedir(d);
		}
	}
	else rv = CKR_ARGUMENTS_BAD;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __load)
(
	CK_IN  KFILE_SYSTEM_STR *self,
	CK_IN  KSPECIAL_FOLDER iFolder,
	CK_IN  char *szObject,
	CK_OUT CK_BYTE_PTR *pEncoding,
	CK_OUT CK_ULONG_PTR pulEncoding
)
{
	CK_RV rv;
	KFUX_FILE_SYSTEM fs = (KFUX_FILE_SYSTEM) self->pSlot;
	char *szFolder, szPath[CK_FILENAME_MAX];

	assert(fs && szObject && pulEncoding);
	if ((szFolder = __get_special_folder(fs, iFolder)))
	{
		strcpy(szPath, szFolder);
		strcat(szPath, szObject);
		rv = self->load_path(self, szPath, pEncoding, pulEncoding);
	}
	else rv = CKR_ARGUMENTS_BAD;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __load_path)
(
	CK_UNUSED CK_IN  KFILE_SYSTEM_STR *self,
	CK_IN  char *szPath,
	CK_OUT CK_BYTE_PTR *pEncoding,
	CK_OUT CK_ULONG_PTR pulEncoding
)
{
	CK_RV rv;
	FILE *f;
	CK_BYTE_PTR pBuffer;
	CK_ULONG ulBuffer;

	assert(szPath && pulEncoding);
	if ((rv = (f = fopen(szPath, "r")) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		fseek(f, 0, SEEK_END);
		ulBuffer = ftell(f);
		rewind(f);
		if ((rv = (pBuffer = (CK_BYTE_PTR) malloc(ulBuffer)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			if ((rv = (fread(pBuffer, 1, ulBuffer, f) == ulBuffer) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
			{
				*pEncoding = pBuffer;
				*pulEncoding = ulBuffer;
			}
			else free(pEncoding);
		}
		fclose(f);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __store)
(
	CK_IN KFILE_SYSTEM_STR *self,
	CK_IN CK_BYTE_PTR pEncoding,
	CK_IN CK_ULONG ulEncoding,
	CK_IN  KSPECIAL_FOLDER iFolder,
	CK_IN  char *szObject
)
{
	CK_RV rv;
	KFUX_FILE_SYSTEM fs = (KFUX_FILE_SYSTEM) self->pSlot;
	char *szFolder, szPath[CK_FILENAME_MAX];

	assert(pEncoding && szObject && fs);
	if ((szFolder = __get_special_folder(fs, iFolder)))
	{
		strcpy(szPath, szFolder);
		strcat(szPath, szObject);
		rv = self->store_path(self, pEncoding, ulEncoding, szPath);
	}
	else rv = CKR_ARGUMENTS_BAD;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __store_path)
(
	CK_UNUSED CK_IN KFILE_SYSTEM_STR *self,
	CK_IN CK_BYTE_PTR pEncoding,
	CK_IN CK_ULONG ulEncoding,
	CK_IN  char *szPath
)
{
	CK_RV rv;
	FILE *f;

	assert(pEncoding && szPath);
	if ((rv = (f = fopen(szPath, "w")) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		rv = fwrite(pEncoding, 1, ulEncoding, f) == ulEncoding ? CKR_OK : CKR_FUNCTION_FAILED;
		fclose(f);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_BBOOL, __erase)(CK_IN KFILE_SYSTEM_STR *self, CK_IN KSPECIAL_FOLDER iFolder, CK_IN  char *szObject)
{
	CK_BBOOL ret = CK_FALSE;
	char szFile[CK_FILENAME_MAX], *szFolder = self->get_folder(self, iFolder);
	if (szFolder)
	{
		strcpy(szFile, szFolder);
		strcat(szFile, szObject);
		ret = !unlink(szFile);
	}
	return ret;
}
CK_IMPLEMENTATION(CK_RV, __open)
(
	CK_IN  KFILE_SYSTEM_STR *self,
	CK_IN  KSPECIAL_FOLDER iFolder,
	CK_IN  char *szObject,
	CK_IN  char *szMode,
	CK_OUT CK_VOID_PTR_PTR fHandle
)
{
	CK_RV rv;
	KFUX_FILE_SYSTEM fs = (KFUX_FILE_SYSTEM) self->pSlot;
	char *szFolder, szPath[CK_FILENAME_MAX];
	FILE *f;

	assert(fs && szObject && szMode);
	if ((szFolder = __get_special_folder(fs, iFolder)))
	{
		strcpy(szPath, szFolder);
		strcat(szPath, szObject);
		if ((rv = (f = fopen(szPath, szMode)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK) *fHandle = f;
	}
	else rv = CKR_ARGUMENTS_BAD;
	return rv;
}
CK_IMPLEMENTATION(void, __write)(CK_UNUSED CK_IN KFILE_SYSTEM_STR *self, CK_IN CK_VOID_PTR fHandle, CK_IN CK_BYTE_PTR pData, CK_IN CK_ULONG ulData)
{
	FILE *f = (FILE*) fHandle;

	assert(f && pData);
	fwrite(pData, ulData, 1, f);
	fflush(f);
}
CK_IMPLEMENTATION(void, __close)(CK_UNUSED CK_IN KFILE_SYSTEM_STR *self, CK_IN CK_VOID_PTR fHandle)
{
	assert(fHandle);
	fclose((FILE*) fHandle);
}
static KFILE_SYSTEM_STR __defaultFS =
{
	NULL,
	__get_folder,
	__exists,
	__list,
	__load,
	__load_path,
	__store,
	__store_path,
	__erase,
	__open,
	__write,
	__close
};
CK_IMPLEMENTATION(CK_RV, __mkdir)(char *szFolder)
{
	CK_RV rv = CKR_OK;
	struct stat st = {0};
	if (stat(szFolder, &st)) rv = mkdir(szFolder, S_IRWXU) ? CKR_FUNCTION_FAILED : CKR_OK;
	return rv;

}
CK_IMPLEMENTATION(CK_RV, __ux_home_directory)(char *szHome)
{
	CK_RV rv = CKR_OK;
	char *home, *data =  NULL;
	struct passwd *pw;
	CK_BBOOL bFree = CK_FALSE;

	assert(szHome);
	if (!(data = getenv("XDG_DATA_HOME")))
	{
		if (!(home = getenv("HOME")))
		{
			if ((rv = (pw = getpwuid(getuid())) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK) home = pw->pw_dir;
		}
		if
		(
			rv == CKR_OK &&
			(rv = (data = (char*) malloc(strlen(home) + strlen("/.local/share") + 1)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			bFree = CK_TRUE;
			strcpy(data, home);
			strcat(data, "/.local");
			if ((rv = __mkdir(data)) == CKR_OK)
			{
				strcat(data, "/share");
				rv = __mkdir(data);
			}
		}
	}
	if (rv == CKR_OK)
	{
		strcpy(szHome, data);
		strcat(szHome, "/kiripema/");
		if (bFree) free(data);
		rv = __mkdir(szHome);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __put_KFMAP)(CK_INOUT khash_t(KFMAP) *kFolders, CK_IN KSPECIAL_FOLDER iFolder, CK_IN KUX_FOLDER pFolder)
{
	CK_RV rv = CKR_OK;
	khiter_t k;
	int ret;
	k = kh_put(KFMAP, kFolders, iFolder, &ret);
	if (ret > 0) kh_value(kFolders, k) = pFolder;
	else rv = CKR_FUNCTION_FAILED;
	return rv;
}
CK_NEW(__new_ux_fs)(CK_OUT KFILE_SYSTEM *hFS)
{
	CK_RV rv;
	char szHome[CK_FILENAME_MAX], szPub[CK_FILENAME_MAX], szPriv[CK_FILENAME_MAX];
	KUX_FOLDER pHome = NULL, pPub = NULL, pPriv = NULL;
	KFUX_FILE_SYSTEM pFS = NULL;
	KFILE_SYSTEM pOut;

	if ((rv = __ux_home_directory(szHome)) == CKR_OK)
	{
		strcpy(szPub, szHome);
		strcat(szPub, "pub/");
		if ((rv = __mkdir(szPub)) == CKR_OK)
		{
			strcpy(szPriv, szHome);
			strcat(szPriv, "priv/");
			rv = __mkdir(szPriv);
		}
	}
	if
	(
		rv == CKR_OK &&
		(rv = (pHome = malloc(sizeof(KUX_FOLDER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = (pPub = malloc(sizeof(KUX_FOLDER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = (pPriv = malloc(sizeof(KUX_FOLDER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = (pFS = (KFUX_FILE_SYSTEM) malloc(sizeof(KFUX_FILE_SYSTEM_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		pHome->iFolder = KSF_HOME;
		strcpy(pHome->szFolder, szHome);
		pPub->iFolder = KSF_PUB_OBJECTS;
		strcpy(pPub->szFolder, szPub);
		pPriv->iFolder = KSF_PRIV_OBJECTS;
		strcpy(pPriv->szFolder, szPriv);
		strcpy(pFS->szAuth, szPub);
		strcat(pFS->szAuth, KP_AUTH_TOKEN);
		if
		(
			(rv = (pFS->kFolders = kh_init(KFMAP)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = __put_KFMAP(pFS->kFolders, pHome->iFolder, pHome)) == CKR_OK &&
			(rv = __put_KFMAP(pFS->kFolders, pPub->iFolder, pPub)) == CKR_OK &&
			(rv = __put_KFMAP(pFS->kFolders, pPriv->iFolder, pPriv)) == CKR_OK &&
			(rv = (pOut = (KFILE_SYSTEM) malloc(sizeof(KFILE_SYSTEM_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			memcpy(pOut, &__defaultFS, sizeof(KFILE_SYSTEM_STR));
			pOut->pSlot = pFS;
			*hFS = pOut;
		}
	}
	if (rv != CKR_OK)
	{
		if (pHome) free(pHome);
		if (pPub) free(pPub);
		if (pPriv) free(pPriv);
		if (pFS)
		{
			if (pFS->kFolders) kh_destroy(KFMAP, pFS->kFolders);
			free(pFS);
		}
	}
	return rv;
}
CK_DELETE(__release_us_fs)(CK_INOUT KFILE_SYSTEM pHandler)
{
	KFUX_FILE_SYSTEM pFS;
	khiter_t k;

	if (pHandler)
	{
		pFS = (KFUX_FILE_SYSTEM) pHandler->pSlot;
		if (pFS)
		{
			for (k = kh_begin(pFS->kFolders); k != kh_end(pFS->kFolders); ++k)
			if (kh_exist(pFS->kFolders, k))
			{
				free(kh_val(pFS->kFolders, k));
				kh_del(KFMAP, pFS->kFolders, k);
			}
			kh_destroy(KFMAP, pFS->kFolders);
			free(pFS);
		}
		free(pHandler);
	}
}


#elif defined(_WINDOWS)
#include <Knownfolders.h>
#include <Shlobj.h>

CK_IMPLEMENTATION(char*, __get_folder)(CK_IN KFILE_SYSTEM_STR * self, CK_IN KSPECIAL_FOLDER iFolder)
{
	char *szHome = (char*) self->pSlot, *szRet = NULL;
	assert(szHome);
	if (iFolder == KSF_LOG) szRet = szHome;
	return szRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __exists)(CK_IN KFILE_SYSTEM_STR *self, CK_IN KSPECIAL_FOLDER iFolder, CK_IN  char *szObject)
{
	return CK_FALSE;
}
CK_IMPLEMENTATION(CK_RV, __list)
(
	CK_IN KFILE_SYSTEM_STR *self,
	CK_IN KSPECIAL_FOLDER iFolder,
	KFS_READDIR callback,
	CK_VOID_PTR pArgument
)
{
	CK_RV rv = CKR_OK;
	char *szFolder = self->get_folder(self, iFolder), *szFile;
	size_t ulLen;
	wchar_t *szTarget;
	HANDLE hFind;
	WIN32_FIND_DATA ffd;
	assert(callback);
	if (szFolder)
	{
		ulLen = mbstowcs(NULL, szFolder, 0);
		if ((rv = (szTarget = (wchar_t *) malloc( (ulLen + 1) * sizeof( wchar_t ))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			mbstowcs(szTarget, szFolder, ulLen + 1);
			if ((rv = (hFind = FindFirstFile(szTarget, &ffd)) != INVALID_HANDLE_VALUE ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
			{
				do
				{
					if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
					{
						ulLen = wcstombs(NULL, ffd.cFileName, 0);
						if ((rv = (szFile = (char*) malloc(ulLen + 1)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
						{
							wcstombs(szFile, ffd.cFileName, ulLen + 1);
							rv = callback(szFolder, szFile, pArgument);
							free(szFile);
						}
					}
				}
				while ( rv == CKR_OK && FindNextFile(hFind, &ffd) != 0);
				FindClose(hFind);
			}
			free(szTarget);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __load)
(
	CK_UNUSED CK_IN  KFILE_SYSTEM_STR *self,
	CK_UNUSED CK_IN  KSPECIAL_FOLDER iFolder,
	CK_UNUSED CK_IN  char *szObject,
	CK_UNUSED CK_OUT CK_BYTE_PTR *pEncoding,
	CK_UNUSED CK_OUT CK_ULONG_PTR pulEncoding
)	{ return CKR_FUNCTION_NOT_SUPPORTED; }
CK_IMPLEMENTATION(CK_RV, __load_path)
(
	CK_UNUSED CK_IN  KFILE_SYSTEM_STR *self,
	CK_IN  char *szPath,
	CK_OUT CK_BYTE_PTR *pEncoding,
	CK_OUT CK_ULONG_PTR pulEncoding
)	{ return CKR_FUNCTION_NOT_SUPPORTED; }
CK_IMPLEMENTATION(CK_RV, __store)
(
	CK_UNUSED CK_IN KFILE_SYSTEM_STR *self,
	CK_UNUSED CK_IN CK_BYTE_PTR pEncoding,
	CK_UNUSED CK_IN CK_ULONG ulEncoding,
	CK_UNUSED CK_IN  KSPECIAL_FOLDER iFolder,
	CK_UNUSED CK_IN  char *szObject
)	{ return CKR_FUNCTION_NOT_SUPPORTED; }
CK_IMPLEMENTATION(CK_RV, __store_path)
(
	CK_UNUSED CK_IN KFILE_SYSTEM_STR *self,
	CK_IN CK_BYTE_PTR pEncoding,
	CK_IN CK_ULONG ulEncoding,
	CK_IN  char *szPath
)	{ return CKR_FUNCTION_NOT_SUPPORTED; }
CK_IMPLEMENTATION(CK_BBOOL, __erase)
(
	CK_IN KFILE_SYSTEM_STR *self,
	CK_IN KSPECIAL_FOLDER iFolder,
	CK_IN  char *szObject
)	{ return CKR_FUNCTION_NOT_SUPPORTED; }
CK_IMPLEMENTATION(CK_RV, __open)
(
	CK_IN  KFILE_SYSTEM_STR *self,
	CK_IN  KSPECIAL_FOLDER iFolder,
	CK_IN  char *szObject,
	CK_IN  char *szMode,
	CK_OUT CK_VOID_PTR_PTR fHandle
)
{
	CK_RV rv = CKR_FUNCTION_NOT_SUPPORTED;
	char szPath[CK_FILENAME_MAX], *szFolder;
	FILE *f = NULL;
	if ((szFolder = self->get_folder(self, iFolder)))
	{
		strcpy(szPath, szFolder);
		strcat(szPath, szObject);
		if ((rv = (f = fopen(szPath, szMode)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK) *fHandle = f;
	}
	return rv;
}
CK_IMPLEMENTATION(void, __write)(CK_UNUSED CK_IN KFILE_SYSTEM_STR *self, CK_IN CK_VOID_PTR fHandle, CK_IN CK_BYTE_PTR pData, CK_IN CK_ULONG ulData)
{
	FILE *f = (FILE*) fHandle;
	assert(fHandle && pData);
	fwrite(pData, ulData, 1, f);
	fflush(f);
}
CK_IMPLEMENTATION(void, __close)(CK_UNUSED CK_IN KFILE_SYSTEM_STR *self, CK_IN CK_VOID_PTR fHandle)
{
	FILE *f = (FILE*) fHandle;
	if (f) fclose(f);
}
static KFILE_SYSTEM_STR __defaultFS =
{
	NULL,
	__get_folder,
	__exists,
	__list,
	__load,
	__load_path,
	__store,
	__store_path,
	__erase,
	__open,
	__write,
	__close
};
CK_NEW(__new_win_fs)(CK_OUT KFILE_SYSTEM *hFS)
{
	CK_RV rv;
	PWSTR szPath;
	size_t ulLen;
	char *szHome;
	KFILE_SYSTEM pRet;

	if ((rv = SHGetKnownFolderPath(&FOLDERID_LocalAppData, 0, NULL, &szPath) == S_OK ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK)
	{
		ulLen = wcstombs(NULL, szPath, 0) + strlen("\\kiripema\\") + 1;
		if ((rv = (szHome = (char*) malloc(ulLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			wcstombs(szHome, szPath, ulLen);
			strcat(szHome, "\\kiripema\\");
			CreateDirectoryA(szHome, NULL);
			if ((rv = (pRet = (KFILE_SYSTEM) malloc(sizeof(KFILE_SYSTEM_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
			{
				memcpy(pRet, &__defaultFS, sizeof(KFILE_SYSTEM_STR));
				pRet->pSlot = szHome;
				*hFS = pRet;
			}
			else free(szHome);
		}
		CoTaskMemFree(szPath);
	}
	return rv;
}
CK_DELETE(__delete_win_fs)(CK_INOUT KFILE_SYSTEM hFS)
{
	if (hFS)
	{
		if (hFS->pSlot) free(hFS->pSlot);
		free(hFS);
	}
}

#elif defined(_DROID)
/* TODO: Android implementation */

#endif
