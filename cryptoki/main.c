#include "kiripema.h"
#include <assert.h>

KLOG_HANDLER __pLog = NULL;
#ifdef _WINDOWS
#include <tchar.h>
BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason, LPVOID lpReserved)
{
	BOOL ret = TRUE;
	LPTSTR pszEnv = NULL;
	CK_BBOOL isOn = CK_FALSE;

	switch (ul_reason)
	{
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_ATTACH:
#ifdef _DEBUG
		isOn = CK_TRUE;
#else
		if ((pszEnv = (LPTSTR) malloc(1024 * sizeof(TCHAR))))
		{
			if (GetEnvironmentVariable(ENV_LOG_ENTRY, pszEnv, 1024) && wcscmp(pszEnv, LOG_IS_ON) == 0) isOn = CK_TRUE;
			free(pszEnv);
		}
#endif
		if (CK_new_log(isOn, &__pLog) != CKR_OK) ret = FALSE;
		break;
	case DLL_PROCESS_DETACH:
		if (__pLog) CK_delete_log(__pLog);
		break;
	}
	return ret;
}

#else
#include <stdlib.h>
#include <pthread.h>

CK_CONSTRUCTOR CK_IMPLEMENTATION(void, __init)(void)
{
	CK_BBOOL isOn = CK_FALSE;
#ifndef _DEBUG
	char *szEnv;
	if ((szEnv = getenv(ENV_LOG_ENTRY)) && (strcmp(szEnv, LOG_IS_ON) == 0)) isOn = CK_TRUE;
#else
	isOn = CK_TRUE;
#endif
	CK_new_log(isOn, &__pLog);
}
CK_DESTRUCTOR CK_IMPLEMENTATION(void, __destroy)(void)
{
	if (__pLog) CK_delete_log(__pLog);
}
#endif

#ifdef _DEBUG
#include <stdio.h>
int saveToFile(unsigned char *buffer, size_t size, char *filename)
{
	FILE *stream;
	int err = 0;
	assert(buffer && filename);
	if ((stream = fopen(filename, "wb")))
	{
		if (fwrite(buffer, sizeof(unsigned char), size, stream) != size) err = ferror(stream);
		fclose(stream);
	}
	else err = errno;
	return err;
}
void kprintASNTree(NH_ASN1_PNODE node, int level)
{
	int i, optional, tag, id;
	while (node)
	{
		for (i = 0; i < level; i++) printf("  ");

		optional = ASN_IS_ON(NH_ASN1_OPTIONAL_BIT, node->knowledge) || ASN_IS_ON(NH_ASN1_DEFAULT_BIT, node->knowledge);
		tag = ((node->knowledge) & NH_ASN1_TAG_MASK);
		if (node->identifier) id = (int) *node->identifier;
		else id = 0;

		printf
		(
			"%p, identifier: 0x%X, tag %d, size: %d, knowledge: %d, valuelen: %d, optional: %d, value:[",
			(void*) node,
			id,
			tag,
			node->size,
			node->knowledge,
			node->valuelen,
			optional
		);
		for(i = 0; i < (int) node->valuelen; i++)
		{
			printf("%02X",*((char *)node->value + i));
		}
		printf("]\n");

		if (node->child) kprintASNTree(node->child, level + 1);
		node = node->next;
	}
	printf("\n");
}
#endif

CK_FUNCTION(char*, CK_bin2hex)(CK_IN unsigned char *bin, CK_IN CK_ULONG len)
{
	unsigned char *out;
	CK_ULONG i;
	if (!bin || !len) return NULL;
	if ((out = (unsigned char*) malloc(len * 2 + 1)))
	{
		for (i = 0; i < len; i++)
		{
			out[i * 2    ] = "0123456789ABCDEF"[bin[i] >> 4  ];
			out[i * 2 + 1] = "0123456789ABCDEF"[bin[i] & 0x0F];
		}
		out[len * 2] = '\0';
	}
	return (char*) out;
}

/**
 * Mutex facility
 */
CK_IMPLEMENTATION(CK_RV, __create_mutex)(CK_VOID_PTR_PTR pMutex)
{
	CK_RV rv;
#ifdef _WINDOWS
	HANDLE _mutex;
	rv = (_mutex = CreateMutex(NULL, FALSE, NULL)) ? CKR_OK : CKR_GENERAL_ERROR;
	WINAPI_ASSERT(rv != CKR_OK, "CreateMutex");

#else
	pthread_mutex_t *_mutex;
	if ((rv = (_mutex = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		if ((rv = pthread_mutex_init(_mutex, NULL) == 0 ? CKR_OK : CKR_GENERAL_ERROR) != CKR_OK) free(_mutex);
	}
#endif
	if (rv == CKR_OK) *pMutex = _mutex;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __lock)(CK_VOID_PTR pMutex)
{
	CK_RV rv;
#ifdef _WINDOWS
	DWORD dwRv;
#endif
	if ((rv = pMutex ? CKR_OK : CKR_MUTEX_BAD) == CKR_OK)
	{
#ifdef _WINDOWS
		WINAPI_ASSERT((dwRv = WaitForSingleObject((HANDLE) pMutex, INFINITE)) == WAIT_FAILED, "WaitForSingleObject");
		rv = dwRv == WAIT_OBJECT_0 ? CKR_OK : CKR_GENERAL_ERROR;
#else
		rv = pthread_mutex_lock((pthread_mutex_t*) pMutex) == 0 ? CKR_OK : CKR_GENERAL_ERROR;
#endif
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __unlock)(CK_VOID_PTR pMutex)
{
	CK_RV rv;
	if ((rv = pMutex ? CKR_OK : CKR_MUTEX_BAD) == CKR_OK)
	{
#ifdef _WINDOWS
		rv = ReleaseMutex((HANDLE) pMutex) ? CKR_OK : CKR_GENERAL_ERROR;
		WINAPI_ASSERT(rv != CKR_OK, "ReleaseMutex");
#else
		rv = pthread_mutex_unlock((pthread_mutex_t*) pMutex) == 0 ? CKR_OK : CKR_GENERAL_ERROR;
#endif
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __destroy_mutex)(CK_VOID_PTR pMutex)
{
#ifdef _WINDOWS
	BOOL ret;
#endif
	CK_RV rv;
	if ((rv = pMutex ? CKR_OK : CKR_MUTEX_BAD) == CKR_OK)
	{
#ifdef _WINDOWS
		
		WINAPI_ASSERT(!(ret = CloseHandle((HANDLE) pMutex)), "CloseHandle");
		rv = ret ? CKR_OK : CKR_GENERAL_ERROR;
#else
		if ((rv = pthread_mutex_destroy((pthread_mutex_t *) pMutex) == 0 ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK) free(pMutex);
#endif
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __clone)(CK_IN KMUTEX_HANDLER_STR *self, CK_OUT KMUTEX_HANDLER_STR **pOut)
{
	CK_RV rv;
	KMUTEX_HANDLER pClone;

	if ((rv = (pClone = (KMUTEX_HANDLER) malloc(sizeof(KMUTEX_HANDLER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(pClone, self, sizeof(KMUTEX_HANDLER_STR));
		*pOut = pClone;
	}
	return rv;
}
CK_NEW(CK_new_mutex_handler)(CK_IN CK_C_INITIALIZE_ARGS_PTR pArgs, CK_OUT KMUTEX_HANDLER *pOut)
{
	CK_RV rv;
	KMUTEX_HANDLER hHandler;

	if ((rv = (hHandler = (KMUTEX_HANDLER) malloc(sizeof(KMUTEX_HANDLER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		if (pArgs)
		{
			hHandler->CreateMutex = pArgs->CreateMutex;
			hHandler->LockMutex = pArgs->LockMutex;
			hHandler->UnlockMutex = pArgs->UnlockMutex;
			hHandler->DestroyMutex = pArgs->UnlockMutex;
		}
		else
		{
			hHandler->CreateMutex = __create_mutex;
			hHandler->LockMutex = __lock;
			hHandler->UnlockMutex = __unlock;
			hHandler->DestroyMutex = __destroy_mutex;
		}
		hHandler->clone = __clone;
		*pOut = hHandler;
	}
	return rv;
}
CK_DELETE(CK_release_mutex_handler)(CK_INOUT KMUTEX_HANDLER hHandler)
{
	if (hHandler) free(hHandler);
}
