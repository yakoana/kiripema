/**
 * @file session.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Session handler implementation
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kiripema.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

CK_INLINE CK_IMPLEMENTATION(CK_RV, __assert_session_handle)
(
	CK_IN KSESSION_HANDLER_STR *hHandler,
	CK_IN CK_SESSION_HANDLE hSession,
	CK_OUT khiter_t *kID
)
{
	CK_RV rv;
	khiter_t k;

	k = kh_get(KSMAP, hHandler->kSessions, hSession);
	if ((rv = k != kh_end(hHandler->kSessions) && kh_exist(hHandler->kSessions, k) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK)
	{
		if (kID) *kID = k;
	}
	return rv;
}
CK_IMPLEMENTATION(CK_BBOOL, __is_open)(CK_IN KSESSION_HANDLER_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	return __assert_session_handle(self, hSession, NULL) == CKR_OK;
}
CK_IMPLEMENTATION(KSESSION, __get_session)(CK_IN KSESSION_HANDLER_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	khiter_t k;
	KSESSION pRet = NULL;

	if (__assert_session_handle(self, hSession, &k) == CKR_OK) pRet = kh_value(self->kSessions, k);
	return pRet;
}
CK_IMPLEMENTATION(CK_STATE, __get_state)(CK_IN KSESSION_STR *self, CK_IN CK_BBOOL isLoged)
{
	if ( isLoged  &&  self->isRW) return CKS_RW_USER_FUNCTIONS;
	if ( isLoged  && !self->isRW) return CKS_RO_USER_FUNCTIONS;
	if (!isLoged  &&  self->isRW) return CKS_RW_PUBLIC_SESSION;
	return CKS_RO_PUBLIC_SESSION;
}
CK_IMPLEMENTATION(CK_RV, __init_crypto)(CK_INOUT KSESSION_STR* self, CK_IN CK_MECHANISM_TYPE ulMechanism, CK_IN KOBJECT pObject)
{
	CK_RV rv;

	assert(pObject);
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		self->Operation.kCrypto.hMechanism = ulMechanism;
		self->Operation.kCrypto.hKey = pObject;
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __finish_crypto)(CK_INOUT KSESSION_STR *self)
{
	CK_RV rv;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		self->Operation.kCrypto.hMechanism = CK_UNAVAILABLE_INFORMATION;
		self->Operation.kCrypto.hKey = NULL;
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __init_search)(CK_INOUT KSESSION_STR *self)
{
	CK_RV rv;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		if
		(
			(rv = (self->Operation.pSearch = (KOBJECT_SEARCH) malloc(sizeof(KOBJECT_SEARCH_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = (self->Operation.pSearch->kSearch = kh_init(KOSET)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)	self->Operation.pSearch->k = kh_begin(self->Operation.pSearch->kSearch);
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __next_object)(CK_IN KSESSION_STR *self, CK_OUT CK_OBJECT_HANDLE_PTR phNext)
{
	CK_OBJECT_HANDLE hNext = CK_UNAVAILABLE_INFORMATION;
	CK_RV rv;
	
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		while (hNext == CK_UNAVAILABLE_INFORMATION && self->Operation.pSearch->k != kh_end(self->Operation.pSearch->kSearch))
		{
			if (kh_exist(self->Operation.pSearch->kSearch, self->Operation.pSearch->k))
				hNext = kh_value(self->Operation.pSearch->kSearch, self->Operation.pSearch->k);
			self->Operation.pSearch->k++;
		}
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	if (rv == CKR_OK) *phNext = hNext;
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __found_object)(CK_IN KSESSION_STR *self, CK_IN CK_OBJECT_HANDLE hObject)
{
	CK_RV rv;
	khiter_t k;
	int ret;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		k = kh_put(KOSET, self->Operation.pSearch->kSearch, hObject, &ret);
		if (ret > 0) kh_value(self->Operation.pSearch->kSearch, k) = hObject;
		else rv = CKR_FUNCTION_FAILED;
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __finish_search)(CK_INOUT KSESSION_STR *self)
{
	CK_RV rv;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		kh_destroy(KOSET, self->Operation.pSearch->kSearch);
		free(self->Operation.pSearch);
		self->Operation.pSearch = NULL;
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
static KSESSION_STR __defaut_session =
{
	NULL,
	NULL,
	NULL,
	0UL,
	CK_FALSE,
	0UL,
	{ NULL, { CK_UNAVAILABLE_INFORMATION, NULL }},
	__get_state,
	__init_crypto,
	__finish_crypto,
	__init_search,
	__next_object,
	__found_object,
	__finish_search
};
CK_IMPLEMENTATION(void, __cleanup_session)(CK_INOUT KSESSION pSession)
{
	if (pSession)
	{
		if (pSession->hMutex)
		{
			if (pSession->pMutex) pSession->hMutex->DestroyMutex(pSession->pMutex);
			CK_release_mutex_handler(pSession->hMutex);
		}
		if (pSession->kObjects) kh_destroy(KOSET, pSession->kObjects);	/* TODO: Session objects must be destroyed */
		if (pSession->Operation.pSearch)
		{
			kh_destroy(KOSET, pSession->Operation.pSearch->kSearch);
			free(pSession->Operation.pSearch);
		}
		free(pSession);
	}
}
CK_IMPLEMENTATION(CK_RV, __new_session)(CK_INOUT KSESSION_HANDLER_STR *self, CK_IN CK_BBOOL isRW, CK_OUT CK_SESSION_HANDLE_PTR phSession)
{
	CK_RV rv;
	KSESSION pSession;
	khiter_t k;
	int ret;

	if ((rv = (pSession = (KSESSION) malloc(sizeof(KSESSION_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(pSession, &__defaut_session, sizeof(KSESSION_STR));
		if
		(
			(rv = self->hMutex->clone(self->hMutex, &pSession->hMutex)) == CKR_OK &&
			(rv = pSession->hMutex->CreateMutex(&pSession->pMutex)) == CKR_OK &&
			(rv = (pSession->kObjects = kh_init(KOSET)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			pSession->isRW = isRW;
			if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
			{
				pSession->hHandle = ++self->hSessionCount;
				k = kh_put(KSMAP, self->kSessions, pSession->hHandle, &ret);
				if (ret > 0)
				{
					kh_val(self->kSessions, k) = pSession;
					*phSession = pSession->hHandle;
				}
				else rv = CKR_FUNCTION_FAILED;
				self->hMutex->UnlockMutex(self->pMutex);
			}
		}
		if (rv != CKR_OK) __cleanup_session(pSession);
	}
	return rv;

}
CK_IMPLEMENTATION(CK_RV, __delete_session)(CK_IN KSESSION_HANDLER_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	CK_RV rv;
	KSESSION pSession;
	khiter_t k;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		if ((rv = __assert_session_handle(self, hSession, &k)) == CKR_OK)
		{
			pSession = kh_value(self->kSessions, k);
			kh_del(KSMAP, self->kSessions, k);
			__cleanup_session(pSession);
		}
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __delete_all)(CK_IN KSESSION_HANDLER_STR *self)
{
	CK_RV rv;
	KSESSION pSession;
	khiter_t k;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		for (k = kh_begin(self->kSessions); k != kh_end(self->kSessions); ++k)
		if (kh_exist(self->kSessions, k))
		{
			pSession = kh_value(self->kSessions, k);
			__cleanup_session(pSession);
		}
		kh_clear(KSMAP, self->kSessions);
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __add_object)(CK_IN KSESSION_HANDLER_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_IN CK_OBJECT_HANDLE hObject)
{
	CK_RV rv;
	KSESSION pSession;
	khiter_t k;
	int ret;

	if ((rv = (pSession = self->get_session(self, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK)
	{
		if ((rv = pSession->hMutex->LockMutex(pSession->pMutex)) == CKR_OK)
		{
			k = kh_get(KOSET, pSession->kObjects, hObject);
			if (k == kh_end(pSession->kObjects))
			{
				k = kh_put(KOSET, pSession->kObjects, hObject, &ret);
				if (ret > 0) kh_value(pSession->kObjects, k) = hObject;
				else rv = CKR_FUNCTION_FAILED;
			}
			else rv = CKR_FUNCTION_FAILED;
			pSession->hMutex->UnlockMutex(pSession->pMutex);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __remove_object)(CK_IN KSESSION_HANDLER_STR *self, CK_IN CK_SESSION_HANDLE hSession, CK_IN CK_OBJECT_HANDLE hObject)
{
	CK_RV rv;
	KSESSION pSession;
	khiter_t k;

	if ((rv = (pSession = self->get_session(self, hSession)) ? CKR_OK : CKR_SESSION_HANDLE_INVALID) == CKR_OK)
	{
		if ((rv = pSession->hMutex->LockMutex(pSession->pMutex)) == CKR_OK)
		{
			k = kh_get(KOSET, pSession->kObjects, hObject);
			if (k != kh_end(pSession->kObjects) && kh_exist(pSession->kObjects, k)) kh_del(KOSET, pSession->kObjects, hObject);
			else rv = CKR_OBJECT_HANDLE_INVALID;
			pSession->hMutex->UnlockMutex(pSession->pMutex);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __remove_all_objects)(CK_IN KSESSION_HANDLER_STR *self)
{
	CK_RV rv = CKR_OK;
	KSESSION pSession;
	khiter_t k, j;

	for (j = kh_begin(self->kSessions); j != kh_end(self->kSessions); ++j)
		if (kh_exist(self->kSessions, j))
		{
			
			if
			(
				(rv = (pSession = kh_value(self->kSessions, j)) ? CKR_OK : CKR_GENERAL_ERROR) == CKR_OK && 
				(rv = pSession->hMutex->LockMutex(pSession->pMutex)) == CKR_OK
			)
			{
				for (k = kh_begin(pSession->kObjects); k!= kh_end(pSession->kObjects); ++k)
					if (kh_exist(pSession->kObjects, k))
					{
						/* TODO: Session objects must be destroyed */
					}
				pSession->hMutex->UnlockMutex(pSession->pMutex);
			}
		}
	return rv;
}

CK_IMPLEMENTATION(CK_BBOOL, __search_is_active)(CK_IN KSESSION_HANDLER_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	CK_BBOOL is = CK_FALSE;
	KSESSION pSession;

	if ((pSession = self->get_session(self, hSession))) is = (pSession->Operation.pSearch != NULL);
	return is;
}
CK_IMPLEMENTATION(CK_BBOOL, __crypto_is_active)(CK_IN KSESSION_HANDLER_STR *self, CK_IN CK_SESSION_HANDLE hSession)
{
	CK_BBOOL is = CK_FALSE;
	KSESSION pSession;

	if ((pSession = self->get_session(self, hSession))) is = pSession->Operation.kCrypto.hKey && pSession->Operation.kCrypto.hMechanism != CK_UNAVAILABLE_INFORMATION;
	return is;
}
static KSESSION_HANDLER_STR __default_session_handler =
{
	NULL,				/* pMutex */
	NULL,				/* hMutex */
	0UL,				/* hSessionCount*/
	NULL,				/* kSessions */

	__is_open,
	__get_session,
	__new_session,
	__delete_session,
	__delete_all,
	__add_object,
	__remove_object,
	__remove_all_objects,
	__search_is_active,
	__crypto_is_active
};
CK_NEW(CK_new_session_handler)(CK_IN KMUTEX_HANDLER  pMutex, CK_OUT KSESSION_HANDLER *hHandler)
{
	CK_RV rv;
	KSESSION_HANDLER hOut;

	if ((rv = (hOut = (KSESSION_HANDLER) malloc(sizeof(KSESSION_HANDLER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(hOut, &__default_session_handler, sizeof(KSESSION_HANDLER_STR));
		if
		(
			(rv = pMutex->clone(pMutex, &hOut->hMutex)) == CKR_OK &&
			(rv = hOut->hMutex->CreateMutex(&hOut->pMutex)) == CKR_OK &&
			(rv = (hOut->kSessions = kh_init(KSMAP)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)	*hHandler = hOut;
		else CK_release_session_handler(hOut);
	}
	return rv;

}
CK_DELETE(CK_release_session_handler)(CK_INOUT KSESSION_HANDLER hHandler)
{
	khiter_t k;
	if (hHandler)
	{
		if (hHandler->hMutex)
		{
			hHandler->hMutex->DestroyMutex(hHandler->pMutex);
			CK_release_mutex_handler(hHandler->hMutex);
		}
		if (hHandler->kSessions)
		{
			for (k = kh_begin(hHandler->kSessions); k != kh_end(hHandler->kSessions); ++k)
			if (kh_exist(hHandler->kSessions, k)) __cleanup_session(kh_value(hHandler->kSessions, k));
			kh_destroy(KSMAP, hHandler->kSessions);
		}
		free(hHandler);
	}
}
