/**
 * @file cache.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Cryptographic Services PKCS#11 implementation - object cache
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "kiripema.h"
#include <assert.h>
#include <string.h>

#define __SEARCH_ONLY_SESSION_OBJECT	(1)						/* Look for session objects */
#define __SEARCH_ONLY_TOKEN_OBJECT		(__SEARCH_ONLY_SESSION_OBJECT << 1)	/* Look for token objects */
#define __SEARCH_TOKEN_SESSION_OBJECTS	(__SEARCH_ONLY_SESSION_OBJECT << 2)	/* Look for token and session objects */
#define __SEARCH_CERT_OBJECT			(__SEARCH_ONLY_SESSION_OBJECT << 3)	/* Look for certificates */
#define __SEARCH_PUBKEY_OBJECT		(__SEARCH_ONLY_SESSION_OBJECT << 4)	/* Look for public keys */
#define __SEARCH_PRIVKEY_OBJECT		(__SEARCH_ONLY_SESSION_OBJECT << 5)	/* Look for private keys */
#define __SEARCH_KEY_OBJECT			(__SEARCH_ONLY_SESSION_OBJECT << 6)	/* Look for secret keys */
#define __SEARCH_ALL_OBJECTS			(0x0F)

#ifdef _UNIX
#include "uxob.h"

CK_INLINE CK_FUNCTION(CK_OBJECT_CLASS, CKO_get_class)(CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_OBJECT_CLASS ulRet = CK_UNAVAILABLE_INFORMATION;
	CK_ULONG i;
	for (i = 0; i < ulCount; i++)
	{
		if (pTemplate[i].type == CKA_CLASS && pTemplate[i].ulValueLen == sizeof(CK_OBJECT_CLASS))
		{
			ulRet = *(CK_OBJECT_CLASS*)pTemplate[i].pValue;
			switch (ulRet)
			{
			case CKO_CERTIFICATE:
			case CKO_PUBLIC_KEY:
			case CKO_PRIVATE_KEY:
				break;
			default: ulRet = CK_UNAVAILABLE_INFORMATION;
			}
			return ulRet;
		}
	}
	return ulRet;
}
CK_INLINE CK_FUNCTION(CK_RV, CKO_willbe_session_object)(CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_OUT CK_BBOOL *pIs)
{
	CK_ULONG i = 0;
	CK_RV rv = CK_UNAVAILABLE_INFORMATION;
	while (rv == CK_UNAVAILABLE_INFORMATION && i < ulCount)
	{
		if (pTemplate[i].type == CKA_TOKEN && pTemplate[i].ulValueLen == sizeof(CK_BBOOL))
		{
			*pIs = !*(CK_BBOOL*)pTemplate[i].pValue;		
			rv = CKR_OK;
		}
		i++;
	}
	return rv;
}
CK_FUNCTION(CK_RV, CKO_encode_object)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT KP_BLOB *pOut)
{
	CK_RV rv;
	size_t ulSize;
	unsigned char *pBlob;

	assert(hEncoder && pOut);
	if
	(
		(rv = (ulSize = hEncoder->encoded_size(hEncoder, hEncoder->root)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
		(rv = (pBlob = (unsigned char*) malloc(ulSize)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		rv = hEncoder->encode(hEncoder, hEncoder->root, pBlob);
		NHARU_ASSERT(rv, "NH_ASN1_ENCODER_HANDLE->encode");
		if ((rv = rv == NH_OK ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
		{
			pOut->data = pBlob;
			pOut->length = ulSize;
		}
		else free(pBlob);
	}
	return rv;
}


CK_IMPLEMENTATION(CK_RV, __ls)(CK_IN char *szFolder, CK_IN char *szObject, CK_VOID_PTR pArgument)
{
	CK_RV rv = CKR_OK;
	khash_t(KFSET) *kset = (khash_t(KFSET)*) pArgument;
	char *szPath;
	khiter_t k;
	int ret;

	assert(kset);
	if
	(
		(
			strstr(szObject, KP_CERT_ID) ||
			strstr(szObject, KP_PUBKEY_ID) ||
			strstr(szObject, KP_PRIVKEY_ID) ||
			strstr(szObject, KP_KEY_ID)
		) &&
		(rv = (szPath = (char*) malloc(strlen(szObject) + strlen(szFolder) + 1)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		strcpy(szPath, szFolder);
		strcat(szPath, szObject);
		k = kh_put(KFSET, kset, szPath, &ret);
		if (ret > 0) kh_value(kset, k) = szPath;
		else { rv = CKR_FUNCTION_FAILED; free(szPath); }
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __load)(CK_INOUT KOBJECT_CACHE_STR *self, KTOKEN_STR *pToken, CK_IN KOC_LOADED status)
{
	CK_RV rv = CKR_OK;
	khash_t(KFSET) *kFiles = NULL;
	KSPECIAL_FOLDER iFolder;
	CK_BBOOL canGo = CK_TRUE;
	khiter_t k, j;
	KP_BLOB blob = { NULL, 0UL };
	KOBJECT pObject;
	int ret;

	assert(pToken);
	if (self->loaded == KOC_CACHE_EMPTY && status == KOC_PUBLIC_OBJECTS) iFolder = KSF_PUB_OBJECTS;
	else if (self->loaded == KOC_PUBLIC_OBJECTS && status == KOC_PRIVATE_OBJECTS) iFolder = KSF_PRIV_OBJECTS;
	else canGo = CK_FALSE;
	if (canGo)
	{
		if
		(
			(rv = (kFiles = kh_init(KFSET)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = pToken->pFS->list(pToken->pFS, iFolder, __ls, kFiles)) == CKR_OK &&
			(rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK
		)
		{
			k = kh_begin(kFiles);
			while (rv == CKR_OK && k != kh_end(kFiles))
			{
				if (kh_exist(kFiles, k) && (rv = pToken->pFS->load_path(pToken->pFS, kh_value(kFiles, k), &blob.data, &blob.length)) == CKR_OK)
				{
					if ((rv = CKO_uxload_object(pToken, blob.data, blob.length, &pObject)) == CKR_OK)
					{
						strcpy(UX_LOADER(pObject)->szFilename, basename(kh_value(kFiles, k)));
						pToken->get_handle(pToken, &pObject->hObject);
						j = kh_put(KOMAP, self->kObjects, pObject->hObject, &ret);
						if (ret > 0) kh_value(self->kObjects, j) = pObject;
						else CKO_delete_object(pObject);
					}
					free(blob.data);
				}
				k++;
			}
			if (rv == CKR_OK) self->loaded = status;
			self->hMutex->UnlockMutex(self->pMutex);
		}
	}
	if (kFiles)
	{
		for (k = kh_begin(kFiles); k != kh_end(kFiles); k++) if (kh_exist(kFiles, k)) free(kh_value(kFiles, k));
		kh_destroy(KFSET, kFiles);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_BBOOL, __session_match)(CK_IN KOBJECT pNext, CK_IN int criteria)
{
	return
	(
		( criteria & __SEARCH_TOKEN_SESSION_OBJECTS) ||
		((criteria & __SEARCH_ONLY_SESSION_OBJECT) && !UX_LOADER(pNext)->szFilename) ||
		((criteria & __SEARCH_ONLY_TOKEN_OBJECT)   &&  UX_LOADER(pNext)->szFilename)
	);
}
#else
#include "winob.h"
CK_IMPLEMENTATION(CK_RV, __load_key_from_store)(KOBJECT_CACHE_STR *self, KTOKEN_STR *pToken, CK_OBJECT_CLASS hClass)
{
	CK_RV rv;
	HCERTSTORE hStore;
	PCCERT_CONTEXT pCtx = NULL;
	HCRYPTPROV_OR_NCRYPT_KEY_HANDLE hKey;
	DWORD dwKeySpec = 0;
	BOOL mustFree = FALSE;
	KOBJECT pObject;
	khiter_t k;
	int ret;

	rv = (hStore = CertOpenSystemStoreA((HCRYPTPROV_LEGACY) NULL, "MY")) ? CKR_OK : CKR_FUNCTION_FAILED;
	WINAPI_ASSERT(rv != CKR_OK, "CertOpenSystemStore");
	while (rv == CKR_OK && (pCtx = CertFindCertificateInStore(hStore, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, 0, CERT_FIND_HAS_PRIVATE_KEY, NULL, pCtx)))
	{
		if
		(
			!CertVerifyTimeValidity((HCRYPTPROV_LEGACY) NULL, pCtx->pCertInfo) &&
			CryptAcquireCertificatePrivateKey(pCtx, CRYPT_ACQUIRE_PREFER_NCRYPT_KEY_FLAG | CRYPT_ACQUIRE_COMPARE_KEY_FLAG, NULL, &hKey, &dwKeySpec, &mustFree)
		)
		{
			if ((rv = CKO_wnew_object(CertDuplicateCertificateContext(pCtx), hClass, &pObject)) == CKR_OK)
			{
				pToken->get_handle(pToken, &pObject->hObject);
				k = kh_put(KOMAP, self->kObjects, pObject->hObject, &ret);
				if (ret > 0) kh_value(self->kObjects, k) = pObject;
				else { CKO_delete_object(pObject); rv = CKR_FUNCTION_FAILED; }
			}
			if (mustFree)
			{
				if (dwKeySpec == CERT_NCRYPT_KEY_SPEC) NCryptFreeObject(hKey);
				else CryptReleaseContext(hKey, 0);
			}
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __load_cert_from_store)(KOBJECT_CACHE_STR *self, KTOKEN_STR *pToken, LPCSTR szSubsystemProtocol)
{
	CK_RV rv;
	HCERTSTORE hStore;
	PCCERT_CONTEXT pCtx = NULL;
	KOBJECT pObject;
	khiter_t k;
	int ret;

	rv = (hStore = CertOpenSystemStoreA((HCRYPTPROV_LEGACY) NULL, szSubsystemProtocol)) ? CKR_OK : CKR_FUNCTION_FAILED;
	WINAPI_ASSERT(rv != CKR_OK, "CertOpenSystemStore");
	while (rv == CKR_OK && (pCtx = CertEnumCertificatesInStore(hStore, pCtx)))
	{
		if ((rv = CKO_wnew_object(CertDuplicateCertificateContext(pCtx), CKO_CERTIFICATE, &pObject)) == CKR_OK)
		{
			pToken->get_handle(pToken, &pObject->hObject);
			k = kh_put(KOMAP, self->kObjects, pObject->hObject, &ret);
			if (ret > 0) kh_value(self->kObjects, k) = pObject;
			else { CKO_delete_object(pObject); rv = CKR_FUNCTION_FAILED; }
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __load)(CK_INOUT KOBJECT_CACHE_STR *self, KTOKEN_STR *pToken, CK_IN KOC_LOADED status)
{
	CK_RV rv = CKR_OK;
	CK_BBOOL canGo = CK_TRUE;

	if (self->loaded == KOC_CACHE_EMPTY && status == KOC_PUBLIC_OBJECTS && (rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		if
		(
			(rv = __load_key_from_store(self, pToken, CKO_PUBLIC_KEY)) == CKR_OK &&
			(rv = __load_cert_from_store(self, pToken, "ROOT")) == CKR_OK &&
			(rv = __load_cert_from_store(self, pToken, "CA")) == CKR_OK
		)	rv = __load_cert_from_store(self, pToken, "MY");
		self->hMutex->UnlockMutex(self->pMutex);
	}
	else if (self->loaded == KOC_PUBLIC_OBJECTS && status == KOC_PRIVATE_OBJECTS && (rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		rv = __load_key_from_store(self, pToken, CKO_PRIVATE_KEY);
		self->hMutex->UnlockMutex(self->pMutex);
	}
	if (rv == CKR_OK) self->loaded = status;
	return rv;
}
CK_IMPLEMENTATION(CK_BBOOL, __session_match)(CK_IN KOBJECT pNext, CK_IN int criteria) { return CK_TRUE; }
#endif

CK_IMPLEMENTATION(CK_RV, __clear)(CK_INOUT KOBJECT_CACHE_STR *self)
{
	CK_RV rv;
	khiter_t k;
	KOBJECT pObject;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		for (k = kh_begin(self->kObjects); k != kh_end(self->kObjects); ++k) if (kh_exist(self->kObjects, k))
		{
			pObject = kh_value(self->kObjects, k);
			kh_del(KOMAP, self->kObjects, k);
			CKO_delete_object(pObject);
		}
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __add)(CK_INOUT KOBJECT_CACHE_STR *self, CK_IN KOBJECT pObject)
{
	CK_RV rv;
	khiter_t k;
	int ret;

	assert(pObject);
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		k = kh_put(KOMAP, self->kObjects, pObject->hObject, &ret);
		if (ret > 0) kh_value(self->kObjects, k) = pObject;
		else rv = CKR_GENERAL_ERROR;
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __remove)(CK_INOUT KOBJECT_CACHE_STR *self, CK_IN CK_OBJECT_HANDLE hObject)
{
	CK_RV rv;
	khiter_t k;
	KOBJECT pObject;

	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		if ((k = kh_get(KOMAP, self->kObjects, hObject)) != kh_end(self->kObjects) && k == kh_exist(self->kObjects, k))
		{
			pObject = kh_value(self->kObjects, k);
			kh_del(KOMAP, self->kObjects, k);
			CKO_delete_object(pObject);
		}
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv ;
}
CK_IMPLEMENTATION(KOBJECT, __get)(CK_INOUT KOBJECT_CACHE_STR *self, CK_IN CK_OBJECT_HANDLE hObject)
{
	khiter_t k;
	KOBJECT pRet = NULL;

	if (self->hMutex->LockMutex(self->pMutex) == CKR_OK)
	{
		k = kh_get(KOMAP, self->kObjects, hObject);
		pRet = k != kh_end(self->kObjects) && kh_exist(self->kObjects, k) ? kh_value(self->kObjects, k) : NULL;
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return pRet;
}
CK_INLINE CK_IMPLEMENTATION(int, __match)(CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i = 0;
	CK_BBOOL found = CK_FALSE;
	int iMatch = 0;
	if (ulCount == 0) return (__SEARCH_ALL_OBJECTS | __SEARCH_TOKEN_SESSION_OBJECTS);
	while (!found && i < ulCount)
	{
		if (pTemplate[i].type == CKA_CLASS && pTemplate[i].ulValueLen == sizeof(CK_OBJECT_CLASS))
		{
			switch (*(CK_OBJECT_CLASS *) pTemplate[i].pValue)
			{
			case CKO_CERTIFICATE:
				iMatch |= __SEARCH_CERT_OBJECT;
				break;
			case CKO_PUBLIC_KEY:
				iMatch |= __SEARCH_PUBKEY_OBJECT;
				break;
			case CKO_PRIVATE_KEY:
				iMatch |= __SEARCH_PRIVKEY_OBJECT;
				break;
			case CKO_SECRET_KEY:
				iMatch |= __SEARCH_KEY_OBJECT;
				break;
			}
			found = CK_TRUE;
		}
		i++;
	}
	i = 0;
	while (!found && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CERTIFICATE_TYPE:
			if (pTemplate[i].ulValueLen == sizeof(CK_CERTIFICATE_TYPE) && *(CK_CERTIFICATE_TYPE*) pTemplate[i].pValue == CKC_X_509)
			{
				iMatch |= __SEARCH_CERT_OBJECT;
				found = CK_TRUE;
			}
			break;
		case CKA_CERTIFICATE_CATEGORY:
			iMatch |= __SEARCH_CERT_OBJECT;
			found = CK_TRUE;
			break;
		case CKA_KEY_TYPE:
			if (pTemplate[i].ulValueLen == sizeof(CK_KEY_TYPE))
			{
				if (*(CK_KEY_TYPE*) pTemplate[i]. pValue == CKK_RSA) iMatch |= __SEARCH_PRIVKEY_OBJECT | __SEARCH_PUBKEY_OBJECT;
				else iMatch |= __SEARCH_KEY_OBJECT;
				found = CK_TRUE;
			}
		}
		i++;
	}
	if (!found) iMatch |= __SEARCH_ALL_OBJECTS;
	found = CK_FALSE;
	i = 0;
	while (!found && i < ulCount)
	{
		if (pTemplate[i].type == CKA_TOKEN && pTemplate[i].ulValueLen == sizeof(CK_BBOOL))
		{
			if (*(CK_BBOOL*) pTemplate[i].pValue) iMatch |= __SEARCH_ONLY_TOKEN_OBJECT;
			else iMatch |= __SEARCH_ONLY_SESSION_OBJECT;
			found = CK_TRUE;
		}
		i++;
	}
	if (!found) iMatch |= __SEARCH_TOKEN_SESSION_OBJECTS;
	return iMatch;
}
CK_IMPLEMENTATION(CK_RV, __iterator)
(
	CK_INOUT KOBJECT_CACHE_STR *self,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_INOUT KOB_ITERATOR hIt
)
{
	CK_RV rv;

	assert(hIt);
	if ((rv = self->hMutex->LockMutex(self->pMutex)) == CKR_OK)
	{
		hIt->iMatch = __match(pTemplate, ulCount);
		hIt->k = kh_begin(self->kObjects);
		rv = self->hMutex->UnlockMutex(self->pMutex);
	}
	return rv;
}
CK_IMPLEMENTATION(KOBJECT, __next)(CK_IN KOBJECT_CACHE_STR *self, CK_INOUT KOB_ITERATOR hIt)
{
	KOBJECT pNext = NULL;

	assert(hIt);
	if (self->hMutex->LockMutex(self->pMutex) == CKR_OK)
	{
		while (!pNext && hIt->k != kh_end(self->kObjects))
		{
			if (kh_exist(self->kObjects, hIt->k))
			{
				if ((pNext = __session_match((pNext = kh_value(self->kObjects, hIt->k)), hIt->iMatch) ? pNext : NULL))
				{
					switch (pNext->hClass)
					{
					case CKO_CERTIFICATE:
						pNext = (hIt->iMatch & __SEARCH_CERT_OBJECT) ? pNext : NULL;
						break;
					case CKO_PUBLIC_KEY:
						pNext = (hIt->iMatch & __SEARCH_PUBKEY_OBJECT) ? pNext : NULL;
						break;
					case CKO_PRIVATE_KEY:
						pNext = (hIt->iMatch & __SEARCH_PRIVKEY_OBJECT) ? pNext : NULL;
						break;
					case CKO_SECRET_KEY:
						pNext =(hIt->iMatch & __SEARCH_KEY_OBJECT) ? pNext : NULL;
						break;
					default:
						pNext = NULL;
					}
				}
			}
			++hIt->k;
		}
		self->hMutex->UnlockMutex(self->pMutex);
	}
	return pNext;
}

static KOBJECT_CACHE_STR __defaultCache =
{
	NULL,			/* pMutex */
	NULL,			/* hMutex */
	NULL,			/* kObjects */
	KOC_CACHE_EMPTY,	/* loaded */

	__load,
	__clear,
	__add,
	__remove,
	__get,
	__iterator,
	__next
};
CK_NEW(CK_new_object_cache)(CK_IN KMUTEX_HANDLER pMutex, CK_OUT KOBJECT_CACHE *hCache)
{
	CK_RV rv;
	KOBJECT_CACHE hOut;

	if ((rv = (hOut = (KOBJECT_CACHE) malloc(sizeof(KOBJECT_CACHE_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(hOut, &__defaultCache, sizeof(KOBJECT_CACHE_STR));
		if
		(
			(rv = pMutex->clone(pMutex, &hOut->hMutex)) == CKR_OK &&
			(rv = hOut->hMutex->CreateMutex(&hOut->pMutex)) == CKR_OK &&
			(rv = (hOut->kObjects = kh_init(KOMAP)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)	*hCache = hOut;
		else CK_release_object_cache(hOut);
	}
	return rv;
}
CK_DELETE(CK_release_object_cache)(CK_INOUT KOBJECT_CACHE hCache)
{
	khiter_t k;

	if (hCache)
	{
		if (hCache->hMutex)
		{
			hCache->hMutex->DestroyMutex(hCache->pMutex);
			CK_release_mutex_handler(hCache->hMutex);
		}
		if (hCache->kObjects)
		{
			for (k = kh_begin(hCache->kObjects); k != kh_end(hCache->kObjects); ++k)
				if (kh_exist(hCache->kObjects, k))
					CKO_delete_object(kh_value(hCache->kObjects, k));
			kh_destroy(KOMAP, hCache->kObjects);
		}
		free(hCache);
	}
}
