/**
 * @file io.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief PKCS#11 objects file system implementation
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "tokenio.h"

#ifdef _UNIX
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <openssl/rand.h>
#include <openssl/err.h>


/* * * * * * * * * * * * * * * * * * *
 * PKCS#11 persistent objects encoding
 * * * * * * * * * * * * * * * * * * *
 */
static CK_BBOOL __true = CK_TRUE;
static CK_BBOOL __false = CK_FALSE;
static CK_ATTRIBUTE __isnot_sensitive = { CKA_SENSITIVE, &__false, sizeof(CK_BBOOL) };
static CK_ATTRIBUTE __is_extractable = { CKA_EXTRACTABLE, &__true, sizeof(CK_BBOOL) };

/**
 * @brief ASN.1 encode fields
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __set_enumerated)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_ULONG ulValue, CK_IN CK_BBOOL isOptional)
{
	NH_RV rv;
	
	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		if (isOptional) hEncoder->register_optional(pNode);
		NHARU_ASSERT((rv = hEncoder->put_enumerated(hEncoder, pNode, (int) ulValue)), "put_enumerated");
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_RV, __set_boolean)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_BBOOL isValue, CK_IN CK_BBOOL isOptional)
{
	NH_RV rv;

	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		if (isOptional) hEncoder->register_optional(pNode);
		NHARU_ASSERT((rv = hEncoder->put_boolean(hEncoder, pNode, isValue ? TRUE : FALSE)), "put_boolean");
		
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_RV, __set_octet_string)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen, CK_IN CK_BBOOL isOptional)
{
	NH_RV rv;

	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		if (isOptional) hEncoder->register_optional(pNode);
		NHARU_ASSERT((rv = hEncoder->put_octet_string(hEncoder, pNode, pValue, ulValueLen)), "put_octet_string");
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_RV, __set_date)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_DATE *pDate, CK_IN CK_BBOOL isOptional)
{
	NH_RV rv;
	char utc[16], year[5] = { 0, 0, 0, 0, 0 }, month[3] = { 0, 0, 0 }, day[3] = { 0, 0 , 0 };

	assert(pDate);
	memcpy(year, pDate->year, sizeof(pDate->year));
	memcpy(month, pDate->month, sizeof(pDate->month));
	memcpy(day, pDate->day, sizeof(pDate->day));
	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		if (isOptional) hEncoder->register_optional(pNode);
		sprintf(utc, "%04d%02d%02d%02d%02d%02dZ", atoi(year), atoi(month), atoi(day), 0, 0, 0);
		NHARU_ASSERT((rv = hEncoder->put_generalized_time(hEncoder, pNode, utc, strlen(utc))), "put_generalized_time");
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_RV, __set_node)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_RV rv;
	unsigned char *next;

	assert(pValue);
	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		NHARU_ASSERT((rv = hEncoder->container->bite_chunk(hEncoder->container, ulValueLen, (void*) &pNode->identifier)), "bite_chunk");
		if (NH_SUCCESS(rv))
		{
			memcpy(pNode->identifier, pValue, ulValueLen);
			NHARU_ASSERT((rv = hEncoder->read_size(&pValue[1], &pValue[ulValueLen - 1], (size_t*) &pNode->size, &pNode->contents, &next)), "read_size");
		}
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_RV, __set_integer)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen, CK_IN CK_BBOOL isOptional)
{
	NH_RV rv;

	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		if (isOptional) hEncoder->register_optional(pNode);
		NHARU_ASSERT((rv = hEncoder->put_integer(hEncoder, pNode, pValue, ulValueLen)), "put_integer");
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __set_ulong)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_ULONG ulValue, CK_IN CK_BBOOL isOptional)
{
	NH_RV rv;

	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		if (isOptional) hEncoder->register_optional(pNode);
		NHARU_ASSERT((rv = hEncoder->put_little_integer(hEncoder, pNode, (int) ulValue)), "put_little_integer");
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __set_string)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode, CK_IN CK_UTF8CHAR_PTR pValue, CK_IN CK_ULONG ulValueLen, CK_IN CK_BBOOL isOptional)
{
	NH_RV rv;

	NHARU_ASSERT((rv = pNode ? NH_OK : NH_CANNOT_SAIL), "NH_ASN1_ENCODER_HANDLE");
	if (NH_SUCCESS(rv))
	{
		if (isOptional) hEncoder->register_optional(pNode);
		NHARU_ASSERT((rv = hEncoder->put_utf8_string(hEncoder, pNode, pValue, ulValueLen)), "put_utf8_string");
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}


/**
 * @brief Sail to StorageAttributes fields
 * 
 */
#define __storage_get_class(_h)		(_h->sail(_h->root,   NH_PARSE_SOUTH | 2))
#define __storage_get_token(_h)		(_h->sail(_h->root, ((NH_PARSE_SOUTH | 2) << 8) | NH_SAIL_SKIP_EAST))
#define __storage_get_private(_h)		(_h->sail(_h->root, ((NH_PARSE_SOUTH | 2) << 8) | (NH_PARSE_EAST | 2)))
#define __storage_get_modifiable(_h)	(_h->sail(_h->root, ((NH_PARSE_SOUTH | 2) << 8) | (NH_PARSE_EAST | 3)))
#define __storage_get_label(_h)		(_h->sail(_h->root, ((NH_PARSE_SOUTH | 2) << 8) | (NH_PARSE_EAST | 4)))
#define __storage_get_copyable(_h)		(_h->sail(_h->root, ((NH_PARSE_SOUTH | 2) << 8) | (NH_PARSE_EAST | 5)))
#define __storage_get_destroyable(_h)	(_h->sail(_h->root, ((NH_PARSE_SOUTH | 2) << 8) | (NH_PARSE_EAST | 6)))


/**
 * @brief Set StorageAttributes fields
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_set_class)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_OBJECT_CLASS oClass)
{
	return __set_enumerated(hEncoder, __storage_get_class(hEncoder), oClass, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_set_token)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isToken)
{
	return __set_boolean(hEncoder, __storage_get_token(hEncoder), isToken, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_set_private)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isPrivate)
{
	return __set_boolean(hEncoder, __storage_get_private(hEncoder), isPrivate, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_set_modifiable)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isModifiable)
{
	return __set_boolean(hEncoder, __storage_get_modifiable(hEncoder), isModifiable, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_set_label)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_UTF8CHAR_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	return __set_string(hEncoder, __storage_get_label(hEncoder), pValue, ulValueLen, TRUE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_set_copyable)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isCopyable)
{
	return __set_boolean(hEncoder, __storage_get_copyable(hEncoder), isCopyable, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_set_destroyable)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isDestroyable)
{
	return __set_boolean(hEncoder, __storage_get_destroyable(hEncoder), isDestroyable, FALSE);
}

static NH_NODE_WAY __storage_attributes_map[] = 
{
	{	/* StorageAttributes */
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* class */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_ENUMERATED | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* token */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* private */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* modifiable */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* label */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_UTF8_STRING | NH_ASN1_HAS_NEXT_BIT | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	},
	{	/* copiable */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* destroyable */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN,
		NULL,
		0
	}
};



/* * * * * * * * * * * * * * * * *
 * CKO_CERTIFICATE PKCS#11 object
 * * * * * * * * * * * * * * * * *
 */
static NH_NODE_WAY __x509_certificate_map[] =
{
	{	/* X509Certificate */
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE,
		NULL,
		0
	},
	{	/* storage */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__storage_attributes_map,
		ASN_NODE_WAY_COUNT(__storage_attributes_map)
	},
	{	/* certificateType */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_ENUMERATED | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* trusted */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* category */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_ENUMERATED | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* checkValue  */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* startDate */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_GENERALIZED_TIME | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_0 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* endDate */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_GENERALIZED_TIME | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_1 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* pubkey */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_2 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* subject */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* Name */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_TWIN_BIT,
		pkix_x500_rdn_map,
		PKIX_X500_RDN_COUNT
	},
	{	/* id */
		(NH_SAIL_SKIP_NORTH << 8) | NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* issuer */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SEQUENCE | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,	/* If present, Name must be parsed/mapped apart */
		NULL,
		0
	},
	{	/* serialNumber */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* url */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_UTF8_STRING | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT ,
		NULL,
		0
	},
	{	/* value */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_0 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* subjectPubkey */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_1 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* issuerPubkey */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_2 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* javaMIDP */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_ENUMERATED | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* hashAlgorithm */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	}
};

/**
 * @brief Sail to X509Certificate fields
 * 
 */
#define __x509_get_type(_h)				(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_SAIL_SKIP_EAST)))
#define __x509_get_trusted(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 2)))
#define __x509_get_category(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 3)))
#define __x509_get_check_value(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 4)))
#define __x509_get_start_date(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 5)))
#define __x509_get_end_date(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 6)))
#define __x509_get_pubkey(_h)				(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 7)))
#define __x509_get_subject(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 8)))
#define __x509_get_id(_h)				(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 9)))
#define __x509_get_issuer(_h)				(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 10)))
#define __x509_get_serial(_h)				(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 11)))
#define __x509_get_url(_h)				(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 12)))
#define __x509_get_value(_h)				(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 13)))
#define __x509_get_subject_pubkey(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 14)))
#define __x509_get_issuer_pubkey(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 15)))
#define __x509_get_java_midp(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 16)))
#define __x509_get_hash_algorithm(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 17)))


/**
 * @brief KX509CERT_ENCODER getters interface
 * 
 */
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_class)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __storage_get_class(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_token)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __storage_get_token(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_private)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __storage_get_private(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_modifiable)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __storage_get_modifiable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_label)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __storage_get_label(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_copyable)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __storage_get_copyable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_destroyable)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __storage_get_destroyable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_type)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_type(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_trusted)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_trusted(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_category)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_category(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_check_value)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_check_value(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_start_date)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_start_date(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_end_date)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_end_date(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_pubkey)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_pubkey(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_subject)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_subject(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_id)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_id(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_issuer)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_issuer(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_serial)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_serial(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_url)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_url(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_value)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_value(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_subject_pubkey)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_subject_pubkey(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_issuer_pubkey)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_issuer_pubkey(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_java_midp)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_java_midp(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __ecert_get_hash_algorithm)(CK_IN KX509CERT_ENCODER_STR *self)
{
	return __x509_get_hash_algorithm(self->hEncoder);
}


/**
 * @brief KX509CERT_ENCODER setters interface
 * 
 */
CK_IMPLEMENTATION(CK_RV, __cert_set_class)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_OBJECT_CLASS oClass)
{
	return __storage_set_class(self->hEncoder, oClass);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_token)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BBOOL isToken)
{
	return __storage_set_token(self->hEncoder, isToken);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_private)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BBOOL isPrivate)
{
	return __storage_set_private(self->hEncoder, isPrivate);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_modifiable)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BBOOL isModifiable)
{
	return __storage_set_modifiable(self->hEncoder, isModifiable);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_label)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_UTF8CHAR_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __storage_set_label(self->hEncoder, pValue, ulValueLen);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_copyable)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BBOOL isCopyable)
{
	return __storage_set_copyable(self->hEncoder, isCopyable);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_destroyable)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BBOOL isDestroyable)
{
	return __storage_set_destroyable(self->hEncoder, isDestroyable);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_type)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_CERTIFICATE_TYPE ulType)
{
	return __set_enumerated(self->hEncoder, self->get_type(self), ulType, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_trusted)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BBOOL isTrusted)
{
	return __set_boolean(self->hEncoder, self->get_trusted(self), isTrusted, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_category)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_CERTIFICATE_TYPE ulCategory)
{
	return __set_enumerated(self->hEncoder, self->get_category(self), ulCategory, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_check_value)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_octet_string(self->hEncoder, self->get_check_value(self), pValue, ulValueLen, TRUE);
}
CK_IMPLEMENTATION(CK_RV, __set_explicit_node)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_INOUT NH_ASN1_PNODE pNode)
{
	NH_RV rv = NH_OK;

	assert(pNode);
	if (!ASN_IS_PRESENT(pNode) || !pNode->child)
	{
		if
		(
			(rv = hEncoder->add_child(hEncoder->container, pNode) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
			(rv = hEncoder->container->bite_chunk(hEncoder->container, sizeof(unsigned char*), (void*) &pNode->child->identifier)) == NH_OK
		)
		{
			*pNode->child->identifier = pNode->knowledge & NH_ASN1_TAG_MASK;
			pNode->child->knowledge = *pNode->child->identifier;
		}
	}
	return NH_SUCCESS(rv) ? CKR_OK : CKR_HOST_MEMORY;
}
CK_IMPLEMENTATION(CK_RV, __cert_set_start_date)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_DATE date)
{
	NH_ASN1_PNODE pNode = self->get_start_date(self);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_date(self->hEncoder, pNode->child, &date, FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_set_end_date)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_DATE date)
{
	NH_ASN1_PNODE pNode = self->get_end_date(self);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_date(self->hEncoder, pNode->child, &date, FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_set_pubkey)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = self->get_pubkey(self);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_octet_string(self->hEncoder, pNode->child, pValue, ulValueLen, TRUE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_set_subject)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_node(self->hEncoder, self->get_subject(self), pValue, ulValueLen);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_id)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_octet_string(self->hEncoder, self->get_id(self), pValue, ulValueLen, TRUE);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_issuer)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_node(self->hEncoder, self->get_issuer(self), pValue, ulValueLen);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_serial)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_integer(self->hEncoder, self->get_serial_number(self), pValue, ulValueLen, TRUE);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_url)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	return __set_string(self->hEncoder, self->get_url(self), pValue, ulValueLen, TRUE);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_value)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = self->get_value(self);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_octet_string(self->hEncoder, pNode->child, pValue, ulValueLen, FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_set_subject_pubkey)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = self->get_subject_pubkey(self);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_octet_string(self->hEncoder, pNode->child, pValue, ulValueLen, FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_set_issuer_pubkey)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = self->get_issuer_pubkey(self);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_octet_string(self->hEncoder, pNode->child, pValue, ulValueLen, FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_set_java_midp)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_JAVA_MIDP_SECURITY_DOMAIN ulDomain)
{
	return __set_enumerated(self->hEncoder, self->get_java_midp(self), ulDomain, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __cert_set_hash_algorithm)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_MECHANISM_TYPE ulValue)
{
	return __set_ulong(self->hEncoder, self->get_hash_algorithm(self), ulValue, TRUE);
}

/**
 * @brief Create object interface
 * 
 */
#define CERT_CKA_LABEL_PRESENT			(1)
#define CERT_CKA_ID_PRESENT				(CERT_CKA_LABEL_PRESENT << 1)
#define CERT_CKA_ISSUER_PRESENT			(CERT_CKA_LABEL_PRESENT << 2)
#define CERT_CKA_SERIAL_NUMBER_PRESENT		(CERT_CKA_LABEL_PRESENT << 3)
CK_IMPLEMENTATION(CK_RV, __cert_from_template)(CK_IN KX509CERT_ENCODER_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i = 0, iValue = CK_UNAVAILABLE_INFORMATION;
	CK_RV rv = CKR_OK;
	int iShould = 0;
	NH_ASN1_PNODE pSource;
	NH_CERTIFICATE_HANDLER hCert;
	assert(pTemplate);
	while (rv == CKR_OK && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_OBJECT_CLASS) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = *((CK_OBJECT_CLASS *) pTemplate[i].pValue) == CKO_CERTIFICATE ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID;
			break;
		case CKA_TOKEN:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_token(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_PRIVATE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_private(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_MODIFIABLE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_modifiable(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_LABEL:
			if
			(
				(rv = self->set_label(self, (CK_UTF8CHAR_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK
			)	iShould |= CERT_CKA_LABEL_PRESENT;
			break;
		case CKA_COPYABLE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_copyable(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_DESTROYABLE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_destroyable(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_CERTIFICATE_TYPE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_CERTIFICATE_TYPE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = *((CK_CERTIFICATE_TYPE *) pTemplate[i].pValue) == CKC_X_509 ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID;
			break;
		case CKA_TRUSTED:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_trusted(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_CERTIFICATE_CATEGORY:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_CERTIFICATE_CATEGORY) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_category(self, *((CK_CERTIFICATE_CATEGORY *) pTemplate[i].pValue));
			break;
		case CKA_CHECK_VALUE:
			if
			(
				(rv = pTemplate[i].ulValueLen == 3 ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_check_value(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_START_DATE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_DATE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_start_date(self, *(CK_DATE *) pTemplate[i].pValue);
			break;
		case CKA_END_DATE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_DATE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_end_date(self, *(CK_DATE *) pTemplate[i].pValue);
			break;
		case CKA_PUBLIC_KEY_INFO:
			rv = self->set_pubkey(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_SUBJECT:
			rv = self->set_subject(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_ID:
			if
			(
				(rv = self->set_id(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK
			)	iShould |= CERT_CKA_ID_PRESENT;
			break;
		case CKA_ISSUER:
			if
			(
				(rv = self->set_issuer(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK
			)	iShould |= CERT_CKA_ISSUER_PRESENT;
			break;
		case CKA_SERIAL_NUMBER:
			if
			(
				(rv = self->set_serial_number(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK
			)	iShould |= CERT_CKA_SERIAL_NUMBER_PRESENT;
			break;
		case CKA_URL:
			rv = self->set_url(self, (CK_UTF8CHAR_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_VALUE:
			if
			(
				(rv = self->set_value(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK
			)	iValue = i;
			break;
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
			rv = self->set_subject_pubkey(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
			rv = self->set_issuer_pubkey(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_JAVA_MIDP_SECURITY_DOMAIN) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_java_midp(self, *((CK_JAVA_MIDP_SECURITY_DOMAIN *) pTemplate[i].pValue));
			break;
		case CKA_NAME_HASH_ALGORITHM:
			if 
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_MECHANISM_TYPE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_hash_algorithm(self, *((CK_MECHANISM_TYPE *) pTemplate[i].pValue));
			break;
		default: rv = CKR_ATTRIBUTE_TYPE_INVALID;
		}
		i++;
	}
	if (rv == CKR_OK && iValue != CK_UNAVAILABLE_INFORMATION)
	{
		NHARU_ASSERT((rv = NH_parse_certificate((unsigned char*) pTemplate[iValue].pValue, pTemplate[iValue].ulValueLen, &hCert)), "NH_parse_certificate");
		if
		(
			(rv = NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
			!(iShould & CERT_CKA_LABEL_PRESENT)
		) 	rv = self->set_label(self, (CK_UTF8CHAR_PTR) hCert->subject->stringprep, strlen(hCert->subject->stringprep));
		if
		(
			rv == CKR_OK &&
			!(iShould & CERT_CKA_ID_PRESENT) &&
			(rv = (pSource = RSA_PUBKEY_GET_MODULUS(hCert)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
		)	rv = self->set_id(self, (CK_BYTE_PTR) pSource->value, pSource->valuelen);
		if
		(
			rv == CKR_OK &&
			!(iShould & CERT_CKA_ISSUER_PRESENT)
		)	rv = self->set_issuer(self, hCert->issuer->node->identifier, hCert->issuer->node->size + (hCert->issuer->node->contents - hCert->issuer->node->identifier));
		if
		(
			rv == CKR_OK &&
			!(iShould & CERT_CKA_SERIAL_NUMBER_PRESENT)
		)	rv = self->set_serial_number(self, hCert->serialNumber->identifier, hCert->serialNumber->size + (hCert->serialNumber->contents - hCert->serialNumber->identifier));
		NH_release_certificate(hCert);
	}
return rv;
}


/**
 * @brief KX509CERT_ENCODER implementation
 * 
 */
static KX509CERT_ENCODER_STR __default_cert_encoder =
{
	NULL,
	__ecert_get_class,
	__ecert_get_token,
	__ecert_get_private,
	__ecert_get_modifiable,
	__ecert_get_label,
	__ecert_get_copyable,
	__ecert_get_destroyable,
	__ecert_get_type,
	__ecert_get_trusted,
	__ecert_get_category,
	__ecert_get_check_value,
	__ecert_get_start_date,
	__ecert_get_end_date,
	__ecert_get_pubkey,
	__ecert_get_subject,
	__ecert_get_id,
	__ecert_get_issuer,
	__ecert_get_serial,
	__ecert_get_url,
	__ecert_get_value,
	__ecert_get_subject_pubkey,
	__ecert_get_issuer_pubkey,
	__ecert_get_java_midp,
	__ecert_get_hash_algorithm,

	__cert_set_class,
	__cert_set_token,
	__cert_set_private,
	__cert_set_modifiable,
	__cert_set_label,
	__cert_set_copyable,
	__cert_set_destroyable,
	__cert_set_type,
	__cert_set_trusted,
	__cert_set_category,
	__cert_set_check_value,
	__cert_set_start_date,
	__cert_set_end_date,
	__cert_set_pubkey,
	__cert_set_subject,
	__cert_set_id,
	__cert_set_issuer,
	__cert_set_serial,
	__cert_set_url,
	__cert_set_value,
	__cert_set_subject_pubkey,
	__cert_set_issuer_pubkey,
	__cert_set_java_midp,
	__cert_set_hash_algorithm,

	__cert_from_template
};

CK_NEW(CKIO_new_certificate_encoder)(CK_OUT KX509CERT_ENCODER *hOut)
{
	CK_RV rv;
	KX509CERT_ENCODER hCert = NULL;
	NH_ASN1_ENCODER_HANDLE hEncoder;
	NH_ASN1_PNODE pNode;
	if 
	(
		
		(rv = (hCert = (KX509CERT_ENCODER) malloc(sizeof(KX509CERT_ENCODER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = NH_new_encoder(64, 4096, &hEncoder) == NH_OK ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		NHARU_ASSERT((rv = hEncoder->chart(hEncoder, __x509_certificate_map, ASN_NODE_WAY_COUNT(__x509_certificate_map), &pNode)), "chart");
		if ((rv = NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
		{
			memcpy(hCert, &__default_cert_encoder, sizeof(KX509CERT_ENCODER_STR));
			hCert->hEncoder  = hEncoder;
			if
			(
				(rv = hCert->set_class(hCert, CKO_CERTIFICATE)) == CKR_OK &&
				(rv = hCert->set_token(hCert, CK_FALSE)) == CKR_OK &&
				(rv = hCert->set_private(hCert, CK_FALSE)) == CKR_OK &&
				(rv = hCert->set_modifiable(hCert, CK_FALSE)) == CKR_OK &&
				(rv = hCert->set_copyable(hCert, CK_TRUE)) == CKR_OK &&
				(rv = hCert->set_destroyable(hCert, CK_TRUE)) == CKR_OK &&
				(rv = hCert->set_type(hCert, CKC_X_509)) == CKR_OK &&
				(rv = hCert->set_trusted(hCert, CK_FALSE)) == CKR_OK &&
				(rv = hCert->set_category(hCert, CK_CERTIFICATE_CATEGORY_UNSPECIFIED)) == CKR_OK &&
				(rv = hCert->set_java_midp(hCert, CK_SECURITY_DOMAIN_UNSPECIFIED)) == CKR_OK
			)	*hOut = hCert;
		}
		if (rv != CKR_OK) NH_release_encoder(hEncoder);
	}
	if (rv != CKR_OK && hCert) free(hCert);
	return rv;
}
CK_DELETE(CKIO_release_certificate_encoder)(CK_INOUT KX509CERT_ENCODER hCert)
{
	if (hCert)
	{
		if (hCert->hEncoder) NH_release_encoder(hCert->hEncoder);
		free(hCert);
	}
}


/**
 * @brief KX09CERT_PARSER getters interface
 * 
 */
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_class)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __storage_get_class(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_token)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __storage_get_token(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_private)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __storage_get_private(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_modifiable)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __storage_get_modifiable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_label)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __storage_get_label(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_copyable)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __storage_get_copyable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_destroyable)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __storage_get_destroyable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_type)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_type(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_trusted)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_trusted(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_category)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_category(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_check_value)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_check_value(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_start_date)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_start_date(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_end_date)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_end_date(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_pubkey)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_pubkey(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_subject)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_subject(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_id)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_id(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_issuer)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_issuer(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_serial)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_serial(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_url)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_url(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_value)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_value(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_subject_pubkey)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_subject_pubkey(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_issuer_pubkey)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_issuer_pubkey(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_java_midp)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_java_midp(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __cert_get_hash_algorithm)(CK_IN KX09CERT_PARSER_STR *self)
{
	return __x509_get_hash_algorithm(self->hParser);
}


/**
 * @brief Match attributes implementation
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __node_match_ulong)(CK_IN NH_ASN1_PNODE pNode, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;

	assert(pAttribute);
	if (pNode) bRet = ASN_IS_PARSED(pNode) && pNode->valuelen == pAttribute->ulValueLen && *(CK_ULONG*) pNode->value == *(CK_ULONG*) pAttribute->pValue;
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __node_match_bool)(CK_IN NH_ASN1_PNODE pNode, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;

	assert(pAttribute);
	if (pNode) bRet = ASN_IS_PARSED(pNode) && pNode->valuelen == pAttribute->ulValueLen && ((*(CK_BBOOL*) pNode->value && *(CK_BBOOL*) pAttribute->pValue) || (!*(CK_BBOOL*) pNode->value && !*(CK_BBOOL*) pAttribute->pValue));
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __node_match_binary)(CK_IN NH_ASN1_PNODE pNode, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;

	assert(pAttribute);
	if (pNode) bRet = ASN_IS_PARSED(pNode) && pNode->valuelen == pAttribute->ulValueLen && memcmp(pNode->value, pAttribute->pValue, pAttribute->ulValueLen) == 0;
	return bRet;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __node_match_node)(CK_IN NH_ASN1_PNODE pNode, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;

	assert(pAttribute);
	if (pNode) bRet = (CK_ULONG) (pNode->size + (pNode->contents - pNode->identifier)) == pAttribute->ulValueLen && memcmp(pNode->identifier, pAttribute->pValue, pAttribute->ulValueLen) == 0;
	return bRet;
}
static NH_NODE_WAY __name_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE,
		NULL,
		0
	},
	{
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_TWIN_BIT,
		pkix_x500_rdn_map,
		PKIX_X500_RDN_COUNT
	}
};
CK_IMPLEMENTATION(CK_BBOOL, __match_x500_name)(CK_INOUT NH_ASN1_PARSER_HANDLE hParser, CK_IN NH_ASN1_PNODE pNode, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL isMatch = CK_FALSE;
	NH_ASN1_PARSER_HANDLE hTarget;
	NH_NAME_NODE pSource, pTarget;
	NH_RV rv;
	
	NHARU_ASSERT((rv = NH_new_parser((unsigned char*) pAttribute->pValue, pAttribute->ulValueLen, 8, 1024, &hTarget)), "NH_new_parser");
	if (NH_SUCCESS(rv))
	{
		NHARU_ASSERT((rv = hTarget->map(hTarget, __name_map, ASN_NODE_WAY_COUNT(__name_map))), "map");
		if (NH_SUCCESS(rv))
		{
			NHARU_ASSERT((rv = NHIX_parse_name(hTarget, hTarget->root, &pTarget)), "NHIX_parse_name");
			if (NH_SUCCESS(rv))
			{
				NHARU_ASSERT((rv = NHIX_parse_name(hParser, pNode, &pSource)), "NHIX_parse_name");
				if (NH_SUCCESS(rv)) isMatch = strcmp(pTarget->stringprep, pSource->stringprep) == 0;
			}
		}
		NH_release_parser(hTarget);
	}
	return isMatch;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __node_match_date)(CK_IN NH_ASN1_PNODE pNode, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	NH_PTIME utime;
	char utc[sizeof(CK_DATE) + 1];
	CK_BBOOL bRet = CK_FALSE;

	assert(pAttribute);
	if (pNode && ASN_IS_PARSED(pNode) && pNode->valuelen == sizeof(NH_TIME))
	{
		utime = (NH_PTIME) pNode->value;
		sprintf(utc, "%04d%02d%02d", utime->tm_year + 1900, utime->tm_mon + 1, utime->tm_mday);
		bRet = pAttribute->ulValueLen == sizeof(CK_DATE) && memcmp(utc, pAttribute->pValue, pAttribute->ulValueLen) == 0;
	}
	return bRet;
}


/**
 * @brief Match StorageAttributes implementation
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __storage_match_class)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_ulong(__storage_get_class(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __storage_match_token)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__storage_get_token(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __storage_match_private)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__storage_get_private(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __storage_match_modifiable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__storage_get_modifiable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __storage_match_label)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__storage_get_label(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __storage_match_copyable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__storage_get_copyable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __storage_match_destroyable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__storage_get_destroyable(hParser), pAttribute);
}
#define __MATCH_STORAGE(_s, _m, _p)			\
{								\
	case CKA_CLASS:					\
		_m = _s->match_class(_s, _p);		\
		break;					\
	case CKA_TOKEN:					\
		_m = _s->match_token(_s, _p);		\
		break;					\
	case CKA_PRIVATE:					\
		_m = _s->match_private(_s, _p);	\
		break;					\
	case CKA_MODIFIABLE:				\
		_m = _s->match_modifiable(_s, _p);	\
		break;					\
	case CKA_COPYABLE:				\
		_m = _s->match_copyable(_s, _p);	\
		break;					\
	case CKA_DESTROYABLE:				\
		_m = _s->match_destroyable(_s, _p);	\
		break;					\
	case CKA_LABEL:					\
		_m = _s->match_label(_s, _p);		\
		break;					\
}

/**
 * @brief KX09CERT_PARSER match fields interface
 * 
 */
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_class)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_class(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_token)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_token(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_private)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_private(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_modifiable)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_modifiable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_label)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_label(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_copyable)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_copyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_destroyable)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_destroyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_type)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_ulong(__x509_get_type(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_trusted)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__x509_get_trusted(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_category)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_ulong(__x509_get_category(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_check_value)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__x509_get_check_value(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_start_date)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __x509_get_start_date(self->hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_date(pNode->child, pAttribute);
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_end_date)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __x509_get_end_date(self->hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_date(pNode->child, pAttribute);
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_pubkey)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __x509_get_pubkey(self->hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_binary(pNode->child, pAttribute);
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_subject)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __match_x500_name(self->hParser, __x509_get_subject(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_id)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__x509_get_id(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_issuer)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __match_x500_name(self->hParser, __x509_get_issuer(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_serial)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__x509_get_serial(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_url)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__x509_get_url(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_value)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __x509_get_value(self->hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_binary(pNode->child, pAttribute);
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_subject_pubkey)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __x509_get_subject_pubkey(self->hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_binary(pNode->child, pAttribute);
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_issuer_pubkey)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __x509_get_issuer_pubkey(self->hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_binary(pNode->child, pAttribute);
	return bRet;
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_java_midp)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_ulong(__x509_get_java_midp(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __cert_match_hash_algorithm)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_ulong(__x509_get_hash_algorithm(self->hParser), pAttribute);
}

CK_IMPLEMENTATION(CK_BBOOL, __cert_match)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_BBOOL match = CK_TRUE;
	CK_ULONG i = 0;
	while (match && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		__MATCH_STORAGE(self, match, &pTemplate[i])
		case CKA_CERTIFICATE_TYPE:
			match = self->match_type(self, &pTemplate[i]);
			break;
		case CKA_TRUSTED:
			match = self->match_trusted(self, &pTemplate[i]);
			break;
		case CKA_CERTIFICATE_CATEGORY:
			match = self->match_category(self, &pTemplate[i]);
			break;
		case CKA_CHECK_VALUE:
			match = self->match_check_value(self, &pTemplate[i]);
			break;
		case CKA_START_DATE:
			match = self->match_start_date(self, &pTemplate[i]);
			break;
		case CKA_END_DATE:
			match = self->match_end_date(self, &pTemplate[i]);
			break;
		case CKA_PUBLIC_KEY_INFO:
			match = self->match_pubkey(self, &pTemplate[i]);
			break;
		case CKA_SUBJECT:
			match = self->match_subject(self, &pTemplate[i]);
			break;
		case CKA_ID:
			match = self->match_id(self, &pTemplate[i]);
			break;
		case CKA_ISSUER:
			match = self->match_issuer(self, &pTemplate[i]);
			break;
		case CKA_SERIAL_NUMBER:
			match = self->match_serial_number(self, &pTemplate[i]);
			break;
		case CKA_URL:
			match = self->match_url(self, &pTemplate[i]);
			break;
		case CKA_VALUE:
			match = self->match_value(self, &pTemplate[i]);
			break;
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
			match = self->match_subject_pubkey(self, &pTemplate[i]);
			break;
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
			match = self->match_issuer_pubkey(self, &pTemplate[i]);
			break;
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
			match = self->match_java_midp(self, &pTemplate[i]);
			break;
		case CKA_NAME_HASH_ALGORITHM:
			match = self->match_hash_algorithm(self, &pTemplate[i]);
			break;
		default: match = CK_FALSE;
		}
		i++;
	}
	return match;
}


/**
 * @brief Get attribute value
 * 
 */
CK_IMPLEMENTATION(CK_RV, __attribute_bool)(CK_IN NH_ASN1_PNODE pNode, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	assert(pAttribute);
	if (ASN_IS_PRESENT(pNode))
	{
		if (!pAttribute->pValue) pAttribute->ulValueLen = pNode->valuelen;
		else
		{
			if (pAttribute->ulValueLen >= pNode->valuelen) { *(CK_BBOOL*) pAttribute->pValue = *(CK_BBOOL*) pNode->value; pAttribute->ulValueLen = pNode->valuelen; }
			else { pAttribute->ulValueLen = pNode->valuelen; return (CKR_BUFFER_TOO_SMALL); }
		}
	}
	else pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION;
	return CKR_OK;
}
CK_IMPLEMENTATION(CK_RV, __attribute_ulong)(CK_IN NH_ASN1_PNODE pNode, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	assert(pAttribute);
	if (ASN_IS_PRESENT(pNode))
	{
		if (!pAttribute->pValue) pAttribute->ulValueLen = pNode->valuelen;
		else
		{
			if (pAttribute->ulValueLen >= pNode->valuelen) { *(CK_ULONG*) pAttribute->pValue = *(CK_ULONG*) pNode->value; pAttribute->ulValueLen = pNode->valuelen; }
			else { pAttribute->ulValueLen = pNode->valuelen; return (CKR_BUFFER_TOO_SMALL); }
		}
	}
	else pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION;
	return CKR_OK;
}
CK_IMPLEMENTATION(CK_RV, __attribute_binary)(CK_IN NH_ASN1_PNODE pNode, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	assert(pAttribute);
	if (ASN_IS_PRESENT(pNode))
	{
		if (!pAttribute->pValue) pAttribute->ulValueLen = pNode->valuelen;
		else
		{
			if (pAttribute->ulValueLen >= pNode->valuelen) { memcpy(pAttribute->pValue, pNode->value, pNode->valuelen); pAttribute->ulValueLen = pNode->valuelen; }
			else { pAttribute->ulValueLen = pNode->valuelen; return (CKR_BUFFER_TOO_SMALL); }
		}
	}
	else pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION;
	return CKR_OK;
}
CK_IMPLEMENTATION(CK_RV, __attribute_node)(CK_IN NH_ASN1_PNODE pNode, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	assert(pAttribute);
	if (ASN_IS_PRESENT(pNode))
	{
		if (!pAttribute->pValue) pAttribute->ulValueLen = pNode->size + (pNode->contents - pNode->identifier);
		else
		{
			if (pAttribute->ulValueLen >= (CK_ULONG) (pNode->size + (pNode->contents - pNode->identifier)))
			{
				memcpy(pAttribute->pValue, pNode->identifier, pNode->size + (pNode->contents - pNode->identifier));
				pAttribute->ulValueLen = pNode->size + (pNode->contents - pNode->identifier); }
			else { pAttribute->ulValueLen = pNode->size + (pNode->contents - pNode->identifier); return (CKR_BUFFER_TOO_SMALL); }
		}
	}
	else pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION;
	return CKR_OK;
}
CK_IMPLEMENTATION(CK_RV, __attribute_date)(CK_IN NH_ASN1_PNODE pNode, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	NH_PTIME utime;
	char utc[sizeof(CK_DATE) + 1];
	assert(pAttribute);
	if (ASN_IS_PRESENT(pNode))
	{
		if (!pAttribute->pValue) pAttribute->ulValueLen = sizeof(CK_DATE);
		else
		{
			if (pAttribute->ulValueLen >= sizeof(CK_DATE))
			{
				utime = (NH_PTIME) pNode->value;
				sprintf(utc, "%04d%02d%02d", utime->tm_year + 1900, utime->tm_mon + 1, utime->tm_mday);
				memcpy(pAttribute->pValue, utc, sizeof(CK_DATE));
				pAttribute->ulValueLen = sizeof(CK_DATE);
			}
			else { pAttribute->ulValueLen = sizeof(CK_DATE); return (CKR_BUFFER_TOO_SMALL); }
		}
	}
	else pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION;
	return CKR_OK;
}

/**
 * @brief Get StorageAttributes value implementation
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_attr_class)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_ulong(__storage_get_class(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_attr_token)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__storage_get_token(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_attr_private)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__storage_get_private(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_attr_modifiable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__storage_get_modifiable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_attr_label)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__storage_get_label(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_attr_copyable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__storage_get_copyable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_attr_destroyable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__storage_get_destroyable(hParser), pAttribute);
}
#define __STORAGE_ATTRIBUTES(_s, _p, _r)			\
{									\
		case CKA_CLASS:					\
			_r = _s->attr_class(_s, _p);		\
			break;					\
		case CKA_TOKEN:					\
			_r = _s->attr_token(_s, _p);		\
			break;					\
		case CKA_PRIVATE:					\
			_r = _s->attr_private(_s, _p);	\
			break;					\
		case CKA_MODIFIABLE:				\
			_r = _s->attr_modifiable(_s, _p);	\
			break;					\
		case CKA_COPYABLE:				\
			_r = _s->attr_copyable(_s, _p);	\
			break;					\
		case CKA_DESTROYABLE:				\
			_r = _s->attr_destroyable(_s, _p);	\
			break;					\
		case CKA_LABEL:					\
			_r = _s->attr_label(_s, _p);		\
			break;					\
}


/**
 * @brief KX09CERT_PARSER get fields interface
 * 
 */
CK_IMPLEMENTATION(CK_RV, __cert_attr_class)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_class(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_token)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_token(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_private)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_private(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_modifiable)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_modifiable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_label)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_label(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_copyable)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_copyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_destroyable)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_destroyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_type)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_ulong(__x509_get_type(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_trusted)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__x509_get_trusted(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_category)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_ulong(__x509_get_category(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_check_value)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__x509_get_check_value(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_start_date)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __x509_get_start_date(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_date(pNode->child, pAttribute);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_end_date)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __x509_get_end_date(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_date(pNode->child, pAttribute);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_pubkey)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __x509_get_pubkey(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_binary(pNode->child, pAttribute);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_subject)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_node(__x509_get_subject(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_id)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__x509_get_id(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_issuer)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_node(__x509_get_issuer(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_serial)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__x509_get_serial(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_url)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__x509_get_url(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_value)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __x509_get_value(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_binary(pNode->child, pAttribute);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_subject_pubkey)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __x509_get_subject_pubkey(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_binary(pNode->child, pAttribute);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_issuer_pubkey)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __x509_get_issuer_pubkey(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_binary(pNode->child, pAttribute);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_java_midp)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_ulong(__x509_get_java_midp(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attr_hash_algorithm)(CK_IN KX09CERT_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_ulong(__x509_get_hash_algorithm(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __cert_attributes)(CK_IN KX09CERT_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_RV rv = CKR_OK;
	CK_ULONG i = 0;
	assert(pTemplate);
	while (rv == CKR_OK && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		__STORAGE_ATTRIBUTES(self, &pTemplate[i], rv)
		case CKA_CERTIFICATE_TYPE:
			rv = self->attr_type(self, &pTemplate[i]);
			break;
		case CKA_TRUSTED:
			rv = self->attr_trusted(self, &pTemplate[i]);
			break;
		case CKA_CERTIFICATE_CATEGORY:
			rv = self->attr_category(self, &pTemplate[i]);
			break;
		case CKA_CHECK_VALUE:
			rv = self->attr_check_value(self, &pTemplate[i]);
			break;
		case CKA_START_DATE:
			rv = self->attr_start_date(self, &pTemplate[i]);
			break;
		case CKA_END_DATE:
			rv = self->attr_end_date(self, &pTemplate[i]);
			break;
		case CKA_PUBLIC_KEY_INFO:
			rv = self->attr_pubkey(self, &pTemplate[i]);
			break;
		case CKA_SUBJECT:
			rv = self->attr_subject(self, &pTemplate[i]);
			break;
		case CKA_ID:
			rv = self->attr_id(self, &pTemplate[i]);
			break;
		case CKA_ISSUER:
			rv = self->attr_issuer(self, &pTemplate[i]);
			break;
		case CKA_SERIAL_NUMBER:
			rv = self->attr_serial_number(self, &pTemplate[i]);
			break;
		case CKA_VALUE:
			rv = self->attr_value(self, &pTemplate[i]);
			break;
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
			rv = self->attr_subject_pubkey(self, &pTemplate[i]);
			break;
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
			rv = self->attr_issuer_pubkey(self, &pTemplate[i]);
			break;
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
			rv = self->attr_java_midp(self, &pTemplate[i]);
			break;
		case CKA_NAME_HASH_ALGORITHM:
			rv = self->attr_hash_algorithm(self, &pTemplate[i]);
			break;
		default: rv = CKR_ATTRIBUTE_TYPE_INVALID;
		}
		i++;
	}
	return rv;
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __storage_parse)(CK_INOUT NH_ASN1_PARSER_HANDLE hParser)
{
	NH_RV rv;
	NH_ASN1_PNODE pNode;

	assert(hParser);
	if
	(
		(rv = (pNode = __storage_get_class(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_enumerated(hParser, pNode)) == NH_OK &&
		(rv = (pNode = __storage_get_token(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __storage_get_private(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __storage_get_modifiable(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __storage_get_label(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
	)
	{
		if (ASN_IS_PRESENT(pNode)) rv = hParser->parse_string(pNode);
		if
		(
			NH_SUCCESS(rv) && 
			(rv = (pNode = __storage_get_copyable(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hParser->parse_boolean(pNode)) == NH_OK &&
			(rv = (pNode = __storage_get_destroyable(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
		)	rv = hParser->parse_boolean(pNode);
	}
	NHARU_ASSERT(rv, "NH_ASN1_PARSER_HANDLE");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
static KX09CERT_PARSER_STR __default_cert_parser =
{
	NULL,
	__cert_get_class,
	__cert_get_token,
	__cert_get_private,
	__cert_get_modifiable,
	__cert_get_label,
	__cert_get_copyable,
	__cert_get_destroyable,
	__cert_get_type,
	__cert_get_trusted,
	__cert_get_category,
	__cert_get_check_value,
	__cert_get_start_date,
	__cert_get_end_date,
	__cert_get_pubkey,
	__cert_get_subject,
	__cert_get_id,
	__cert_get_issuer,
	__cert_get_serial,
	__cert_get_url,
	__cert_get_value,
	__cert_get_subject_pubkey,
	__cert_get_issuer_pubkey,
	__cert_get_java_midp,
	__cert_get_hash_algorithm,

	__cert_match_class,
	__cert_match_token,
	__cert_match_private,
	__cert_match_modifiable,
	__cert_match_label,
	__cert_match_copyable,
	__cert_match_destroyable,
	__cert_match_type,
	__cert_match_trusted,
	__cert_match_category,
	__cert_match_check_value,
	__cert_match_start_date,
	__cert_match_end_date,
	__cert_match_pubkey,
	__cert_match_subject,
	__cert_match_id,
	__cert_match_issuer,
	__cert_match_serial,
	__cert_match_url,
	__cert_match_value,
	__cert_match_subject_pubkey,
	__cert_match_issuer_pubkey,
	__cert_match_java_midp,
	__cert_match_hash_algorithm,
	__cert_match,

	__cert_attr_class,
	__cert_attr_token,
	__cert_attr_private,
	__cert_attr_modifiable,
	__cert_attr_label,
	__cert_attr_copyable,
	__cert_attr_destroyable,
	__cert_attr_type,
	__cert_attr_trusted,
	__cert_attr_category,
	__cert_attr_check_value,
	__cert_attr_start_date,
	__cert_attr_end_date,
	__cert_attr_pubkey,
	__cert_attr_subject,
	__cert_attr_id,
	__cert_attr_issuer,
	__cert_attr_serial,
	__cert_attr_url,
	__cert_attr_value,
	__cert_attr_subject_pubkey,
	__cert_attr_issuer_pubkey,
	__cert_attr_java_midp,
	__cert_attr_hash_algorithm,
	__cert_attributes
};

static NH_NODE_WAY __name[] =
{
	{	/* issuer */
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* Name */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_TWIN_BIT,
		pkix_x500_rdn_map,
		PKIX_X500_RDN_COUNT
	}
};
CK_NEW(CKIO_new_certificate_parser)(CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulSize, CK_OUT KX09CERT_PARSER *hOut)
{
	NH_RV rv;
	KX09CERT_PARSER hCert = NULL;
	NH_ASN1_PARSER_HANDLE hParser = NULL;
	NH_ASN1_PNODE pNode;
	NH_NAME_NODE ignored;

	if
	(
		(rv = (hCert = (KX09CERT_PARSER) malloc(sizeof(KX09CERT_PARSER_STR))) ? NH_OK : NH_OUT_OF_MEMORY_ERROR) == CKR_OK &&
		(rv = NH_new_parser(pEncoding, ulSize, 64, 4096, &hParser)) == NH_OK &&
		(rv = hParser->map(hParser, __x509_certificate_map, ASN_NODE_WAY_COUNT(__x509_certificate_map))) == NH_OK &&
		(rv =__storage_parse(hParser)) == CKR_OK
	)
	{
		memcpy(hCert, &__default_cert_parser, sizeof(KX09CERT_PARSER_STR));
		hCert->hParser = hParser;
		if(
			(rv = (pNode = hCert->get_type(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hParser->parse_enumerated(hParser, pNode)) == NH_OK &&
			(rv = (pNode = hCert->get_trusted(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hParser->parse_boolean(pNode)) == NH_OK &&
			(rv = (pNode = hCert->get_category(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hParser->parse_enumerated(hParser, pNode)) == NH_OK &&
			(rv = (pNode = hCert->get_check_value(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hParser->parse_octetstring(hParser, pNode)) == NH_OK
		)	rv = (pNode = hCert->get_start_date(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING;
		if (NH_SUCCESS(rv) && ASN_IS_PRESENT(pNode) && ASN_IS_PRESENT(pNode->child)) rv = hParser->parse_time(hParser, pNode->child);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_end_date(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode) && ASN_IS_PRESENT(pNode->child)
		)	rv = hParser->parse_time(hParser, pNode->child);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_pubkey(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode)
		)	rv = hParser->parse_octetstring(hParser, pNode);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_subject(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
		)	rv = NHIX_parse_name(hParser, pNode, &ignored);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_id(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode)
		)	rv = hParser->parse_octetstring(hParser, pNode);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_issuer(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode) &&
			(rv = hParser->map_from(hParser, pNode, __name, ASN_NODE_WAY_COUNT(__name))) == CKR_OK
		)	rv = NHIX_parse_name(hParser, pNode, &ignored);
		
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_serial_number(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode)
		)	rv = hParser->parse_integer(pNode);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_value(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
		)	rv = hParser->parse_octetstring(hParser, pNode);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_subject_pubkey(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode) && ASN_IS_PRESENT(pNode->child)
		)	rv = hParser->parse_octetstring(hParser, pNode->child);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_issuer_pubkey(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode) && ASN_IS_PRESENT(pNode->child)
		)	rv = hParser->parse_octetstring(hParser, pNode->child);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_java_midp(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
		)	rv = hParser->parse_enumerated(hParser, pNode);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = hCert->get_hash_algorithm(hCert)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode)
		)	rv = hParser->parse_little_integer(hParser, pNode);
	}
	if (NH_SUCCESS(rv)) *hOut = hCert;
	else
	{
		if (hParser) NH_release_parser(hParser);
		if (hCert) free(hCert);
	}
	NHARU_ASSERT(rv, "CKIO_new_certificate_parser");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_DELETE(CKIO_release_certificate_parser)(CK_INOUT KX09CERT_PARSER hCert)
{
	if (hCert)
	{
		if (hCert->hParser) NH_release_parser(hCert->hParser);
		free(hCert);
	}
}



/* * * * * * * * * * * * * * * * *
 * CKO_PUBLIC_KEY PKCS#11 object
 * * * * * * * * * * * * * * * * *
 */
/**
 * @brief Key attributes
 * 
 */
static NH_NODE_WAY __mechanism_list_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_INTEGER | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	}
};
static NH_NODE_WAY __key_attributes_map[] =
{
	{	/* KeyAttributes */
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* keyType */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_INTEGER | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_HAS_NEXT_BIT | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	},
	{	/* startDate */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_GENERALIZED_TIME | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_0 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* endDate */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_GENERALIZED_TIME | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_1 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* derive */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* local */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* keyGenMechanism */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_HAS_NEXT_BIT | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	},
	{	/* allowedMechanisms */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SET | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	},
	{
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_TWIN_BIT,
		__mechanism_list_map,
		ASN_NODE_WAY_COUNT(__mechanism_list_map)
	}
};

#define __key_get_keytype(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 16) | (NH_SAIL_SKIP_EAST << 8) | NH_SAIL_SKIP_SOUTH))
#define __key_get_id(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | (NH_SAIL_SKIP_EAST << 16) | (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST))
#define __key_get_start_date(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | (NH_SAIL_SKIP_EAST << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 2)))
#define __key_get_end_date(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | (NH_SAIL_SKIP_EAST << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 3)))
#define __key_get_derive(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | (NH_SAIL_SKIP_EAST << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 4)))
#define __key_get_local(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | (NH_SAIL_SKIP_EAST << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 5)))
#define __key_get_keyGenMech(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | (NH_SAIL_SKIP_EAST << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 6)))
#define __key_get_allowedMech(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | (NH_SAIL_SKIP_EAST << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 7)))

CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_keytype)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_KEY_TYPE ulType)
{
	return __set_ulong(hEncoder, __key_get_keytype(hEncoder), ulType, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_keyid)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_octet_string(hEncoder, __key_get_id(hEncoder), pValue, ulValueLen, TRUE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_start_date)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_DATE pDate)
{
	NH_ASN1_PNODE pNode = __key_get_start_date(hEncoder);
	CK_RV rv = __set_explicit_node(hEncoder, pNode);
	if (rv == CKR_OK)
	{
		hEncoder->register_optional(pNode);
		rv = __set_date(hEncoder, pNode->child, &pDate, FALSE);
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_end_date)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_DATE pDate)
{
	NH_ASN1_PNODE pNode = __key_get_end_date(hEncoder);
	CK_RV rv = __set_explicit_node(hEncoder, pNode);
	if (rv == CKR_OK)
	{
		hEncoder->register_optional(pNode);
		rv = __set_date(hEncoder, pNode->child, &pDate, FALSE);
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_derive)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __key_get_derive(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_local)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __key_get_local(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_keyGenMech)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_MECHANISM_TYPE ulType)
{
	return __set_ulong(hEncoder, __key_get_keyGenMech(hEncoder), ulType, TRUE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_set_allowedMech)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_MECHANISM_TYPE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	CK_ULONG i = 0, ulCount = ulValueLen / sizeof(CK_MECHANISM_TYPE);
	NH_ASN1_PNODE pSet = __key_get_allowedMech(hEncoder), pNode;
	NH_RV rv;
	assert(pValue);
	if ((rv = pSet ? NH_OK : NH_CANNOT_SAIL) == NH_OK)
	{
		if (!ASN_IS_PRESENT(pSet)) hEncoder->register_optional(pSet);
		pSet->child = NULL;
		while (NH_SUCCESS(rv) && i < ulCount)
		{
			if
			(
				(rv = (pNode = hEncoder->add_to_set(hEncoder->container, pSet)) ? NH_OK : NH_OUT_OF_MEMORY_ERROR) == NH_OK &&
				(rv = hEncoder->chart_from(hEncoder, pNode, __mechanism_list_map, ASN_NODE_WAY_COUNT(__mechanism_list_map))) == NH_OK &&
				(rv = hEncoder->put_little_integer(hEncoder, pNode, (int) pValue[i])) == NH_OK
			)	i++;
		}
	}
	NHARU_ASSERT(rv, "__key_set_allowedMech");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}

CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_keyType)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_ulong(__key_get_keytype(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_keyid)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__key_get_id(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_start_date)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __key_get_start_date(hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_date(pNode->child, pAttribute);
	return bRet;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_end_date)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __key_get_end_date(hParser);
	if (ASN_IS_PRESENT(pNode)) bRet = __node_match_date(pNode->child, pAttribute);
	return bRet;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_derive)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__key_get_derive(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_local)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__key_get_local(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_keyGenMech)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_ulong(__key_get_keyGenMech(hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __get_mechanism_list)(CK_IN NH_ASN1_PNODE pNode, CK_OUT CK_MECHANISM_TYPE_PTR *pValue, CK_OUT CK_ULONG_PTR pulValueLen)
{
	CK_RV rv;
	NH_ASN1_PNODE pChild;
	CK_ULONG ulCount = 0, i = 0;
	CK_MECHANISM_TYPE_PTR pList;
	if ((rv = (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED((pChild = pNode->child))) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		while (ASN_IS_PARSED(pChild)) { if (pChild->valuelen == sizeof(CK_MECHANISM_TYPE)) ulCount++; pChild = pChild->next; }
		if
		(
			(rv = (ulCount > 0) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
			(rv = (pList = (CK_MECHANISM_TYPE_PTR) malloc(ulCount * sizeof(CK_MECHANISM_TYPE))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			pChild = pNode->child;
			while (ASN_IS_PARSED(pChild))
			{
				if (pChild->valuelen == sizeof(CK_MECHANISM_TYPE)) pList[i++] = *(CK_MECHANISM_TYPE_PTR) pChild->value;
				pChild = pChild->next; 
			}
			*pValue = pList;
			*pulValueLen = ulCount * sizeof(CK_MECHANISM_TYPE);
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __key_match_allowedMech)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE pNode = __key_get_allowedMech(hParser);
	CK_ULONG ulValueLen;
	CK_MECHANISM_TYPE_PTR pValue;

	assert(pAttribute);
	if (__get_mechanism_list(pNode, &pValue, &ulValueLen) == CKR_OK)
	{
		bRet = ulValueLen == pAttribute->ulValueLen && memcmp(pValue, pAttribute->pValue, ulValueLen) == 0;
		free(pValue);
	}
	return bRet;
}
#define __MATCH_KEY(_s, _m, _p)			\
{								\
	case CKA_KEY_TYPE:				\
		_m = _s->match_keytype(_s, _p);	\
		break;					\
	case CKA_ID:					\
		_m = _s->match_id(_s, _p);		\
		break;					\
	case CKA_START_DATE:				\
		_m = _s->match_start_date(_s, _p);	\
		break;					\
	case CKA_END_DATE:				\
		_m = _s->match_end_date(_s, _p);	\
		break;					\
	case CKA_DERIVE:					\
		_m = _s->match_derive(_s, _p);	\
		break;					\
	case CKA_LOCAL:					\
		_m = _s->match_local(_s, _p);		\
		break;					\
	case CKA_KEY_GEN_MECHANISM:			\
		_m = _s->match_keyGenMech(_s, _p);	\
		break;					\
	case CKA_ALLOWED_MECHANISMS:			\
		_m = _s->match_allowedMech(_s, _p);	\
		break;					\
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_keyType)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_ulong(__key_get_keytype(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_keyid)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__key_get_id(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_start_date)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __key_get_start_date(hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_date(pNode->child, pAttribute);
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_end_date)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __key_get_end_date(hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child)) rv = __attribute_date(pNode->child, pAttribute);
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_derive)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__key_get_derive(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_local)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__key_get_local(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_keyGenMech)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __key_get_keyGenMech(hParser);
	if (ASN_IS_PRESENT(pNode)) rv = __attribute_ulong(pNode, pAttribute);
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attr_allowedMech)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv;
	NH_ASN1_PNODE pNode = __key_get_allowedMech(hParser);
	CK_ULONG ulValueLen;
	CK_MECHANISM_TYPE_PTR pValue;

	assert(pAttribute);
	if ((rv = __get_mechanism_list(pNode, &pValue, &ulValueLen)) == CKR_OK)
	{
		if (!pAttribute->pValue) pAttribute->ulValueLen = ulValueLen;
		else
		{
			if (pAttribute->ulValueLen >= ulValueLen) { memcpy(pAttribute->pValue, pValue, ulValueLen); pAttribute->ulValueLen = ulValueLen; }
			else { pAttribute->ulValueLen = ulValueLen; rv = CKR_BUFFER_TOO_SMALL; }
		}
		free(pValue);
	}
	return rv;
}
#define __KEY_ATTRIBUTES(_s, _p, _r)			\
{									\
		case CKA_KEY_TYPE:				\
			_r = _s->attr_keytype(_s, _p);	\
			break;					\
		case CKA_ID:					\
			_r = _s->attr_id(_s, _p);		\
			break;					\
		case CKA_START_DATE:				\
			_r = _s->attr_start_date(_s, _p);	\
			break;					\
		case CKA_END_DATE:				\
			_r = _s->attr_end_date(_s, _p);	\
			break;					\
		case CKA_DERIVE:					\
			_r = _s->attr_derive(_s, _p);		\
			break;					\
		case CKA_LOCAL:					\
			_r = _s->attr_local(_s, _p);		\
			break;					\
		case CKA_KEY_GEN_MECHANISM:			\
			_r = _s->attr_keyGenMech(_s, _p);	\
			break;					\
		case CKA_ALLOWED_MECHANISMS:			\
			_r = _s->attr_allowedMech(_s, _p);	\
			break;					\
}


/**
 * @brief PubkeyAttributes attriutes
 * 
 */
static NH_NODE_WAY __pubkey_attributes_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* subject */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_SEQUENCE | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* encrypt */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* verify */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* verifyRecover */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* wrap */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* trusted */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* pubKeyInfo */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	}
};

#define __pubkey_get_subject(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 16) | ((NH_PARSE_EAST | 2) << 8)  | NH_SAIL_SKIP_SOUTH))
#define __pubkey_get_encrypt(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST))
#define __pubkey_get_verify(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 2)))
#define __pubkey_get_verifyRec(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 3)))
#define __pubkey_get_wrap(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 4)))
#define __pubkey_get_trusted(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 5)))
#define __pubkey_get_keyinfo(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 6)))

CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_set_subject)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __set_node(hEncoder, __pubkey_get_subject(hEncoder), pValue, ulValuelen);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_set_encrypt)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __pubkey_get_encrypt(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_set_verify)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __pubkey_get_verify(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_set_verifyRec)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __pubkey_get_verifyRec(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_set_wrap)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __pubkey_get_wrap(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_set_trusted)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __pubkey_get_trusted(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_set_keyinfo)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __set_octet_string(hEncoder, __pubkey_get_keyinfo(hEncoder), pValue, ulValuelen, TRUE);
}

CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __pubkey_match_subject)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __match_x500_name(hParser, __pubkey_get_subject(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __pubkey_match_encrypt)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__pubkey_get_encrypt(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __pubkey_match_verify)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__pubkey_get_verify(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __pubkey_match_verifyRec)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__pubkey_get_verifyRec(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __pubkey_match_wrap)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__pubkey_get_wrap(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __pubkey_match_trusted)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__pubkey_get_trusted(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __pubkey_match_keyinfo)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__pubkey_get_keyinfo(hParser), pAttribute);;
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_attr_subject)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __pubkey_get_subject(hParser);
	if (ASN_IS_PRESENT(pNode)) rv = __attribute_node(pNode, pAttribute);
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_attr_encrypt)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__pubkey_get_encrypt(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_attr_verify)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__pubkey_get_verify(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_attr_verifyRec)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__pubkey_get_verifyRec(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_attr_wrap)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__pubkey_get_wrap(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_attr_trusted)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__pubkey_get_trusted(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __pubkey_attr_keyinfo)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__pubkey_get_keyinfo(hParser), pAttribute);
}


/**
 * @brief RSA public key attributes
 * 
 */
static NH_NODE_WAY __rsapubkey_attributes_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE,
		NULL,
		0
	},
	{	/* storage */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__storage_attributes_map,
		ASN_NODE_WAY_COUNT(__storage_attributes_map)
	},
	{	/* keyAttrs */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__key_attributes_map,
		ASN_NODE_WAY_COUNT(__key_attributes_map)
	},
	{	/* pubkeyAttrs */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__pubkey_attributes_map,
		ASN_NODE_WAY_COUNT(__pubkey_attributes_map)
	},
	{	/* modulus n */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* publicExponent e */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER,
		NULL,
		0
	}
};

#define __rsapubkey_get_modulus(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 3)))
#define __rsapubkey_get_pubexponent(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 4)))

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_class)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __storage_get_class(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_token)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __storage_get_token(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_private)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __storage_get_private(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_modifiable)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __storage_get_modifiable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_label)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __storage_get_label(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_copyable)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __storage_get_copyable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_destroyable)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __storage_get_destroyable(self->hEncoder);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_keytype)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_keytype(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_id)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_id(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_start_date)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_start_date(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_end_date)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_end_date(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_derive)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_derive(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_local)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_local(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_keyGenMech)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_keyGenMech(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_allowedMech)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __key_get_allowedMech(self->hEncoder);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_subject)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __pubkey_get_subject(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_encrypt)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __pubkey_get_encrypt(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_verify)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __pubkey_get_verify(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_verifyRec)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __pubkey_get_verifyRec(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_wrap)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __pubkey_get_wrap(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_trusted)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __pubkey_get_trusted(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapubkey_get_keyinfo)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __pubkey_get_keyinfo(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsa_pubkey_get_modulus)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __rsapubkey_get_modulus(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsa_pubkey_get_pubexponent)(CK_IN KRSAPUBKEY_ENCODER_STR *self)
{
	return __rsapubkey_get_pubexponent(self->hEncoder);
}

CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_class)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_OBJECT_CLASS oClass)
{
	return __storage_set_class(self->hEncoder, oClass);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_token)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isToken)
{
	return __storage_set_token(self->hEncoder, isToken);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_private)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isPrivate)
{
	return __storage_set_private(self->hEncoder, isPrivate);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_modifiable)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isModifiable)
{
	return __storage_set_modifiable(self->hEncoder, isModifiable);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_label)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_UTF8CHAR_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __storage_set_label(self->hEncoder, pValue, ulValueLen);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_copyable)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isCopyable)
{
	return __storage_set_copyable(self->hEncoder, isCopyable);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_destroyable)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isDestroyable)
{
	return __storage_set_destroyable(self->hEncoder, isDestroyable);
}

CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_keytype)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_KEY_TYPE ulValue)
{
	return __key_set_keytype(self->hEncoder, ulValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_id)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	return __key_set_keyid(self->hEncoder, pValue, ulValueLen);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_start_date)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_DATE pDate)
{
	return __key_set_start_date(self->hEncoder, pDate);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_end_date)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_DATE pDate)
{
	return __key_set_end_date(self->hEncoder, pDate);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_derive)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __key_set_derive(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_local)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __key_set_local(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_keyGenMech)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_MECHANISM_TYPE ulType)
{
	return __key_set_keyGenMech(self->hEncoder, ulType);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_allowedMech)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_MECHANISM_TYPE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	return __key_set_allowedMech(self->hEncoder, pValue, ulValueLen);
}

CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_subject)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __pubkey_set_subject(self->hEncoder, pValue, ulValuelen);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_encrypt)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __pubkey_set_encrypt(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_verify)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __pubkey_set_verify(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_verifyRec)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __pubkey_set_verifyRec(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_wrap)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __pubkey_set_wrap(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_trusted)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __pubkey_set_trusted(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_keyinfo)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __pubkey_set_keyinfo(self->hEncoder, pValue, ulValuelen);
}

CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_modulus)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __set_integer(self->hEncoder, __rsapubkey_get_modulus(self->hEncoder), pValue, ulValuelen, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __rsapubkey_set_pubexponent)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __set_integer(self->hEncoder, __rsapubkey_get_pubexponent(self->hEncoder), pValue, ulValuelen, FALSE);
}

CK_IMPLEMENTATION(CK_RV, __toPubKeyInfo)(CK_IN CK_ATTRIBUTE_PTR pModulus, CK_IN CK_ATTRIBUTE_PTR pPubExponent, CK_OUT CK_BYTE_PTR *pPubKeyInfo, CK_OUT CK_ULONG_PTR pulInfoLen)
{
	NH_RV rv;
	NH_RSA_PUBKEY_HANDLER hPubKey;
	NH_BIG_INTEGER n, e;
	NH_ASN1_ENCODER_HANDLE hEncoder;
	CK_BYTE_PTR pInfo;
	CK_ULONG ulLen;

	if ((rv = NH_new_RSA_pubkey_handler(&hPubKey)) == NH_OK)
	{
		n.data = pModulus->pValue;
		n.length = pModulus->ulValueLen;
		e.data = pPubExponent->pValue;
		e.length = pPubExponent->ulValueLen;
		if ((rv = hPubKey->create(hPubKey, &n, &e)) == NH_OK && (rv = NH_new_encoder(12, 512, &hEncoder)) == NH_OK)
		{
			if
			(
				(rv = hEncoder->new_node(hEncoder->container, &hEncoder->root)) == NH_OK &&
				(rv = (hEncoder->root->knowledge = NH_ASN1_SEQUENCE) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
				(rv = hPubKey->encode_info(hPubKey, hEncoder, NH_PARSE_ROOT)) == NH_OK &&
				(rv = (ulLen = hEncoder->encoded_size(hEncoder, hEncoder->root)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
				(rv = (pInfo = (CK_BYTE_PTR) malloc(ulLen)) ? NH_OK : NH_OUT_OF_MEMORY_ERROR) == NH_OK
			)
			{
				if ((rv = hEncoder->encode(hEncoder, hEncoder->root, pInfo)) == NH_OK)
				{
					*pPubKeyInfo = pInfo;
					*pulInfoLen = ulLen;
				}
				else free(pInfo);
			}
			NH_release_encoder(hEncoder);
		}
	NH_release_RSA_pubkey_handler(hPubKey);
	}
	NHARU_ASSERT(rv, "__toPubKeyInfo");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}

#define RSAPUBKEY_CKA_MODULUS_PRESENT			(1)
#define RSAPUBKEY_CKA_PUBLIC_EXPONENT_PRESENT		(RSAPUBKEY_CKA_MODULUS_PRESENT << 1)
#define RSAPUBKEYCKA_PUBLIC_KEY_INFO_PRESENT		(RSAPUBKEY_CKA_MODULUS_PRESENT << 2)
#define RSAPUBKEY_TEMPLATE_WITH_KEYS			0x03
#define RSAPUBKEY_TEMPLATE_WITH_INFO			0x04
#define RSAPUBKEY_CKA_ID_PRESENT				(1)
#define RSAPUBKEY_CKA_KEY_TYPE_PRESENT			(RSAPUBKEY_CKA_ID_PRESENT << 1)
CK_IMPLEMENTATION(CK_RV, __rsapubkey_from_template)(CK_IN KRSAPUBKEY_ENCODER_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i = 0, iInfo = CK_UNAVAILABLE_INFORMATION, ulModulusLen = 0UL, iMod = CK_UNAVAILABLE_INFORMATION, iExpo = CK_UNAVAILABLE_INFORMATION, ulInfoLen;
	CK_RV rv = CKR_OK;
	int iTemplate = 0, iShould = 0;
	CK_BYTE_PTR pModulus = NULL, pPubKeyInfo;
	NHIX_PUBLIC_KEY hPubkey;
	NH_ASN1_PNODE pNode;
	assert(pTemplate);
	while (rv == CKR_OK && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_OBJECT_CLASS) ? CKR_OK :  CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = *((CK_OBJECT_CLASS *) pTemplate[i].pValue) == CKO_PUBLIC_KEY ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID;
			break;
		case CKA_TOKEN:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_token(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_PRIVATE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_private(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_MODIFIABLE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_modifiable(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_LABEL:
			rv = self->set_label(self, (CK_UTF8CHAR_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_COPYABLE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_copyable(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_DESTROYABLE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_destroyable(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;

		case CKA_KEY_TYPE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_KEY_TYPE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK &&
				(rv = *((CK_KEY_TYPE *) pTemplate[i].pValue) == CKK_RSA ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	iShould |= RSAPUBKEY_CKA_KEY_TYPE_PRESENT;
			break;
		case CKA_ID:
			if
			(
				(rv = self->set_id(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK
			)	iShould |= RSAPUBKEY_CKA_ID_PRESENT;
			break;
		case CKA_START_DATE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_DATE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_start_date(self, *(CK_DATE *) pTemplate[i].pValue);
			break;
		case CKA_END_DATE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_DATE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_end_date(self, *(CK_DATE *) pTemplate[i].pValue);
			break;
		case CKA_DERIVE:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_derive(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_LOCAL:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_local(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_KEY_GEN_MECHANISM:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_MECHANISM_TYPE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_keyGenMech(self, *(CK_MECHANISM_TYPE_PTR) pTemplate[i].pValue);
			break;
		case CKA_ALLOWED_MECHANISMS:
			if
			(
				(rv = pTemplate[i].ulValueLen % sizeof(CK_MECHANISM_TYPE) == 0 ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_allowedMech(self, (CK_MECHANISM_TYPE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;

		case CKA_SUBJECT:
			rv = self->set_subject(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			break;
		case CKA_ENCRYPT:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_encrypt(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_VERIFY:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_verify(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_VERIFY_RECOVER:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_verifyRec(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_WRAP:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_wrap(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_TRUSTED:
			if
			(
				(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
			)	rv = self->set_trusted(self, *((CK_BBOOL *) pTemplate[i].pValue));
			break;
		case CKA_PUBLIC_KEY_INFO:
			if ((rv = self->set_keyinfo(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK)
			{
				iTemplate |= RSAPUBKEYCKA_PUBLIC_KEY_INFO_PRESENT;
				iInfo = i;
			}
			break;
	
		case CKA_MODULUS_BITS:
			break;
		case CKA_MODULUS:
			if
			(
				(rv = self->set_modulus(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK &&
				(rv = (pModulus = (CK_BYTE_PTR) malloc((ulModulusLen = pTemplate[i].ulValueLen))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				memcpy(pModulus, pTemplate[i].pValue, ulModulusLen);
				iMod = i;
				iTemplate |= RSAPUBKEY_CKA_MODULUS_PRESENT;
			}
			break;
		case CKA_PUBLIC_EXPONENT:
			if ((rv = self->set_public_exponent(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK)
			{
				iExpo = i;
				iTemplate|= RSAPUBKEY_CKA_PUBLIC_EXPONENT_PRESENT;
			}
			break;
		default: rv = CKR_ATTRIBUTE_TYPE_INVALID;
		}
		i++;
	}
	if (rv == CKR_OK && iTemplate == RSAPUBKEY_TEMPLATE_WITH_INFO)
	{
		rv = NHIX_pubkey_parser((unsigned char *) pTemplate[iInfo].pValue, pTemplate[iInfo].ulValueLen, &hPubkey);
		NHARU_ASSERT(rv, "NHIX_pubkey_parser");
		if ((rv = NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
		{
			if
			(
				(rv = (pNode = hPubkey->hParser->sail(hPubkey->pubkey, NH_PARSE_SOUTH | 2)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
				(rv = self->set_modulus(self, (CK_BYTE_PTR) pNode->value, pNode->valuelen)) == CKR_OK &&
				(rv = (pModulus = (CK_BYTE_PTR) malloc((ulModulusLen = pNode->valuelen))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
			)
			{
				memcpy(pModulus, pNode->value, ulModulusLen);
				rv = (pNode = pNode->next) ? CKR_OK : CKR_FUNCTION_FAILED;
				if (rv == CKR_OK) rv = self->set_public_exponent(self, (CK_BYTE_PTR) pNode->value, pNode->valuelen);
			}
			NHIX_release_pubkey(hPubkey);
		}
	}
	if (rv == CKR_OK && iTemplate == RSAPUBKEY_TEMPLATE_WITH_KEYS)
	{
		if ((rv = __toPubKeyInfo(&pTemplate[iMod], &pTemplate[iExpo], &pPubKeyInfo, &ulInfoLen)) == CKR_OK)
		{
			rv = self->set_keyinfo(self, pPubKeyInfo, ulInfoLen);
			free(pPubKeyInfo);
		}
	}
	if (rv == CKR_OK)
	{
		if (!(iShould & RSAPUBKEY_CKA_ID_PRESENT) && pModulus) rv = self->set_id(self, pModulus, ulModulusLen);
		if (rv == CKR_OK && !(iShould & RSAPUBKEY_CKA_KEY_TYPE_PRESENT)) rv = self->set_keytype(self, CKK_RSA);
	}
	if (pModulus) free(pModulus);
	return rv;
}


/**
 * @brief RSA public key encoder
 * 
 */
static KRSAPUBKEY_ENCODER_STR __default_rsapubkey_encoder =
{
	NULL,

	__rsapubkey_get_class,
	__rsapubkey_get_token,
	__rsapubkey_get_private,
	__rsapubkey_get_modifiable,
	__rsapubkey_get_label,
	__rsapubkey_get_copyable,
	__rsapubkey_get_destroyable,

	__rsapubkey_get_keytype,
	__rsapubkey_get_id,
	__rsapubkey_get_start_date,
	__rsapubkey_get_end_date,
	__rsapubkey_get_derive,
	__rsapubkey_get_local,
	__rsapubkey_get_keyGenMech,
	__rsapubkey_get_allowedMech,

	__rsapubkey_get_subject,
	__rsapubkey_get_encrypt,
	__rsapubkey_get_verify,
	__rsapubkey_get_verifyRec,
	__rsapubkey_get_wrap,
	__rsapubkey_get_trusted,
	__rsapubkey_get_keyinfo,

	__rsa_pubkey_get_modulus,
	__rsa_pubkey_get_pubexponent,

	__rsapubkey_set_class,
	__rsapubkey_set_token,
	__rsapubkey_set_private,
	__rsapubkey_set_modifiable,
	__rsapubkey_set_label,
	__rsapubkey_set_copyable,
	__rsapubkey_set_destroyable,

	__rsapubkey_set_keytype,
	__rsapubkey_set_id,
	__rsapubkey_set_start_date,
	__rsapubkey_set_end_date,
	__rsapubkey_set_derive,
	__rsapubkey_set_local,
	__rsapubkey_set_keyGenMech,
	__rsapubkey_set_allowedMech,

	__rsapubkey_set_subject,
	__rsapubkey_set_encrypt,
	__rsapubkey_set_verify,
	__rsapubkey_set_verifyRec,
	__rsapubkey_set_wrap,
	__rsapubkey_set_trusted,
	__rsapubkey_set_keyinfo,

	__rsapubkey_set_modulus,
	__rsapubkey_set_pubexponent,

	__rsapubkey_from_template
};
CK_NEW(CKIO_new_rsapubkey_encoder)(CK_OUT KRSAPUBKEY_ENCODER *hOut)
{
	CK_RV rv;
	KRSAPUBKEY_ENCODER hKey = NULL;
	NH_ASN1_ENCODER_HANDLE hEncoder = NULL;
	NH_ASN1_PNODE pNode;
	if
	(
		(rv = (hKey = (KRSAPUBKEY_ENCODER) malloc(sizeof(KRSAPUBKEY_ENCODER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = NH_new_encoder(64, 4096, &hEncoder) == NH_OK ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		NHARU_ASSERT((rv = hEncoder->chart(hEncoder, __rsapubkey_attributes_map, ASN_NODE_WAY_COUNT(__rsapubkey_attributes_map), &pNode)), "chart");
		if ((rv = NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
		{
			memcpy(hKey, &__default_rsapubkey_encoder, sizeof(KRSAPUBKEY_ENCODER_STR));
			hKey->hEncoder = hEncoder;
			if
			(
				(rv = hKey->set_class(hKey, CKO_PUBLIC_KEY)) == CKR_OK &&
				(rv = hKey->set_token(hKey, CK_FALSE)) == CKR_OK &&
				(rv = hKey->set_private(hKey, CK_FALSE)) == CKR_OK &&
				(rv = hKey->set_modifiable(hKey, CK_TRUE)) == CKR_OK &&
				(rv = hKey->set_copyable(hKey, CK_TRUE)) == CKR_OK &&
				(rv = hKey->set_destroyable(hKey, CK_TRUE)) == CKR_OK &&
				(rv = hKey->set_keytype(hKey, CKK_RSA)) == CKR_OK &&
				(rv = hKey->set_derive(hKey, CK_FALSE)) == CKR_OK &&
				(rv = hKey->set_local(hKey, CK_FALSE)) == CKR_OK &&
				(rv = hKey->set_allowedMech(hKey, __pSignMechanisms, KCKM_SIGN_MECHANISMS * sizeof(CKA_MECHANISM_TYPE))) == CKR_OK &&
				(rv = hKey->set_encrypt(hKey, CK_FALSE)) == CKR_OK &&
				(rv = hKey->set_verify(hKey, CK_TRUE)) == CKR_OK &&
				(rv = hKey->set_verifyRec(hKey, CK_FALSE)) == CKR_OK &&
				(rv = hKey->set_wrap(hKey, CK_FALSE)) == CKR_OK &&
				(rv = hKey->set_trusted(hKey, CK_FALSE)) == CKR_OK
			)	*hOut = hKey;
		}
	}
	if (rv != CKR_OK)
	{
		if (hKey) free(hKey);
		if (hEncoder) NH_release_encoder(hEncoder);
	}
	return rv;
}
CK_DELETE(CKIO_release_rsapubkey_encoder)(CK_INOUT KRSAPUBKEY_ENCODER hKey)
{
	if (hKey)
	{
		if (hKey->hEncoder) NH_release_encoder(hKey->hEncoder);
		free(hKey);
	}
}



/**
 * @brief RSA public key PKCS#11 object parser
 * 
 */
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_class)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __storage_get_class(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_token)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __storage_get_token(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_private)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __storage_get_private(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_modifiable)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __storage_get_modifiable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_label)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __storage_get_label(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_copyable)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __storage_get_copyable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_destroyable)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __storage_get_destroyable(self->hParser);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_keytype)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_keytype(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_id)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_id(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_start_date)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_start_date(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_end_date)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_end_date(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_derive)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_derive(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_local)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_local(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_keyGenMech)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_keyGenMech(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_allowedMech)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __key_get_allowedMech(self->hParser);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_subject)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __pubkey_get_subject(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_encrypt)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __pubkey_get_encrypt(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_verify)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __pubkey_get_verify(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_verifyRec)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __pubkey_get_verifyRec(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_wrap)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __pubkey_get_wrap(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_trusted)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __pubkey_get_trusted(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_keyinfo)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __pubkey_get_keyinfo(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_modulus)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __rsapubkey_get_modulus(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsapkey_get_pubexponent)(CK_IN KRSAPUBKEY_PARSER_STR *self)
{
	return __rsapubkey_get_pubexponent(self->hParser);
}

CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_class)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_class(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_token)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_token(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_private)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_private(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_modifiable)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_modifiable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_label)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_label(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_copyable)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_copyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_destroyable)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_destroyable(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_keyType)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_keyType(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_keyid)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_keyid(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_start_date)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_start_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_end_date)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_end_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_derive)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_derive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_local)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_local(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_keyGenMech)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_keyGenMech(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_allowedMech)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_allowedMech(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_subject)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_match_subject(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_encrypt)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_match_encrypt(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_verify)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_match_verify(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_verifyRec)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_match_verifyRec(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_wrap)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_match_wrap(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_trusted)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_match_trusted(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_keyinfo)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_match_keyinfo(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_modulus)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__rsapubkey_get_modulus(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match_pubexponent)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__rsapubkey_get_pubexponent(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsapkey_match)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_BBOOL match = CK_TRUE;
	CK_ULONG i = 0;
	while (match && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		__MATCH_STORAGE(self, match, &pTemplate[i])
		__MATCH_KEY(self, match, &pTemplate[i])
		case CKA_SUBJECT:
			match = self->match_subject(self, &pTemplate[i]);
			break;
		case CKA_ENCRYPT:
			match = self->match_encrypt(self, &pTemplate[i]);
			break;
		case CKA_VERIFY:
			match = self->match_verify(self, &pTemplate[i]);
			break;
		case CKA_VERIFY_RECOVER:
			match = self->match_verifyRec(self, &pTemplate[i]);
			break;
		case CKA_WRAP:
			match = self->match_wrap(self, &pTemplate[i]);
			break;
		case CKA_TRUSTED:
			match = self->match_trusted(self, &pTemplate[i]);
			break;
		case CKA_WRAP_TEMPLATE: return CK_FALSE;
		case CKA_PUBLIC_KEY_INFO:
			match = self->match_keyinfo(self, &pTemplate[i]);
			break;
		case CKA_MODULUS:
			match = self->match_modulus(self, &pTemplate[i]);
			break;
		case CKA_PUBLIC_EXPONENT:
			match = self->match_pubexponent(self, &pTemplate[i]);
			break;
		default: match = CK_FALSE;
		}
		i++;
	}
	return match;
}

CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_class)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_class(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_token)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_token(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_private)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_private(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_modifiable)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_modifiable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_label)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_label(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_copyable)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_copyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_destroyable)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_destroyable(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_keyType)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_keyType(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_keyid)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_keyid(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_start_date)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_start_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_end_date)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_end_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_derive)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_derive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_local)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_local(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_keyGenMech)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_keyGenMech(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_allowedMech)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_allowedMech(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_subject)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_attr_subject(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_encrypt)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_attr_encrypt(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_verify)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_attr_verify(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_verifyRec)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_attr_verifyRec(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_wrap)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_attr_wrap(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_trusted)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_attr_trusted(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_keyinfo)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __pubkey_attr_keyinfo(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_modulus)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__rsapubkey_get_modulus(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attr_pubexponent)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__rsapubkey_get_pubexponent(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsapkey_attributes)(CK_IN KRSAPUBKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_RV rv = CKR_OK;
	CK_ULONG i = 0;
	assert(pTemplate);
	while (rv == CKR_OK && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		__STORAGE_ATTRIBUTES(self, &pTemplate[i], rv)
		__KEY_ATTRIBUTES(self, &pTemplate[i], rv)
		case CKA_SUBJECT:
			rv = self->attr_subject(self, &pTemplate[i]);
			break;
		case CKA_ENCRYPT:
			rv = self->attr_encrypt(self, &pTemplate[i]);
			break;
		case CKA_VERIFY:
			rv = self->attr_verify(self, &pTemplate[i]);
			break;
		case CKA_VERIFY_RECOVER:
			rv = self->attr_verifyRec(self, &pTemplate[i]);
			break;
		case CKA_WRAP:
			rv = self->attr_wrap(self, &pTemplate[i]);
			break;
		case CKA_TRUSTED:
			rv = self->attr_trusted(self, &pTemplate[i]);
			break;
		case CKA_WRAP_TEMPLATE:
			rv = CKR_ATTRIBUTE_TYPE_INVALID;
			break;
		case CKA_PUBLIC_KEY_INFO:
			rv = self->attr_keyinfo(self, &pTemplate[i]);
			break;
		case CKA_MODULUS:
			rv = self->attr_modulus(self, &pTemplate[i]);
			break;
		case CKA_PUBLIC_EXPONENT:
			rv = self->attr_pubexponent(self, &pTemplate[i]);
			break;
		default: rv = CKR_ATTRIBUTE_TYPE_INVALID;
		}
		i++;
	}
	return rv;
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __key_attributes_parse)(CK_INOUT NH_ASN1_PARSER_HANDLE hParser)
{
	NH_RV rv;
	NH_ASN1_PNODE pNode;
	assert(hParser);
	if
	(
		(rv = (pNode = __key_get_keytype(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_little_integer(hParser, pNode)) == NH_OK &&
		(rv = (pNode = __key_get_id(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_octetstring(hParser, pNode)) == NH_OK &&
		(rv = (pNode = __key_get_derive(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __key_get_local(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK
	)
	{
		if
		(
			(rv = (pNode = __key_get_start_date(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode) &&
			ASN_IS_PRESENT(pNode->child)
		)	rv = hParser->parse_time(hParser, pNode->child);
		if
		(
			(NH_SUCCESS(rv)) &&
			(rv = (pNode = __key_get_end_date(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode) &&
			ASN_IS_PRESENT(pNode->child)
		)	rv = hParser->parse_time(hParser, pNode->child);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = __key_get_keyGenMech(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode)
		)	rv = hParser->parse_little_integer(hParser, pNode);
		if
		(
			NH_SUCCESS(rv) &&
			(rv = (pNode = __key_get_allowedMech(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode) && 
			(rv = (pNode = pNode->child) ? NH_OK : NH_OUT_OF_MEMORY_ERROR) == NH_OK
		)
		{
			while (NH_SUCCESS(rv) && pNode)
			{
				rv = hParser->parse_little_integer(hParser, pNode);
				pNode = pNode->next;
			}
		}
	}
	NHARU_ASSERT(rv, "__key_attributes_parse");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}

static KRSAPUBKEY_PARSER_STR __default_rsapubkey_parser =
{
	NULL,

	__rsapkey_get_class,
	__rsapkey_get_token,
	__rsapkey_get_private,
	__rsapkey_get_modifiable,
	__rsapkey_get_label,
	__rsapkey_get_copyable,
	__rsapkey_get_destroyable,

	__rsapkey_get_keytype,
	__rsapkey_get_id,
	__rsapkey_get_start_date,
	__rsapkey_get_end_date,
	__rsapkey_get_derive,
	__rsapkey_get_local,
	__rsapkey_get_keyGenMech,
	__rsapkey_get_allowedMech,

	__rsapkey_get_subject,
	__rsapkey_get_encrypt,
	__rsapkey_get_verify,
	__rsapkey_get_verifyRec,
	__rsapkey_get_wrap,
	__rsapkey_get_trusted,
	__rsapkey_get_keyinfo,

	__rsapkey_get_modulus,
	__rsapkey_get_pubexponent,

	__rsapkey_match_class,
	__rsapkey_match_token,
	__rsapkey_match_private,
	__rsapkey_match_modifiable,
	__rsapkey_match_label,
	__rsapkey_match_copyable,
	__rsapkey_match_destroyable,

	__rsapkey_match_keyType,
	__rsapkey_match_keyid,
	__rsapkey_match_start_date,
	__rsapkey_match_end_date,
	__rsapkey_match_derive,
	__rsapkey_match_local,
	__rsapkey_match_keyGenMech,
	__rsapkey_match_allowedMech,

	__rsapkey_match_subject,
	__rsapkey_match_encrypt,
	__rsapkey_match_verify,
	__rsapkey_match_verifyRec,
	__rsapkey_match_wrap,
	__rsapkey_match_trusted,
	__rsapkey_match_keyinfo,

	__rsapkey_match_modulus,
	__rsapkey_match_pubexponent,
	__rsapkey_match,

	__rsapkey_attr_class,
	__rsapkey_attr_token,
	__rsapkey_attr_private,
	__rsapkey_attr_modifiable,
	__rsapkey_attr_label,
	__rsapkey_attr_copyable,
	__rsapkey_attr_destroyable,

	__rsapkey_attr_keyType,
	__rsapkey_attr_keyid,
	__rsapkey_attr_start_date,
	__rsapkey_attr_end_date,
	__rsapkey_attr_derive,
	__rsapkey_attr_local,
	__rsapkey_attr_keyGenMech,
	__rsapkey_attr_allowedMech,

	__rsapkey_attr_subject,
	__rsapkey_attr_encrypt,
	__rsapkey_attr_verify,
	__rsapkey_attr_verifyRec,
	__rsapkey_attr_wrap,
	__rsapkey_attr_trusted,
	__rsapkey_attr_keyinfo,

	__rsapkey_attr_modulus,
	__rsapkey_attr_pubexponent,
	__rsapkey_attributes
};
CK_NEW(CKIO_new_rsapubkey_parser)(CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulSize, CK_OUT KRSAPUBKEY_PARSER *hOut)
{
	NH_RV rv;
	KRSAPUBKEY_PARSER hKey = NULL;
	NH_ASN1_PARSER_HANDLE hParser = NULL;
	NH_ASN1_PNODE pNode;
	NH_NAME_NODE ignored;
	if
	(
		(rv = (hKey = (KRSAPUBKEY_PARSER) malloc(sizeof(KRSAPUBKEY_PARSER_STR))) ? NH_OK : NH_OUT_OF_MEMORY_ERROR) == NH_OK &&
		(rv = NH_new_parser(pEncoding, ulSize, 64, 4096, &hParser)) == NH_OK &&
		(rv = hParser->map(hParser, __rsapubkey_attributes_map, ASN_NODE_WAY_COUNT(__rsapubkey_attributes_map))) == NH_OK &&
		(rv = __storage_parse(hParser)) == NH_OK &&
		(rv = __key_attributes_parse(hParser)) == NH_OK
	)
	{
		memcpy(hKey, &__default_rsapubkey_parser, sizeof(KRSAPUBKEY_PARSER_STR));
		hKey->hParser = hParser;
		if
		(
			(rv = (pNode = hKey->get_encrypt(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_boolean(pNode)) == NH_OK &&
			(rv = (pNode = hKey->get_verify(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_boolean(pNode)) == NH_OK &&
			(rv = (pNode = hKey->get_verifyRec(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_boolean(pNode)) == NH_OK &&
			(rv = (pNode = hKey->get_wrap(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_boolean(pNode)) == NH_OK &&
			(rv = (pNode = hKey->get_trusted(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_boolean(pNode)) == NH_OK &&
			(rv = (pNode = hKey->get_keyinfo(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_octetstring(hKey->hParser, pNode)) == NH_OK &&
			(rv = (pNode = hKey->get_modulus(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_integer(pNode)) == NH_OK &&
			(rv = (pNode = hKey->get_pubexponent(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = hKey->hParser->parse_integer(pNode)) == NH_OK
		)
		{
			if
			(
				NH_SUCCESS(rv) &&
				(rv = (pNode = hKey->get_subject(hKey)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
			)	rv = NHIX_parse_name(hKey->hParser, pNode, &ignored);
		}
	}
	if (NH_SUCCESS(rv)) *hOut = hKey;
	else
	{
		if (hParser) NH_release_parser(hParser);
		if (hKey) free(hKey);
	}
	NHARU_ASSERT(rv, "CKIO_new_rsapubkey_parser");
	return NH_SUCCESS(rv)? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_DELETE(CKIO_release_rsapubkey_parser)(CK_INOUT KRSAPUBKEY_PARSER hKey)
{
	if (hKey)
	{
		if (hKey->hParser) NH_release_parser(hKey->hParser);
		free(hKey);
	}
}



/* * * * * * * * * * * * * * * * *
 * CKO_PRIVATE_KEY PKCS#11 object
 * * * * * * * * * * * * * * * * *
 */
/**
 * @brief PrivkeyAttributes attriutes
 * 
 */
static NH_NODE_WAY __privkey_attributes_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* subject */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_SEQUENCE | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* sensitive */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* decrypt */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* sign */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* signRecover */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* unwrap */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* extractable */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* alwaysSensitive */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* neverExtractable */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* wrapWithTrusted */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* alwaysAuth */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_BOOLEAN | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* pubKeyInfo */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	}
};

#define __privkey_get_subject(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 16) | ((NH_PARSE_EAST | 2) << 8)  | NH_SAIL_SKIP_SOUTH))
#define __privkey_get_sensitive(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST))
#define __privkey_get_decrypt(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 2)))
#define __privkey_get_sign(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 3)))
#define __privkey_get_signRecover(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 4)))
#define __privkey_get_unwrap(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 5)))
#define __privkey_get_extractable(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 6)))
#define __privkey_get_alwaysSensitive(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 7)))
#define __privkey_get_neverExtractable(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 8)))
#define __privkey_get_wrapWithTrusted(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 9)))
#define __privkey_get_alwaysAuth(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 10)))
#define __privkey_get_pubKeyInfo(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 2) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 11)))

CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_subject)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __set_node(hEncoder, __privkey_get_subject(hEncoder), pValue, ulValuelen);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_sensitive)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_sensitive(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_decrypt)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_decrypt(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_sign)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_sign(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_signRecover)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_signRecover(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_unwrap)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_unwrap(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_extractable)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_extractable(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_alwaysSensitive)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_alwaysSensitive(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_neverExtractable)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_neverExtractable(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_wrapWithTrusted)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_wrapWithTrusted(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_alwaysAuth)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BBOOL isValue)
{
	return __set_boolean(hEncoder, __privkey_get_alwaysAuth(hEncoder), isValue, FALSE);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_set_keyinfo)(CK_IN NH_ASN1_ENCODER_HANDLE hEncoder, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __set_octet_string(hEncoder, __privkey_get_pubKeyInfo(hEncoder), pValue, ulValuelen, TRUE);
}

CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_subject)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __match_x500_name(hParser, __privkey_get_subject(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_sensitive)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_sensitive(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_decrypt)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_decrypt(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_sign)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_sign(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_signRecover)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_signRecover(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_unwrap)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_unwrap(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_extractable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_extractable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_alwaysSensitive)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_alwaysSensitive(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_neverExtractable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_neverExtractable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_wrapWithTrusted)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_wrapWithTrusted(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_alwaysAuth)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_bool(__privkey_get_alwaysAuth(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __privkey_match_keyinfo)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __node_match_binary(__privkey_get_pubKeyInfo(hParser), pAttribute);;
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_subject)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	NH_ASN1_PNODE pNode = __privkey_get_subject(hParser);
	if (ASN_IS_PRESENT(pNode)) rv = __attribute_node(pNode, pAttribute);
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_sensitive)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_sensitive(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_decrypt)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_decrypt(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_sign)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_sign(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_signRecover)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_signRecover(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_unwrap)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_unwrap(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_extractable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_extractable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_alwaysSensitive)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_alwaysSensitive(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_neverExtractable)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_neverExtractable(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_wrapWithTrusted)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_wrapWithTrusted(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_alwaysAuth)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_bool(__privkey_get_alwaysAuth(hParser), pAttribute);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_attr_keyinfo)(CK_IN NH_ASN1_PARSER_HANDLE hParser, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__privkey_get_pubKeyInfo(hParser), pAttribute);
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __privkey_parse)(CK_INOUT NH_ASN1_PARSER_HANDLE hParser)
{
	NH_RV rv;
	NH_ASN1_PNODE pNode;
	NH_NAME_NODE ignored;

	assert(hParser);
	if
	(
		(rv = (pNode = __privkey_get_sensitive(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_decrypt(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_sign(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_signRecover(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_unwrap(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_extractable(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_alwaysSensitive(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_neverExtractable(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_wrapWithTrusted(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_alwaysAuth(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_boolean(pNode)) == NH_OK &&
		(rv = (pNode = __privkey_get_subject(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
	)
	{
		if (ASN_IS_PRESENT(pNode)) rv = NHIX_parse_name(hParser, pNode, &ignored);
		if
		(
			NH_SUCCESS(rv) && 
			(rv = (pNode = __privkey_get_pubKeyInfo(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			ASN_IS_PRESENT(pNode)
		)	rv = hParser->parse_octetstring(hParser, pNode);
	}
	NHARU_ASSERT(rv, "__privkey_parse");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}

/**
 * @brief RSA key PKCS#11 attributes handler
 * 
 */
static NH_NODE_WAY __rsa_key_attributes_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE,
		NULL,
		0
	},
	{	/* modulus */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_INTEGER | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* publicExponent */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* privateExponent */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* prime1 */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_0 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* prime2 */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_1 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* exponent1 */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_2 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* exponent2 */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_3 | NH_ASN1_OPTIONAL_BIT | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* coefficient */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_INTEGER | NH_ASN1_EXPLICIT_BIT | NH_ASN1_CONTEXT_BIT | NH_ASN1_CT_TAG_4 | NH_ASN1_OPTIONAL_BIT,
		NULL,
		0
	}
};
#define __rsa_key_get_modulus(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH)))
#define __rsa_key_get_publicExponent(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 1)))
#define __rsa_key_get_privateExponent(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 2)))
#define __rsa_key_get_prime1(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 3)))
#define __rsa_key_get_prime2(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 4)))
#define __rsa_key_get_exponent1(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 5)))
#define __rsa_key_get_exponent2(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 6)))
#define __rsa_key_get_coefficient(_h)	(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 7)))
CK_IMPLEMENTATION(CK_RV, __rsakey_set_modulus)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_integer(self->hEncoder, __rsa_key_get_modulus(self->hEncoder), pValue, ulValueLen, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __rsakey_set_publicExponent)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_integer(self->hEncoder, __rsa_key_get_publicExponent(self->hEncoder), pValue, ulValueLen, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __rsakey_set_privateExponent)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __set_integer(self->hEncoder, __rsa_key_get_privateExponent(self->hEncoder), pValue, ulValueLen, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __rsakey_set_prime1)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = __rsa_key_get_prime1(self->hEncoder);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	assert(pValue);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_integer(self->hEncoder, pNode->child, pValue, ulValueLen, CK_FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_set_prime2)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = __rsa_key_get_prime2(self->hEncoder);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	assert(pValue);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_integer(self->hEncoder, pNode->child, pValue, ulValueLen, CK_FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_set_exponent1)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = __rsa_key_get_exponent1(self->hEncoder);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	assert(pValue);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_integer(self->hEncoder, pNode->child, pValue, ulValueLen, CK_FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_set_exponent2)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = __rsa_key_get_exponent2(self->hEncoder);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	assert(pValue);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_integer(self->hEncoder, pNode->child, pValue, ulValueLen, CK_FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_set_coefficient)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	NH_ASN1_PNODE pNode = __rsa_key_get_coefficient(self->hEncoder);
	CK_RV rv = __set_explicit_node(self->hEncoder, pNode);
	assert(pValue);
	if (rv == CKR_OK)
	{
		self->hEncoder->register_optional(pNode);
		rv = __set_integer(self->hEncoder, pNode->child, pValue, ulValueLen, CK_FALSE);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_encode)(CK_IN KRSAKEY_HANDLER_STR *self, CK_OUT CK_BYTE_PTR pEncoding, CK_INOUT CK_ULONG_PTR pulEncodingLen)
{
	CK_RV rv;
	CK_ULONG ulSize = self->hEncoder->encoded_size(self->hEncoder, self->hEncoder->root);
	assert(pulEncodingLen);
	if ((rv = ulSize > 0 ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		if (!pEncoding) *pulEncodingLen = ulSize;
		else
		{
			if
			(
				(rv = *pulEncodingLen >= ulSize ? CKR_OK : CKR_BUFFER_TOO_SMALL) == CKR_OK &&
				(rv = self->hEncoder->encode(self->hEncoder, self->hEncoder->root, pEncoding)) == CKR_OK
			)	*pulEncodingLen = ulSize;
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_modulus)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *n)
{
	CK_RV rv;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && n);
	if
	(
		(rv = __attribute_binary(__rsa_key_get_modulus(self->hParser), &attr)) == CKR_OK &&
		(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = __attribute_binary(__rsa_key_get_modulus(self->hParser), &attr)) == CKR_OK)
		{
			n->data = (unsigned char*) attr.pValue;
			n->length = attr.ulValueLen;
		}
		else free(attr.pValue);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_publicExponent)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *e)
{
	CK_RV rv;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && e);
	if
	(
		(rv = __attribute_binary(__rsa_key_get_publicExponent(self->hParser), &attr)) == CKR_OK &&
		(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = __attribute_binary(__rsa_key_get_publicExponent(self->hParser), &attr)) == CKR_OK)
		{
			e->data = (unsigned char*) attr.pValue;
			e->length = attr.ulValueLen;
		}
		else free(attr.pValue);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_privateExponent)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *d)
{
	CK_RV rv;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && d);
	if
	(
		(rv = __attribute_binary(__rsa_key_get_privateExponent(self->hParser), &attr)) == CKR_OK &&
		(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if ((rv = __attribute_binary(__rsa_key_get_privateExponent(self->hParser), &attr)) == CKR_OK)
		{
			d->data = (unsigned char*) attr.pValue;
			d->length = attr.ulValueLen;
		}
		else free(attr.pValue);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_prime1)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *p)
{
	CK_RV rv = CKR_KP_ATTRIBUTE_UNAVAILABLE;
	NH_ASN1_PNODE pNode;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && p);
	pNode = __rsa_key_get_prime1(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child))
	{
		if
		(
			(rv = __attribute_binary(pNode->child, &attr)) == CKR_OK &&
			(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = __attribute_binary(pNode->child, &attr)) == CKR_OK)
			{
				p->data = (unsigned char*) attr.pValue;
				p->length = attr.ulValueLen;
			}
			else free(attr.pValue);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_prime2)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *q)
{
	CK_RV rv = CKR_KP_ATTRIBUTE_UNAVAILABLE;
	NH_ASN1_PNODE pNode;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && q);
	pNode = __rsa_key_get_prime2(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child))
	{
		if
		(
			(rv = __attribute_binary(pNode->child, &attr)) == CKR_OK &&
			(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = __attribute_binary(pNode->child, &attr)) == CKR_OK)
			{
				q->data = (unsigned char*) attr.pValue;
				q->length = attr.ulValueLen;
			}
			else free(attr.pValue);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_exponent1)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *dmp)
{
	CK_RV rv = CKR_KP_ATTRIBUTE_UNAVAILABLE;
	NH_ASN1_PNODE pNode;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && dmp);
	pNode = __rsa_key_get_exponent1(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child))
	{
		if
		(
			(rv = __attribute_binary(pNode->child, &attr)) == CKR_OK &&
			(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = __attribute_binary(pNode->child, &attr)) == CKR_OK)
			{
				dmp->data = (unsigned char*) attr.pValue;
				dmp->length = attr.ulValueLen;
			}
			else free(attr.pValue);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_exponent2)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *dmq)
{
	CK_RV rv = CKR_KP_ATTRIBUTE_UNAVAILABLE;
	NH_ASN1_PNODE pNode;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && dmq);
	pNode = __rsa_key_get_exponent2(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child))
	{
		if
		(
			(rv = __attribute_binary(pNode->child, &attr)) == CKR_OK &&
			(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = __attribute_binary(pNode->child, &attr)) == CKR_OK)
			{
				dmq->data = (unsigned char*) attr.pValue;
				dmq->length = attr.ulValueLen;
			}
			else free(attr.pValue);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_get_coefficient)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT NH_BIG_INTEGER *qmp)
{
	CK_RV rv = CKR_KP_ATTRIBUTE_UNAVAILABLE;
	NH_ASN1_PNODE pNode;
	CK_ATTRIBUTE attr = { 0UL, NULL, 0UL };
	assert(self->hParser && qmp);
	pNode = __rsa_key_get_coefficient(self->hParser);
	if (ASN_IS_PRESENT(pNode) && ASN_IS_PARSED(pNode->child))
	{
		if
		(
			(rv = __attribute_binary(pNode->child, &attr)) == CKR_OK &&
			(rv = (attr.pValue = malloc(attr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
		)
		{
			if ((rv = __attribute_binary(pNode->child, &attr)) == CKR_OK)
			{
				qmp->data = (unsigned char*) attr.pValue;
				qmp->length = attr.ulValueLen;
			}
			else free(attr.pValue);
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_parse)(CK_INOUT KRSAKEY_HANDLER_STR *self, CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulEncodingLen)
{
	NH_RV rv;
	NH_ASN1_PNODE pNode;

	assert(!self->hParser && pEncoding);
	if ((rv = NH_new_parser(pEncoding, ulEncodingLen, 16, 4096, &self->hParser)) == NH_OK)
	{
		if
		(
			(rv = self->hParser->map(self->hParser, __rsa_key_attributes_map, ASN_NODE_WAY_COUNT(__rsa_key_attributes_map))) == NH_OK &&
			(rv = (pNode = __rsa_key_get_modulus(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = self->hParser->parse_integer(pNode)) == NH_OK &&
			(rv = (pNode = __rsa_key_get_publicExponent(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = self->hParser->parse_integer(pNode)) == NH_OK &&
			(rv = (pNode = __rsa_key_get_privateExponent(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
			(rv = self->hParser->parse_integer(pNode)) == NH_OK &&
			(rv = (pNode = __rsa_key_get_prime1(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
		)
		{
			if (ASN_IS_PRESENT(pNode) && pNode->child) rv = self->hParser->parse_integer(pNode->child);
			if
			(
				NH_SUCCESS(rv) &&
				(rv = (pNode = __rsa_key_get_prime2(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
				ASN_IS_PRESENT(pNode) && pNode->child
			)	rv = self->hParser->parse_integer(pNode->child);
			if
			(
				NH_SUCCESS(rv) &&
				(rv = (pNode = __rsa_key_get_exponent1(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
				ASN_IS_PRESENT(pNode) && pNode->child
			)	rv = self->hParser->parse_integer(pNode->child);
			if
			(
				NH_SUCCESS(rv) &&
				(rv = (pNode = __rsa_key_get_exponent2(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
				ASN_IS_PRESENT(pNode) && pNode->child
			)	rv = self->hParser->parse_integer(pNode->child);
			if
			(
				NH_SUCCESS(rv) &&
				(rv = (pNode = __rsa_key_get_coefficient(self->hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
				ASN_IS_PRESENT(pNode) && pNode->child
			)	rv = self->hParser->parse_integer(pNode->child);
		}
		if (rv != NH_OK)
		{
			NH_release_parser(self->hParser);
			self->hParser = NULL;
		}
	}
	NHARU_ASSERT(rv, "__rsakey_parse");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_BBOOL, __match_function)(CK_IN KRSAKEY_HANDLER_STR *self, CK_IN KRSA_GET_OCTETS call, CK_IN CK_ATTRIBUTE_PTR pAttr)
{
	CK_BBOOL match = CK_FALSE;
	NH_BIG_INTEGER value = { NULL, 0UL };
	if (call(self, &value) == CKR_OK)
	{
		match = pAttr->ulValueLen == value.length && memcmp(pAttr->pValue, value.data, value.length) == 0;
		OPENSSL_cleanse(value.data, value.length);
		free(value.data);
	}
	return match;
}
CK_IMPLEMENTATION(CK_BBOOL, __rsakey_match)(CK_IN KRSAKEY_HANDLER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttr)
{
	CK_BBOOL match = CK_FALSE;
	assert(pAttr);
	switch (pAttr->type)
	{
	case CKA_MODULUS:
		match = __match_function(self, self->get_modulus, pAttr);
		break;
	case CKA_PUBLIC_EXPONENT:
		match = __match_function(self, self->get_publicExponent, pAttr);
		break;
	case CKA_PRIVATE_EXPONENT:
		match = __match_function(self, self->get_privateExponent, pAttr);
		break;
	case CKA_PRIME_1:
		match = __match_function(self, self->get_prime1, pAttr);
		break;
	case CKA_PRIME_2:
		match = __match_function(self, self->get_prime2, pAttr);
		break;
	case CKA_EXPONENT_1:
		match = __match_function(self, self->get_exponent1, pAttr);
		break;
	case CKA_EXPONENT_2:
		match = __match_function(self, self->get_exponent2, pAttr);
		break;
	case CKA_COEFFICIENT:
		match = __match_function(self, self->get_coefficient, pAttr);
		break;
	}
	return match;
}
CK_IMPLEMENTATION(CK_RV, __attr_function)(CK_IN KRSAKEY_HANDLER_STR *self, CK_IN KRSA_GET_OCTETS call, CK_INOUT CK_ATTRIBUTE_PTR pAttr)
{
	CK_RV rv;
	NH_BIG_INTEGER value = { NULL, 0UL };
	if ((rv = call(self, &value)) == CKR_OK)
	{
		if (!pAttr->pValue) pAttr->ulValueLen = value.length;
		else
		{
			if (pAttr->ulValueLen >= value.length)
			{
				memcpy(pAttr->pValue, value.data, value.length);
				pAttr->ulValueLen = value.length;
			}
			else { rv = CKR_BUFFER_TOO_SMALL; pAttr->ulValueLen = value.length; }
		}
		OPENSSL_cleanse(value.data, value.length);
		free(value.data);
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsakey_attribute)(CK_IN KRSAKEY_HANDLER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttr)
{
	CK_RV rv = CKR_OK;
	assert(pAttr);
	switch (pAttr->type)
	{
	case CKA_MODULUS:
		rv = __attr_function(self, self->get_modulus, pAttr);
		break;
	case CKA_PUBLIC_EXPONENT:
		rv = __attr_function(self, self->get_publicExponent, pAttr);
		break;
	case CKA_PRIVATE_EXPONENT:
		rv = __attr_function(self, self->get_privateExponent, pAttr);
		break;
	case CKA_PRIME_1:
		rv = __attr_function(self, self->get_prime1, pAttr);
		break;
	case CKA_PRIME_2:
		rv = __attr_function(self, self->get_prime2, pAttr);
		break;
	case CKA_EXPONENT_1:
		rv = __attr_function(self, self->get_exponent1, pAttr);
		break;
	case CKA_EXPONENT_2:
		rv = __attr_function(self, self->get_exponent2, pAttr);
		break;
	case CKA_COEFFICIENT:
		rv = __attr_function(self, self->get_coefficient, pAttr);
		break;
	}
	return rv;
}

static KRSAKEY_HANDLER_STR __default_rsa_key_handler =
{
	NULL,
	__rsakey_set_modulus,
	__rsakey_set_publicExponent,
	__rsakey_set_privateExponent,
	__rsakey_set_prime1,
	__rsakey_set_prime2,
	__rsakey_set_exponent1,
	__rsakey_set_exponent2,
	__rsakey_set_coefficient,
	__rsakey_encode,

	NULL,
	__rsakey_get_modulus,
	__rsakey_get_publicExponent,
	__rsakey_get_privateExponent,
	__rsakey_get_prime1,
	__rsakey_get_prime2,
	__rsakey_get_exponent1,
	__rsakey_get_exponent2,
	__rsakey_get_coefficient,
	__rsakey_parse,

	__rsakey_match,
	__rsakey_attribute
};
CK_NEW(CKIO_new_rsa_handler)(CK_OUT KRSAKEY_HANDLER *hOut)
{
	CK_RV rv;
	KRSAKEY_HANDLER hHandler = NULL;
	NH_ASN1_PNODE pNode;

	if ((rv = (hHandler = (KRSAKEY_HANDLER) malloc(sizeof(KRSAKEY_HANDLER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(hHandler, &__default_rsa_key_handler, sizeof(KRSAKEY_HANDLER_STR));
		if ((rv = NH_SUCCESS(NH_new_encoder(16, 4096, &hHandler->hEncoder)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
		{
			rv = hHandler->hEncoder->chart(hHandler->hEncoder, __rsa_key_attributes_map, ASN_NODE_WAY_COUNT(__rsa_key_attributes_map), &pNode);
			NHARU_ASSERT(rv, "chart");
			if ((rv = NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK) *hOut = hHandler;
		}
		if (rv != CKR_OK)
		{
			if (hHandler->hEncoder) NH_release_encoder(hHandler->hEncoder);
			free(hHandler);
		}
	}
	return rv;
}
CK_DELETE(CKIO_release_rsa_handler)(CK_INOUT KRSAKEY_HANDLER hHandler)
{
	if (hHandler)
	{
		if (hHandler->hEncoder)
		{
			hHandler->hEncoder->container->zeroize(hHandler->hEncoder->container, OPENSSL_cleanse);
			NH_release_encoder(hHandler->hEncoder);
		}
		if (hHandler->hParser)
		{
			hHandler->hParser->container->zeroize(hHandler->hParser->container, OPENSSL_cleanse);
			NH_release_parser(hHandler->hParser);
		}
		free(hHandler);
	}
}


/**
 * @brief RSA private key attributes
 * 
 */
static NH_NODE_WAY __sensitive_material_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE,
		NULL,
		0
	},
	{	/* salt */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_OCTET_STRING | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* iv */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING | NH_ASN1_HAS_NEXT_BIT,
		NULL,
		0
	},
	{	/* ciphertext */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_OCTET_STRING,
		NULL,
		0
	}
};
static NH_NODE_WAY __rsa_privkey_attributes_map[] =
{
	{
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE,
		NULL,
		0
	},
	{	/* storage */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__storage_attributes_map,
		ASN_NODE_WAY_COUNT(__storage_attributes_map)
	},
	{	/* keyAttrs*/
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__key_attributes_map,
		ASN_NODE_WAY_COUNT(__key_attributes_map)
	},
	{	/* privkeyAttrs */
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__privkey_attributes_map,
		ASN_NODE_WAY_COUNT(__privkey_attributes_map)
	},
	{
		NH_SAIL_SKIP_EAST,
		NH_ASN1_SEQUENCE,
		__sensitive_material_map,
		ASN_NODE_WAY_COUNT(__sensitive_material_map)
	}
};

#define  __rsa_privkey_get_salt(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 16) | ((NH_PARSE_EAST | 3) << 8)  | NH_SAIL_SKIP_SOUTH))
#define  __rsa_privkey_get_iv(_h)			(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 3) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | NH_SAIL_SKIP_EAST))
#define  __rsa_privkey_get_ciphertext(_h)		(_h->sail(_h->root, (NH_SAIL_SKIP_SOUTH << 24) | ((NH_PARSE_EAST | 3) << 16) | (NH_SAIL_SKIP_SOUTH << 8) | (NH_PARSE_EAST | 2)))

CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_class)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __storage_get_class(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_token)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __storage_get_token(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_private)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __storage_get_private(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_modifiable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __storage_get_modifiable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_label)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __storage_get_label(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_copyable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __storage_get_copyable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_destroyable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __storage_get_destroyable(self->hEncoder);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_keytype)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_keytype(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_id)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_id(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_start_date)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_start_date(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_end_date)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_end_date(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_derive)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_derive(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_local)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_local(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_keyGenMech)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_keyGenMech(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_allowedMech)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __key_get_allowedMech(self->hEncoder);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_subject)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_subject(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_sensitive)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_sensitive(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_decrypt)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_decrypt(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_sign)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_sign(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_signRecover)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_signRecover(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_unwrap)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_unwrap(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_extractable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_extractable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_alwaysSensitive)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_alwaysSensitive(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_neverExtractable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_neverExtractable(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_wrapWithTrusted)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_wrapWithTrusted(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_alwaysAuth)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_alwaysAuth(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, ___rsaprivkey_get_keyinfo)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __privkey_get_pubKeyInfo(self->hEncoder);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivkey_get_salt)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __rsa_privkey_get_salt(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivkey_get_iv)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __rsa_privkey_get_iv(self->hEncoder);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivkey_get_ciphertext)(CK_IN KRSAPRIVKEY_ENCODER_STR *self)
{
	return __rsa_privkey_get_ciphertext(self->hEncoder);
}

CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_class)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_OBJECT_CLASS oClass)
{
	return __storage_set_class(self->hEncoder, oClass);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_token)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isToken)
{
	return __storage_set_token(self->hEncoder, isToken);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_private)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isPrivate)
{
	return __storage_set_private(self->hEncoder, isPrivate);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_modifiable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isModifiable)
{
	return __storage_set_modifiable(self->hEncoder, isModifiable);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_label)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_UTF8CHAR_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pValue);
	return __storage_set_label(self->hEncoder, pValue, ulValueLen);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_copyable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isCopyable)
{
	return __storage_set_copyable(self->hEncoder, isCopyable);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_destroyable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isDestroyable)
{
	return __storage_set_destroyable(self->hEncoder, isDestroyable);
}

CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_keytype)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_KEY_TYPE ulValue)
{
	return __key_set_keytype(self->hEncoder, ulValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_id)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	return __key_set_keyid(self->hEncoder, pValue, ulValueLen);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_start_date)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_DATE pDate)
{
	return __key_set_start_date(self->hEncoder, pDate);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_end_date)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_DATE pDate)
{
	return __key_set_end_date(self->hEncoder, pDate);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_derive)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __key_set_derive(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_local)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __key_set_local(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_keyGenMech)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_MECHANISM_TYPE ulType)
{
	return __key_set_keyGenMech(self->hEncoder, ulType);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_allowedMech)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_MECHANISM_TYPE_PTR pValue, CK_IN CK_ULONG ulValueLen)
{
	return __key_set_allowedMech(self->hEncoder, pValue, ulValueLen);
}

CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_subject)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __privkey_set_subject(self->hEncoder, pValue, ulValuelen);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_sensitive)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_sensitive(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_decrypt)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_decrypt(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_sign)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_sign(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_signRecover)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_signRecover(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_unwrap)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_unwrap(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_extractable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_extractable(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_alwaysSensitive)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_alwaysSensitive(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_neverExtractable)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_neverExtractable(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_wrapWithTrusted)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_wrapWithTrusted(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_alwaysAuth)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BBOOL isValue)
{
	return __privkey_set_alwaysAuth(self->hEncoder, isValue);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_keyinfo)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_IN CK_ULONG ulValuelen)
{
	return __privkey_set_keyinfo(self->hEncoder, pValue, ulValuelen);
}

CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_salt)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_ULONG ulValueLen)
{
	return __set_octet_string(self->hEncoder, __rsa_privkey_get_salt(self->hEncoder), pValue, ulValueLen, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_iv)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_ULONG ulValueLen)
{
	return __set_octet_string(self->hEncoder, __rsa_privkey_get_iv(self->hEncoder), pValue, ulValueLen, FALSE);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_set_ciphertext)(CK_IN KRSAPRIVKEY_ENCODER_STR *self, CK_IN CK_BYTE_PTR pValue, CK_ULONG ulValueLen)
{
	return __set_octet_string(self->hEncoder, __rsa_privkey_get_ciphertext(self->hEncoder), pValue, ulValueLen, FALSE);
}

#define RSAPRIVKEY_CKA_KEY_TYPE_PRESENT			(1)
#define RSAPRIVKEY_CKA_ID_PRESENT				(RSAPRIVKEY_CKA_KEY_TYPE_PRESENT << 1)
#define RSAPRIVKEY_CKA_PUBLIC_KEY_INFO_PRESENT		(RSAPRIVKEY_CKA_KEY_TYPE_PRESENT << 2)
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_from_template)
(
	CK_IN KRSAPRIVKEY_ENCODER_STR *self,
	CK_IN KTOKEN_STR *pToken,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount
)
{
	CK_RV rv = CKR_OK;
	CK_ULONG i = 0, iMod = CK_UNAVAILABLE_INFORMATION, iExpo = CK_UNAVAILABLE_INFORMATION, ulInfoLen;
	int iShould = 0;
	KRSAKEY_HANDLER hRSAKey;
	CK_BYTE *pPubKeyInfo;

	assert(pTemplate && pToken);
	if ((rv = CKIO_new_rsa_handler(&hRSAKey) == CKR_OK ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
	{
		while (rv == CKR_OK && i < ulCount)
		{
			switch (pTemplate[i].type)
			{
			case CKA_CLASS:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_OBJECT_CLASS) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = *((CK_OBJECT_CLASS *) pTemplate[i].pValue) == CKO_PRIVATE_KEY ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID;
				break;
			case CKA_TOKEN:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_token(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_PRIVATE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_private(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_MODIFIABLE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_modifiable(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_LABEL:
				rv = self->set_label(self, (CK_UTF8CHAR_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;
			case CKA_COPYABLE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_copyable(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_DESTROYABLE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_destroyable(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;

			case CKA_KEY_TYPE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_KEY_TYPE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK &&
					(rv = *((CK_KEY_TYPE *) pTemplate[i].pValue) == CKK_RSA ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	iShould |= RSAPRIVKEY_CKA_KEY_TYPE_PRESENT;
				break;
			case CKA_ID:
				if ((rv = self->set_id(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK) iShould |= RSAPRIVKEY_CKA_ID_PRESENT;
				break;
			case CKA_START_DATE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_DATE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_start_date(self, *(CK_DATE *) pTemplate[i].pValue);
				break;
			case CKA_END_DATE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_DATE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_end_date(self, *(CK_DATE *) pTemplate[i].pValue);
				break;
			case CKA_DERIVE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_derive(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_LOCAL:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_local(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_KEY_GEN_MECHANISM:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_MECHANISM_TYPE) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_keyGenMech(self, *(CK_MECHANISM_TYPE_PTR) pTemplate[i].pValue);
				break;
			case CKA_ALLOWED_MECHANISMS:
				if
				(
					(rv = pTemplate[i].ulValueLen % sizeof(CK_MECHANISM_TYPE) == 0 ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv =  self->set_allowedMech(self, (CK_MECHANISM_TYPE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;

			case CKA_SUBJECT:
				rv = self->set_subject(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;
			case CKA_SENSITIVE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_sensitive(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_DECRYPT:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_decrypt(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_SIGN:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_sign(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_SIGN_RECOVER:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_signRecover(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_UNWRAP:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_unwrap(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_EXTRACTABLE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_extractable(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_ALWAYS_SENSITIVE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_alwaysSensitive(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_NEVER_EXTRACTABLE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_neverExtractable(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_WRAP_WITH_TRUSTED:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_wrapWithTrusted(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_ALWAYS_AUTHENTICATE:
				if
				(
					(rv = pTemplate[i].ulValueLen == sizeof(CK_BBOOL) ? CKR_OK : CKR_ATTRIBUTE_VALUE_INVALID) == CKR_OK
				)	rv = self->set_alwaysAuth(self, *((CK_BBOOL *) pTemplate[i].pValue));
				break;
			case CKA_PUBLIC_KEY_INFO:
				if ((rv = self->set_keyinfo(self, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK) iShould |= RSAPRIVKEY_CKA_PUBLIC_KEY_INFO_PRESENT;
				break;
			
			case CKA_MODULUS:
				if ((rv = hRSAKey->set_modulus(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK) iMod = i;
				break;
			case CKA_PUBLIC_EXPONENT:
				if ((rv = hRSAKey->set_publicExponent(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen)) == CKR_OK) iExpo = i;
				break;
			case CKA_PRIVATE_EXPONENT:
				rv = hRSAKey->set_privateExponent(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;
			case CKA_PRIME_1:
				rv = hRSAKey->set_prime1(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;
			case CKA_PRIME_2:
				rv = hRSAKey->set_prime2(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;
			case CKA_EXPONENT_1:
				rv = hRSAKey->set_exponent1(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;
			case CKA_EXPONENT_2:
				rv = hRSAKey->set_exponent2(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;
			case CKA_COEFFICIENT:
				rv = hRSAKey->set_coefficient(hRSAKey, (CK_BYTE_PTR) pTemplate[i].pValue, pTemplate[i].ulValueLen);
				break;

			default: rv = CKR_ATTRIBUTE_TYPE_INVALID;
			}
			i++;
		}
		if
		(
			rv == CKR_OK &&
			iMod != CK_UNAVAILABLE_INFORMATION &&
			iExpo != CK_UNAVAILABLE_INFORMATION
		)	rv = CKIO_encrypt_material((KRSAPRIVKEY_ENCODER) self, hRSAKey, pToken);
		CKIO_release_rsa_handler(hRSAKey);
	}
	if
	(
		rv == CKR_OK &&
		!(iShould & RSAPRIVKEY_CKA_KEY_TYPE_PRESENT)
	)	rv = self->set_keytype(self, CKK_RSA);
	if
	(
		rv == CKR_OK &&
		!(iShould & RSAPRIVKEY_CKA_ID_PRESENT) &&
		(iMod != CK_UNAVAILABLE_INFORMATION)
	)	rv = self->set_id(self, (CK_BYTE_PTR) pTemplate[iMod].pValue, pTemplate[iMod].ulValueLen);
	if
	(
		rv == CKR_OK &&
		!(iShould & RSAPRIVKEY_CKA_PUBLIC_KEY_INFO_PRESENT) &&
		(iExpo != CK_UNAVAILABLE_INFORMATION) &&
		(rv = __toPubKeyInfo(&pTemplate[iMod], &pTemplate[iExpo], &pPubKeyInfo, &ulInfoLen)) == CKR_OK
	)
	{
		rv = self->set_keyinfo(self, pPubKeyInfo, ulInfoLen);
		free(pPubKeyInfo);
	}
	return rv;
}

static KRSAPRIVKEY_ENCODER_STR __default_rsaprivkey_encoder =
{
	NULL,

	___rsaprivkey_get_class,
	___rsaprivkey_get_token,
	___rsaprivkey_get_private,
	___rsaprivkey_get_modifiable,
	___rsaprivkey_get_label,
	___rsaprivkey_get_copyable,
	___rsaprivkey_get_destroyable,

	___rsaprivkey_get_keytype,
	___rsaprivkey_get_id,
	___rsaprivkey_get_start_date,
	___rsaprivkey_get_end_date,
	___rsaprivkey_get_derive,
	___rsaprivkey_get_local,
	___rsaprivkey_get_keyGenMech,
	___rsaprivkey_get_allowedMech,

	___rsaprivkey_get_subject,
	___rsaprivkey_get_sensitive,
	___rsaprivkey_get_decrypt,
	___rsaprivkey_get_sign,
	___rsaprivkey_get_signRecover,
	___rsaprivkey_get_unwrap,
	___rsaprivkey_get_extractable,
	___rsaprivkey_get_alwaysSensitive,
	___rsaprivkey_get_neverExtractable,
	___rsaprivkey_get_wrapWithTrusted,
	___rsaprivkey_get_alwaysAuth,
	___rsaprivkey_get_keyinfo,

	__rsaprivkey_get_salt,
	__rsaprivkey_get_iv,
	__rsaprivkey_get_ciphertext,

	__rsaprivkey_set_class,
	__rsaprivkey_set_token,
	__rsaprivkey_set_private,
	__rsaprivkey_set_modifiable,
	__rsaprivkey_set_label,
	__rsaprivkey_set_copyable,
	__rsaprivkey_set_destroyable,

	__rsaprivkey_set_keytype,
	__rsaprivkey_set_id,
	__rsaprivkey_set_start_date,
	__rsaprivkey_set_end_date,
	__rsaprivkey_set_derive,
	__rsaprivkey_set_local,
	__rsaprivkey_set_keyGenMech,
	__rsaprivkey_set_allowedMech,

	__rsaprivkey_set_subject,
	__rsaprivkey_set_sensitive,
	__rsaprivkey_set_decrypt,
	__rsaprivkey_set_sign,
	__rsaprivkey_set_signRecover,
	__rsaprivkey_set_unwrap,
	__rsaprivkey_set_extractable,
	__rsaprivkey_set_alwaysSensitive,
	__rsaprivkey_set_neverExtractable,
	__rsaprivkey_set_wrapWithTrusted,
	__rsaprivkey_set_alwaysAuth,
	__rsaprivkey_set_keyinfo,

	__rsaprivkey_set_salt,
	__rsaprivkey_set_iv,
	__rsaprivkey_set_ciphertext,

	__rsaprivkey_from_template
};
CK_NEW(CKIO_new_rsaprivkey_encoder)(CK_OUT KRSAPRIVKEY_ENCODER *hOut)
{
	CK_RV rv;
	KRSAPRIVKEY_ENCODER hHandler = NULL;
	NH_ASN1_ENCODER_HANDLE hEncoder = NULL;
	NH_ASN1_PNODE pNode;

	if
	(
		(rv = (hHandler = (KRSAPRIVKEY_ENCODER) malloc(sizeof(KRSAPRIVKEY_ENCODER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = NH_SUCCESS(NH_new_encoder(64, 4096, &hEncoder)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		NHARU_ASSERT((rv = hEncoder->chart(hEncoder, __rsa_privkey_attributes_map, ASN_NODE_WAY_COUNT(__rsa_privkey_attributes_map), &pNode)), "chart");
		if ((rv = NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
		{
			memcpy(hHandler, &__default_rsaprivkey_encoder, sizeof(KRSAPRIVKEY_ENCODER_STR));
			hHandler->hEncoder = hEncoder;
			if
			(
				(rv = hHandler->set_class(hHandler, CKO_PRIVATE_KEY)) == CKR_OK &&
				(rv = hHandler->set_token(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_private(hHandler, CK_TRUE)) == CKR_OK &&
				(rv = hHandler->set_modifiable(hHandler, CK_TRUE)) == CKR_OK &&
				(rv = hHandler->set_copyable(hHandler, CK_TRUE)) == CKR_OK &&
				(rv = hHandler->set_destroyable(hHandler, CK_TRUE)) == CKR_OK &&
				(rv = hHandler->set_keytype(hHandler, CKK_RSA)) == CKR_OK &&
				(rv = hHandler->set_derive(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_local(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_allowedMech(hHandler, __pSignMechanisms, KCKM_SIGN_MECHANISMS * sizeof(CKA_MECHANISM_TYPE))) == CKR_OK &&
				(rv = hHandler->set_sensitive(hHandler, CK_TRUE)) == CKR_OK &&
				(rv = hHandler->set_decrypt(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_sign(hHandler, CK_TRUE)) == CKR_OK &&
				(rv = hHandler->set_signRecover(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_unwrap(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_extractable(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_alwaysSensitive(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_neverExtractable(hHandler, CK_FALSE)) == CKR_OK &&
				(rv = hHandler->set_wrapWithTrusted(hHandler, CK_FALSE)) == CKR_OK
			)	rv = hHandler->set_alwaysAuth(hHandler, CK_FALSE);
		}
	}
	if (rv == CKR_OK) *hOut = hHandler;
	else
	{
		if (hEncoder) NH_release_encoder(hEncoder);
		if (hHandler) free(hHandler);
	}
	return rv;
}
CK_DELETE(CKIO_release_rsaprivkey_encoder)(CK_INOUT KRSAPRIVKEY_ENCODER hHandler)
{
	if (hHandler)
	{
		if (hHandler->hEncoder) NH_release_encoder(hHandler->hEncoder);
		free(hHandler);
	}	
}


/**
 * @brief RSA public key PKCS#11 object parser
 * 
 */
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_class)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __storage_get_class(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_token)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __storage_get_token(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_private)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __storage_get_private(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_modifiable)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __storage_get_modifiable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_label)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __storage_get_label(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_copyable)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __storage_get_copyable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_destroyable)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __storage_get_destroyable(self->hParser);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_keytype)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_keytype(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_id)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_id(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_start_date)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_start_date(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_end_date)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_end_date(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_derive)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_derive(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_local)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_local(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_keyGenMech)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_keyGenMech(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_allowedMech)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __key_get_allowedMech(self->hParser);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_subject)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_subject(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_sensitive)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_sensitive(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_decrypt)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_decrypt(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_sign)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_sign(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_signRecover)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_signRecover(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_unwrap)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_unwrap(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_extractable)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_extractable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_alwaysSensitive)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_alwaysSensitive(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_neverExtractable)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_neverExtractable(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_wrapWithTrusted)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_wrapWithTrusted(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_alwaysAuth)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_alwaysAuth(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_keyinfo)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __privkey_get_pubKeyInfo(self->hParser);
}

CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_salt)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __rsa_privkey_get_salt(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_iv)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __rsa_privkey_get_iv(self->hParser);
}
CK_IMPLEMENTATION(NH_ASN1_PNODE, __rsaprivatekey_get_ciphertext)(CK_IN KRSAPRIVKEY_PARSER_STR *self)
{
	return __rsa_privkey_get_ciphertext(self->hParser);
}

CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_class)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_class(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_token)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_token(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_private)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_private(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_modifiable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_modifiable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_label)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_label(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_copyable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_copyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_destroyable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_match_destroyable(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_keyType)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_keyType(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_keyid)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_keyid(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_start_date)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_start_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_end_date)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_end_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_derive)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_derive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_local)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_local(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_keyGenMech)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_keyGenMech(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekeymatch_allowedMech)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_match_allowedMech(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_subject)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_subject(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_sensitive)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_sensitive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_decrypt)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_decrypt(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_sign)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_sign(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_signRecover)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_signRecover(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_unwrap)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_unwrap(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_extractable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_extractable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_alwaysSensitive)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_alwaysSensitive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_neverExtractable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_neverExtractable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_wrapWithTrusted)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_wrapWithTrusted(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_alwaysAuth)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_alwaysAuth(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match_keyinfo)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_match_keyinfo(self->hParser, pAttribute);;
}
CK_IMPLEMENTATION(CK_BBOOL, __rsaprivatekey_match)
(
	CK_IN KRSAPRIVKEY_PARSER_STR *self,
	CK_IN KRSAKEY_HANDLER hKey,
	CK_IN CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_IN KP_BLOB *pPin
)
{
	CK_ULONG i = 0;
	CK_BBOOL match = CK_TRUE;

	assert(pPin);
	while (match && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
		__MATCH_STORAGE(self, match, &pTemplate[i])
		__MATCH_KEY(self, match, &pTemplate[i])
		case CKA_SUBJECT:
			match = self->match_subject(self, &pTemplate[i]);
			break;
		case CKA_SENSITIVE:
			match = self->match_sensitive(self, &pTemplate[i]);
			break;
		case CKA_DECRYPT:
			match = self->match_decrypt(self, &pTemplate[i]);
			break;
		case CKA_SIGN:
			match = self->match_sign(self, &pTemplate[i]);
			break;
		case CKA_SIGN_RECOVER:
			match = self->match_signRecover(self, &pTemplate[i]);
			break;
		case CKA_UNWRAP:
			match = self->match_unwrap(self, &pTemplate[i]);
			break;
		case CKA_EXTRACTABLE:
			match = self->match_extractable(self, &pTemplate[i]);
			break;
		case CKA_ALWAYS_SENSITIVE:
			match = self->match_alwaysSensitive(self, &pTemplate[i]);
			break;
		case CKA_NEVER_EXTRACTABLE:
			match = self->match_extractable(self, &pTemplate[i]);
			break;
		case CKA_WRAP_WITH_TRUSTED:
			match = self->match_wrapWithTrusted(self, &pTemplate[i]);
			break;
		case CKA_ALWAYS_AUTHENTICATE:
			match = self->match_alwaysAuth(self, &pTemplate[i]);
			break;
		case CKA_PUBLIC_KEY_INFO:
			match = self->match_keyinfo(self, &pTemplate[i]);
			break;
		case CKA_UNWRAP_TEMPLATE: match = CK_FALSE;
			break;

		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
			match = hKey && pPin->data && hKey->match(hKey, &pTemplate[i]);
			break;
		case CKA_PRIVATE_EXPONENT:
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			match = hKey && pPin->data && self->match_sensitive(self, &__isnot_sensitive) && hKey->match(hKey, &pTemplate[i]);
			break;
		default: match = CK_FALSE;
		}
		i++;
	}
	return match;
}

CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_class)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_class(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_token)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_token(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_private)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_private(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_modifiable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_modifiable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_label)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_label(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_copyable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_copyable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_destroyable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __storage_attr_destroyable(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_keyType)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_keyType(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_keyid)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_keyid(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_start_date)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_start_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_end_date)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_end_date(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_derive)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_derive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_local)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_local(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_keyGenMech)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_keyGenMech(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_allowedMech)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_IN CK_ATTRIBUTE_PTR pAttribute)
{
	return __key_attr_allowedMech(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_subject)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_subject(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_sensitive)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_sensitive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_decrypt)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_decrypt(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_sign)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_sign(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_signRecover)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_signRecover(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_unwrap)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_unwrap(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_extractable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_extractable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_alwaysSensitive)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_alwaysSensitive(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_neverExtractable)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_neverExtractable(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_wrapWithTrusted)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_wrapWithTrusted(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_alwaysAuth)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_alwaysAuth(self->hParser, pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivatekey_attr_keyinfo)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __privkey_attr_keyinfo(self->hParser, pAttribute);
}

CK_IMPLEMENTATION(CK_RV, __rsaprivkey_attr_salt)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__rsa_privkey_get_salt(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_attr_iv)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__rsa_privkey_get_iv(self->hParser), pAttribute);
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_attr_ciphertext)(CK_IN KRSAPRIVKEY_PARSER_STR *self, CK_INOUT CK_ATTRIBUTE_PTR pAttribute)
{
	return __attribute_binary(__rsa_privkey_get_ciphertext(self->hParser), pAttribute);
}

CK_INLINE CK_IMPLEMENTATION(CK_RV, __rsaprivkey_parse_material)(CK_INOUT NH_ASN1_PARSER_HANDLE hParser)
{
	NH_RV rv;
	NH_ASN1_PNODE pNode;

	assert(hParser);
	if
	(
		(rv = (pNode = __rsa_privkey_get_salt(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_octetstring(hParser, pNode)) == NH_OK &&
		(rv = (pNode = __rsa_privkey_get_iv(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK &&
		(rv = hParser->parse_octetstring(hParser, pNode)) == NH_OK &&
		(rv = (pNode = __rsa_privkey_get_ciphertext(hParser)) ? NH_OK : NH_UNEXPECTED_ENCODING) == NH_OK
	)	rv = hParser->parse_octetstring(hParser, pNode);
	NHARU_ASSERT(rv, "__rsaprivkey_parse_material");
	return NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED;
}
CK_IMPLEMENTATION(CK_RV, __get_sensitive_material)
(
	CK_IN KRSAPRIVKEY_PARSER_STR *self,
	CK_IN KTOKEN_STR *pToken,
	CK_OUT CK_BYTE_PTR *ppOut,
	CK_OUT CK_ULONG_PTR pulOut
)
{
	CK_RV rv;
	CK_ATTRIBUTE pSaltAttr = { 0UL, NULL, 0UL }, pIV = { 0UL, NULL, 0UL }, pCiphertext = { 0UL, NULL, 0UL };
	CK_BYTE pKey[PBE_KEY_LEN], *pPlainText = NULL;
	int iKey = PBE_KEY_LEN;
	CK_ULONG ulPlainText;
	KP_BLOB pSalt = { NULL, 0UL };

	if
	(
		(rv = self->attr_salt(self, &pSaltAttr)) == CKR_OK &&
		(rv = (pSaltAttr.pValue = malloc(pSaltAttr.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = self->attr_salt(self, &pSaltAttr)) == CKR_OK &&
		(rv = self->attr_iv(self, &pIV)) == CKR_OK &&
		(rv = (pIV.pValue = malloc(pIV.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = self->attr_iv(self, &pIV)) == CKR_OK &&
		(rv = self->attr_ciphertext(self, &pCiphertext)) == CKR_OK &&
		(rv = (pCiphertext.pValue = malloc(pCiphertext.ulValueLen)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = self->attr_ciphertext(self, &pCiphertext)) == CKR_OK
	)
	{
		pSalt.data = (unsigned char*) pSaltAttr.pValue;
		pSalt.length = pSaltAttr.ulValueLen;
		if 
		(
			(rv = pToken->hCrypto->pbe_derive(pToken->hCrypto, &pToken->pin, &pSalt, pKey, iKey)) == CKR_OK &&
			(rv = pToken->hCrypto->decrypt(pToken->hCrypto, pKey, (CK_BYTE_PTR) pIV.pValue, (CK_BYTE_PTR) pCiphertext.pValue, pCiphertext.ulValueLen, &pPlainText, &ulPlainText)) == CKR_OK
		)
		{
			*ppOut = pPlainText;
			*pulOut = ulPlainText;
		}
	}
	OPENSSL_cleanse(pKey, iKey);
	if (pSaltAttr.pValue) free(pSaltAttr.pValue);
	if (pIV.pValue) free(pIV.pValue);
	if (pCiphertext.pValue) free(pCiphertext.pValue);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __rsaprivkey_attributes)
(
	CK_IN KRSAPRIVKEY_PARSER_STR *self,
	CK_IN KRSAKEY_HANDLER hKey,
	CK_INOUT CK_ATTRIBUTE_PTR pTemplate,
	CK_IN CK_ULONG ulCount,
	CK_IN KP_BLOB *pPin
)
{
	CK_RV rv = CKR_OK;
	CK_ULONG i = 0;

	assert(pPin);
	while (rv == CKR_OK && i < ulCount)
	{
		switch (pTemplate[i].type)
		{
	
		__STORAGE_ATTRIBUTES(self, &pTemplate[i], rv)
		__KEY_ATTRIBUTES(self, &pTemplate[i], rv)
		case CKA_SUBJECT:
			rv = self->attr_subject(self, &pTemplate[i]);
			break;
		case CKA_SENSITIVE:
			rv = self->attr_sensitive(self, &pTemplate[i]);
			break;
		case CKA_DECRYPT:
			rv = self->attr_decrypt(self, &pTemplate[i]);
			break;
		case CKA_SIGN:
			rv = self->attr_sign(self, &pTemplate[i]);
			break;
		case CKA_SIGN_RECOVER:
			rv = self->attr_signRecover(self, &pTemplate[i]);
			break;
		case CKA_UNWRAP:
			rv = self->attr_unwrap(self, &pTemplate[i]);
			break;
		case CKA_EXTRACTABLE:
			rv = self->attr_extractable(self, &pTemplate[i]);
			break;
		case CKA_ALWAYS_SENSITIVE:
			rv = self->attr_alwaysSensitive(self, &pTemplate[i]);
			break;
		case CKA_NEVER_EXTRACTABLE:
			rv = self->attr_extractable(self, &pTemplate[i]);
			break;
		case CKA_WRAP_WITH_TRUSTED:
			rv = self->attr_wrapWithTrusted(self, &pTemplate[i]);
			break;
		case CKA_ALWAYS_AUTHENTICATE:
			rv = self->attr_alwaysAuth(self, &pTemplate[i]);
			break;
		case CKA_PUBLIC_KEY_INFO:
			rv = self->attr_keyinfo(self, &pTemplate[i]);
			break;
		case CKA_UNWRAP_TEMPLATE: rv = CKR_ATTRIBUTE_TYPE_INVALID;
			break;

		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
			if ((rv = hKey && pPin->data ? CKR_OK : CKR_ATTRIBUTE_SENSITIVE) == CKR_OK) rv = hKey->attribute(hKey, &pTemplate[i]);
			break;
		case CKA_PRIVATE_EXPONENT:
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			if ((rv = hKey && pPin->data && self->match_extractable(self, &__is_extractable) ? CKR_OK : CKR_ATTRIBUTE_SENSITIVE) == CKR_OK) rv = hKey->attribute(hKey, &pTemplate[i]);
			break;
		}
		i++;
	}
	return rv;
}

static KRSAPRIVKEY_PARSER_STR __default_rsaprivkey_parser =
{
	NULL,

	__rsaprivatekey_get_class,
	__rsaprivatekey_get_token,
	__rsaprivatekey_get_private,
	__rsaprivatekey_get_modifiable,
	__rsaprivatekey_get_label,
	__rsaprivatekey_get_copyable,
	__rsaprivatekey_get_destroyable,

	__rsaprivatekey_get_keytype,
	__rsaprivatekey_get_id,
	__rsaprivatekey_get_start_date,
	__rsaprivatekey_get_end_date,
	__rsaprivatekey_get_derive,
	__rsaprivatekey_get_local,
	__rsaprivatekey_get_keyGenMech,
	__rsaprivatekey_get_allowedMech,

	__rsaprivatekey_get_subject,
	__rsaprivatekey_get_sensitive,
	__rsaprivatekey_get_decrypt,
	__rsaprivatekey_get_sign,
	__rsaprivatekey_get_signRecover,
	__rsaprivatekey_get_unwrap,
	__rsaprivatekey_get_extractable,
	__rsaprivatekey_get_alwaysSensitive,
	__rsaprivatekey_get_neverExtractable,
	__rsaprivatekey_get_wrapWithTrusted,
	__rsaprivatekey_get_alwaysAuth,
	__rsaprivatekey_get_keyinfo,

	__rsaprivatekey_get_salt,
	__rsaprivatekey_get_iv,
	__rsaprivatekey_get_ciphertext,

	__rsaprivatekeymatch_class,
	__rsaprivatekeymatch_token,
	__rsaprivatekeymatch_private,
	__rsaprivatekeymatch_modifiable,
	__rsaprivatekeymatch_label,
	__rsaprivatekeymatch_copyable,
	__rsaprivatekeymatch_destroyable,

	__rsaprivatekeymatch_keyType,
	__rsaprivatekeymatch_keyid,
	__rsaprivatekeymatch_start_date,
	__rsaprivatekeymatch_end_date,
	__rsaprivatekeymatch_derive,
	__rsaprivatekeymatch_local,
	__rsaprivatekeymatch_keyGenMech,
	__rsaprivatekeymatch_allowedMech,

	__rsaprivatekey_match_subject,
	__rsaprivatekey_match_sensitive,
	__rsaprivatekey_match_decrypt,
	__rsaprivatekey_match_sign,
	__rsaprivatekey_match_signRecover,
	__rsaprivatekey_match_unwrap,
	__rsaprivatekey_match_extractable,
	__rsaprivatekey_match_alwaysSensitive,
	__rsaprivatekey_match_neverExtractable,
	__rsaprivatekey_match_wrapWithTrusted,
	__rsaprivatekey_match_alwaysAuth,
	__rsaprivatekey_match_keyinfo,
	__rsaprivatekey_match,

	__rsaprivatekey_attr_class,
	__rsaprivatekey_attr_token,
	__rsaprivatekey_attr_private,
	__rsaprivatekey_attr_modifiable,
	__rsaprivatekey_attr_label,
	__rsaprivatekey_attr_copyable,
	__rsaprivatekey_attr_destroyable,

	__rsaprivatekey_attr_keyType,
	__rsaprivatekey_attr_keyid,
	__rsaprivatekey_attr_start_date,
	__rsaprivatekey_attr_end_date,
	__rsaprivatekey_attr_derive,
	__rsaprivatekey_attr_local,
	__rsaprivatekey_attr_keyGenMech,
	__rsaprivatekey_attr_allowedMech,

	__rsaprivatekey_attr_subject,
	__rsaprivatekey_attr_sensitive,
	__rsaprivatekey_attr_decrypt,
	__rsaprivatekey_attr_sign,
	__rsaprivatekey_attr_signRecover,
	__rsaprivatekey_attr_unwrap,
	__rsaprivatekey_attr_extractable,
	__rsaprivatekey_attr_alwaysSensitive,
	__rsaprivatekey_attr_neverExtractable,
	__rsaprivatekey_attr_wrapWithTrusted,
	__rsaprivatekey_attr_alwaysAuth,
	__rsaprivatekey_attr_keyinfo,

	__rsaprivkey_attr_salt,
	__rsaprivkey_attr_iv,
	__rsaprivkey_attr_ciphertext,

	__get_sensitive_material,
	__rsaprivkey_attributes
};
CK_NEW(CKIO_new_rsaprivkey_parser)(CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulSize, CK_OUT KRSAPRIVKEY_PARSER *hOut)
{
	CK_RV rv;
	KRSAPRIVKEY_PARSER hKey = NULL;
	NH_ASN1_PARSER_HANDLE hParser = NULL;
	if
	(
		(rv = (hKey = (KRSAPRIVKEY_PARSER) malloc(sizeof(KRSAPRIVKEY_PARSER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = NH_SUCCESS(NH_new_parser(pEncoding, ulSize, 64, 4096, &hParser)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		NHARU_ASSERT((rv = hParser->map(hParser, __rsa_privkey_attributes_map, ASN_NODE_WAY_COUNT(__rsa_privkey_attributes_map))), "map");
		if
		(
			(rv = NH_SUCCESS(rv) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK &&
			(rv = __storage_parse(hParser)) == CKR_OK &&
			(rv = __key_attributes_parse(hParser)) == CKR_OK &&
			(rv = __privkey_parse(hParser)) == CKR_OK &&
			(rv = __rsaprivkey_parse_material(hParser)) == CKR_OK
		)
		{
			memcpy(hKey, &__default_rsaprivkey_parser, sizeof(KRSAPRIVKEY_PARSER_STR));
			hKey->hParser = hParser;
		}
	}
	if (rv == CKR_OK) *hOut = hKey;
	else
	{
		if (hParser) NH_release_parser(hParser);
		if (hKey) free(hKey);
	}
	return rv;
}
CK_DELETE(CKIO_release_rsaprivkey_parser)(CK_INOUT KRSAPRIVKEY_PARSER hHandler)
{
	if (hHandler)
	{
		if (hHandler->hParser) NH_release_parser(hHandler->hParser);
		free(hHandler);
	}
}

static NH_NODE_WAY __discover_map[] =
{
	{	/* Any Object */
		NH_PARSE_ROOT,
		NH_ASN1_SEQUENCE,
		NULL,
		0
	},
	{	/* storage */
		NH_SAIL_SKIP_SOUTH,
		NH_ASN1_SEQUENCE | NH_ASN1_HAS_NEXT_BIT,
		__storage_attributes_map,
		ASN_NODE_WAY_COUNT(__storage_attributes_map)
	}
};
CK_FUNCTION(CK_OBJECT_CLASS, CKIO_discover)(CK_IN CK_BYTE_PTR pEncoding, CK_IN CK_ULONG ulSize)
{
	NH_ASN1_PARSER_HANDLE hParser;
	NH_ASN1_PNODE pNode;
	CK_OBJECT_CLASS ulRet = CK_UNAVAILABLE_INFORMATION;
	NH_RV rv;

	NHARU_ASSERT((rv = NH_new_parser(pEncoding, ulSize, 64, 4096, &hParser)), "NH_new_parser");
	if (NH_SUCCESS(rv))
	{
		NHARU_ASSERT((rv = hParser->map(hParser, __discover_map, ASN_NODE_WAY_COUNT(__discover_map))), "map");
		if
		(
			NH_SUCCESS(rv) &&
			__storage_parse(hParser) == NH_OK &&
			(pNode =__storage_get_class(hParser)) &&
			pNode->valuelen == sizeof(CK_OBJECT_CLASS)
		) ulRet = *(CK_OBJECT_CLASS*) pNode->value;
		NH_release_parser(hParser);
	}
	return ulRet;
}
CK_FUNCTION(CK_RV, CKIO_encrypt_material)
(
	CK_IN KRSAPRIVKEY_ENCODER hEncoder,
	CK_IN KRSAKEY_HANDLER hRSAKey,
	CK_IN KTOKEN_STR *pToken
)
{
	CK_RV rv;
	CK_ULONG ulMaterial, ulCipher;
	CK_BYTE *pMaterial, pKey[PBE_KEY_LEN], *pCipherText;
	KP_BLOB pSalt = { NULL, PBE_SALT_LEN }, pIV = { NULL, PBE_IV_LEN };
	int iKey = PBE_KEY_LEN;

	if
	(
		(rv = (pSalt.data = (unsigned char*) malloc(PBE_SALT_LEN)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = (pIV.data = (unsigned char*) malloc(PBE_IV_LEN)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
		(rv = hRSAKey->encode(hRSAKey, NULL, &ulMaterial)) == CKR_OK &&
		(rv = (pMaterial = (CK_BYTE_PTR) malloc(ulMaterial)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK
	)
	{
		if
		(
			(rv = hRSAKey->encode(hRSAKey, pMaterial, &ulMaterial)) == CKR_OK &&
			(rv = pToken->hCrypto->rand(pToken->hCrypto, pSalt.data, (int) pSalt.length)) == CKR_OK &&
			(rv = pToken->hCrypto->rand(pToken->hCrypto, pIV.data, (int) pIV.length)) == CKR_OK &&
			(rv = pToken->hCrypto->pbe_derive(pToken->hCrypto, &pToken->pin, &pSalt, pKey, iKey)) == CKR_OK
		)
		{
			if ((rv = pToken->hCrypto->encrypt(pToken->hCrypto, pKey, pIV.data, pMaterial, ulMaterial, &pCipherText, &ulCipher)) == CKR_OK)
			{
				if
				(
					(rv = hEncoder->set_salt(hEncoder, pSalt.data, pSalt.length)) == CKR_OK &&
					(rv = hEncoder->set_iv(hEncoder, pIV.data, pSalt.length)) == CKR_OK
				)	rv = hEncoder->set_ciphertext(hEncoder, pCipherText, ulCipher);
				free(pCipherText);
			}
			OPENSSL_cleanse(pKey, iKey);
		}
		OPENSSL_cleanse(pMaterial, ulMaterial);
		free(pMaterial);
		if (pSalt.data) free(pSalt.data);
		if (pIV.data) free(pIV.data);
	}
	return rv;
}

#endif
