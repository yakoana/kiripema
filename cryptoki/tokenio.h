/**
 * @file tokenio.h
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief PKCS#11 objects file system implementation
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#ifndef __TOKENIO_H__
#define __TOKENIO_H__

#include "kiripema.h"
#include <openssl/evp.h>

/**
 * @brief X509 Certificate PKCS#11 object encoder
 * 
 */
typedef struct KX509CERT_ENCODER_STR				KX509CERT_ENCODER_STR;
typedef CK_METHOD(NH_ASN1_PNODE, KIO_ECERT_GET_FIELD)		/* Gets certificate attribute node */
(
	CK_IN KX509CERT_ENCODER_STR*					/* self */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_CLASS)			/* Gets certificate attribute node */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_OBJECT_CLASS						/* oClass */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_BOOLEAN)			/* Sets certificate boolean attribute */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_BBOOL							/* isValue */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_UTF8)			/* Sets certificate UTF-8 attribute */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_UTF8CHAR_PTR,						/* pValue */
	CK_IN CK_ULONG							/* ulValue */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_TYPE)			/* Sets certificate type */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_CERTIFICATE_TYPE					/* ulType */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_CATEGORY)			/* Sets certificate category */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_CERTIFICATE_TYPE					/* ulCategory */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_OCTETS)			/* Sets certificate binary attribute */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_BYTE_PTR,						/* pValue */
	CK_IN CK_ULONG							/* ulValue */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_DATE)			/* Sets certificate date attribute */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_DATE							/* date */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_DOMAIN)			/* Sets certificate Java security domain */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_JAVA_MIDP_SECURITY_DOMAIN				/* ulDomain */
);
typedef CK_METHOD(CK_RV, KIO_CERT_SET_MECHANISM)		/* Sets certificate hash algorithm attribute */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_MECHANISM_TYPE						/* ulMechanism */
);
typedef CK_METHOD(CK_RV, KIO_CERT_FROM_TEMPLATE)		/* Sets certificate values from specified template */
(
	CK_IN KX509CERT_ENCODER_STR*,					/* self */
	CK_IN CK_ATTRIBUTE_PTR, 					/* pTemplate */
	CK_IN CK_ULONG							/* ulCount */
);
struct KX509CERT_ENCODER_STR
{
	NH_ASN1_ENCODER_HANDLE		hEncoder;			/* Object encoder */

	KIO_ECERT_GET_FIELD		get_class;			/* CKA_CLASS */
	KIO_ECERT_GET_FIELD		get_token;			/* CKA_TOKEN */
	KIO_ECERT_GET_FIELD		get_private;		/* CKA_PRIVATE */
	KIO_ECERT_GET_FIELD		get_modifiable;		/* CKA_MODIFIABLE */
	KIO_ECERT_GET_FIELD		get_label;			/* CKA_LABEL */
	KIO_ECERT_GET_FIELD		get_copyable;		/* CKA_COPYABLE */
	KIO_ECERT_GET_FIELD		get_destroyable;		/* CKA_DESTROYABLE */
	KIO_ECERT_GET_FIELD		get_type;			/* CKA_CERTIFICATE_TYPE */
	KIO_ECERT_GET_FIELD		get_trusted;		/* CKA_TRUSTED */
	KIO_ECERT_GET_FIELD		get_category;		/* CKA_CERTIFICATE_CATEGORY */
	KIO_ECERT_GET_FIELD		get_check_value;		/* CKA_CHECK_VALUE */
	KIO_ECERT_GET_FIELD		get_start_date;		/* CKA_START_DATE */
	KIO_ECERT_GET_FIELD		get_end_date;		/* CKA_END_DATE */
	KIO_ECERT_GET_FIELD		get_pubkey;			/* CKA_PUBLIC_KEY_INFO */
	KIO_ECERT_GET_FIELD		get_subject;		/* CKA_SUBJECT */
	KIO_ECERT_GET_FIELD		get_id;			/* CKA_ID */
	KIO_ECERT_GET_FIELD		get_issuer;			/* CKA_ISSUER */
	KIO_ECERT_GET_FIELD		get_serial_number;	/* CKA_SERIAL_NUMBER */
	KIO_ECERT_GET_FIELD		get_url;			/* CKA_URL */
	KIO_ECERT_GET_FIELD		get_value;			/* CKA_VALUE */
	KIO_ECERT_GET_FIELD		get_subject_pubkey;	/* CKA_HASH_OF_SUBJECT_PUBLIC_KEY */
	KIO_ECERT_GET_FIELD		get_issuer_pubkey;	/* CKA_HASH_OF_ISSUER_PUBLIC_KEY */
	KIO_ECERT_GET_FIELD		get_java_midp;		/* CKA_JAVA_MIDP_SECURITY_DOMAIN */
	KIO_ECERT_GET_FIELD		get_hash_algorithm;	/* CKA_NAME_HASH_ALGORITHM */

	KIO_CERT_SET_CLASS		set_class;
	KIO_CERT_SET_BOOLEAN		set_token;
	KIO_CERT_SET_BOOLEAN		set_private;
	KIO_CERT_SET_BOOLEAN		set_modifiable;
	KIO_CERT_SET_UTF8			set_label;
	KIO_CERT_SET_BOOLEAN		set_copyable;
	KIO_CERT_SET_BOOLEAN		set_destroyable;
	KIO_CERT_SET_TYPE			set_type;
	KIO_CERT_SET_BOOLEAN		set_trusted;
	KIO_CERT_SET_CATEGORY		set_category;
	KIO_CERT_SET_OCTETS		set_check_value;
	KIO_CERT_SET_DATE			set_start_date;
	KIO_CERT_SET_DATE			set_end_date;
	KIO_CERT_SET_OCTETS		set_pubkey;
	KIO_CERT_SET_OCTETS		set_subject;
	KIO_CERT_SET_OCTETS		set_id;
	KIO_CERT_SET_OCTETS		set_issuer;
	KIO_CERT_SET_OCTETS		set_serial_number;
	KIO_CERT_SET_UTF8			set_url;
	KIO_CERT_SET_OCTETS		set_value;
	KIO_CERT_SET_OCTETS		set_subject_pubkey;
	KIO_CERT_SET_OCTETS		set_issuer_pubkey;
	KIO_CERT_SET_DOMAIN		set_java_midp;
	KIO_CERT_SET_MECHANISM		set_hash_algorithm;

	KIO_CERT_FROM_TEMPLATE		from;
};
typedef KX509CERT_ENCODER_STR*				KX509CERT_ENCODER;
CK_NEW(CKIO_new_certificate_encoder)			(	/* Creates a new certificate encoder */
										CK_OUT KX509CERT_ENCODER*		/* hOut */
									);
CK_DELETE(CKIO_release_certificate_encoder)		(	/* Releases certificate encoder */
										CK_INOUT KX509CERT_ENCODER		/* hCert */
									);


/**
 * @brief X509 Certificate PKCS#11 object parser
 * 
 */
typedef struct KX09CERT_PARSER_STR				KX09CERT_PARSER_STR;
typedef CK_METHOD(NH_ASN1_PNODE, KIO_CERT_GET_FIELD)	(	/* Gets certificate attribute node */
										CK_IN KX09CERT_PARSER_STR*		/* self */
									);
typedef CK_METHOD(CK_BBOOL, KIO_CERT_MATCH_FIELD)	(	/* Matches certificate attribute value */
										CK_IN KX09CERT_PARSER_STR*,		/* self */
										CK_IN CK_ATTRIBUTE_PTR			/* pAttribute */
									);
typedef CK_METHOD(CK_RV, KIO_CERT_GET_ATTR)		(	/* Gets certificate attribute value */
										CK_IN KX09CERT_PARSER_STR*,		/* self */
										CK_INOUT CK_ATTRIBUTE_PTR		/* pAttribute */
									);
typedef CK_METHOD(CK_BBOOL, KIO_CERT_MATCH)		(	/* Checks if this certificate matches template */
										CK_IN KX09CERT_PARSER_STR*,		/* self */
										CK_IN CK_ATTRIBUTE_PTR,			/* pTemplate */
										CK_IN CK_ULONG				/* ulCount */
									);
typedef CK_METHOD(CK_RV, KIO_CERT_GET_TEMPLATE)		(	/* Gets certificate attributes */
										CK_IN KX09CERT_PARSER_STR*,		/* self */
										CK_IN CK_ATTRIBUTE_PTR,			/* pTemplate */
										CK_IN CK_ULONG				/* ulCount */
									);
struct KX09CERT_PARSER_STR
{
	NH_ASN1_PARSER_HANDLE		hParser;			/* Certificate parser */

	KIO_CERT_GET_FIELD		get_class;
	KIO_CERT_GET_FIELD		get_token;
	KIO_CERT_GET_FIELD		get_private;
	KIO_CERT_GET_FIELD		get_modifiable;
	KIO_CERT_GET_FIELD		get_label;
	KIO_CERT_GET_FIELD		get_copyable;
	KIO_CERT_GET_FIELD		get_destroyable;
	KIO_CERT_GET_FIELD		get_type;
	KIO_CERT_GET_FIELD		get_trusted;
	KIO_CERT_GET_FIELD		get_category;
	KIO_CERT_GET_FIELD		get_check_value;
	KIO_CERT_GET_FIELD		get_start_date;
	KIO_CERT_GET_FIELD		get_end_date;
	KIO_CERT_GET_FIELD		get_pubkey;
	KIO_CERT_GET_FIELD		get_subject;
	KIO_CERT_GET_FIELD		get_id;
	KIO_CERT_GET_FIELD		get_issuer;
	KIO_CERT_GET_FIELD		get_serial_number;
	KIO_CERT_GET_FIELD		get_url;
	KIO_CERT_GET_FIELD		get_value;
	KIO_CERT_GET_FIELD		get_subject_pubkey;
	KIO_CERT_GET_FIELD		get_issuer_pubkey;
	KIO_CERT_GET_FIELD		get_java_midp;
	KIO_CERT_GET_FIELD		get_hash_algorithm;

	KIO_CERT_MATCH_FIELD		match_class;
	KIO_CERT_MATCH_FIELD		match_token;
	KIO_CERT_MATCH_FIELD		match_private;
	KIO_CERT_MATCH_FIELD		match_modifiable;
	KIO_CERT_MATCH_FIELD		match_label;
	KIO_CERT_MATCH_FIELD		match_copyable;
	KIO_CERT_MATCH_FIELD		match_destroyable;
	KIO_CERT_MATCH_FIELD		match_type;
	KIO_CERT_MATCH_FIELD		match_trusted;
	KIO_CERT_MATCH_FIELD		match_category;
	KIO_CERT_MATCH_FIELD		match_check_value;
	KIO_CERT_MATCH_FIELD		match_start_date;
	KIO_CERT_MATCH_FIELD		match_end_date;
	KIO_CERT_MATCH_FIELD		match_pubkey;
	KIO_CERT_MATCH_FIELD		match_subject;
	KIO_CERT_MATCH_FIELD		match_id;
	KIO_CERT_MATCH_FIELD		match_issuer;
	KIO_CERT_MATCH_FIELD		match_serial_number;
	KIO_CERT_MATCH_FIELD		match_url;
	KIO_CERT_MATCH_FIELD		match_value;
	KIO_CERT_MATCH_FIELD		match_subject_pubkey;
	KIO_CERT_MATCH_FIELD		match_issuer_pubkey;
	KIO_CERT_MATCH_FIELD		match_java_midp;
	KIO_CERT_MATCH_FIELD		match_hash_algorithm;
	KIO_CERT_MATCH			match;

	KIO_CERT_GET_ATTR			attr_class;
	KIO_CERT_GET_ATTR			attr_token;
	KIO_CERT_GET_ATTR			attr_private;
	KIO_CERT_GET_ATTR			attr_modifiable;
	KIO_CERT_GET_ATTR			attr_label;
	KIO_CERT_GET_ATTR			attr_copyable;
	KIO_CERT_GET_ATTR			attr_destroyable;
	KIO_CERT_GET_ATTR			attr_type;
	KIO_CERT_GET_ATTR			attr_trusted;
	KIO_CERT_GET_ATTR			attr_category;
	KIO_CERT_GET_ATTR			attr_check_value;
	KIO_CERT_GET_ATTR			attr_start_date;
	KIO_CERT_GET_ATTR			attr_end_date;
	KIO_CERT_GET_ATTR			attr_pubkey;
	KIO_CERT_GET_ATTR			attr_subject;
	KIO_CERT_GET_ATTR			attr_id;
	KIO_CERT_GET_ATTR			attr_issuer;
	KIO_CERT_GET_ATTR			attr_serial_number;
	KIO_CERT_GET_ATTR			attr_url;
	KIO_CERT_GET_ATTR			attr_value;
	KIO_CERT_GET_ATTR			attr_subject_pubkey;
	KIO_CERT_GET_ATTR			attr_issuer_pubkey;
	KIO_CERT_GET_ATTR			attr_java_midp;
	KIO_CERT_GET_ATTR			attr_hash_algorithm;

	KIO_CERT_GET_TEMPLATE		attributes;
};
typedef KX09CERT_PARSER_STR*					KX09CERT_PARSER;
CK_NEW(CKIO_new_certificate_parser)				(	/* Creates new certificate parser */
										CK_IN CK_BYTE_PTR,			/* pEncoding */
										CK_IN CK_ULONG,				/* ulEncodingSize */
										CK_OUT KX09CERT_PARSER*			/* hOut */
									);
CK_DELETE(CKIO_release_certificate_parser)		(	/* Releases certificate parser */
										CK_INOUT KX09CERT_PARSER		/* hCert */
									);


/**
 * @brief RSA public key PKCS#11 object encoder
 * 
 */
typedef struct KRSAPUBKEY_ENCODER_STR				KRSAPUBKEY_ENCODER_STR;
typedef CK_METHOD(NH_ASN1_PNODE, KIO_RSAPUB_GET_FIELD)	(CK_IN KRSAPUBKEY_ENCODER_STR*);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_CLASS)			(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_OBJECT_CLASS);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_BOOLEAN)		(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_BBOOL);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_UTF8)			(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_UTF8CHAR_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_TYPE)			(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_KEY_TYPE);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_OCTETS)			(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_BYTE_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_DATE)			(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_DATE);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_MECHANISM)		(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_MECHANISM_TYPE);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_SET_MECHANISM_ARR)	(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_MECHANISM_TYPE_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KIO_RSAPUB_FROM_TEMPLATE)		(CK_IN KRSAPUBKEY_ENCODER_STR*, CK_IN CK_ATTRIBUTE_PTR, CK_IN CK_ULONG);

struct KRSAPUBKEY_ENCODER_STR
{
	NH_ASN1_ENCODER_HANDLE		hEncoder;

	KIO_RSAPUB_GET_FIELD		get_class;
	KIO_RSAPUB_GET_FIELD		get_token;
	KIO_RSAPUB_GET_FIELD		get_private;
	KIO_RSAPUB_GET_FIELD		get_modifiable;
	KIO_RSAPUB_GET_FIELD		get_label;
	KIO_RSAPUB_GET_FIELD		get_copyable;
	KIO_RSAPUB_GET_FIELD		get_destroyable;

	KIO_RSAPUB_GET_FIELD		get_keytype;
	KIO_RSAPUB_GET_FIELD		get_id;
	KIO_RSAPUB_GET_FIELD		get_start_date;
	KIO_RSAPUB_GET_FIELD		get_end_date;
	KIO_RSAPUB_GET_FIELD		get_derive;
	KIO_RSAPUB_GET_FIELD		get_local;
	KIO_RSAPUB_GET_FIELD		get_keyGenMech;
	KIO_RSAPUB_GET_FIELD		get_allowedMech;

	KIO_RSAPUB_GET_FIELD		get_subject;
	KIO_RSAPUB_GET_FIELD		get_encrypt;
	KIO_RSAPUB_GET_FIELD		get_verify;
	KIO_RSAPUB_GET_FIELD		get_verifyRec;
	KIO_RSAPUB_GET_FIELD		get_wrap;
	KIO_RSAPUB_GET_FIELD		get_trusted;
	KIO_RSAPUB_GET_FIELD		get_keyinfo;

	KIO_RSAPUB_GET_FIELD		get_modulus;
	KIO_RSAPUB_GET_FIELD		get_pubexponent;

	KIO_RSAPUB_SET_CLASS		set_class;
	KIO_RSAPUB_SET_BOOLEAN		set_token;
	KIO_RSAPUB_SET_BOOLEAN		set_private;
	KIO_RSAPUB_SET_BOOLEAN		set_modifiable;
	KIO_RSAPUB_SET_UTF8		set_label;
	KIO_RSAPUB_SET_BOOLEAN		set_copyable;
	KIO_RSAPUB_SET_BOOLEAN		set_destroyable;

	KIO_RSAPUB_SET_TYPE		set_keytype;
	KIO_RSAPUB_SET_OCTETS		set_id;
	KIO_RSAPUB_SET_DATE		set_start_date;
	KIO_RSAPUB_SET_DATE		set_end_date;
	KIO_RSAPUB_SET_BOOLEAN		set_derive;
	KIO_RSAPUB_SET_BOOLEAN		set_local;
	KIO_RSAPUB_SET_MECHANISM	set_keyGenMech;
	KIO_RSAPUB_SET_MECHANISM_ARR	set_allowedMech;

	KIO_RSAPUB_SET_OCTETS		set_subject;
	KIO_RSAPUB_SET_BOOLEAN		set_encrypt;
	KIO_RSAPUB_SET_BOOLEAN		set_verify;
	KIO_RSAPUB_SET_BOOLEAN		set_verifyRec;
	KIO_RSAPUB_SET_BOOLEAN		set_wrap;
	KIO_RSAPUB_SET_BOOLEAN		set_trusted;
	KIO_RSAPUB_SET_OCTETS		set_keyinfo;

	KIO_RSAPUB_SET_OCTETS		set_modulus;
	KIO_RSAPUB_SET_OCTETS		set_public_exponent;

	KIO_RSAPUB_FROM_TEMPLATE	from;
};
typedef KRSAPUBKEY_ENCODER_STR*					KRSAPUBKEY_ENCODER;
CK_NEW(CKIO_new_rsapubkey_encoder)					(CK_OUT KRSAPUBKEY_ENCODER*);
CK_DELETE(CKIO_release_rsapubkey_encoder)				(CK_INOUT KRSAPUBKEY_ENCODER);


/**
 * @brief RSA public key PKCS#11 object parser
 * 
 */
typedef struct KRSAPUBKEY_PARSER_STR				KRSAPUBKEY_PARSER_STR;
typedef CK_METHOD(NH_ASN1_PNODE, KIO_RSAPUBK_GET_FIELD)	(CK_IN KRSAPUBKEY_PARSER_STR*);
typedef CK_METHOD(CK_BBOOL, KIO_RSAPUBK_MATCH_FIELD)		(CK_IN KRSAPUBKEY_PARSER_STR*, CK_IN CK_ATTRIBUTE_PTR);
typedef CK_METHOD(CK_RV, KIO_RSAPUBK_GET_ATTR)			(CK_IN KRSAPUBKEY_PARSER_STR*, CK_INOUT CK_ATTRIBUTE_PTR);
typedef CK_METHOD(CK_BBOOL, KIO_RSAPUBK_MATCH)			(CK_IN KRSAPUBKEY_PARSER_STR*, CK_IN CK_ATTRIBUTE_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KIO_RSAPUBK_GET_TEMPLATE)		(CK_IN KRSAPUBKEY_PARSER_STR*, CK_IN CK_ATTRIBUTE_PTR, CK_IN CK_ULONG);
struct KRSAPUBKEY_PARSER_STR
{
	NH_ASN1_PARSER_HANDLE		hParser;

	KIO_RSAPUBK_GET_FIELD		get_class;
	KIO_RSAPUBK_GET_FIELD		get_token;
	KIO_RSAPUBK_GET_FIELD		get_private;
	KIO_RSAPUBK_GET_FIELD		get_modifiable;
	KIO_RSAPUBK_GET_FIELD		get_label;
	KIO_RSAPUBK_GET_FIELD		get_copyable;
	KIO_RSAPUBK_GET_FIELD		get_destroyable;

	KIO_RSAPUBK_GET_FIELD		get_keytype;
	KIO_RSAPUBK_GET_FIELD		get_id;
	KIO_RSAPUBK_GET_FIELD		get_start_date;
	KIO_RSAPUBK_GET_FIELD		get_end_date;
	KIO_RSAPUBK_GET_FIELD		get_derive;
	KIO_RSAPUBK_GET_FIELD		get_local;
	KIO_RSAPUBK_GET_FIELD		get_keyGenMech;
	KIO_RSAPUBK_GET_FIELD		get_allowedMech;

	KIO_RSAPUBK_GET_FIELD		get_subject;
	KIO_RSAPUBK_GET_FIELD		get_encrypt;
	KIO_RSAPUBK_GET_FIELD		get_verify;
	KIO_RSAPUBK_GET_FIELD		get_verifyRec;
	KIO_RSAPUBK_GET_FIELD		get_wrap;
	KIO_RSAPUBK_GET_FIELD		get_trusted;
	KIO_RSAPUBK_GET_FIELD		get_keyinfo;

	KIO_RSAPUBK_GET_FIELD		get_modulus;
	KIO_RSAPUBK_GET_FIELD		get_pubexponent;

	KIO_RSAPUBK_MATCH_FIELD		match_class;
	KIO_RSAPUBK_MATCH_FIELD		match_token;
	KIO_RSAPUBK_MATCH_FIELD		match_private;
	KIO_RSAPUBK_MATCH_FIELD		match_modifiable;
	KIO_RSAPUBK_MATCH_FIELD		match_label;
	KIO_RSAPUBK_MATCH_FIELD		match_copyable;
	KIO_RSAPUBK_MATCH_FIELD		match_destroyable;

	KIO_RSAPUBK_MATCH_FIELD		match_keytype;
	KIO_RSAPUBK_MATCH_FIELD		match_id;
	KIO_RSAPUBK_MATCH_FIELD		match_start_date;
	KIO_RSAPUBK_MATCH_FIELD		match_end_date;
	KIO_RSAPUBK_MATCH_FIELD		match_derive;
	KIO_RSAPUBK_MATCH_FIELD		match_local;
	KIO_RSAPUBK_MATCH_FIELD		match_keyGenMech;
	KIO_RSAPUBK_MATCH_FIELD		match_allowedMech;

	KIO_RSAPUBK_MATCH_FIELD		match_subject;
	KIO_RSAPUBK_MATCH_FIELD		match_encrypt;
	KIO_RSAPUBK_MATCH_FIELD		match_verify;
	KIO_RSAPUBK_MATCH_FIELD		match_verifyRec;
	KIO_RSAPUBK_MATCH_FIELD		match_wrap;
	KIO_RSAPUBK_MATCH_FIELD		match_trusted;
	KIO_RSAPUBK_MATCH_FIELD		match_keyinfo;

	KIO_RSAPUBK_MATCH_FIELD		match_modulus;
	KIO_RSAPUBK_MATCH_FIELD		match_pubexponent;
	KIO_RSAPUBK_MATCH			match;

	KIO_RSAPUBK_GET_ATTR		attr_class;
	KIO_RSAPUBK_GET_ATTR		attr_token;
	KIO_RSAPUBK_GET_ATTR		attr_private;
	KIO_RSAPUBK_GET_ATTR		attr_modifiable;
	KIO_RSAPUBK_GET_ATTR		attr_label;
	KIO_RSAPUBK_GET_ATTR		attr_copyable;
	KIO_RSAPUBK_GET_ATTR		attr_destroyable;

	KIO_RSAPUBK_GET_ATTR		attr_keytype;
	KIO_RSAPUBK_GET_ATTR		attr_id;
	KIO_RSAPUBK_GET_ATTR		attr_start_date;
	KIO_RSAPUBK_GET_ATTR		attr_end_date;
	KIO_RSAPUBK_GET_ATTR		attr_derive;
	KIO_RSAPUBK_GET_ATTR		attr_local;
	KIO_RSAPUBK_GET_ATTR		attr_keyGenMech;
	KIO_RSAPUBK_GET_ATTR		attr_allowedMech;

	KIO_RSAPUBK_GET_ATTR		attr_subject;
	KIO_RSAPUBK_GET_ATTR		attr_encrypt;
	KIO_RSAPUBK_GET_ATTR		attr_verify;
	KIO_RSAPUBK_GET_ATTR		attr_verifyRec;
	KIO_RSAPUBK_GET_ATTR		attr_wrap;
	KIO_RSAPUBK_GET_ATTR		attr_trusted;
	KIO_RSAPUBK_GET_ATTR		attr_keyinfo;

	KIO_RSAPUBK_GET_ATTR		attr_modulus;
	KIO_RSAPUBK_GET_ATTR		attr_pubexponent;
	KIO_RSAPUBK_GET_TEMPLATE	attributes;
};
typedef KRSAPUBKEY_PARSER_STR*				KRSAPUBKEY_PARSER;
CK_NEW(CKIO_new_rsapubkey_parser)				(CK_IN CK_BYTE_PTR, CK_IN CK_ULONG, CK_OUT KRSAPUBKEY_PARSER*);
CK_DELETE(CKIO_release_rsapubkey_parser)			(CK_INOUT KRSAPUBKEY_PARSER);


/**
 * @brief RSA private (and public) key PKCS#11 object encoder
 * 
 */
typedef struct KRSAKEY_HANDLER_STR					KRSAKEY_HANDLER_STR;
typedef CK_METHOD(CK_RV, KRSA_SET_OCTETS)				(CK_INOUT KRSAKEY_HANDLER_STR*, CK_IN CK_BYTE_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KRSA_GET_OCTETS)				(CK_IN KRSAKEY_HANDLER_STR*, CK_INOUT NH_BIG_INTEGER*);
typedef CK_METHOD(CK_RV, KRSA_ENCODE)				(CK_IN KRSAKEY_HANDLER_STR*, CK_OUT CK_BYTE_PTR, CK_INOUT CK_ULONG_PTR);
typedef CK_METHOD(CK_BBOOL, KRSA_MATCH)				(CK_IN KRSAKEY_HANDLER_STR*, CK_IN CK_ATTRIBUTE_PTR);
typedef CK_METHOD(CK_RV, KRSA_GET_ATTRIBUTE)			(CK_IN KRSAKEY_HANDLER_STR*, CK_INOUT CK_ATTRIBUTE_PTR);
struct  KRSAKEY_HANDLER_STR
{
	NH_ASN1_ENCODER_HANDLE		hEncoder;
	KRSA_SET_OCTETS			set_modulus;
	KRSA_SET_OCTETS			set_publicExponent;
	KRSA_SET_OCTETS			set_privateExponent;
	KRSA_SET_OCTETS			set_prime1;
	KRSA_SET_OCTETS			set_prime2;
	KRSA_SET_OCTETS			set_exponent1;
	KRSA_SET_OCTETS			set_exponent2;
	KRSA_SET_OCTETS			set_coefficient;
	KRSA_ENCODE				encode;

	NH_ASN1_PARSER_HANDLE		hParser;	
	KRSA_GET_OCTETS			get_modulus;
	KRSA_GET_OCTETS			get_publicExponent;
	KRSA_GET_OCTETS			get_privateExponent;
	KRSA_GET_OCTETS			get_prime1;
	KRSA_GET_OCTETS			get_prime2;
	KRSA_GET_OCTETS			get_exponent1;
	KRSA_GET_OCTETS			get_exponent2;
	KRSA_GET_OCTETS			get_coefficient;
	KRSA_SET_OCTETS			parse;

	KRSA_MATCH				match;
	KRSA_GET_ATTRIBUTE		attribute;
};
typedef KRSAKEY_HANDLER_STR*						KRSAKEY_HANDLER;
CK_NEW(CKIO_new_rsa_handler)						(CK_OUT KRSAKEY_HANDLER*);
CK_DELETE(CKIO_release_rsa_handler)					(CK_INOUT KRSAKEY_HANDLER);
#define CKR_KP_ATTRIBUTE_UNAVAILABLE				(CKR_VENDOR_DEFINED + 1)	/* RSA field not available to retrieve */


typedef struct KRSAPRIVKEY_ENCODER_STR				KRSAPRIVKEY_ENCODER_STR;
typedef CK_METHOD(NH_ASN1_PNODE, KIO_RSAPRIV_GET_FIELD)	(CK_IN KRSAPRIVKEY_ENCODER_STR*);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_CLASS)			(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_OBJECT_CLASS);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_BOOLEAN)		(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_BBOOL);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_UTF8)			(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_UTF8CHAR_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_TYPE)			(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_KEY_TYPE);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_OCTETS)		(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_BYTE_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_DATE)			(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_DATE);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_MECHANISM)		(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_MECHANISM_TYPE);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_SET_MECHANISM_ARR)	(CK_IN KRSAPRIVKEY_ENCODER_STR*, CK_IN CK_MECHANISM_TYPE_PTR, CK_IN CK_ULONG);
typedef CK_METHOD(CK_RV, KIO_RSAPRIV_FROM_TEMPLATE)
(
	CK_IN KRSAPRIVKEY_ENCODER_STR*,				/* self */
	CK_IN KTOKEN_STR*,						/* pToken */
	CK_IN CK_ATTRIBUTE_PTR,						/* pTemplate */
	CK_IN CK_ULONG							/* ulCount */
);
struct KRSAPRIVKEY_ENCODER_STR
{
	NH_ASN1_ENCODER_HANDLE		hEncoder;

	KIO_RSAPRIV_GET_FIELD		get_class;
	KIO_RSAPRIV_GET_FIELD		get_token;
	KIO_RSAPRIV_GET_FIELD		get_private;
	KIO_RSAPRIV_GET_FIELD		get_modifiable;
	KIO_RSAPRIV_GET_FIELD		get_label;
	KIO_RSAPRIV_GET_FIELD		get_copyable;
	KIO_RSAPRIV_GET_FIELD		get_destroyable;

	KIO_RSAPRIV_GET_FIELD		get_keytype;
	KIO_RSAPRIV_GET_FIELD		get_id;
	KIO_RSAPRIV_GET_FIELD		get_start_date;
	KIO_RSAPRIV_GET_FIELD		get_end_date;
	KIO_RSAPRIV_GET_FIELD		get_derive;
	KIO_RSAPRIV_GET_FIELD		get_local;
	KIO_RSAPRIV_GET_FIELD		get_keyGenMech;
	KIO_RSAPRIV_GET_FIELD		get_allowedMech;

	KIO_RSAPRIV_GET_FIELD		get_subject;
	KIO_RSAPRIV_GET_FIELD		get_sensitive;
	KIO_RSAPRIV_GET_FIELD		get_decrypt;
	KIO_RSAPRIV_GET_FIELD		get_sign;
	KIO_RSAPRIV_GET_FIELD		get_signRecover;
	KIO_RSAPRIV_GET_FIELD		get_unwrap;
	KIO_RSAPRIV_GET_FIELD		get_extractable;
	KIO_RSAPRIV_GET_FIELD		get_alwaysSensitive;
	KIO_RSAPRIV_GET_FIELD		get_neverExtractable;
	KIO_RSAPRIV_GET_FIELD		get_wrapWithTrusted;
	KIO_RSAPRIV_GET_FIELD		get_alwaysAuth;
	KIO_RSAPRIV_GET_FIELD		get_keyinfo;

	KIO_RSAPRIV_GET_FIELD		get_salt;
	KIO_RSAPRIV_GET_FIELD		get_iv;
	KIO_RSAPRIV_GET_FIELD		get_ciphertext;

	KIO_RSAPRIV_SET_CLASS		set_class;
	KIO_RSAPRIV_SET_BOOLEAN		set_token;
	KIO_RSAPRIV_SET_BOOLEAN		set_private;
	KIO_RSAPRIV_SET_BOOLEAN		set_modifiable;
	KIO_RSAPRIV_SET_UTF8		set_label;
	KIO_RSAPRIV_SET_BOOLEAN		set_copyable;
	KIO_RSAPRIV_SET_BOOLEAN		set_destroyable;

	KIO_RSAPRIV_SET_TYPE		set_keytype;
	KIO_RSAPRIV_SET_OCTETS		set_id;
	KIO_RSAPRIV_SET_DATE		set_start_date;
	KIO_RSAPRIV_SET_DATE		set_end_date;
	KIO_RSAPRIV_SET_BOOLEAN		set_derive;
	KIO_RSAPRIV_SET_BOOLEAN		set_local;
	KIO_RSAPRIV_SET_MECHANISM	set_keyGenMech;
	KIO_RSAPRIV_SET_MECHANISM_ARR	set_allowedMech;

	KIO_RSAPRIV_SET_OCTETS		set_subject;
	KIO_RSAPRIV_SET_BOOLEAN		set_sensitive;
	KIO_RSAPRIV_SET_BOOLEAN		set_decrypt;
	KIO_RSAPRIV_SET_BOOLEAN		set_sign;
	KIO_RSAPRIV_SET_BOOLEAN		set_signRecover;
	KIO_RSAPRIV_SET_BOOLEAN		set_unwrap;
	KIO_RSAPRIV_SET_BOOLEAN		set_extractable;
	KIO_RSAPRIV_SET_BOOLEAN		set_alwaysSensitive;
	KIO_RSAPRIV_SET_BOOLEAN		set_neverExtractable;
	KIO_RSAPRIV_SET_BOOLEAN		set_wrapWithTrusted;
	KIO_RSAPRIV_SET_BOOLEAN		set_alwaysAuth;
	KIO_RSAPRIV_SET_OCTETS		set_keyinfo;

	KIO_RSAPRIV_SET_OCTETS		set_salt;
	KIO_RSAPRIV_SET_OCTETS		set_iv;
	KIO_RSAPRIV_SET_OCTETS		set_ciphertext;

	KIO_RSAPRIV_FROM_TEMPLATE	from;
};
typedef KRSAPRIVKEY_ENCODER_STR*					KRSAPRIVKEY_ENCODER;
CK_NEW(CKIO_new_rsaprivkey_encoder)					(CK_OUT KRSAPRIVKEY_ENCODER*);
CK_DELETE(CKIO_release_rsaprivkey_encoder)			(CK_INOUT KRSAPRIVKEY_ENCODER);

/**
 * @brief RSA private key PKCS#11 object parser
 * 
 */
typedef struct KRSAPRIVKEY_PARSER_STR				KRSAPRIVKEY_PARSER_STR;
typedef CK_METHOD(NH_ASN1_PNODE, KIO_RSAPRIVK_GET_FIELD)	(CK_IN KRSAPRIVKEY_PARSER_STR*);
typedef CK_METHOD(CK_BBOOL, KIO_RSAPRIVK_MATCH_FIELD)		(CK_IN KRSAPRIVKEY_PARSER_STR*, CK_IN CK_ATTRIBUTE_PTR);
typedef CK_METHOD(CK_RV, KIO_RSAPRIVK_GET_ATTR)			(CK_IN KRSAPRIVKEY_PARSER_STR*, CK_INOUT CK_ATTRIBUTE_PTR);
typedef CK_METHOD(CK_RV, KIO_RSAPRIVK_GET_MATERIAL)
(
	CK_IN KRSAPRIVKEY_PARSER_STR*,				/* self */
	CK_IN KTOKEN_STR*,						/* pToken */
	CK_OUT CK_BYTE_PTR*,						/* ppOut */
	CK_OUT CK_ULONG_PTR						/* pulOut */
);
typedef CK_METHOD(CK_BBOOL, KIO_RSAPRIVK_MATCH)
(
	CK_IN KRSAPRIVKEY_PARSER_STR*,
	CK_IN KRSAKEY_HANDLER,
	CK_IN CK_ATTRIBUTE_PTR,
	CK_IN CK_ULONG,
	CK_IN KP_BLOB*
);
typedef CK_METHOD(CK_RV, KIO_RSAPRIVK_GET_TEMPLATE)
(
	CK_IN KRSAPRIVKEY_PARSER_STR*,
	CK_IN KRSAKEY_HANDLER,
	CK_INOUT CK_ATTRIBUTE_PTR,
	CK_IN CK_ULONG,
	CK_IN KP_BLOB*
);
struct KRSAPRIVKEY_PARSER_STR
{
	NH_ASN1_PARSER_HANDLE		hParser;

	KIO_RSAPRIVK_GET_FIELD		get_class;
	KIO_RSAPRIVK_GET_FIELD		get_token;
	KIO_RSAPRIVK_GET_FIELD		get_private;
	KIO_RSAPRIVK_GET_FIELD		get_modifiable;
	KIO_RSAPRIVK_GET_FIELD		get_label;
	KIO_RSAPRIVK_GET_FIELD		get_copyable;
	KIO_RSAPRIVK_GET_FIELD		get_destroyable;

	KIO_RSAPRIVK_GET_FIELD		get_keytype;
	KIO_RSAPRIVK_GET_FIELD		get_id;
	KIO_RSAPRIVK_GET_FIELD		get_start_date;
	KIO_RSAPRIVK_GET_FIELD		get_end_date;
	KIO_RSAPRIVK_GET_FIELD		get_derive;
	KIO_RSAPRIVK_GET_FIELD		get_local;
	KIO_RSAPRIVK_GET_FIELD		get_keyGenMech;
	KIO_RSAPRIVK_GET_FIELD		get_allowedMech;

	KIO_RSAPRIVK_GET_FIELD		get_subject;
	KIO_RSAPRIVK_GET_FIELD		get_sensitive;
	KIO_RSAPRIVK_GET_FIELD		get_decrypt;
	KIO_RSAPRIVK_GET_FIELD		get_sign;
	KIO_RSAPRIVK_GET_FIELD		get_signRecover;
	KIO_RSAPRIVK_GET_FIELD		get_unwrap;
	KIO_RSAPRIVK_GET_FIELD		get_extractable;
	KIO_RSAPRIVK_GET_FIELD		get_alwaysSensitive;
	KIO_RSAPRIVK_GET_FIELD		get_neverExtractable;
	KIO_RSAPRIVK_GET_FIELD		get_wrapWithTrusted;
	KIO_RSAPRIVK_GET_FIELD		get_alwaysAuth;
	KIO_RSAPRIVK_GET_FIELD		get_keyinfo;

	KIO_RSAPRIVK_GET_FIELD		get_salt;
	KIO_RSAPRIVK_GET_FIELD		get_iv;
	KIO_RSAPRIVK_GET_FIELD		get_ciphertext;

	KIO_RSAPRIVK_MATCH_FIELD	match_class;
	KIO_RSAPRIVK_MATCH_FIELD	match_token;
	KIO_RSAPRIVK_MATCH_FIELD	match_private;
	KIO_RSAPRIVK_MATCH_FIELD	match_modifiable;
	KIO_RSAPRIVK_MATCH_FIELD	match_label;
	KIO_RSAPRIVK_MATCH_FIELD	match_copyable;
	KIO_RSAPRIVK_MATCH_FIELD	match_destroyable;

	KIO_RSAPRIVK_MATCH_FIELD	match_keytype;
	KIO_RSAPRIVK_MATCH_FIELD	match_id;
	KIO_RSAPRIVK_MATCH_FIELD	match_start_date;
	KIO_RSAPRIVK_MATCH_FIELD	match_end_date;
	KIO_RSAPRIVK_MATCH_FIELD	match_derive;
	KIO_RSAPRIVK_MATCH_FIELD	match_local;
	KIO_RSAPRIVK_MATCH_FIELD	match_keyGenMech;
	KIO_RSAPRIVK_MATCH_FIELD	match_allowedMech;

	KIO_RSAPRIVK_MATCH_FIELD	match_subject;
	KIO_RSAPRIVK_MATCH_FIELD	match_sensitive;
	KIO_RSAPRIVK_MATCH_FIELD	match_decrypt;
	KIO_RSAPRIVK_MATCH_FIELD	match_sign;
	KIO_RSAPRIVK_MATCH_FIELD	match_signRecover;
	KIO_RSAPRIVK_MATCH_FIELD	match_unwrap;
	KIO_RSAPRIVK_MATCH_FIELD	match_extractable;
	KIO_RSAPRIVK_MATCH_FIELD	match_alwaysSensitive;
	KIO_RSAPRIVK_MATCH_FIELD	match_neverExtractable;
	KIO_RSAPRIVK_MATCH_FIELD	match_wrapWithTrusted;
	KIO_RSAPRIVK_MATCH_FIELD	match_alwaysAuth;
	KIO_RSAPRIVK_MATCH_FIELD	match_keyinfo;
	KIO_RSAPRIVK_MATCH		match;

	KIO_RSAPRIVK_GET_ATTR		attr_class;
	KIO_RSAPRIVK_GET_ATTR		attr_token;
	KIO_RSAPRIVK_GET_ATTR		attr_private;
	KIO_RSAPRIVK_GET_ATTR		attr_modifiable;
	KIO_RSAPRIVK_GET_ATTR		attr_label;
	KIO_RSAPRIVK_GET_ATTR		attr_copyable;
	KIO_RSAPRIVK_GET_ATTR		attr_destroyable;

	KIO_RSAPRIVK_GET_ATTR		attr_keytype;
	KIO_RSAPRIVK_GET_ATTR		attr_id;
	KIO_RSAPRIVK_GET_ATTR		attr_start_date;
	KIO_RSAPRIVK_GET_ATTR		attr_end_date;
	KIO_RSAPRIVK_GET_ATTR		attr_derive;
	KIO_RSAPRIVK_GET_ATTR		attr_local;
	KIO_RSAPRIVK_GET_ATTR		attr_keyGenMech;
	KIO_RSAPRIVK_GET_ATTR		attr_allowedMech;

	KIO_RSAPRIVK_GET_ATTR		attr_subject;
	KIO_RSAPRIVK_GET_ATTR		attr_sensitive;
	KIO_RSAPRIVK_GET_ATTR		attr_decrypt;
	KIO_RSAPRIVK_GET_ATTR		attr_sign;
	KIO_RSAPRIVK_GET_ATTR		attr_signRecover;
	KIO_RSAPRIVK_GET_ATTR		attr_unwrap;
	KIO_RSAPRIVK_GET_ATTR		attr_extractable;
	KIO_RSAPRIVK_GET_ATTR		attr_alwaysSensitive;
	KIO_RSAPRIVK_GET_ATTR		attr_neverExtractable;
	KIO_RSAPRIVK_GET_ATTR		attr_wrapWithTrusted;
	KIO_RSAPRIVK_GET_ATTR		attr_alwaysAuth;
	KIO_RSAPRIVK_GET_ATTR		attr_keyinfo;

	KIO_RSAPRIVK_GET_ATTR		attr_salt;
	KIO_RSAPRIVK_GET_ATTR		attr_iv;
	KIO_RSAPRIVK_GET_ATTR		attr_ciphertext;

	KIO_RSAPRIVK_GET_MATERIAL	get_sensitive_material;
	KIO_RSAPRIVK_GET_TEMPLATE	attributes;
};
typedef KRSAPRIVKEY_PARSER_STR*				KRSAPRIVKEY_PARSER;
CK_NEW(CKIO_new_rsaprivkey_parser)				(CK_IN CK_BYTE_PTR, CK_IN CK_ULONG, CK_OUT KRSAPRIVKEY_PARSER*);
CK_DELETE(CKIO_release_rsaprivkey_parser)			(CK_INOUT KRSAPRIVKEY_PARSER);

CK_FUNCTION(CK_OBJECT_CLASS, CKIO_discover)		(	/* Discover a class of a persistent object */
										CK_IN CK_BYTE_PTR,	/* pEncoding */
										CK_IN CK_ULONG		/* ulSize */
									);
CK_FUNCTION(CK_RV, CKIO_encrypt_material)				/* Encrypt private key sensitive material */
(
	CK_IN KRSAPRIVKEY_ENCODER,					/* hEncoder */
	CK_IN KRSAKEY_HANDLER,						/* hRSAKey */
	CK_IN KTOKEN_STR*							/* pToken */
);


#endif	/* __TOKENIO_H__ */