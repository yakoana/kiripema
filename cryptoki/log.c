/**
 * @file log.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Log implementation
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */

#include "kiripema.h"
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

#ifdef _WINDOWS
typedef struct _stat		STAT;
typedef __time64_t		TIME64_T;
#define __STAT			_stat
#else
typedef struct stat		STAT;
typedef __time_t			TIME64_T;
#define __STAT			stat
#endif
#define EVENT_LOG_SOURCE	"%skiripema-%i.log"
#define LOG_SOURCE_MAX_SIZE	0x01000000
#define MAX__TIME64_T		0x0793406fffL
#define LOG_BUFFER_LENGTH	0x1000


static CK_RV __open_log(CK_INOUT KLOG_HANDLER_STR *self)
{
	CK_RV rv = CKR_OK;
	char szFileName[CK_FILENAME_MAX], *szHome;
	const char *szMode;
	STAT buf;
	int i = 0, oldI = 0;
	TIME64_T oldT = MAX__TIME64_T;
	FILE *fLog;

	szHome = self->hFS->get_folder(self->hFS, KSF_LOG);
	assert(szHome);
	if (self->fHandle) rv = CKR_ARGUMENTS_BAD;
	while (rv == CKR_OK && i < 3)
	{
		snprintf(szFileName, CK_FILENAME_MAX, EVENT_LOG_SOURCE, szHome, i);
		if (__STAT(szFileName, &buf))
		{
			if (errno == ENOENT) break;
			else rv = CKR_GENERAL_ERROR;
		}
		else
		{
			if (buf.st_size < LOG_SOURCE_MAX_SIZE) break;
			else
			{
				if (buf.st_mtime < oldT)
				{
					oldT = buf.st_mtime;
					oldI = i;
				}
			}
		}
		i++;
	}
	if (rv == CKR_OK)
	{
		if (i < 3) szMode = "a+";
		else
		{
			i = oldI;
			szMode = "w";
		}
		snprintf(szFileName, CK_FILENAME_MAX, EVENT_LOG_SOURCE, "", i);
		if ((rv = self->hFS->open(self->hFS, KSF_LOG, szFileName, szMode, (CK_VOID_PTR) &fLog)) == CKR_OK) self->fHandle = fLog;
	}
	return rv;
}
static void __terminate(CK_INOUT KLOG_HANDLER_STR *self)
{
	if (self->hFS && self->fHandle) self->hFS->close(self->hFS, self->fHandle);
}

CK_IMPLEMENTATION(CK_RV, __init_mutex)(CK_INOUT KLOG_HANDLER_STR *self, CK_IN KMUTEX_HANDLER hToClone)
{
	CK_RV rv = CKR_OK;
	if (!self->hMutex && (rv = hToClone->clone(hToClone, &self->hMutex)) == CKR_OK) rv = self->hMutex->CreateMutex(&self->pMutex);
	return rv;
}
CK_IMPLEMENTATION(void, __log)(CK_IN KLOG_HANDLER_STR *self, CK_IN char* szMsg, ...)
{
	va_list argp;
	char szBuffer[LOG_BUFFER_LENGTH], szLine[LOG_BUFFER_LENGTH];
	time_t current;
	char szTime[64];
	struct tm *p;

	if (self->hFS && self->pMutex && self->hMutex)
	{
		assert(szMsg && self->fHandle && self->hFS);
		va_start(argp, szMsg);
		vsnprintf(szBuffer, LOG_BUFFER_LENGTH, szMsg, argp);
		va_end(argp);
		current = time(NULL);
		p = localtime(&current);
		strftime(szTime, 64, "%c", p);
		snprintf(szLine, LOG_BUFFER_LENGTH, "%s - %s\n", szTime, szBuffer);
		if (self->hMutex->LockMutex(self->pMutex) == CKR_OK)
		{
			self->hFS->write(self->hFS, self->fHandle, (CK_BYTE_PTR) szLine, strlen(szLine));
			self->hMutex->UnlockMutex(self->pMutex);
		}
	}
}
CK_IMPLEMENTATION(void, __log_template)(CK_IN KLOG_HANDLER_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_IN char *szMsg, CK_IN CK_BBOOL printValue)
{
	CK_ULONG i;
	char ulBuffer[11], *szBuffer;
	CK_ULONG ulValue;

	self->log(self, szMsg);
	for (i = 0; i < ulCount; i++)
	{
		self->log(self, "Attribute type:  0x%08x", pTemplate[i].type);
		if (printValue && pTemplate[i].pValue)
		{
			if (pTemplate[i].ulValueLen == sizeof(CK_ULONG))
			{
				ulValue = *((CK_ULONG*) pTemplate[i].pValue);
				sprintf(ulBuffer, "0x%08x", (unsigned int) ulValue);
				szBuffer = ulBuffer;
			}
			else szBuffer = CK_bin2hex((unsigned char*) pTemplate[i].pValue, pTemplate[i].ulValueLen);
			self->log(self, "Attribute value: %s", szBuffer);
			if (pTemplate[i].ulValueLen != sizeof(CK_ULONG)) free(szBuffer);
		}
	}
}
CK_IMPLEMENTATION(void, __log_handles)(CK_IN KLOG_HANDLER_STR *self, CK_IN char *szMsg, CK_IN khash_t(KOSET) *kSet)
{
	khiter_t k;
	self->log(self, szMsg);
	for (k = kh_begin(kSet); k != kh_end(kSet); ++k) if (kh_exist(kSet, k)) self->log(self, "Object handle: %lu", kh_val(kSet, k));
}
CK_IMPLEMENTATION(void, __log_objects)(CK_IN KLOG_HANDLER_STR *self, CK_IN char *szMsg, CK_IN CK_OBJECT_HANDLE_PTR phObject, CK_IN CK_ULONG ulObjectCount)
{
	CK_ULONG i;
	self->log(self, szMsg);
	for (i = 0; i < ulObjectCount; i++) self->log(self, "Object handle: %lu", phObject[i]);
}
static KLOG_HANDLER_STR __default_log =
{
	NULL,
	NULL,
	NULL,
	NULL,

	__init_mutex,
	__log,
	__log_template,
	__log_handles,
	__log_objects,
};
CK_NEW(CK_new_log)(CK_IN CK_BBOOL isOn, CK_OUT KLOG_HANDLER *hHandler)
{
	CK_RV rv;
	KLOG_HANDLER pLog = NULL;

	if ((rv = (pLog = (KLOG_HANDLER) malloc(sizeof(KLOG_HANDLER_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(pLog, &__default_log, sizeof(KLOG_HANDLER_STR));
		if (isOn && (rv = CKO_new_fs(&pLog->hFS)) == CKR_OK) rv = __open_log(pLog);
		*hHandler = pLog;
	}
	if (rv != CKR_OK) CK_delete_log(pLog);
	return rv;
}
CK_DELETE(CK_delete_log)(CK_INOUT KLOG_HANDLER hHandler)
{
	if (hHandler)
	{
		__terminate(hHandler);
		CKO_release_fs(hHandler->hFS);
		if (hHandler->hMutex)
		{
			hHandler->hMutex->DestroyMutex(hHandler->pMutex);
			CK_release_mutex_handler(hHandler->hMutex);
		}
		free(hHandler);
	}
}

