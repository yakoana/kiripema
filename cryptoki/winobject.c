/**
 * @file winobject.c
 * @author Marco Gutierrez (yorick.flannagan@gmail.com)
 * @brief Windows Cryptographic Services PKCS#11 implementation 
 * 
 * @copyright Copyleft (c) 2019 by The Crypthing Initiative
 * @see  https://bitbucket.org/yakoana/kiripema.git
 * 
 *                     *  *  *
 * This software is distributed under GNU General Public License 3.0
 * https://www.gnu.org/licenses/gpl-3.0.html
 * 
 *                     *  *  *
 */
#include "winob.h"

#ifdef _WINDOWS
#include <assert.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief General match functions
 *
 */
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_bool)(CK_IN CK_ATTRIBUTE_PTR pTarget, CK_IN CK_BBOOL bDefault)
{
	assert(pTarget);
	return (pTarget->ulValueLen == sizeof(CK_BBOOL) && *(CK_BBOOL*) pTarget->pValue == bDefault);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_binary)(CK_IN unsigned char *pValue, CK_IN CK_ULONG ulValueLen, CK_IN CK_ATTRIBUTE_PTR pTarget)
{
	assert(pTarget && pValue);
	return (ulValueLen == pTarget->ulValueLen && memcmp(pValue, pTarget->pValue, ulValueLen) == 0);
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_ulong)(CK_IN CK_ATTRIBUTE_PTR pTarget, CK_IN CK_ULONG bDefault)
{
	assert(pTarget);
	return (pTarget->ulValueLen == sizeof(CK_ULONG) && *(CK_ULONG*) pTarget->pValue == bDefault);
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __attribute_bool)(CK_INOUT CK_ATTRIBUTE_PTR pAttribute, CK_IN CK_BBOOL bValue)
{
	assert(pAttribute);
	if (!pAttribute->pValue) pAttribute->ulValueLen = sizeof(CK_BBOOL);
	else
	{
		if (pAttribute->ulValueLen >= sizeof(CK_BBOOL)) { *(CK_BBOOL*) pAttribute->pValue = bValue; pAttribute->ulValueLen = sizeof(CK_BBOOL); }
		else { pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION; return CKR_BUFFER_TOO_SMALL; }
	}
	return CKR_OK;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __attribute_ulong)(CK_INOUT CK_ATTRIBUTE_PTR pAttribute, CK_IN CK_ULONG ulValue)
{
	assert(pAttribute);
	if (!pAttribute->pValue) pAttribute->ulValueLen = sizeof(CK_ULONG);
	else
	{
		if (pAttribute->ulValueLen >= sizeof(CK_ULONG)) { *(CK_ULONG*) pAttribute->pValue = ulValue; pAttribute->ulValueLen = sizeof(CK_ULONG); }
		else { pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION; return CKR_BUFFER_TOO_SMALL; }
	}
	return CKR_OK;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __attribute_binary)(CK_INOUT CK_ATTRIBUTE_PTR pAttribute, CK_IN void *pValue, CK_IN CK_ULONG ulValueLen)
{
	assert(pAttribute && pValue);
	if (!pAttribute->pValue) pAttribute->ulValueLen = ulValueLen;
	else
	{
		if (pAttribute->ulValueLen >= ulValueLen) { memcpy(pAttribute->pValue, pValue, ulValueLen); pAttribute->ulValueLen = ulValueLen; }
		else { pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION; return CKR_BUFFER_TOO_SMALL; }
	}
	return CKR_OK;
}


CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_date)(CK_IN PFILETIME pSource, CK_IN CK_ATTRIBUTE_PTR pTarget)
{
	CK_BBOOL bRet = CK_FALSE;
	SYSTEMTIME pTime;
	CK_DATE *pDate;

	assert(pTarget && pSource);
	pDate = (CK_DATE*) pTarget->pValue;
	if
	(
		pTarget->ulValueLen == sizeof(CK_DATE) &&
		FileTimeToSystemTime(pSource, &pTime)
	)	bRet = (pTime.wYear + 1600 == atoi(pDate->year) && pTime.wMonth == atoi(pDate->month) && pTime.wDay == atoi(pDate->day));
	return bRet;
}

CK_IMPLEMENTATION(BOOL, __get_friedly_name)(CK_IN PCCERT_CONTEXT pCtx, CK_IN DWORD dwFlags, CK_OUT LPSTR *pszName)
{
	CK_BBOOL bRet = CK_FALSE;
	LPSTR pszNameString;
	DWORD cchNameString = CertGetNameStringA(pCtx, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, NULL, 0);
	if (cchNameString > 1 && (pszNameString = (LPSTR) malloc(cchNameString * sizeof(CHAR))))
	{
		CertGetNameStringA(pCtx, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszNameString, cchNameString);
		*pszName = pszNameString;
		bRet = CK_TRUE;
	}
	return bRet;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __is_rsa)(CK_IN NH_CERTIFICATE_HANDLER hCert)
{
	CK_BBOOL bRet = CK_FALSE;
	NH_ASN1_PNODE node;

	if ((node = hCert->hParser->sail(hCert->pubkey, NH_PARSE_SOUTH | 2))) bRet = (NH_oid_to_mechanism(node->value, node->valuelen) == CKM_RSA_PKCS_KEY_PAIR_GEN);
	return bRet;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_common_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	LPSTR pszNameString;
	CK_BBOOL bRet;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
			if (!__match_ulong(&pTemplate[i], pObject->hClass)) return CK_FALSE;
			break;
		case CKA_TOKEN:
			if (!__match_bool(&pTemplate[i], CK_TRUE)) return CK_FALSE;
			break;
		case CKA_PRIVATE:
			if (!__match_bool(&pTemplate[i], pObject->hClass == CKO_PRIVATE_KEY ? CK_TRUE : CK_FALSE)) return CK_FALSE;
			break;
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
			if (!__match_bool(&pTemplate[i], CK_FALSE)) return CK_FALSE;
			break;
		case CKA_LABEL:
			if ((bRet = __get_friedly_name(pWin->pCtx, 0, &pszNameString)))
			{
				bRet = strlen(pszNameString) == pTemplate[i].ulValueLen && memcmp(pszNameString, pTemplate[i].pValue, strlen(pszNameString) == pTemplate[i].ulValueLen) == 0;
				free(pszNameString);
			}
			if (!bRet) return CK_FALSE;
			break;
		}
	}
	return CK_TRUE;
}


/**
 * @brief Certificates match functions
 *
 */
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_cert_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	CK_BBOOL bRet;
	NH_ASN1_PNODE node;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CERTIFICATE_TYPE:
			if (!__match_ulong(&pTemplate[i], CKC_X_509)) return CK_FALSE;
			break;
		case CKA_TRUSTED:
			bRet = pWin->hCert->basic_constraints(pWin->hCert, &node) == NH_OK;
			if (bRet) bRet = __match_bool(&pTemplate[i], (node && ASN_IS_PRESENT(node->child) && *(CK_BBOOL*) node->child->value));
			if (!bRet) return (CK_FALSE);
			break;
		case CKA_CERTIFICATE_CATEGORY:
			bRet = pWin->hCert->basic_constraints(pWin->hCert, &node) == NH_OK;
			if (bRet) bRet = __match_ulong(&pTemplate[i], (node && ASN_IS_PRESENT(node->child) && *(CK_BBOOL*) node->child->value) ? CK_CERTIFICATE_CATEGORY_AUTHORITY : CK_CERTIFICATE_CATEGORY_TOKEN_USER);
			if (!bRet) return (CK_FALSE);
			break;
		case CKA_CHECK_VALUE:
			if (!__match_binary(pObject->hash.data, 3, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_START_DATE:
			if (!__match_date(&pWin->pCtx->pCertInfo->NotBefore, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_END_DATE:
			if (!__match_date(&pWin->pCtx->pCertInfo->NotAfter, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_PUBLIC_KEY_INFO:
			if (!__match_binary(pWin->hCert->pubkey->identifier, (CK_ULONG) (pWin->hCert->pubkey->size + pWin->hCert->pubkey->contents - pWin->hCert->pubkey->identifier), &pTemplate[i])) return CK_FALSE;
			break;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_x509cert_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_BBOOL bRet;
	NH_ASN1_PNODE pNode;
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_SUBJECT:
			
			if (!__match_binary(pWin->pCtx->pCertInfo->Subject.pbData, pWin->pCtx->pCertInfo->Subject.cbData, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_ID:
			bRet = CK_FALSE;
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if (pNode) bRet = __match_binary((unsigned char*) pNode->value, pNode->valuelen, &pTemplate[i]);
			if (!bRet) return CK_FALSE;
			break;
		case CKA_ISSUER:
			if (!__match_binary(pWin->pCtx->pCertInfo->Issuer.pbData, pWin->pCtx->pCertInfo->Issuer.cbData, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_SERIAL_NUMBER:
			if (!__match_binary(pWin->pCtx->pCertInfo->SerialNumber.pbData, pWin->pCtx->pCertInfo->SerialNumber.cbData, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_VALUE:
			if (!__match_binary(pWin->pCtx->pbCertEncoded, pWin->pCtx->cbCertEncoded, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
		case CKA_NAME_HASH_ALGORITHM:
			return CK_FALSE;
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
			if (!__match_ulong(&pTemplate[i], CK_SECURITY_DOMAIN_UNSPECIFIED)) return CK_FALSE;
			break;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_x509cert_default_attributes)(CK_IN CK_UNUSED KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	assert(pObject);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:
		case CKA_CERTIFICATE_TYPE:
		case CKA_TRUSTED:
		case CKA_CERTIFICATE_CATEGORY:
		case CKA_CHECK_VALUE:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_PUBLIC_KEY_INFO:
		case CKA_SUBJECT:
		case CKA_ID:
		case CKA_ISSUER:
		case CKA_SERIAL_NUMBER:
		case CKA_VALUE:
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
		case CKA_NAME_HASH_ALGORITHM:
			break;
		default: return CK_FALSE;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_certificates)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	return
	(
		(pObject->hClass == CKO_CERTIFICATE) &&
		__match_common_attributes(pObject, pTemplate, ulCount) &&
		__match_cert_attributes(pObject, pTemplate, ulCount) &&
		__match_x509cert_attributes(pObject, pTemplate, ulCount) &&
		__match_x509cert_default_attributes(pObject, pTemplate, ulCount) 
	);
}

/**
 * @brief Keys match functions
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_keys_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i, ulList, j, k;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_BBOOL bRet;
	NH_ASN1_PNODE pNode;
	CK_MECHANISM_TYPE_PTR pType;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_KEY_TYPE:
			if (!__match_ulong(&pTemplate[i], CKK_RSA)) return CK_FALSE;
			break;
		case CKA_ID:
			bRet = CK_FALSE;
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if (pNode) bRet = __match_binary((unsigned char*) pNode->value, pNode->valuelen, &pTemplate[i]);
			if (!bRet) return CK_FALSE;
			break;
		case CKA_START_DATE:
			if (!__match_date(&pWin->pCtx->pCertInfo->NotBefore, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_END_DATE:
			if (!__match_date(&pWin->pCtx->pCertInfo->NotAfter, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_DERIVE:
			if (!__match_bool(&pTemplate[i], CK_FALSE)) return CK_FALSE;
			break;
		case CKA_LOCAL:
			if (!__match_bool(&pTemplate[i], CK_FALSE)) return CK_FALSE;
			break;
		case CKA_KEY_GEN_MECHANISM:
			if (!__match_ulong(&pTemplate[i], CKM_RSA_PKCS_KEY_PAIR_GEN)) return CK_FALSE;
			break;
		case CKA_ALLOWED_MECHANISMS:
			
			if ((ulList = pTemplate[i].ulValueLen / sizeof(CK_MECHANISM_TYPE)) != KP_SIGN_MECHANISMS_COUNT) return CK_FALSE;
			for (j = 0, pType = (CK_MECHANISM_TYPE_PTR) pTemplate[i].pValue; j < ulList; j++)
			{
				bRet = CK_FALSE;
				k = 0;
				while (!bRet && k < ulList)
				{
					bRet = pType[j] == __pSignMechanisms[k];
					k++;
				}
				if (!bRet) return CK_FALSE;
			}
			break;
		}
	}
	return CK_TRUE;
}


/**
 * @brief Public keys match attributes
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_pubkeys_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_BBOOL bRet;
	NH_ASN1_PNODE node;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_SUBJECT:
			if (!__match_binary(pWin->pCtx->pCertInfo->Subject.pbData, pWin->pCtx->pCertInfo->Subject.cbData, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_ENCRYPT:
		case CKA_VERIFY:
		case CKA_VERIFY_RECOVER:
		case CKA_WRAP:
			if (!__match_bool(&pTemplate[i], CK_FALSE)) return CK_FALSE;
			break;
		case CKA_TRUSTED:
			bRet = pWin->hCert->basic_constraints(pWin->hCert, &node) == NH_OK;
			if (bRet) bRet = __match_bool(&pTemplate[i], (node && ASN_IS_PRESENT(node->child) && *(CK_BBOOL*) node->child->value));
			if (!bRet) return (CK_FALSE);
			break;
		case CKA_WRAP_TEMPLATE:
			return CK_FALSE;
		case CKA_PUBLIC_KEY_INFO:
			if (!__match_binary(pWin->hCert->pubkey->identifier, (CK_ULONG) (pWin->hCert->pubkey->size + pWin->hCert->pubkey->contents - pWin->hCert->pubkey->identifier), &pTemplate[i])) return CK_FALSE;
			break;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_rsa_pubkeys_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_BBOOL bRet;
	NH_ASN1_PNODE pNode;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_MODULUS:
			bRet = CK_FALSE;
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if (pNode) bRet = __match_binary((unsigned char*) pNode->value, pNode->valuelen, &pTemplate[i]);
			if (!bRet) return CK_FALSE;
			break;
		case CKA_PUBLIC_EXPONENT:
			bRet = CK_FALSE;
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if (pNode && (pNode = pNode->next)) bRet = __match_binary((unsigned char*) pNode->value, pNode->valuelen, &pTemplate[i]);
			if (!bRet) return CK_FALSE;
			break;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_rsa_pubkey_default_attributes)(CK_IN CK_UNUSED KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	assert(pObject);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:
		case CKA_KEY_TYPE:
		case CKA_ID:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_DERIVE:
		case CKA_LOCAL:
		case CKA_KEY_GEN_MECHANISM:
		case CKA_ALLOWED_MECHANISMS:
		case CKA_SUBJECT:
		case CKA_ENCRYPT:
		case CKA_VERIFY:
		case CKA_VERIFY_RECOVER:
		case CKA_WRAP:
		case CKA_TRUSTED:
		case CKA_WRAP_TEMPLATE:
		case CKA_PUBLIC_KEY_INFO:
		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
			break;
		default: return CK_FALSE;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_pubkeys)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	return
	(
		(pObject->hClass == CKO_PUBLIC_KEY) &&
		__match_common_attributes(pObject, pTemplate, ulCount) &&
		__match_keys_attributes(pObject, pTemplate, ulCount) &&
		__match_pubkeys_attributes(pObject, pTemplate, ulCount) &&
		__match_rsa_pubkeys_attributes(pObject, pTemplate, ulCount) &&
		__match_rsa_pubkey_default_attributes(pObject, pTemplate, ulCount)
	);
}


/**
 * @brief Private keys match attributes
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_privkeys_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_SUBJECT:
			if (!__match_binary(pWin->pCtx->pCertInfo->Subject.pbData, pWin->pCtx->pCertInfo->Subject.cbData, &pTemplate[i])) return CK_FALSE;
			break;
		case CKA_SENSITIVE:
		case CKA_SIGN:
			if (!__match_bool(&pTemplate[i], CK_TRUE)) return CK_FALSE;
			break;
		case CKA_DECRYPT:
		case CKA_SIGN_RECOVER:
		case CKA_UNWRAP:
		case CKA_EXTRACTABLE:
		case CKA_ALWAYS_SENSITIVE:
		case CKA_NEVER_EXTRACTABLE:
		case CKA_ALWAYS_AUTHENTICATE:
		case CKA_WRAP_WITH_TRUSTED:
			if (!__match_bool(&pTemplate[i], CK_FALSE)) return CK_FALSE;
			break;
		case CKA_UNWRAP_TEMPLATE:
			return CK_FALSE;
		case CKA_PUBLIC_KEY_INFO:
			if (!__match_binary(pWin->hCert->pubkey->identifier, (CK_ULONG) (pWin->hCert->pubkey->size + pWin->hCert->pubkey->contents - pWin->hCert->pubkey->identifier), &pTemplate[i])) return CK_FALSE;
			break;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_rsa_privkeys_attributes)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_BBOOL bRet;
	NH_ASN1_PNODE pNode;
	assert(pObject && pWin);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_MODULUS:
			bRet = CK_FALSE;
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if (pNode) bRet = __match_binary((unsigned char*) pNode->value, pNode->valuelen, &pTemplate[i]);
			if (!bRet) return CK_FALSE;
			break;
		case CKA_PUBLIC_EXPONENT:
			bRet = CK_FALSE;
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if (pNode && (pNode = pNode->next)) bRet = __match_binary((unsigned char*) pNode->value, pNode->valuelen, &pTemplate[i]);
			if (!bRet) return CK_FALSE;
			break;
		case CKA_PRIVATE_EXPONENT:
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			return CK_FALSE;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_rsa_privkey_default_attributes)(CK_IN CK_UNUSED KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	assert(pObject);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:
		case CKA_KEY_TYPE:
		case CKA_ID:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_DERIVE:
		case CKA_LOCAL:
		case CKA_KEY_GEN_MECHANISM:
		case CKA_ALLOWED_MECHANISMS:
		case CKA_SUBJECT:
		case CKA_SENSITIVE:
		case CKA_DECRYPT:
		case CKA_SIGN:
		case CKA_SIGN_RECOVER:
		case CKA_UNWRAP:
		case CKA_EXTRACTABLE:
		case CKA_ALWAYS_SENSITIVE:
		case CKA_NEVER_EXTRACTABLE:
		case CKA_WRAP_WITH_TRUSTED:
		case CKA_UNWRAP_TEMPLATE:
		case CKA_ALWAYS_AUTHENTICATE:
		case CKA_PUBLIC_KEY_INFO:
		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
		case CKA_PRIVATE_EXPONENT:
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			break;
		default: return CK_FALSE;
		}
	}
	return CK_TRUE;
}
CK_INLINE CK_IMPLEMENTATION(CK_BBOOL, __match_privkeys)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	return
	(
		(pObject->hClass == CKO_PRIVATE_KEY) &&
		__match_common_attributes(pObject, pTemplate, ulCount) &&
		__match_keys_attributes(pObject, pTemplate, ulCount) &&
		__match_privkeys_attributes(pObject, pTemplate, ulCount) &&
		__match_rsa_privkeys_attributes(pObject, pTemplate, ulCount) &&
		__match_rsa_privkey_default_attributes(pObject, pTemplate, ulCount)
	);
}


CK_IMPLEMENTATION(CK_BBOOL, __match_win_object)(CK_IN KOBJECT_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_IN KP_BLOB *pPin)
{
	KWIN_OBJECT pWin = (KWIN_OBJECT) self->hPersistence;
	assert(pPin && pWin);
	if (ulCount == 0) return CK_TRUE;
	if (__is_rsa(pWin->hCert))
	{
		switch (self->hClass)
		{
		case CKO_CERTIFICATE:	return __match_certificates(self, pTemplate, ulCount);
		case CKO_PUBLIC_KEY:	return __match_pubkeys(self, pTemplate, ulCount);
		case CKO_PRIVATE_KEY:
			if (!pPin->data) 	return CK_FALSE;
						return __match_privkeys(self, pTemplate, ulCount);
		}
	}
	return CK_FALSE;
}


/**
 * @brief Get attributes general funcions
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __attribute_date)(CK_INOUT CK_ATTRIBUTE_PTR pAttribute, CK_IN void *pValue)
{
	CK_RV rv = CKR_OK;
	char pDate[sizeof(CK_DATE) + 1];
	SYSTEMTIME pTime;

	assert(pAttribute && pValue);
	if (!pAttribute->pValue) pAttribute->ulValueLen = sizeof(CK_DATE);
	else
	{
		if (pAttribute->ulValueLen >= sizeof(CK_DATE))
		{
			if (FileTimeToSystemTime((PFILETIME) pValue, &pTime))
			{
				sprintf(pDate, "%.4d%.2d%.2d", pTime.wYear + 1600, pTime.wMonth, pTime.wDay);
				memcpy(pAttribute->pValue, pDate, sizeof(CK_DATE));
				pAttribute->ulValueLen = sizeof(CK_DATE);
			}
			else rv = CKR_FUNCTION_FAILED;
		}
		else { pAttribute->ulValueLen = CK_UNAVAILABLE_INFORMATION; rv = CKR_BUFFER_TOO_SMALL; }
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_common_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	LPSTR pszNameString;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
			rv = __attribute_ulong(&pTemplate[i], pObject->hClass);
			break;
		case CKA_TOKEN:
			rv = __attribute_bool(&pTemplate[i], CK_TRUE);
			break;
		case CKA_PRIVATE:
			rv = __attribute_bool(&pTemplate[i], (pObject->hClass == CKO_PRIVATE_KEY));
			break;
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
			rv = __attribute_bool(&pTemplate[i], CK_FALSE);
			break;
		case CKA_LABEL:
			if ((rv = __get_friedly_name(pWin->pCtx, 0, &pszNameString) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK)
			{
				rv = __attribute_binary(&pTemplate[i], pszNameString, strlen(pszNameString));
				free(pszNameString);
			}
			break;
		}
	}
	return rv;
}

/**
 * @brief Certificate get attributes funcions
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_cert_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	NH_ASN1_PNODE pNode;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CERTIFICATE_TYPE:
			rv = __attribute_ulong(&pTemplate[i], CKC_X_509);
			break;
		case CKA_TRUSTED:
			if
			(
				(rv = pWin->hCert->basic_constraints(pWin->hCert, &pNode) == NH_OK ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_bool(&pTemplate[i], (pNode && ASN_IS_PRESENT(pNode->child) && *(CK_BBOOL*) pNode->child->value));
			break;
		case CKA_CERTIFICATE_CATEGORY:
			if
			(
				(rv = pWin->hCert->basic_constraints(pWin->hCert, &pNode) == NH_OK ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_ulong(&pTemplate[i], (pNode && ASN_IS_PRESENT(pNode->child) && *(CK_BBOOL*) pNode->child->value) ? CK_CERTIFICATE_CATEGORY_AUTHORITY : CK_CERTIFICATE_CATEGORY_TOKEN_USER);
			break;
		case CKA_CHECK_VALUE:
			rv = __attribute_binary(&pTemplate[i], pObject->hash.data, 3);
			break;
		case CKA_START_DATE:
			rv = __attribute_date(&pTemplate[i], &pWin->pCtx->pCertInfo->NotBefore);
			break;
		case CKA_END_DATE:
			rv = __attribute_date(&pTemplate[i], &pWin->pCtx->pCertInfo->NotAfter);
			break;
		case CKA_PUBLIC_KEY_INFO:
			rv = __attribute_binary(&pTemplate[i], pWin->hCert->pubkey->identifier, (CK_ULONG) (pWin->hCert->pubkey->size + pWin->hCert->pubkey->contents - pWin->hCert->pubkey->identifier));
			break;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_x509cert_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	NH_ASN1_PNODE pNode;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_SUBJECT:
			rv = __attribute_binary(&pTemplate[i], pWin->pCtx->pCertInfo->Subject.pbData, pWin->pCtx->pCertInfo->Subject.cbData);
			break;
		case CKA_ID:
			if
			(
				(rv = (pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_binary(&pTemplate[i], (unsigned char*) pNode->value, pNode->valuelen);
			break;
		case CKA_ISSUER:
			rv = __attribute_binary(&pTemplate[i], pWin->pCtx->pCertInfo->Issuer.pbData, pWin->pCtx->pCertInfo->Issuer.cbData);
			break;
		case CKA_SERIAL_NUMBER:
			rv = __attribute_binary(&pTemplate[i], pWin->pCtx->pCertInfo->SerialNumber.pbData, pWin->pCtx->pCertInfo->SerialNumber.cbData);
			break;
		case CKA_VALUE:
			rv = __attribute_binary(&pTemplate[i], pWin->pCtx->pbCertEncoded, pWin->pCtx->cbCertEncoded);
			break;
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
		case CKA_NAME_HASH_ALGORITHM:
			return CKR_ATTRIBUTE_TYPE_INVALID;
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
			rv = __attribute_ulong(&pTemplate[i], CK_SECURITY_DOMAIN_UNSPECIFIED);
		break;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_x509cert_default_attributes)(CK_IN CK_UNUSED KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	assert(pObject);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:
		case CKA_CERTIFICATE_TYPE:
		case CKA_TRUSTED:
		case CKA_CERTIFICATE_CATEGORY:
		case CKA_CHECK_VALUE:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_PUBLIC_KEY_INFO:
		case CKA_SUBJECT:
		case CKA_ID:
		case CKA_ISSUER:
		case CKA_SERIAL_NUMBER:
		case CKA_VALUE:
		case CKA_HASH_OF_SUBJECT_PUBLIC_KEY:
		case CKA_HASH_OF_ISSUER_PUBLIC_KEY:
		case CKA_JAVA_MIDP_SECURITY_DOMAIN:
		case CKA_NAME_HASH_ALGORITHM:
			break;
		default: return CKR_ATTRIBUTE_TYPE_INVALID;
		}
	}
	return CKR_OK;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_certificates)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_RV rv;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	if
	(
		(rv = (pObject->hClass == CKO_CERTIFICATE) ? CKR_OK : CKR_OBJECT_HANDLE_INVALID) == CKR_OK &&
		(rv = __get_common_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_cert_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_x509cert_attributes(pObject, pTemplate, ulCount)) == CKR_OK
	)	rv = __get_x509cert_default_attributes(pObject, pTemplate, ulCount);
	return rv;
}

/**
 * @brief Key attributes functions
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_key_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	NH_ASN1_PNODE pNode;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_KEY_TYPE:
			rv = __attribute_ulong(&pTemplate[i], CKK_RSA);
			break;
		case CKA_ID:
			if
			(
				(rv = (pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_binary(&pTemplate[i], (unsigned char*) pNode->value, pNode->valuelen);
			break;
		case CKA_START_DATE:
			rv = __attribute_date(&pTemplate[i], &pWin->pCtx->pCertInfo->NotBefore);
			break;
		case CKA_END_DATE:
			rv = __attribute_date(&pTemplate[i], &pWin->pCtx->pCertInfo->NotAfter);
			break;
		case CKA_DERIVE:
			rv = __attribute_bool(&pTemplate[i], CK_FALSE);
			break;
		case CKA_LOCAL:
			rv = __attribute_bool(&pTemplate[i], CK_TRUE);
			break;
		case CKA_KEY_GEN_MECHANISM:
			rv = __attribute_ulong(&pTemplate[i], CKM_RSA_PKCS_KEY_PAIR_GEN);
			break;
		case CKA_ALLOWED_MECHANISMS:
			rv = __attribute_binary(&pTemplate[i], __pSignMechanisms, KP_SIGN_MECHANISMS_COUNT * sizeof(CK_MECHANISM_TYPE));
			break;
		}
	}
	return rv;
}


/**
 * @brief RSA publick keys attributes
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_pubkey_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_SUBJECT:
			rv = __attribute_binary(&pTemplate[i], pWin->pCtx->pCertInfo->Subject.pbData, pWin->pCtx->pCertInfo->Subject.cbData);
			break;
		case CKA_VERIFY_RECOVER:
		case CKA_WRAP:
		case CKA_ENCRYPT:
		case CKA_TRUSTED:
			rv = __attribute_bool(&pTemplate[i], CK_FALSE);
			break;
		case CKA_VERIFY:
			rv = __attribute_bool(&pTemplate[i], CK_TRUE);
			break;
		case CKA_WRAP_TEMPLATE:
			rv = CKR_ATTRIBUTE_TYPE_INVALID;
			break;
		case CKA_PUBLIC_KEY_INFO:
			rv = __attribute_binary(&pTemplate[i], pWin->hCert->pubkey->identifier, (CK_ULONG) (pWin->hCert->pubkey->size + pWin->hCert->pubkey->contents - pWin->hCert->pubkey->identifier));
			break;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_rsa_pubkey_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	NH_ASN1_PNODE pNode;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_MODULUS:
			if
			(
				(rv = (pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_binary(&pTemplate[i], (unsigned char*) pNode->value, pNode->valuelen);
			break;
		case CKA_PUBLIC_EXPONENT:
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if
			(
				(rv  = (pNode && (pNode = pNode->next)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_binary(&pTemplate[i], (unsigned char*) pNode->value, pNode->valuelen);
			break;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_rsa_pubkey_default_attributes)(CK_IN CK_UNUSED KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	assert(pObject);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:

		case CKA_KEY_TYPE:
		case CKA_ID:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_DERIVE:
		case CKA_LOCAL:
		case CKA_KEY_GEN_MECHANISM:
		case CKA_ALLOWED_MECHANISMS:

		case CKA_SUBJECT:
		case CKA_ENCRYPT:
		case CKA_VERIFY:
		case CKA_VERIFY_RECOVER:
		case CKA_WRAP:
		case CKA_TRUSTED:
		case CKA_WRAP_TEMPLATE:
		case CKA_PUBLIC_KEY_INFO:

		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
			break;
		default: return CKR_ATTRIBUTE_TYPE_INVALID;
		}
	}
	return CKR_OK;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_public_keys)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_RV rv;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	if
	(
		(rv = (pObject->hClass == CKO_PUBLIC_KEY) ? CKR_OK : CKR_OBJECT_HANDLE_INVALID) == CKR_OK &&
		(rv = __get_common_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_key_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_pubkey_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_rsa_pubkey_attributes(pObject, pTemplate, ulCount)) == CKR_OK
	)	rv = __get_rsa_pubkey_default_attributes(pObject, pTemplate, ulCount);
	return rv;
}

/**
 * @brief RSA private key attributes
 * 
 */
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_privkey_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_SUBJECT:
			rv = __attribute_binary(&pTemplate[i], pWin->pCtx->pCertInfo->Subject.pbData, pWin->pCtx->pCertInfo->Subject.cbData);
			break;
		case CKA_SENSITIVE:
		case CKA_SIGN:
			rv = __attribute_bool(&pTemplate[i], CK_TRUE);
			break;
		case CKA_DECRYPT:
		case CKA_SIGN_RECOVER:
		case CKA_UNWRAP:
		case CKA_EXTRACTABLE:
		case CKA_ALWAYS_SENSITIVE:
		case CKA_NEVER_EXTRACTABLE:
		case CKA_WRAP_WITH_TRUSTED:
		case CKA_ALWAYS_AUTHENTICATE:
			rv = __attribute_bool(&pTemplate[i], CK_FALSE);
			break;
		case CKA_UNWRAP_TEMPLATE:
			rv = CKR_ATTRIBUTE_TYPE_INVALID;
			break;
		case CKA_PUBLIC_KEY_INFO:
			rv = __attribute_binary(&pTemplate[i], pWin->hCert->pubkey->identifier, (CK_ULONG) (pWin->hCert->pubkey->size + pWin->hCert->pubkey->contents - pWin->hCert->pubkey->identifier));
			break;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_rsa_privkey_attributes)(CK_IN KOBJECT_STR *pObject, CK_INOUT CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	CK_RV rv = CKR_OK;
	NH_ASN1_PNODE pNode;
	assert(pObject && pWin && pTemplate);
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{
		case CKA_MODULUS:
			if
			(
				(rv = (pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_binary(&pTemplate[i], (unsigned char*) pNode->value, pNode->valuelen);
			break;
		case CKA_PUBLIC_EXPONENT:
			pNode = RSA_PUBKEY_GET_MODULUS(pWin->hCert);
			if
			(
				(rv = (pNode && (pNode = pNode->next)) ? CKR_OK : CKR_FUNCTION_FAILED) == CKR_OK
			)	rv = __attribute_binary(&pTemplate[i], (unsigned char*) pNode->value, pNode->valuelen);
			break;
		case CKA_PRIVATE_EXPONENT:
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			rv = CKR_ATTRIBUTE_SENSITIVE;
			break;
		}
	}
	return rv;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_rsa_privkey_default_attributes)(CK_IN CK_UNUSED KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_ULONG i;
	for (i = 0; i < ulCount; i++)
	{
		switch (pTemplate[i].type)
		{

		case CKA_CLASS:
		case CKA_TOKEN:
		case CKA_PRIVATE:
		case CKA_MODIFIABLE:
		case CKA_COPYABLE:
		case CKA_DESTROYABLE:
		case CKA_LABEL:

		case CKA_KEY_TYPE:
		case CKA_ID:
		case CKA_START_DATE:
		case CKA_END_DATE:
		case CKA_DERIVE:
		case CKA_LOCAL:
		case CKA_KEY_GEN_MECHANISM:
		case CKA_ALLOWED_MECHANISMS:

		case CKA_SUBJECT:
		case CKA_SENSITIVE:
		case CKA_DECRYPT:
		case CKA_SIGN:
		case CKA_SIGN_RECOVER:
		case CKA_UNWRAP:
		case CKA_EXTRACTABLE:
		case CKA_ALWAYS_SENSITIVE:
		case CKA_NEVER_EXTRACTABLE:
		case CKA_WRAP_WITH_TRUSTED:
		case CKA_UNWRAP_TEMPLATE:
		case CKA_ALWAYS_AUTHENTICATE:
		case CKA_PUBLIC_KEY_INFO:

		case CKA_MODULUS:
		case CKA_PUBLIC_EXPONENT:
		case CKA_PRIVATE_EXPONENT:
		case CKA_PRIME_1:
		case CKA_PRIME_2:
		case CKA_EXPONENT_1:
		case CKA_EXPONENT_2:
		case CKA_COEFFICIENT:
			break;
		default: return CKR_ATTRIBUTE_TYPE_INVALID;
		}
	}
	return CKR_OK;
}
CK_INLINE CK_IMPLEMENTATION(CK_RV, __get_private_keys)(CK_IN KOBJECT_STR *pObject, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount)
{
	CK_RV rv;
	KWIN_OBJECT pWin = (KWIN_OBJECT) pObject->hPersistence;
	assert(pObject && pWin);
	if
	(
		(rv = (pObject->hClass == CKO_PRIVATE_KEY) ? CKR_OK : CKR_OBJECT_HANDLE_INVALID) == CKR_OK &&
		(rv = __get_common_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_key_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_privkey_attributes(pObject, pTemplate, ulCount)) == CKR_OK &&
		(rv = __get_rsa_privkey_attributes(pObject, pTemplate, ulCount)) == CKR_OK
	)	rv = __get_rsa_privkey_default_attributes(pObject, pTemplate, ulCount);
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __get_win_object)(CK_INOUT KOBJECT_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_IN KP_BLOB *pPin)
{
	CK_RV rv = CKR_OBJECT_HANDLE_INVALID;
	KWIN_OBJECT pWin = (KWIN_OBJECT) self->hPersistence;
	assert(pTemplate && pPin && pWin);
	if (__is_rsa(pWin->hCert))
	{
		switch (self->hClass)
		{
		case CKO_CERTIFICATE:
			rv = __get_certificates(self, pTemplate, ulCount);
			break;
		case CKO_PUBLIC_KEY:
			rv = __get_public_keys(self, pTemplate, ulCount);
			break;
		case CKO_PRIVATE_KEY:;
			rv = __get_private_keys(self, pTemplate, ulCount);
			break;
		}
	}
	return rv;
}
CK_IMPLEMENTATION(CK_RV, __create_win_object)(CK_INOUT KOBJECT_STR *self, CK_IN CK_ATTRIBUTE_PTR pTemplate, CK_IN CK_ULONG ulCount, CK_IN KTOKEN_STR *pToken)
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}

static KOBJECT_STR __default_win_object =
{
	0UL,
	0UL,
	{ NULL, 0UL },
	NULL,

	__match_win_object,
	__get_win_object,
	__create_win_object
};
CK_NEW(CKO_wnew_object)(CK_IN PCCERT_CONTEXT pCtx, CK_IN CK_OBJECT_CLASS hClass, CK_OUT KOBJECT *hOut)
{
	CK_RV rv;
	KOBJECT pObject;
	NH_HASH_HANDLER hHash;
	KWIN_OBJECT pWin;
	assert(pCtx);

	if ((rv = (pObject = (KOBJECT) malloc(sizeof(KOBJECT_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
	{
		memcpy(pObject, &__default_win_object, sizeof(KOBJECT_STR));
		pObject->hClass = hClass;
		if ((rv = NH_new_hash(&hHash)) == CKR_OK)
		{
			if
			(
				(rv = hHash->init(hHash, CKM_SHA_1)) == CKR_OK &&
				(rv = hHash->update(hHash, pCtx->pbCertEncoded, pCtx->cbCertEncoded)) == CKR_OK &&
				(rv = hHash->update(hHash, (unsigned char*) &pObject->hClass, sizeof(CK_OBJECT_CLASS))) == CKR_OK &&
				(rv = hHash->finish(hHash, NULL, &pObject->hash.length)) == CKR_OK &&
				(rv = (pObject->hash.data = (unsigned char*) malloc(pObject->hash.length)) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK &&
				(rv = hHash->finish(hHash, pObject->hash.data, &pObject->hash.length)) == CKR_OK
			)
			{
				if ((rv = (pWin = (KWIN_OBJECT) malloc(sizeof(KWIN_OBJECT_STR))) ? CKR_OK : CKR_HOST_MEMORY) == CKR_OK)
				{
					memset(pWin, 0, sizeof(KWIN_OBJECT_STR));
					if ((rv = NH_parse_certificate(pCtx->pbCertEncoded, pCtx->cbCertEncoded, &pWin->hCert)) == CKR_OK)
					{
						pWin->pCtx = CertDuplicateCertificateContext(pCtx);
						pObject->hPersistence = pWin;
						*hOut = pObject;
					}
				}
			}
			NH_release_hash(hHash);
		}
		if (rv != CKR_OK) CKO_wdelete_object(pObject);
	}
	return rv;
}
CK_DELETE(CKO_wdelete_object)(CK_INOUT KOBJECT hHandler)
{
	KWIN_OBJECT pWin;
	if (hHandler)
	{
		if (hHandler->hash.data) free(hHandler->hash.data);
		pWin = (KWIN_OBJECT) hHandler->hPersistence;
		if (pWin)
		{
			if (pWin->hCert) NH_release_certificate(pWin->hCert);
			if (pWin->pCtx) CertFreeCertificateContext(pWin->pCtx);
		}
		free(hHandler);
	}
}


#endif